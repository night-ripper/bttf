#include <cstdint>
#include <cfloat>

#include <iostream>
#include <algorithm>
#include <set>
#include <thread>

#include <nwd/geometry/gridtree.hpp>

#include <testing/testing.hpp>

using namespace nwd;
using namespace nwd::geometry;

std::initializer_list<testing::Test_case> testing::test_cases
{
Test_case("basic", []
{
	using Gd = Gridtree<int>;
	auto gd = Gd();
	auto value = int(-1);
	auto box = Bounding_box3D(Vector3D(coord_t(0.), coord_t(0.), coord_t(0.)), Vector3D(coord_t(1.), coord_t(1.), coord_t(1.)));
	auto h = gd.insert(value, box);
	assert_eq(box, gd.box_of(h));
	
	int count = 0;
	for (auto i : gd.query([](auto b) noexcept -> bool {return overlap(b, Vector3D(coord_t(0.5), coord_t(0.5), coord_t(0.5)));}) | std::views::keys)
	{
		++count;
		assert_eq(value, i);
	}
	assert_eq(1, count);
	
	for ([[maybe_unused]] auto i : gd.query([](auto b) noexcept -> bool {return overlap(b, Vector3D(coord_t(1.5), coord_t(0.5), coord_t(0.5)));}) | std::views::keys)
	{
		assert_true(false);
	}
}),
};
