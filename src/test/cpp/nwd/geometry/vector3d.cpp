#include <iostream>

#include <nwd/geometry/vector3d.hpp>

#include <testing/testing.hpp>

using namespace nwd::geometry;

std::initializer_list<testing::Test_case> testing::test_cases
{
Test_case("basic", []
{
	auto v = Vector3D();
	auto v1 = Vector3D(coord_t(1.f), coord_t(1.f), coord_t(1.f));
	assert_eq(v1, v + v1);
	
	auto v2 = v1 * 2;
	assert_true(coord_t(2.f) == v2[0]);
	assert_true(coord_t(2.f) == v2[1]);
	assert_true(coord_t(2.f) == v2[2]);
	
	v2 /= 2;
	assert_eq(v1, v2);
	
	assert_true(coord_t(-1.f) == -v1[0]);
	assert_true(coord_t(-1.f) == -v1[1]);
	assert_true(coord_t(-1.f) == -v1[2]);
	
	assert_eq(7.f, dot({coord_t(2.f), coord_t(-3.f), coord_t(4.f)}, {coord_t(-2.f), coord_t(3.f), coord_t(5.f)}));
	assert_eq(Vector3D(coord_t(-27.f), coord_t(-18.f), coord_t(0.f)), cross({coord_t(2.f), coord_t(-3.f), coord_t(4.f)}, {coord_t(-2.f), coord_t(3.f), coord_t(5.f)}));
}),
};
