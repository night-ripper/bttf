#include <cstdint>
#include <cfloat>

#include <iostream>
#include <limits>
#include <bitset>

#include <nwd/geometry/unit_vector3d.hpp>

#include <testing/testing.hpp>

using namespace nwd::geometry;

std::initializer_list<testing::Test_case> testing::test_cases
{
Test_case("basic", []
{
	auto u = Unit_vector3D(1, -1, 1);
	std::cout << u.x() << "\n";
	std::cout << u.y() << "\n";
	std::cout << u.z() << "\n";
	
	std::cout << std::bitset<64>(u.bitfield_) << "\n";
}),
};
