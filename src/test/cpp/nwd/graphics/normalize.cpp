#include <cstdint>
#include <cfloat>

#include <limits>

#include <nwd/graphics/opengl/normalize.hpp>

#include <testing/testing.hpp>

using namespace nwd::graphics::opengl;

std::initializer_list<testing::Test_case> testing::test_cases
{
#if 0
Test_case("float_to_uint8", []
{
	using type = std::uint8_t;
	
	assert_true(type(nrm(0.f)) == type(0));
	assert_true(type(nrm(1.f)) == std::numeric_limits<type>::max());
	assert_true(type(nrm(0.5f)) == std::numeric_limits<type>::max() / 2);
	
	assert_true(type(nrm(0.f)) == type(0));
	assert_true(type(nrm(0.f)) <= type(nrm(0.001f)));
	assert_true(type(nrm(0.999f)) < type(nrm(1.f)));
	assert_true(type(nrm(0.001f)) < type(nrm(0.5f)));
	assert_true(type(nrm(0.5f)) < type(nrm(0.999f)));
	assert_true(type(nrm(0.001f)) < type(nrm(0.999f)));
	assert_true(type(nrm(0.f)) < type(nrm(1.f)));
}),

Test_case("float_to_int8", []
{
	using type = std::int8_t;
	
	assert_true(type(nrm(0.f)) == type(0));
	assert_true(type(nrm(1.f)) == std::numeric_limits<type>::max());
	assert_true(type(nrm(-1.f)) == std::numeric_limits<type>::min() + 1);
	assert_true(type(nrm(0.5f)) == std::numeric_limits<type>::max() / 2);
	
	assert_true(type(nrm(0.f)) == type(0));
	assert_true(type(nrm(0.f)) <= type(nrm(0.001f)));
	assert_true(type(nrm(0.999f)) < type(nrm(1.f)));
	assert_true(type(nrm(0.001f)) < type(nrm(0.5f)));
	assert_true(type(nrm(0.5f)) < type(nrm(0.999f)));
	assert_true(type(nrm(0.001f)) < type(nrm(0.999f)));
	assert_true(type(nrm(0.f)) < type(nrm(1.f)));
}),

Test_case("float_to_uint16", []
{
	using type = std::uint16_t;
	
	assert_true(type(nrm(0.f)) == type(0));
	assert_true(type(nrm(1.f)) == std::numeric_limits<type>::max());
	assert_true(type(nrm(0.5f)) == std::numeric_limits<type>::max() / 2);
	
	assert_true(type(nrm(0.f)) == type(0));
	assert_true(type(nrm(0.f)) < type(nrm(0.001f)));
	assert_true(type(nrm(0.999f)) < type(nrm(1.f)));
	assert_true(type(nrm(0.001f)) < type(nrm(0.5f)));
	assert_true(type(nrm(0.5f)) < type(nrm(0.999f)));
	assert_true(type(nrm(0.001f)) < type(nrm(0.999f)));
	assert_true(type(nrm(0.f)) < type(nrm(1.f)));
}),

Test_case("float_to_int16", []
{
	using type = std::int16_t;
	
	assert_true(type(nrm(0.f)) == type(0));
	assert_true(type(nrm(1.f)) == std::numeric_limits<type>::max());
	assert_true(type(nrm(-1.f)) == std::numeric_limits<type>::min() + 1);
	assert_true(type(nrm(0.5f)) == std::numeric_limits<type>::max() / 2);
	
	assert_true(type(nrm(0.f)) == type(0));
	assert_true(type(nrm(0.f)) < type(nrm(0.001f)));
	assert_true(type(nrm(0.999f)) < type(nrm(1.f)));
	assert_true(type(nrm(0.001f)) < type(nrm(0.5f)));
	assert_true(type(nrm(0.5f)) < type(nrm(0.999f)));
	assert_true(type(nrm(0.001f)) < type(nrm(0.999f)));
	assert_true(type(nrm(0.f)) < type(nrm(1.f)));
}),

Test_case("float_to_uint32", []
{
	using type = std::uint32_t;
	
	assert_true(type(nrm(0.f)) == type(0));
	assert_true(type(nrm(0.f)) < type(nrm(0.001f)));
	assert_true(type(nrm(0.999f)) < type(nrm(1.f)));
	assert_true(type(nrm(0.001f)) < type(nrm(0.5f)));
	assert_true(type(nrm(0.5f)) < type(nrm(0.999f)));
	assert_true(type(nrm(0.001f)) < type(nrm(0.999f)));
	assert_true(type(nrm(0.f)) < type(nrm(1.f)));
}),

Test_case("float_to_int32", []
{
	using type = std::int32_t;
	
	assert_true(type(nrm(0.f)) == type(0));
	assert_true(type(nrm(0.f)) < type(nrm(0.001f)));
	assert_true(type(nrm(0.999f)) < type(nrm(1.f)));
	assert_true(type(nrm(0.001f)) < type(nrm(0.5f)));
	assert_true(type(nrm(0.5f)) < type(nrm(0.999f)));
	assert_true(type(nrm(0.001f)) < type(nrm(0.999f)));
	assert_true(type(nrm(0.f)) < type(nrm(1.f)));
}),

Test_case("double_to_uint32", []
{
	using type = std::uint32_t;
	
	assert_true(type(nrm(0.)) == type(0));
	assert_true(type(nrm(1.)) == std::numeric_limits<type>::max());
	assert_true(type(nrm(0.5)) == std::numeric_limits<type>::max() / 2);
	
	assert_true(type(nrm(0.)) == type(0));
	assert_true(type(nrm(0.)) < type(nrm(0.001)));
	assert_true(type(nrm(0.999)) < type(nrm(1.)));
	assert_true(type(nrm(0.001)) < type(nrm(0.5)));
	assert_true(type(nrm(0.5)) < type(nrm(0.999)));
	assert_true(type(nrm(0.001)) < type(nrm(0.999)));
	assert_true(type(nrm(0.)) < type(nrm(1.)));
}),

Test_case("double_to_int32", []
{
	using type = std::int32_t;
	
	assert_true(type(nrm(0.)) == type(0));
	assert_true(type(nrm(1.)) == std::numeric_limits<type>::max());
	assert_true(type(nrm(-1.)) == std::numeric_limits<type>::min() + 1);
	assert_true(type(nrm(0.5)) == std::numeric_limits<type>::max() / 2);
	
	assert_true(type(nrm(0.)) == type(0));
	assert_true(type(nrm(0.)) < type(nrm(0.001)));
	assert_true(type(nrm(0.999)) < type(nrm(1.)));
	assert_true(type(nrm(0.001)) < type(nrm(0.5)));
	assert_true(type(nrm(0.5)) < type(nrm(0.999)));
	assert_true(type(nrm(0.001)) < type(nrm(0.999)));
	assert_true(type(nrm(0.)) < type(nrm(1.)));
}),
#endif
};
