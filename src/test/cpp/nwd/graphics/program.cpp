#include <fstream>
#include <string>
#include <fstream>
#include <filesystem>

#include <nwd/graphics/opengl/buffers.hpp>
#include <nwd/graphics/opengl/program.hpp>
#include <nwd/graphics/glfw.hpp>

#include <testing/testing.hpp>

using namespace nwd::graphics;
using namespace nwd::graphics::opengl;

std::initializer_list<testing::Test_case> testing::test_cases
{
Test_case("construction", []
{
	auto lib = glfw::Library();
	lib.window_hint(glfw::Context::Hint::visible(false));
	auto context = lib.create_context(1, 1, "test");
	context.make_current();
	
	auto vs = Vertex_shader(std::ifstream("src/main/glsl/main.vert.glsl"), std::ifstream("src/main/glsl/shared.glsl"));
	auto fs = Fragment_shader(std::ifstream("src/main/glsl/main.frag.glsl"), std::ifstream("src/main/glsl/shared.glsl"));
	auto program = Program(vs, fs);
}),
};
