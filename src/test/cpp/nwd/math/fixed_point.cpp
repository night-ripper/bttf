#include <iostream>
#include <sstream>

#include <nwd/math/fixed_point.hpp>

#include <testing/testing.hpp>

using namespace nwd::math;

std::initializer_list<testing::Test_case> testing::test_cases
{
Test_case("fraction", []
{
	using fraction_t = Fixed_point<std::int16_t, 15>;
	
	static_assert(std::is_nothrow_copy_constructible_v<fraction_t>);
	static_assert(std::is_trivially_destructible_v<fraction_t>);
	static_assert(std::is_trivial_v<fraction_t>);
	
	auto assert_similar = Assert_floating_similar(float(fraction_t::epsilon()));
	
	assert_eq(fraction_t::max().value(), fraction_t(1.f).value());
	assert_eq(0, fraction_t(0.f).value());
	
	assert_eq(1.f, float(fraction_t(1.f)));
	assert_eq(0.f, float(fraction_t(0.f)));
	assert_eq(-1.f, float(fraction_t(-1.f)));
	
	assert_similar(0.5f, float(fraction_t(0.5f)));
	assert_similar(0.25f, float(fraction_t(0.5f) * fraction_t(0.5f)));
	assert_similar(0.25f, float(fraction_t(0.5f) * 0.5f));
	
	using fixed_t = Fixed_point<std::int32_t, 15>;
	assert_similar(0.25f, float(fraction_t(0.5f) * fixed_t(0.5f)));
}),

Test_case("fixed", []
{
	using fixed_t = Fixed_point<std::int32_t, 14>;
	auto assert_similar = Assert_floating_similar(float(fixed_t::epsilon()));
	
	assert_eq(fixed_t::underlying_type(1) << (fixed_t::point_position + 1), fixed_t(2.f).value());
	assert_eq(fixed_t::underlying_type(1) << fixed_t::point_position, fixed_t(1.f).value());
	assert_eq(0, fixed_t(0.f).value());
	assert_eq(-1 * (fixed_t::underlying_type(1) << fixed_t::point_position), fixed_t(-1.f).value());
	assert_eq(-1 * (fixed_t::underlying_type(1) << (fixed_t::point_position + 1)), fixed_t(-2.f).value());
	
	assert_similar(0, float(fixed_t(0.f)));
	assert_similar(1, float(fixed_t(1.f)));
	assert_similar(100, float(fixed_t(100.f)));
	assert_similar(-100, float(fixed_t(-100.f)));
}),

Test_case("comparison", []
{
	using fixed_t = Fixed_point<std::int32_t, 14>;
	
	assert_true(fixed_t(0.5) == fixed_t(0.5));
	assert_false(fixed_t(0.5) != fixed_t(0.5));
	assert_true(fixed_t(0.5) > fixed_t(0.1));
	assert_true(fixed_t(0.5) < fixed_t(0.7));
}),

Test_case("printing", []
{
	using ufraction_t = Fixed_point<std::uint64_t, 64>;
	using fraction_t = Fixed_point<std::int64_t, 63>;
	using fixed_t = Fixed_point<std::int32_t, 14>;
	
	assert_eq("0.1111111111111111110689476932922303831219323910772800445556640625",
		(std::stringstream() << ufraction_t(0.111111111111111111111L)).view()
	);
	assert_eq("0.111111111111111111014737584667955161421559751033782958984375000",
		(std::stringstream() << fraction_t(0.111111111111111111111L)).view()
	);
	assert_eq("0.11108398437500",
		(std::stringstream() << fixed_t(0.111111111111111111111L)).view()
	);
	assert_eq("-0.111111111111111111014737584667955161421559751033782958984375000",
		(std::stringstream() << fraction_t(-0.111111111111111111111L)).view()
	);
	assert_eq("-0.11108398437500",
		(std::stringstream() << fixed_t(-0.111111111111111111111L)).view()
	);
}),
};
