#include <type_traits>

#include <iostream>
#include <functional>

#include <nwd/utility/executor.hpp>

#include <testing/testing.hpp>

std::initializer_list<testing::Test_case> testing::test_cases
{
Test_case("simple", []
{
	auto exec = nwd::utility::Executor(std::chrono::milliseconds(1));
	
	auto count = std::atomic<int>();
	
	for (int i = 0; i != 200; ++i)
	{
		exec.submit(decltype(exec)::duration(i), [&count](auto&&...) noexcept -> void
		{
			count.fetch_add(1, std::memory_order::acq_rel);
		});
	}
	
	std::this_thread::sleep_for(std::chrono::milliseconds(200));
	assert_eq(200, count.load(std::memory_order::acquire));
}),
};
