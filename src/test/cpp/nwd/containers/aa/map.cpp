#include <iostream>
#include <fstream>
#include <memory>
#include <random>

#include <nwd/containers/aa/map.hpp>

#include <testing/testing.hpp>

using namespace nwd::containers;

using Imap = aa::Map<int, int>;

static_assert(std::assignable_from<decltype(std::declval<Imap>().begin()->second), int>);
static_assert(not std::assignable_from<decltype(std::declval<const Imap>().begin()->second), int>);

std::initializer_list<testing::Test_case> testing::test_cases
{
Test_case("const", []
{
	auto map = Imap();
	map.try_emplace(0, 0);
	auto cit = std::as_const(map).begin();
	assert_eq(0, cit->first);
	assert_eq(0, cit->second);
	map.begin()->second = 1;
	assert_eq(1, map.find(0)->second);
}),
};
