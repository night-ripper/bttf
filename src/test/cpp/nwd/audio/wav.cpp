#include <iostream>
#include <string>
#include <vector>

#include <nwd/audio/wav.hpp>

#include <testing/testing.hpp>

using namespace nwd::audio;

std::initializer_list<testing::Test_case> testing::test_cases
{
Test_case("creation", []
{
	wav::create_stream(Format::mono_8, "src/test/resources/audio/synth_mono_8.wav");
	wav::create_stream(Format::stereo_8, "src/test/resources/audio/synth_stereo_8.wav");
}),

Test_case("read_data", []
{
	for (Format format : {Format::mono_8, Format::mono_16, Format::stereo_8, Format::stereo_16})
	{
		for (std::string name : {"mono", "stereo"})
		{
			for (int source_bit_depth : {8, 16, 24, 32})
			{
				auto stream = wav::create_stream(format, "src/test/resources/audio/synth_" + name + "_" + std::to_string(source_bit_depth) + ".wav");
				std::ptrdiff_t expected_size = 23552 * format.byte_depth() * format.channels();
				assert_eq(expected_size, stream->in_avail());
				
				auto data = std::vector<char>();
				std::copy(std::istreambuf_iterator(stream.get()), {}, std::back_inserter(data));
				assert_eq(expected_size, std::ssize(data));
				assert_eq(-1, stream->in_avail());
				
				stream->pubseekoff(0, std::ios_base::beg);
				assert_eq(expected_size, stream->in_avail());
				
				{
					std::ptrdiff_t size = expected_size;
					
					while (stream->sbumpc() != Streaming_sound::traits_type::eof())
					{
						--size;
					}
					
					assert_eq(0, size);
					assert_eq(-1, stream->in_avail());
				}
			}
		}
	}
}),
};
