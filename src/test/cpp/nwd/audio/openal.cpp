#include <iostream>
#include <string>
#include <vector>

#include <nwd/audio/openal.hpp>

#include <testing/testing.hpp>

using namespace nwd::audio;

std::initializer_list<testing::Test_case> testing::test_cases
{
Test_case("devices", []
{
	for ([[maybe_unused]] auto name : openal::Device::names())
	{
	}
	
	for ([[maybe_unused]] auto name : openal::Capture_device::names())
	{
	}
}),
};
