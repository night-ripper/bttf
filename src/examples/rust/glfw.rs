extern "C" fn error_callback(_error_code: std::ffi::c_int, description: *const std::ffi::c_char)
{
	panic!("GLFW error: {}", unsafe {std::ffi::CStr::from_ptr(description).to_str().unwrap_or("<GLFW error could not be decoded...>")});
}

fn main()
{
	/*
	glfw::set_error_callback(Some(error_callback));
	let glfw = glfw::init();
	
	glfw::Window::hint(glfw::constants::GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfw::Window::hint(glfw::constants::GLFW_CONTEXT_VERSION_MINOR, 5);
	glfw::Window::hint(glfw::constants::GLFW_OPENGL_FORWARD_COMPAT, 1);
	glfw::Window::hint(glfw::constants::GLFW_OPENGL_DEBUG_CONTEXT, 1);
	
	let mut context = glfw::Window::create(800, 450, "New World Disorder\0");
	
	context.set_cursor(Some(glfw::Cursor::create(
		&graphics::png::read_top_down(&mut std::fs::File::open("src/main/resources/cursors/mouse.png").unwrap()), 0, 0
	)));
	
	{
		let mut icons = Vec::<graphics::image::Image>::new();
		
		for entry in walkdir::WalkDir::new("src/main/resources/icons")
		{
			if let Ok(entry) = entry
			{
				if entry.path().is_file()
				{
					if let Some(extension) = entry.path().extension()
					{
						if extension == "png"
						{
							icons.push(graphics::png::read_top_down(&mut std::fs::File::open(entry.path()).unwrap()));
						}
					}
				}
			}
		}
		
		context.set_icons(icons.as_slice());
	}
	
	context.make_current();
	
	unsafe
	{
		opengl::glEnable(opengl::constants::GL_DEBUG_OUTPUT_SYNCHRONOUS);
		opengl::glDebugMessageCallback(Some(graphics::opengl::opengl_debug_callback), std::ptr::null());
		opengl::glEnable(opengl::constants::GL_CULL_FACE);
		opengl::glEnable(opengl::constants::GL_PRIMITIVE_RESTART_FIXED_INDEX);
		opengl::glDepthFunc(opengl::constants::GL_LEQUAL);
	}
	
	let program = opengl::program::Program::create();
	
	program.link([
		opengl::program::Shader::create(opengl::program::Shader_type::vertex)
		.compile([read_file_binary("src/main/glsl/bindless.vert.glsl").unwrap()].iter().map(Vec::as_slice)).unwrap(),
		opengl::program::Shader::create(opengl::program::Shader_type::fragment)
		.compile([read_file_binary("src/main/glsl/bindless.frag.glsl").unwrap()].iter().map(Vec::as_slice)).unwrap(),
	].iter().map(|&s| s)).unwrap();
	
	while ! context.should_close()
	{
		context.swap_buffers();
		glfw.poll_events();
	}
	*/
}
