use nwd::graphics::freetype;

fn main() -> std::io::Result<()>
{
	let mut library = freetype::Library::new();
	let mut fc = library.load_face(&std::ffi::CStr::from_bytes_until_nul(
		b"/usr/share/fonts/dejavu-sans-fonts/DejaVuSansCondensed-Bold.ttf\0").unwrap()
	);
	fc.set_pixel_sizes(80, 50);
	// let glyph = fc.load_char('₴').unwrap();
	let glyph = fc.load_char('я');
	
	println!("{}", glyph.height);
	println!("{}", glyph.width);
	println!("{}", glyph.bearing_y);
	println!("{}", glyph.advance_x_64);
	
	for i in 0 .. glyph.height
	{
		for j in 0 .. glyph.width
		{
			let val = glyph.bitmap[i as usize * glyph.width as usize + j as usize];
			if val > 128
			{
				print!("#");
			}
			else if val > 0
			{
				print!("+");
			}
			else
			{
				print!(" ");
			}
		}
		println!();
	}
	
	Ok(())
}
