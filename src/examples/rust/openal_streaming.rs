use nwd::audio;
use std::io::Write;

fn main()
{
	let device = audio::openal::Device::default();
	
	print!("Device: ");
	std::io::stdout().lock().write(device.name().to_bytes()).unwrap();
	println!();
	
	let mut context = audio::openal::Context::new(&device);
	
	// audio::openal::listener_orientation([1.0, 0.0, 0.0], [0.0, 0.0, 1.0]);
	// audio::openal::listener_position([0.0, 70.0, 0.0]);
	
	let mut stream = context.new_stream(audio::from_file(audio::Format::mono_8,
		std::path::Path::new("src/test/resources/audio/synth_stereo_16.wav")
	).unwrap());
	
	stream.lock().unwrap().source.air_absorption_factor(7.0);
	// stream.lock().unwrap().source.room_rolloff_factor(10.0);
	
	stream.play();
	
	while ! stream.is_finished()
	{
		std::thread::sleep(std::time::Duration::from_millis(50));
	}
}
