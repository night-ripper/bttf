fn main() -> std::io::Result<()>
{
	let connection = rusqlite::Connection::open_in_memory().unwrap();
	connection.execute(include_str!("../../main/sql/logins.sql"), ()).unwrap();
	connection.execute("insert into logins values(?, ?, ?)",
		(&"user", &[1_u8; 32], &[2_u8; 32]),
	).unwrap();
	
	let mut statement = connection.prepare("select * from logins where username = ?").unwrap();
	let mut rows = statement.query(["user"]).unwrap();
	
	while let Some(row) = rows.next().unwrap()
	{
		println!("{}", row.get::<_, String>("username").unwrap());
		println!("{}", nwd::utility::hex::Encoder::new(row.get::<_, [u8; 32]>("password").unwrap().iter().copied()));
	}
	
	Ok(())
}
