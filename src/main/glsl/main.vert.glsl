#version 450

layout(location = 0) in ivec3 position;
layout(location = 1) in vec2 texture_coordinate;
layout(location = 2) in ivec2 texture_array_coordinate;

layout(location = 3) in vec3 relative_position;
layout(location = 4) in vec3 scale;
layout(location = 5) in vec4 quaternion;

layout(location = 6) in mediump vec3 normal;
layout(location = 7) in mediump vec4 color;
layout(location = 8) in mediump vec3 ambient_color;

layout(location = 0) out vec2 vs_texture_coordinate;
layout(location = 1) out vec3 vs_normal;
layout(location = 2) out ivec2 vs_texture_array_coordinate;
layout(location = 3) out vec4 vs_color;
layout(location = 4) out vec3 vs_position;
layout(location = 5) out vec3 vs_ambient_color;

layout(location = 0) uniform float multiplier;
layout(location = 1) uniform ivec3 camera_position;
layout(location = 2) uniform mat4 projection_matrix;

vec3 float_position(ivec3 position);

vec4 q_mul(vec4 ql, vec4 qr)
{
	return vec4(
		ql[3] * qr[0] + ql[0] * qr[3] + ql[1] * qr[2] - ql[2] * qr[1],
		ql[3] * qr[1] + ql[1] * qr[3] + ql[2] * qr[0] - ql[0] * qr[2],
		ql[3] * qr[2] + ql[2] * qr[3] + ql[0] * qr[1] - ql[1] * qr[0],
		ql[3] * qr[3] - ql[0] * qr[0] - ql[1] * qr[1] - ql[2] * qr[2]
	);
}

vec4 q_inverse(vec4 q)
{
	return vec4(-q.xyz, q.w);
}

vec3 rotate(vec3 vector, vec4 quaternion)
{
	vec4 v = vec4(vector, 0);
	
	v = q_mul(q_mul(quaternion, v), q_inverse(quaternion));
	
	return v.xyz;
}

vec3 scale_normal(vec3 vector, vec3 scale)
{
	return vector * scale.yzx * scale.zxy;
}

void main()
{
	vs_texture_coordinate = texture_coordinate;
	vs_texture_array_coordinate = texture_array_coordinate;
	vs_color = color;
	vs_ambient_color = ambient_color;
	
	vs_normal = normal;
	vs_normal = scale_normal(vs_normal, scale);
	vs_normal = rotate(vs_normal, quaternion);
	vs_normal = normalize(vs_normal);
	
	vs_position = relative_position * scale;
	vs_position = rotate(vs_position, quaternion);
	vs_position += float_position(position);
	gl_Position = projection_matrix * vec4(vs_position, 1);
}
