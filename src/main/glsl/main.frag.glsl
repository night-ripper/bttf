#version 450

// https://on-demand.gputechconf.com/gtc/2014/presentations/S4385-order-independent-transparency-opengl.pdf

layout(location = 0) in vec2 fs_texture_coordinate;
layout(location = 1) in flat mediump vec3 fs_normal;
layout(location = 2) in flat ivec2 fs_texture_array_coordinate;
layout(location = 3) in mediump vec4 fs_color;
layout(location = 4) in vec3 fs_position;
layout(location = 5) in mediump vec3 fs_ambient_color;

layout(location = 0) out vec4 final_color;

layout(location = 0) uniform float multiplier;
layout(location = 1) uniform ivec3 camera_position;
layout(location = 3) uniform int texture_array_binding_color_only;

layout(binding = 0) uniform sampler2DArray texture_in[16];

struct Light
{
	ivec3 position;
	float light_distance;
	vec3 color;
	float cos_inner_cutoff; // or the distance of the line light
	vec3 direction;
	float cos_outer_cutoff;
};

layout(std430, binding = 0) buffer Light_buffer
{
	Light lights[];
};

vec3 hsv(vec3 rgb);
vec3 rgb(vec3 hsv);

vec3 float_position(ivec3 position);

const float specular_strength = 0.5;
const float shininess = 4;

// A simple function with a quadratic term in the denominator such that:
// x = 0 -> y = 1
// x = 1 -> y = 0
float quadratic_attenuation(float dist)
{
	const float third = 1. / 3;
	
	return (1 + third) / pow(dist + 1, 2) - third;
}

void main()
{
	vec4 texture_color;
	
	if (fs_texture_array_coordinate.x == texture_array_binding_color_only.x)
	{
		texture_color = vec4(1, 1, 1, 1);
	}
	else
	{
		texture_color = texture(texture_in[fs_texture_array_coordinate.x],
			vec3(fs_texture_coordinate, fs_texture_array_coordinate.y)
		);
	}
	
	if (texture_color.a == 0)
	{
		discard;
	}
	
	vec3 diffuse_color = vec3(0);
	vec3 specular_color = vec3(0);
	
	for (int i = 0; i < lights.length(); ++i)
	{
		const vec3 light_position = float_position(lights[i].position);
		vec3 light_color = lights[i].color;
		
		vec3 towards_light = light_position - fs_position;
		float dist = length(towards_light);
		// normalize
		towards_light /= dist;
		
		if (lights[i].cos_inner_cutoff < 1)
		{
			float cos_angle = dot(towards_light, -lights[i].direction);
			
			if (cos_angle < lights[i].cos_outer_cutoff)
			{
				continue;
			}
			else if (cos_angle < lights[i].cos_inner_cutoff)
			{
				float multiplier = cos_angle - lights[i].cos_outer_cutoff;
				multiplier /= lights[i].cos_inner_cutoff - lights[i].cos_outer_cutoff ;
				multiplier = pow(multiplier, 3);
				light_color *= multiplier;
			}
			
			const float normalized_dist = dist / lights[i].light_distance;
			
			if (normalized_dist < 1)
			{
				const float attenuation_factor = quadratic_attenuation(normalized_dist);
				
				float diffuse = max(dot(fs_normal, towards_light), 0);
				diffuse *= attenuation_factor;
				diffuse_color += diffuse * light_color;
				
				vec3 reflect_direction = reflect(-towards_light, fs_normal);
				float specular = pow(max(dot(-normalize(fs_position), reflect_direction), 0), shininess);
				specular *= attenuation_factor;
				specular_color += specular_strength * specular * light_color;
			}
		}
		// Code for laser beam lighting, not working yet, need to figure out
		// how diffuse / specular components add up
		else
		{
			// https://mathworld.wolfram.com/Point-LineDistance3-Dimensional.html
			
			/*
			vec3 light_end = light_position + lights[i].direction * lights[i].cos_inner_cutoff;
			float line_distance = length(cross(fs_position - light_position, fs_position - light_end)) /
				length(light_end - light_position)
			;
			
			if (line_distance > lights[i].cos_outer_cutoff - 1)
			{
				continue;
			}
			else if (line_distance > lights[i].cos_inner_cutoff - 1)
			{
				float multiplier = line_distance - (lights[i].cos_inner_cutoff - 1);
				multiplier /= lights[i].cos_outer_cutoff - lights[i].cos_inner_cutoff;
				multiplier = pow(multiplier, 3);
				light_color *= multiplier;
			}
			*/
		}
	}
	
	vec3 light_color = fs_ambient_color + diffuse_color + specular_color;
	
	final_color = texture_color * fs_color * vec4(light_color, 1);
}
