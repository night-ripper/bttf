#version 450
#extension GL_ARB_bindless_texture : require
#extension GL_ARB_gpu_shader5 : require

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 texture_coordinate;
layout(location = 2) in sampler2D texture_handle;

layout(location = 0) out vec2 out_texture_coordinate;
layout(location = 1) out sampler2D out_texture_handle;

void main()
{
	gl_Position = vec4(position.x, position.y, position.z, 1.0);
	out_texture_coordinate = texture_coordinate;
	out_texture_handle = texture_handle;
}
