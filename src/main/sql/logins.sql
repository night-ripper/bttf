-- pragma strict = on;

create table logins(
	username text unique not null,
	salt blob(32) not null,
	password blob(32) not null
);

create unique index logins_index on logins(username);
