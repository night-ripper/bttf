#include <sodium/crypto_secretbox.h>

#include <nwd/crypto/shared_key.hpp>

namespace nwd::crypto
{
static_assert(Shared_key::length == crypto_secretbox_KEYBYTES);
static_assert(Shared_key::mac_length == crypto_secretbox_MACBYTES);
static_assert(Nonce::length == crypto_secretbox_NONCEBYTES);

Shared_key Shared_key::create()
{
	auto result = Shared_key();
	crypto_secretbox_keygen(reinterpret_cast<unsigned char*>(result.data()));
	return result;
}

std::span<std::byte> encrypt(std::span<const std::byte> message,
	std::byte* result, const Nonce& nonce, const Shared_key& key)
{
	crypto_secretbox_easy(
		reinterpret_cast<unsigned char*>(result),
		reinterpret_cast<const unsigned char*>(message.data()), message.size(),
		reinterpret_cast<const unsigned char*>(nonce.data()),
		reinterpret_cast<const unsigned char*>(key.data())
	);
	
	return std::span<std::byte>(result, Shared_key::mac_length + message.size());
}

std::optional<std::span<std::byte>> decrypt(std::span<const std::byte> message,
	std::byte* result, const Nonce& nonce, const Shared_key& key)
{
	auto result_value = std::optional<std::span<std::byte>>();
	
	if (crypto_secretbox_open_easy(
		reinterpret_cast<unsigned char*>(result),
		reinterpret_cast<const unsigned char*>(message.data()), message.size(),
		reinterpret_cast<const unsigned char*>(nonce.data()),
		reinterpret_cast<const unsigned char*>(key.data())) == 0)
	{
		result_value.emplace(std::span<std::byte>(result, message.size() - Shared_key::mac_length));
	}
	
	return result_value;
}
} // namespace nwd::crypto
