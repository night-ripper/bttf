#include <sodium/crypto_box.h>

#include <nwd/crypto/keypair.hpp>

namespace nwd::crypto::keypair
{
static_assert(Public::length == crypto_box_PUBLICKEYBYTES);
static_assert(Private::length == crypto_box_SECRETKEYBYTES);
static_assert(seal_size == crypto_box_SEALBYTES);

keypair_type create()
{
	auto result = keypair_type();
	crypto_box_keypair(reinterpret_cast<unsigned char*>(result.first.data()), reinterpret_cast<unsigned char*>(result.second.data()));
	return result;
}

std::span<std::byte> encrypt(std::span<const std::byte> message, std::byte* result, const Public& public_key)
{
	crypto_box_seal(
		reinterpret_cast<unsigned char*>(result),
		reinterpret_cast<const unsigned char*>(message.data()), message.size(),
		reinterpret_cast<const unsigned char*>(public_key.data())
	);
	
	return std::span<std::byte>(result, message.size() + seal_size);
}

std::optional<std::span<std::byte>> decrypt(std::span<const std::byte> message,
	std::byte* result, const Public& public_key, const Private& private_key)
{
	auto result_value = std::optional<std::span<std::byte>>();
	
	if (crypto_box_seal_open(
		reinterpret_cast<unsigned char*>(result),
		reinterpret_cast<const unsigned char*>(message.data()), message.size(),
		reinterpret_cast<const unsigned char*>(public_key.data()),
		reinterpret_cast<const unsigned char*>(private_key.data())) == 0)
	{
		result_value.emplace(std::span<std::byte>(result, message.size() - seal_size));
	}
	
	return result_value;
}
} // namespace nwd::crypto::keypair
