#pragma once

#include "rendering-coordinates.hpp"
#include "post.hpp"
#include "rendering/buffers.hpp"
#include "rendering/program.hpp"
#include "rendering/textures.hpp"

#include "glue/glsl.hpp"

#include <vector>

namespace bttf
{
struct Rendering_component
{
	float framebuffer_width_;
	float framebuffer_height_;
	float gamma_;
	float exposure_;
	
	rendering::opengl::Vertex_array vao_;
	rendering::opengl::Buffer buf_data_;
	rendering::opengl::Buffer buf_indices_;
	
	rendering::opengl::Shader_storage ssbo_vpos_;
	rendering::opengl::Shader_storage ssbo_vnorm_;
	rendering::opengl::Shader_storage ssbo_vtex_;
	
	rendering::opengl::Shader_storage ssbo_lights_;
	
	rendering::opengl::Program program_;
	
	rendering::opengl::Texture_2D texture_;
	rendering::opengl::Framebuffer framebuffer_;
	rendering::opengl::Renderbuffer renderbuffer_;
	
	rendering::opengl::Vertex_array framebuffer_vao_;
	rendering::opengl::Program framebuffer_program_;
	rendering::opengl::Buffer framebuffer_buf_;
	
	std::int64_t last_index_ = 0;
	std::pmr::vector<Vertex_data> vdata_;
	std::pmr::vector<Element_index> vindices_;
	
	
	std::int64_t last_index_transparent_ = 0;
	std::pmr::vector<Vertex_data> vdata_transparent_;
	std::pmr::vector<Element_index> vindices_transparent_;
	
	std::pmr::vector<Light_coordinates> vlights_;
	
	std::pmr::vector<Post_data> post_triangles_;
	
	~Rendering_component();
	Rendering_component();
	
	Rendering_component(Rendering_component&&) noexcept = delete;
	Rendering_component& operator=(Rendering_component&&) noexcept = delete;
	
	static Rendering_component* current();
	
	void rescale(float width, float height);
	void set_gamma();
	void set_exposure();
	
	void draw(geometry::Vector3D camera_position, const glsl::mat4& camera_matrix);
};
} // namespace bttf
