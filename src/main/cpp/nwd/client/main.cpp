#include <iostream>
#include <syncstream>
#include <iterator>
#include <fstream>
#include <bitset>
#include <filesystem>
#include <thread>
#include <map>

#include <nwd/shared/config.hpp>
#include <nwd/crypto/shared_key.hpp>
#include <nwd/networking/common.hpp>
#include <nwd/networking/socket.hpp>
#include <nwd/utility/random.hpp>
#include <nwd/utility/hex.hpp>
#include <nwd/utility/streams.hpp>
#include <nwd/utility/endian.hpp>
#include <nwd/crypto/keypair.hpp>
#include <nwd/shared/globals/random.hpp>
#include <nwd/shared/messages/common.hpp>
#include <nwd/networking/session.hpp>
#include <nwd/networking/heartbeat_session.hpp>
#include <nwd/networking/messages/from_client.hpp>

#include <nwd/math/vector.hpp>

using namespace nwd;

struct Client_session final : virtual networking::Session
{
	Client_session(const networking::messages::From_client_initial& initial, auto&&... args)
		:
		Session(std::forward<decltype(args)>(args)...)
	{
		this->receive_key_ = initial.server_send_key_;
		this->send_key_ = initial.client_send_key_;
		this->receive_nonce_ = initial.server_send_nonce_;
		this->send_nonce_ = initial.client_send_nonce_;
		std::osyncstream(std::clog) << "YES" << "\n";
	}
	
	Receive_result do_receive(std::span<std::byte> data) final override
	{
		std::cout << remote_endpoint_.port().value() << ": " << data.size() << "\n";
		return Receive_result::ok;
	}
	
	void handle_out_of_order(sequence_type sequence, std::span<std::byte> message) override
	{
		std::osyncstream(std::cout) << "handle_out_of_order" << "\n";
	}
	
	std::ptrdiff_t handle_in_order(std::span<std::byte> message) override
	{
		std::osyncstream(std::cout) << "handle_in_order" << "\n";
		return std::ssize(message);
	}
};

int main()
{
	auto pubkey = crypto::keypair::Public();
	
	{
		auto ifs = std::ifstream(std::filesystem::path(std::getenv("HOME")) / ".nwd_server" / "x25519-pub.pem");
		while (ifs.good() and ifs.get() != '\n');
		utility::hex::decode(utility::read_array<crypto::keypair::Public::length * 2>(ifs), pubkey.begin());
	}
	
	auto initial = networking::messages::From_client_initial();
	auto local_socket = networking::Unique_socket_udp::create();
	local_socket.get().bind(networking::Endpoint(networking::Address::localhost, networking::Port(25566)));
	auto session = Client_session(initial, networking::Endpoint(networking::Address::localhost, networking::Port(25565)), local_socket.get());
	
	local_socket.get().send(session.remote_endpoint(), initial.prepare(pubkey));
	
	auto msg = std::vector<std::byte>();
	msg.reserve(config::max_udp_length);
	
	
	for (int i = 0; i != 50; ++i)
	{
		auto len = crypto::encrypt(msg, msg.data() + 4, initial.client_send_nonce_, initial.client_send_key_).size() + 4;
		std::copy_n(initial.client_send_nonce_.end() - 4, 4, msg.data());
		local_socket.get().send(session.remote_endpoint(), std::span(msg.data(), len));
		++initial.client_send_nonce_;
	}
}
