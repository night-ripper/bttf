#pragma once

#include <cstddef>
#include <cstdint>

#include <array>

#include <nwd/math/vector.hpp>
#include <nwd/geometry/vector3d.hpp>
#include <nwd/math/quaternion.hpp>
#include <nwd/graphics/opengl/normalize.hpp>

namespace nwd::graphics::opengl
{
struct Vertex_data
{
	constexpr static std::int8_t default_ambient_color = std::int8_t(0.05 * std::numeric_limits<std::int8_t>::max());
	
	std::array<nwd::geometry::coord_t::underlying_type, 3> vertex_coordinates_;
	std::array<std::int16_t, 2> texture_coordinates_;
	std::array<std::int16_t, 2> texture_indices_;
	std::array<std::int16_t, 3> relative_position_;
	std::array<_Float16, 3> scale_ {1, 1, 1}; // normalize = false
	std::array<std::int16_t, 4> quaternion_ {0, 0, 0, nrm_max()};
	std::array<std::int16_t, 3> normal_coordinates_;
	std::array<std::int8_t, 4> color_ {nrm_max(), nrm_max(), nrm_max(), nrm_max()};
	std::array<std::int8_t, 3> ambient_color_ {default_ambient_color, default_ambient_color, default_ambient_color};
	
	static std::array<std::int16_t, 4> quaternion(math::Quaternion q)
	{
		constexpr auto max = std::numeric_limits<std::int16_t>::max();
		return std::to_array<std::int16_t>(
		{
			std::int16_t(q[0] * max), std::int16_t(q[1] * max), std::int16_t(q[2] * max), std::int16_t(q[3] * max)
		});
	}
};

struct Element_index
{
	// Must be unsigned
	using index_type = std::uint32_t;
	
	static_assert(std::is_unsigned_v<index_type>);
	
	constexpr static index_type restart = std::numeric_limits<index_type>::max();
	
	Element_index() = default;
	
	constexpr Element_index(std::uint32_t value) noexcept
		:
		value_(value)
	{
	}
	
	constexpr operator index_type() const noexcept
	{
		return value_;
	}
	
private:
	index_type value_;
};

struct alignas(16) Light_coordinates
{
	geometry::Vector3D position;
	float distance;
	math::Vec3f color;
	float cos_inner_cutoff = -1;
	math::Vec3f unit_direction = math::Vec3f(0.f, 0.f, 0.f);
	float cos_outer_cutoff = cos_inner_cutoff;
};

static_assert(std::is_standard_layout_v<Vertex_data>);
static_assert(std::is_standard_layout_v<Element_index>);
static_assert(std::is_standard_layout_v<Light_coordinates>);

// Make sure that vector::clear() does nothing
static_assert(std::is_trivially_destructible_v<Vertex_data>);
static_assert(std::is_trivially_destructible_v<Element_index>);
static_assert(std::is_trivially_destructible_v<Light_coordinates>);
} // namespace nwd::graphics::opengl
