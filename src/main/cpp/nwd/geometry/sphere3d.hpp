#pragma once

#include <ostream>

#include <nwd/geometry/vector3d.hpp>
#include <nwd/serialization/fwd.hpp>

namespace nwd::geometry
{
struct Sphere3D
{
	Sphere3D() = default;
	
	Sphere3D(Vector3D center, float radius) noexcept
		:
		center_(center), radius_(radius)
	{
	}
	
	Vector3D center() const noexcept {return center_;}
	float radius() const noexcept {return radius_;}
	
	Sphere3D& move(Vector3D vector) noexcept
	{
		center_ += vector;
		return *this;
	}
	
public:
	Vector3D center_;
	float radius_;
};

inline std::ostream& operator<<(std::ostream& os, Sphere3D sphere)
{
	return os << "Sphere3D {center: " << sphere.center() << ", radius: " << float(sphere.radius()) << "}";
}
} // namespace nwd::geometry

namespace nwd::serialization
{
template<> inline constexpr std::ptrdiff_t serialized_size<geometry::Sphere3D>
= serialized_size<geometry::Vector3D> + serialized_size<float>;

template<> inline constexpr auto& private_serialize<geometry::Sphere3D> =
*[](std::byte* target, geometry::Sphere3D value) -> void
{
	serialize_shift(target, value.center_);
	serialize_shift(target, value.radius_);
};

template<> inline constexpr auto& private_deserialize<geometry::Sphere3D> =
*[](const std::byte* source, geometry::Sphere3D& result) -> void
{
	deserialize_shift(source, result.center_);
	deserialize_shift(source, result.radius_);
};
} // namespace nwd::serialization
