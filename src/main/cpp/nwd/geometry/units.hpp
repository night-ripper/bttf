#pragma once

#include <cstddef>
#include <cstdint>

#include <limits>

#include <numbers>

#include <nwd/math/fixed_cyclic.hpp>
#include <nwd/math/fixed_point.hpp>

#include <nwd/serialization/fundamental.hpp>

namespace nwd::geometry
{
struct coord_t : math::Fixed_point<std::int32_t, 14>
{
	using Base = math::Fixed_point<std::int32_t, 14>;
	
	using Base::Base;
	
	constexpr coord_t(Base value) noexcept
		:
		Base(value)
	{
	}
	
	constexpr Base& base() noexcept {return static_cast<Base&>(*this);}
	constexpr const Base& base() const noexcept {return static_cast<const Base&>(*this);}
};

namespace privates
{
// TODO wait until clang implements NTTP
struct Two_pi
{
	// std::pow(2, std::log2(2 * std::numbers::pi_v<long double>) - std::numeric_limits<std::uint32_t>::digits)
	constexpr static long double value = 1.46291807926715968188249154e-09L;
};
} // namespace privates

struct angle_t : math::Fixed_cyclic<std::uint32_t, privates::Two_pi>
{
	using Base = math::Fixed_cyclic<std::uint32_t, privates::Two_pi>;
	
	using Base::Base;
	
	constexpr angle_t(Base value) noexcept
		:
		Base(value)
	{
	}
	
	constexpr operator double() const noexcept
	{
		return double(static_cast<Base>(*this));
	}
	
	static const angle_t pi;
	static const angle_t pi_half;
};

constexpr const angle_t angle_t::pi = angle_t::from(
	std::numeric_limits<typename angle_t::underlying_type>::max() / 2 + 1
);

static_assert(angle_t::pi + angle_t::pi == angle_t::from(0));
} // namespace nwd::geometry

namespace nwd::serialization
{
template<> inline constexpr std::ptrdiff_t serialized_size<geometry::coord_t>
= sizeof(geometry::coord_t::underlying_type);

template<> inline constexpr auto& private_serialize<geometry::coord_t> =*
[](std::byte* target, geometry::coord_t value) -> void
{
	serialize(target, value.value());
};

template<> inline constexpr auto& private_deserialize<geometry::coord_t> =*
[](const std::byte* source, geometry::coord_t& result) -> void
{
	deserialize(source, result.value());
};

////////////////////////////////////////////////////////////////////////////////

template<> inline constexpr std::ptrdiff_t serialized_size<geometry::angle_t>
= sizeof(geometry::angle_t::underlying_type);

template<> inline constexpr auto& private_serialize<geometry::angle_t> =*
[](std::byte* target, geometry::angle_t value) -> void
{
	serialize(target, value.value());
};

template<> inline constexpr auto& private_deserialize<geometry::angle_t> =*
[](const std::byte* source, geometry::angle_t& result) -> void
{
	deserialize(source, result.value());
};
} // namespace nwd::serialization
