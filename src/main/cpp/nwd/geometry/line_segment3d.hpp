#pragma once

#include <ostream>
#include <stdexcept>
#include <utility>

#include <nwd/geometry/vector3d.hpp>

namespace nwd::geometry
{
struct Line_segment3D
{
private:
	constexpr Line_segment3D(Vector3D point, Vector3D vector) noexcept
		:
		point_(point),
		vector_(vector)
	{
		if (vector == Vector3D::zero())
		{
			std::unreachable();
		}
	}
	
public:
	Line_segment3D() = default;
	
	constexpr static Line_segment3D point_vec(Vector3D point, Vector3D vector) noexcept
	{
		return Line_segment3D(point, vector);
	}
	
	constexpr static Line_segment3D begin_end(Vector3D begin, Vector3D end) noexcept
	{
		return Line_segment3D(begin, end - begin);
	}
	
	Vector3D begin() const noexcept {return point_;}
	Vector3D end() const noexcept {return begin() + vector();}
	
	Vector3D point() const noexcept {return point_;}
	Vector3D vector() const noexcept {return vector_;}
	
	Line_segment3D& move(Vector3D vector) noexcept
	{
		point_.move(vector);
		return *this;
	}
	
public:
	Vector3D point_;
	Vector3D vector_;
};

inline std::ostream& operator<<(std::ostream& os, Line_segment3D line)
{
	return os << "Line_segment3D {begin: " << line.begin() << ", vector: " << line.vector() << ", end: " << line.end() << "}";
}
} // namespace nwd::geometry

namespace nwd::serialization
{
template<> inline constexpr std::ptrdiff_t serialized_size<geometry::Line_segment3D>
= 2 * serialized_size<geometry::Vector3D>;

template<> inline constexpr auto& private_serialize<geometry::Line_segment3D> =
*[](std::byte* target, geometry::Line_segment3D value) -> void
{
	serialize_shift(target, value.point_);
	serialize_shift(target, value.vector_);
};

template<> inline constexpr auto& private_deserialize<geometry::Line_segment3D> =
*[](const std::byte* source, geometry::Line_segment3D& result) -> void
{
	deserialize_shift(source, result.point_);
	deserialize_shift(source, result.vector_);
};
} // namespace nwd::serialization
