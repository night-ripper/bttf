#pragma once

#include <nwd/geometry/vector3d.hpp>
#include <nwd/geometry/bounding_box3d.hpp>
#include <nwd/math/plane3d.hpp>

namespace nwd::geometry
{
struct Visibility_frustum
{
	geometry::Vector3D position_;
	math::Plane3D visibility_planes_[6];
	
	bool operator()(geometry::Vector3D vector) const noexcept;
	bool operator()(geometry::Bounding_box3D box) const noexcept;
};
} // namespace nwd::geometry
