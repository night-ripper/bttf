#include <cstdint>

#include <nwd/geometry/common.hpp>
#include <nwd/geometry/vector3d.hpp>
#include <nwd/geometry/line_segment3d.hpp>
#include <nwd/geometry/line_iterator3d.hpp>
#include <nwd/geometry/cylinder3d.hpp>
#include <nwd/geometry/sphere3d.hpp>
#include <nwd/geometry/triangle3d.hpp>
#include <nwd/geometry/bounding_box3d.hpp>

#include <nwd/math/vector.hpp>
#include <nwd/math/quaternion.hpp>

namespace nwd::geometry
{
static float cos_angle(Vector3D vector1, Vector3D vector2) noexcept
{
	return dot<float>(vector1, vector2) / (length<float>(vector1) * length<float>(vector2));
}

template<typename Type>
static Type cross2d(Type left_x, Type left_y, Type right_x, Type right_y) noexcept
{
	return left_x * right_y - left_y * right_x;
}

angle_t angle(Vector3D vector1, Vector3D vector2) noexcept
{
	return angle_t(std::acos(cos_angle(vector1, vector2)));
}

angle_t angle(Vector3D vector, Line_segment3D line) noexcept
{
	return angle(line.vector(), vector - line.begin());
}

angle_t angle(Line_segment3D line, Vector3D vector) noexcept
{
	return angle(vector, line);
}

////////////////////////////////////////////////////////////////////////////////

bool overlap(Vector3D, Vector3D) noexcept
{
	return false;
}

bool overlap(Vector3D, Line_segment3D) noexcept
{
	return false;
}

bool overlap(Vector3D, Triangle3D) noexcept
{
	return false;
}

bool overlap(Vector3D lhs, Sphere3D rhs) noexcept
{
	return contains(rhs, lhs);
}

bool overlap(Vector3D lhs, Cylinder3D rhs) noexcept
{
	return contains(rhs, lhs);
}

////////////////////////////////////////////////////////////////////////////////

bool overlap(Line_segment3D lhs, Vector3D rhs) noexcept
{
	return overlap(rhs, lhs);
}

bool overlap(Line_segment3D, Line_segment3D) noexcept
{
	return false;
}

bool overlap(Line_segment3D lhs, Triangle3D rhs) noexcept
{
	const auto normal = rhs.normal();
	
	// t = -(a * x_0 + b * y_0 + c * z_0) / (a * x_t + b * y_t + c * z_t)
	
	if (const float divisor = dot<float>(normal, lhs.vector()); divisor == 0)
	{
		// Line segment lies on the same plane
		if (const float numerator = dot<float>(normal, lhs.begin() - rhs.point()); numerator == 0)
		{
			bool inside_neg = false, inside_eq = false, inside_pos = false;
			
			for (const auto points = rhs.points(); const auto line : line_range(points))
			{
				const auto triangle_line_normal = cross(normal, line.vector());
				bool neg = false, eq = false, pos = false;
				
				{
					const auto direction = dot<float>(
						triangle_line_normal, lhs.begin() - line.begin()
					);
					
					if (direction < 0) {inside_neg = neg = true;}
					if (direction == 0) {inside_eq = eq = true;}
					if (direction > 0) {inside_pos = pos = true;}
				}
				{
					const auto direction = dot<float>(
						triangle_line_normal, lhs.end() - line.begin()
					);
					
					if (direction < 0) {neg = true;}
					if (direction == 0) {eq = true;}
					if (direction > 0) {pos = true;}
				}
				
				if (eq or (neg and pos))
				{
					const auto line_normal = cross(normal, lhs.vector());
					bool negl = false, eql = false, posl = false;
					
					for (const auto p : {line.begin(), line.end()})
					{
						const auto direction = dot<float>(
							line_normal, p - lhs.begin()
						);
						
						if (direction < 0) {negl = true; continue;}
						if (direction == 0) {eql = true; continue;}
						if (direction > 0) {posl = true; continue;}
					}
					
					return eql or (negl + posl > 1);
				}
			}
			
			return inside_eq or not (inside_neg and inside_pos);
		}
		
		return false;
	}
	
	const auto [t, v0, v1, div] = math::solve<float>({
		Vector3D::floating(-lhs.vector()),
		Vector3D::floating(rhs.vectors()[0]),
		Vector3D::floating(rhs.vectors()[1]),
		Vector3D::floating(lhs.begin() - rhs.point()),
	});
	
	if (t < 0 or v0 < 0 or v1 < 0)
	{
		return false;
	}
	
	if (div < t or div < v0 + v1)
	{
		return false;
	}
	
	return true;
}

bool overlap(Line_segment3D lhs, Sphere3D rhs) noexcept
{
	if (contains(rhs, lhs.begin())) {return true;}
	
	lhs.move(-rhs.center());
	
	const float a = dot_sqr<float>(lhs.vector());
	const float b = 2 * dot<float>(lhs.begin(), lhs.vector());
	const float c = dot_sqr<float>(lhs.begin()) - std::pow(rhs.radius(), 2.f);
	
	if (const float discriminant = b * b - 4 * a * c; discriminant >= 0)
	{
		const float sqrt_dis = std::sqrt(discriminant);
		const float t_1 = -b - sqrt_dis;
		const float t_2 = -b + sqrt_dis;
		
		return (0 <= t_1 and t_1 <= 2 * a) or (0 <= t_2 and t_2 <= 2 * a);
	}
	
	return false;
}

bool overlap(Line_segment3D lhs, Cylinder3D rhs) noexcept
{
	if (contains(rhs, lhs.begin()))
	{
		return true;
	}
	
	lhs.move(-rhs.center());
	
	const auto z_0 = float(lhs.begin()[2]);
	const auto z_v = float(lhs.vector()[2]);
	
	if (not (lhs.vector()[0] == coord_t::from(0) and lhs.vector()[1] == coord_t::from(0)))
	{
		auto line_xy = Line_segment3D::point_vec(
			Vector3D(lhs.begin()[0], lhs.begin()[1], coord_t::from(0)),
			Vector3D(lhs.vector()[0], lhs.vector()[1], coord_t::from(0))
		);
		
		const float a = dot_sqr<float>(line_xy.vector());
		const float b = 2 * dot<float>(line_xy.begin(), line_xy.vector());
		const float c = dot_sqr<float>(line_xy.begin()) - std::pow(rhs.radius(), 2.f);
		
		if (const float discriminant = b * b - 4 * a * c; discriminant >= 0)
		{
			const float sqrt_dis = std::sqrt(discriminant);
			
			for (const float t : {-b - sqrt_dis, -b + sqrt_dis})
			{
				// We are supposed to divide t by (2 * a) to make t a line parameter
				// of range [0, 1]
				// Instead we multiply everything else by 2 * a.
				const float z = z_0 * 2 * a + t * z_v;
				
				if (float(-rhs.height()) * 2 * a <= z and z <= float(rhs.height()) * 2 * a and 0 <= t and t <= 2 * a)
				{
					return true;
				}
			}
		}
	}
	
	if (lhs.vector()[2] != coord_t::from(0))
	{
		const auto z_v = float(lhs.vector()[2]);
		
		const auto t_numerator = float(rhs.height() + lhs.begin()[2]);
		
		for (const float t : {t_numerator / -z_v, t_numerator / z_v})
		{
			if (0 <= t and t <= 1)
			{
				float value = std::hypot(float(lhs.begin()[0]) + t * float(lhs.vector()[0]),
					float(lhs.begin()[1]) + t * float(lhs.vector()[1])
				);
				
				if (value <= rhs.radius())
				{
					return true;
				}
			}
		}
	}
	
	return false;
}

bool overlap(Line_segment3D lhs, Bounding_box3D rhs) noexcept
{
	rhs.move(-lhs.begin());
	
	// For all three axis-aligned views
	// Keep the order so that d0 is always a different dimension
	for (std::size_t dimensions[3][2] = {{0, 1}, {1, 2}, {2, 0}}; auto [d0, d1] : dimensions)
	{
		const coord_t crds[2][2] = {
			{rhs.min()[d0], rhs.max()[d0]},
			{rhs.min()[d1], rhs.max()[d1]},
		};
		
		const coord_t v[2] = {lhs.vector()[d0], lhs.vector()[d1]};
		
		if (crds[0][0] <= coord_t::from(0) and coord_t::from(0) <= crds[0][1]
			and crds[1][0] <= coord_t::from(0) and coord_t::from(0) <= crds[1][1])
		{
			continue;
		}
		
		// Check only d0, the outer loop will check for all coordinates
		if (coord_t::from(0) < crds[0][0] and v[0] < crds[0][0])
		{
			return false;
		}
		
		if (crds[0][1] < coord_t::from(0) and crds[0][1] < v[0])
		{
			return false;
		}
		
		bool cross_negative = false;
		bool cross_zero = false;
		bool cross_positive = false;
		
		for (coord_t crd0 : crds[0])
		{
			for (coord_t crd1 : crds[1])
			{
				float result = cross2d<float>(float(crd0), float(crd1), float(v[0]), float(v[1]));
				
				if (result < 0) {cross_negative = true;}
				if (result == 0) {cross_zero = true;}
				if (result > 0) {cross_positive = true;}
			}
		}
		
		if (not cross_zero and not (cross_negative and cross_positive))
		{
			return false;
		}
	}
	
	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool overlap(Triangle3D lhs, Vector3D rhs) noexcept
{
	return overlap(rhs, lhs);
}

bool overlap(Triangle3D lhs, Line_segment3D rhs) noexcept
{
	return overlap(rhs, lhs);
}

bool overlap(Triangle3D lhs, Triangle3D rhs) noexcept
{
	for (const auto lp = lhs.points(); const auto ll : line_range(lp))
	{
		if (overlap(ll, rhs))
		{
			return true;
		}
	}
	
	for (const auto rp = rhs.points(); const auto rl : line_range(rp))
	{
		if (overlap(lhs, rl))
		{
			return true;
		}
	}
	
	return false;
}

bool overlap(Triangle3D lhs, Sphere3D rhs) noexcept
{
	if (contains(rhs, lhs.point()))
	{
		return true;
	}
	
	for (const auto points = lhs.points(); const auto line : line_range(points))
	{
		if (overlap(line, rhs))
		{
			return true;
		}
	}
	
	const auto normal = cross(lhs.vectors()[0], lhs.vectors()[1]);
	
	const auto [t, v0, v1, div] = math::solve<float>({
		Vector3D::floating(normal),
		Vector3D::floating(lhs.vectors()[0]),
		Vector3D::floating(lhs.vectors()[1]),
		Vector3D::floating(rhs.center() - lhs.point()),
	});
	
	if (std::abs(t) > rhs.radius() * length(normal))
	{
		return false;
	}
	
	if (v0 < 0 or v1 < 0)
	{
		return false;
	}
	
	if (div < v0 + v1)
	{
		return false;
	}
	
	return true;
}

bool overlap(Triangle3D lhs, Cylinder3D rhs) noexcept
{
	for (const auto points = lhs.points(); const auto line : line_range(points))
	{
		if (overlap(line, rhs))
		{
			return true;
		}
	}
	
	if (overlap(lhs, Line_segment3D::point_vec(
		rhs.center() - Vector3D(coord_t::from(0), coord_t::from(0), rhs.height()),
		Vector3D(coord_t::from(0), coord_t::from(0), rhs.height() * 2))))
	{
		return true;
	}
	
	auto normal = cross(lhs.vectors()[0], lhs.vectors()[1]);
	normal[2] = coord_t::from(0);
	
	for (coord_t heights[2] {-rhs.height(), rhs.height()}; const coord_t h : heights)
	{
		const auto [t, v0, v1, div] = math::solve<float>({
			Vector3D::floating(normal),
			Vector3D::floating(lhs.vectors()[0]),
			Vector3D::floating(lhs.vectors()[1]),
			Vector3D::floating(rhs.center() + Vector3D(coord_t::from(0), coord_t::from(0), h) - lhs.point()),
		});
		
		if (std::abs(t) > rhs.radius() * length(normal))
		{
			continue;
		}
		
		if (v0 < 0 or v1 < 0)
		{
			continue;
		}
		
		if (div < v0 + v1)
		{
			continue;
		}
		
		return true;
	}
	
	return false;
}

bool overlap(Triangle3D lhs, Bounding_box3D rhs) noexcept
{
	auto ps = lhs.points();
	
	for (std::uint8_t indices[3][2] = {{0, 1}, {1, 2}, {2, 0}}; auto [i0, i1] : indices)
	{
		auto line = Line_segment3D::begin_end(ps[i0], ps[i1]);
		
		if (overlap(line, rhs))
		{
			return true;
		}
	}
	
	const auto min = rhs.min();
	const auto max = rhs.max();
	
	if (overlap(lhs, Line_segment3D::begin_end(min, max)))
	{
		return true;
	}
	
	if (overlap(lhs, Line_segment3D::begin_end(
		Vector3D(max[0], min[1], min[2]), Vector3D(min[0], max[1], max[2]))))
	{
		return true;
	}
	
	if (overlap(lhs, Line_segment3D::begin_end(
		Vector3D(min[0], min[1], max[2]), Vector3D(max[0], max[1], min[2]))))
	{
		return true;
	}
	
	if (overlap(lhs, Line_segment3D::begin_end(
		Vector3D(max[0], min[1], max[2]), Vector3D(min[0], max[1], min[2]))))
	{
		return true;
	}
	
	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool overlap(Sphere3D lhs, Vector3D rhs) noexcept
{
	return overlap(rhs, lhs);
}

bool overlap(Sphere3D lhs, Line_segment3D rhs) noexcept
{
	return overlap(rhs, lhs);
}

bool overlap(Sphere3D lhs, Triangle3D rhs) noexcept
{
	return overlap(rhs, lhs);
}

bool overlap(Sphere3D lhs, Sphere3D rhs) noexcept
{
	return distance(lhs.center(), rhs.center()) <= lhs.radius() + rhs.radius();
}

bool overlap(Sphere3D lhs, Cylinder3D rhs) noexcept
{
	if (contains(lhs, rhs) or contains(rhs, lhs))
	{
		return true;
	}
	
	rhs.move(-lhs.center());
	
	const float dist_xy = length_xy(rhs.center());
	
	if (dist_xy > lhs.radius() + rhs.radius())
	{
		return false;
	}
	
	for (coord_t heights[2] {-rhs.height(), rhs.height()}; const coord_t h : heights)
	{
		float z = float(rhs.center()[2] + h);
		
		if (std::abs(z) <= lhs.radius()
			and std::sqrt(std::pow(lhs.radius(), 2) - std::pow(z, 2)) + rhs.radius() >= dist_xy)
		{
			return true;
		}
	}
	
	return false;
}

bool overlap(Sphere3D lhs, Bounding_box3D rhs) noexcept
{
	if (contains(rhs, lhs.center()))
	{
		return true;
	}
	
	rhs.move(-lhs.center());
	
	const float r = lhs.radius();
	
	// 2D checks
	const auto box_inside = [&](std::size_t d0, std::size_t d1) noexcept -> bool
	{
		return std::hypot(float(rhs.center()[d0]), float(rhs.center()[d1])) <= r;
	};
	
	const auto sphere_inside = [&](std::size_t d0, std::size_t d1) noexcept -> bool
	{
		return float(rhs.min()[d0]) <= -r and r <= float(rhs.max()[d0])
			and float(rhs.min()[d1]) <= -r and r <= float(rhs.max()[d1])
		;
	};
	
	for (std::size_t dimensions[3][2] = {{0, 1}, {0, 2}, {1, 2}}; auto [d0, d1] : dimensions)
	{
		auto values = {rhs.min()[d0], rhs.min()[d1], rhs.max()[d0], rhs.max()[d1]};
		
		// Check if any of the top-down view quadratic equations has at least one solution
		// Need to check for all 4
		if (not sphere_inside(d0, d1) and not box_inside(d0, d1)
			and std::none_of(std::begin(values), std::end(values), [&](auto value) noexcept -> bool
			{
				return std::abs(float(value)) <= std::abs(r);
			}))
		{
			return false;
		}
	}
	
	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool overlap(Cylinder3D lhs, Vector3D rhs) noexcept
{
	return overlap(rhs, lhs);
}

bool overlap(Cylinder3D lhs, Line_segment3D rhs) noexcept
{
	return overlap(rhs, lhs);
}

bool overlap(Cylinder3D lhs, Triangle3D rhs) noexcept
{
	return overlap(rhs, lhs);
}

bool overlap(Cylinder3D lhs, Sphere3D rhs) noexcept
{
	return overlap(rhs, lhs);
}

bool overlap(Cylinder3D lhs, Cylinder3D rhs) noexcept
{
	if (distance_xy(lhs.center(), rhs.center()) > lhs.radius() + rhs.radius())
	{
		return false;
	}
	
	if (lhs.center()[2] - lhs.height() > rhs.center()[2] + rhs.height()
		or rhs.center()[2] - rhs.height() > lhs.center()[2] + lhs.height())
	{
		return false;
	}
	
	return true;
}

bool overlap(Cylinder3D lhs, Bounding_box3D rhs) noexcept
{
	if (contains(rhs, lhs.center()))
	{
		return true;
	}
	
	rhs.move(-lhs.center());
	
	const float r = lhs.radius();
	
	const auto box_inside_xy = [&]() -> bool {return length_xy(rhs.center()) <= r;};
	const auto cylinder_inside_xy = [&]() -> bool
	{
		return float(rhs.min()[0]) <= -r and r <= float(rhs.max()[0])
			and float(rhs.min()[1]) <= -r and r <= float(rhs.max()[1])
		;
	};
	
	auto values = {rhs.min()[0], rhs.min()[1], rhs.max()[0], rhs.max()[1]};
	
	// Check of any of the top-down view quadratic equations has at least one solution
	// Need to check for all 4
	if (not cylinder_inside_xy() and not box_inside_xy()
		and std::none_of(std::begin(values), std::end(values), [&](auto value) noexcept -> bool
		{
			return std::abs(float(value)) <= std::abs(r);
		}))
	{
		return false;
	}
	
	// Side view for vertical intersections in x and y dimensions
	for (auto d : std::initializer_list<std::uint8_t>{0, 1})
	{
		if (float(rhs.max()[d]) < -r or r < float(rhs.min()[d]))
		{
			return false;
		}
	}
	
	// Side view for horizontal intersections from any direction since height is the same
	if (const auto h = lhs.height(); rhs.max()[2] < -h or h < rhs.min()[2])
	{
		return false;
	}
	
	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool overlap(Bounding_box3D lhs, Line_segment3D rhs) noexcept
{
	return overlap(rhs, lhs);
}

bool overlap(Bounding_box3D lhs, Triangle3D rhs) noexcept
{
	return overlap(rhs, lhs);
}

bool overlap(Bounding_box3D lhs, Sphere3D rhs) noexcept
{
	return overlap(rhs, lhs);
}

bool overlap(Bounding_box3D lhs, Cylinder3D rhs) noexcept
{
	return overlap(rhs, lhs);
}

////////////////////////////////////////////////////////////////////////////////

bool contains(Sphere3D outer, Vector3D inner) noexcept
{
	return distance(outer.center(), inner) <= outer.radius();
}

bool contains(Sphere3D outer, Line_segment3D inner) noexcept
{
	return contains(outer, inner.begin()) and contains(outer, inner.end());
}

bool contains(Sphere3D outer, Triangle3D inner) noexcept
{
	auto pts = inner.points();
	return std::all_of(std::begin(pts), std::end(pts), [&](auto v) -> bool
	{
		return contains(outer, v);
	});
}

bool contains(Sphere3D outer, Sphere3D inner) noexcept
{
	return distance(outer.center(), inner.center()) <= outer.radius() - inner.radius();
}

bool contains(Sphere3D outer, Cylinder3D inner) noexcept
{
	return contains(outer, Sphere3D(inner.center(), std::hypot(inner.radius(), float(inner.height()))));
}

////////////////////////////////////////////////////////////////////////////////

bool contains(Cylinder3D outer, Vector3D inner) noexcept
{
	inner -= outer.center();
	
	if (length_xy(inner) > outer.radius())
	{
		return false;
	}
	
	if (inner[2] < -outer.height()
		or inner[2] > outer.height())
	{
		return false;
	}
	
	return true;
}

bool contains(Cylinder3D outer, Line_segment3D inner) noexcept
{
	return contains(outer, inner.begin()) and contains(outer, inner.end());
}

bool contains(Cylinder3D outer, Triangle3D inner) noexcept
{
	for (auto&& v : inner.points())
	{
		if (not contains(outer, v))
		{
			return false;
		}
	}
	
	return true;
}

bool contains(Cylinder3D outer, Sphere3D inner) noexcept
{
	inner.move(-outer.center());
	
	if (length_xy(inner.center()) > outer.radius() - inner.radius())
	{
		return false;
	}
	
	if (auto sph_radius = coord_t::from_ceil(inner.radius());
		inner.center()[2] - sph_radius < -outer.height()
		or inner.center()[2] + sph_radius > outer.height())
	{
		return false;
	}
	
	return true;
}

bool contains(Cylinder3D outer, Cylinder3D inner) noexcept
{
	inner.move(-outer.center());
	
	if (length_xy(inner.center()) > outer.radius() - inner.radius())
	{
		return false;
	}
	
	if (inner.center()[2] - inner.height() < -outer.height()
		or inner.center()[2] + inner.height() > outer.height())
	{
		return false;
	}
	
	return true;
}

////////////////////////////////////////////////////////////////////////////////

Bounding_box3D box_of(Line_segment3D line) noexcept
{
	const auto b = line.begin();
	const auto e = line.end();
	
	Vector3D min(std::min(b[0], e[0]), std::min(b[1], e[1]), std::min(b[2], e[2]));
	Vector3D max(std::max(b[0], e[0]), std::max(b[1], e[1]), std::max(b[2], e[2]));
	
	return Bounding_box3D(min - Vector3D::epsilon(), max + Vector3D::epsilon());
}

Bounding_box3D box_of(Triangle3D triangle) noexcept
{
	auto pts = triangle.points();
	auto result = std::accumulate(std::begin(pts), std::end(pts), Bounding_box3D::empty());
	return Bounding_box3D(result.min() - Vector3D::epsilon(), result.max() + Vector3D::epsilon());
}

Bounding_box3D box_of(Sphere3D sphere) noexcept
{
	auto r = coord_t::from_ceil(sphere.radius());
	Vector3D min = sphere.center() - Vector3D(r, r, r);
	Vector3D max = sphere.center() + Vector3D(r, r, r);
	return Bounding_box3D(min, max);
}

Bounding_box3D box_of(Cylinder3D cylinder) noexcept
{
	auto r = coord_t::from_ceil(cylinder.radius());
	Vector3D min = cylinder.center() - Vector3D(r, r, cylinder.height());
	Vector3D max = cylinder.center() + Vector3D(r, r, cylinder.height());
	return Bounding_box3D(min, max);
}
} // namespace nwd::geometry
