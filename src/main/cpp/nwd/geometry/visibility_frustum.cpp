#include <nwd/geometry/visibility_frustum.hpp>

namespace nwd::geometry
{
bool Visibility_frustum::operator()(geometry::Vector3D vector) const noexcept
{
	vector -= position_;
	
	for (const auto& plane : visibility_planes_)
	{
		if (not plane.half_space(vector))
		{
			return false;
		}
	}
	
	return true;
}

bool Visibility_frustum::operator()(geometry::Bounding_box3D box) const noexcept
{
	box.move(-position_);
	
	auto vertices = std::array<math::Vec3d, 8>();
	
	{
		auto box_vertices = box.vertices();
		
		for (std::size_t i = 0; i != 8; ++i)
		{
			vertices[i] = box_vertices[i];
		}
	}
	
	for (const auto& plane : visibility_planes_)
	{
		auto result = [&]() noexcept -> bool
		{
			for (auto vertex : vertices)
			{
				if (plane.half_space(vertex))
				{
					return true;
				}
			}
			
			return false;
		}();
		
		if (not result)
		{
			return false;
		}
	}
	
	return true;
}
} // namespace nwd::geometry
