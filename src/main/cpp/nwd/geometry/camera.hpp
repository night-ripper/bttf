#pragma once

#include <array>
#include <tuple>
#include <utility>

#include <nwd/math/vector.hpp>
#include <nwd/math/matrix.hpp>
#include <nwd/geometry/visibility_frustum.hpp>

namespace nwd::geometry
{
struct Camera
{
	struct Perspective
	{
		float near_z_;
		float far_z_;
		float x_fov_;
		float aspect_ratio_;
	};
	
	Camera() = default;
	
	Camera(geometry::Vector3D origin,
		math::Vec3f unit_direction, math::Vec3f unit_up_vector,
		Perspective perspective);
	
	static Camera upwards_camera(geometry::Vector3D origin,
		geometry::angle_t angle_xy, geometry::angle_t angle_z,
		Perspective perspective);
	
	void fov(float x_fov);
	void aspect_ratio(float ratio);
	void near_distance(float near_z);
	void far_distance(float far_z);
	
	void position(geometry::Vector3D vector);
	
	math::Mat4f final_matrix();
	
	geometry::Vector3D position() const {return position_;}
	math::Vec3f unit_direction() const {return unit_direction_;}
	math::Vec3f unit_up_vector() const {return unit_up_vector_;}
	
	Perspective perspective() const {return {near_z_, far_z_, x_fov_, aspect_ratio_};}
	
	float near_distance() const {return near_z_;}
	float far_distance() const {return far_z_;}
	float fov() const {return x_fov_;}
	float aspect_ratio() const {return aspect_ratio_;}
	
	void rotated(geometry::angle_t angle_xy, geometry::angle_t angle_z);
	void rotate_around(math::Vec3f unit_axis, geometry::angle_t angle);
	void rotate_xy(geometry::angle_t angle);
	void rotate_z(geometry::angle_t angle);
	
	void move_absolute(float forward, float right, float up);
	void move_relative(float forward, float right, float up);
	
	Visibility_frustum visibility_frustum() const;
	
private:
	void unit_direction(math::Vec3f vector);
	void unit_up_vector(math::Vec3f vector);
	void compute_projection();
	void compute_tangents();
	void compute_view();
	
private:
	bool recompute_projection_ = true;
	bool recompute_view_ = true;
	
	geometry::Vector3D position_;
	math::Vec3f unit_direction_;
	math::Vec3f unit_up_vector_;
	
	float near_z_;
	float far_z_;
	
	float x_fov_;
	float aspect_ratio_;
	
	float tan_x_half_fov_;
	float tan_y_half_fov_;
	
	math::Mat4f projection_;
	math::Mat4f view_;
};
} // namespace nwd::geometry
