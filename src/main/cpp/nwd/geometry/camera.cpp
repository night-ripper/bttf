#include <utility>

#include <iostream>

#include <nwd/geometry/camera.hpp>
#include <nwd/math/plane3d.hpp>
#include <nwd/geometry/vector3d.hpp>
#include <nwd/geometry/bounding_box3d.hpp>

namespace nwd::geometry
{
namespace
{
std::tuple<float, float> tangents(float x_fov, float aspect_ratio)
{
	float result = std::tan(x_fov / 2);
	return {result, result / aspect_ratio};
}

math::Mat4f look_at(math::Vec3f unit_direction, math::Vec3f unit_perpendicular_up)
{
	const math::Vec3f unit_perpendicular_right = math::cross(unit_direction, unit_perpendicular_up);
	
	auto result = math::Mat4f();
	
	result[0, 0] = unit_perpendicular_right[0];
	result[1, 0] = unit_perpendicular_right[1];
	result[2, 0] = unit_perpendicular_right[2];
	
	result[0, 1] = unit_perpendicular_up[0];
	result[1, 1] = unit_perpendicular_up[1];
	result[2, 1] = unit_perpendicular_up[2];
	
	result[0, 2] = -unit_direction[0];
	result[1, 2] = -unit_direction[1];
	result[2, 2] = -unit_direction[2];
	
	/// We are sending the camera position to the shader where it is subtracted.
	// result(3, 0) = 0;
	// result(3, 1) = 0;
	// result(3, 2) = 0;
	
	result[3, 3] = 1;
	
	return result;
}

math::Mat4f perspective_matrix(float tan_x_half_fov, float tan_y_half_fov, float near, float far)
{
	auto result = math::Mat4f();
	
	result[0, 0] = 1.f / tan_x_half_fov;
	result[1, 1] = 1.f / tan_y_half_fov;
	result[2, 2] = -(far + near) / (far - near);
	result[2, 3] = -1;
	result[3, 2] = -(2 * far * near) / (far - near);
	
	return result;
}
} // namespace

Camera::Camera(geometry::Vector3D origin,
	math::Vec3f unit_direction, math::Vec3f unit_up_vector,
	Perspective perspective)
	:
	position_(origin),
	unit_direction_(unit_direction),
	unit_up_vector_(unit_up_vector),
	near_z_(perspective.near_z_),
	far_z_(perspective.far_z_),
	x_fov_(perspective.x_fov_),
	aspect_ratio_(perspective.aspect_ratio_)
{
	compute_tangents();
	
	if (std::abs(1.f - math::length(unit_direction)) > 2 * std::numeric_limits<float>::epsilon())
	{
		std::clog
		<<
		"Internal warning: passing a unit vector of length different than 1 to "
		"Camera constructor as the parameter `unit_direction`, length differnce: "
		<< (1 - math::length(unit_direction))
		<< "\n"
		;
	}
	
	if (std::abs(1.f - math::length(unit_up_vector)) > 2 * std::numeric_limits<float>::epsilon())
	{
		std::clog
		<<
		"Internal warning: passing a unit vector of length different than 1 to "
		"Camera constructor as the parameter `unit_up_vector`, length difference: "
		<< (1. - math::length(unit_up_vector)) << "\n"
		<< std::numeric_limits<float>::epsilon() << "\n"
		;
	}
}

Camera Camera::upwards_camera(geometry::Vector3D origin,
	geometry::angle_t angle_xy, geometry::angle_t angle_z,
	Perspective perspective)
{
	auto [direction, up] = math::direction_up_unit_vectors(double(angle_xy), double(angle_z));
	
	return Camera(origin, direction, up, perspective);
}

void Camera::compute_projection()
{
	projection_ = perspective_matrix(tan_x_half_fov_, tan_y_half_fov_, near_z_, far_z_);
}

void Camera::compute_tangents()
{
	std::tie(tan_x_half_fov_, tan_y_half_fov_) = tangents(x_fov_, aspect_ratio_);
}

void Camera::fov(float x_fov)
{
	x_fov_ = x_fov;
	compute_tangents();
	recompute_projection_ = true;
}

void Camera::aspect_ratio(float ratio)
{
	aspect_ratio_ = ratio;
	compute_tangents();
	recompute_projection_ = true;
}

void Camera::near_distance(float near_z)
{
	near_z_ = near_z;
	recompute_projection_ = true;
}

void Camera::far_distance(float far_z)
{
	far_z_ = far_z;
	recompute_projection_ = true;
}

void Camera::compute_view()
{
	view_ = look_at(unit_direction_, unit_up_vector_);
}

void Camera::position(geometry::Vector3D vector)
{
	position_ = vector;
	recompute_view_ = true;
}

void Camera::unit_direction(math::Vec3f vector)
{
	unit_direction_ = math::normalize(vector);
	recompute_view_ = true;
}

void Camera::unit_up_vector(math::Vec3f vector)
{
	unit_up_vector_ = math::normalize(vector);
	recompute_view_ = true;
}

math::Mat4f Camera::final_matrix()
{
	if (std::exchange(recompute_projection_, false))
	{
		compute_projection();
	}
	
	if (std::exchange(recompute_view_, false))
	{
		compute_view();
	}
	
	return projection_ * view_;
}

void Camera::rotated(geometry::angle_t angle_xy, geometry::angle_t angle_z)
{
	auto [direction, up] = math::direction_up_unit_vectors(double(angle_xy), double(angle_z));
	
	unit_direction(direction);
	unit_up_vector(up);
}

void Camera::rotate_around(math::Vec3f unit_axis, geometry::angle_t angle)
{
	/// This approach should keep the direction and up vectors perpendicular.
	
	auto normal = math::cross(unit_direction_, unit_up_vector_);
	unit_direction(math::rotate<float>(unit_direction_, float(angle), unit_axis));
	normal = math::rotate<float>(normal, float(angle), unit_axis);
	unit_up_vector(math::rotate<float>(unit_direction_, float(geometry::angle_t::pi_half), normal));
}

void Camera::rotate_xy(geometry::angle_t angle)
{
	rotate_around(math::Vec3f(0, 0, 1), angle);
}

void Camera::rotate_z(geometry::angle_t angle)
{
	auto unit_axis = math::cross(unit_direction_, unit_up_vector_);
	rotate_around(unit_axis, angle);
}

/// Moves the camera @p forward and @p right keeping its xy coordinates
/// and @p up on the z-axis
void Camera::move_absolute(float forward, float right, float up)
{
	auto direction_xy = unit_direction_ + unit_up_vector_;
	direction_xy[2] = 0;
	direction_xy = math::normalize(direction_xy);
	
	auto unit_right_vector = math::cross(unit_direction_, unit_up_vector_);
	
	auto position_difference = math::Vec3f();
	
	position_difference += forward * direction_xy;
	position_difference += right * unit_right_vector;
	position_difference[2] += up;
	
	geometry::Vector3D new_position = position_;
	
	new_position[0] += geometry::coord_t::from_round(position_difference[0]);
	new_position[1] += geometry::coord_t::from_round(position_difference[1]);
	new_position[2] += geometry::coord_t::from_round(position_difference[2]);
	
	position(new_position);
}

/// Moves the camera relative to where it is facing
void Camera::move_relative(float forward, float right, float up)
{
	auto unit_right_vector = math::cross(unit_direction_, unit_up_vector_);
	
	auto position_difference = math::Vec3f();
	
	position_difference += forward * unit_direction();
	position_difference += right * unit_right_vector;
	position_difference += up * unit_up_vector();
	
	geometry::Vector3D new_position = position_;
	
	new_position[0] += geometry::coord_t::from_round(position_difference[0]);
	new_position[1] += geometry::coord_t::from_round(position_difference[1]);
	new_position[2] += geometry::coord_t::from_round(position_difference[2]);
	
	position(new_position);
}

Visibility_frustum Camera::visibility_frustum() const
{
	auto up_vector = unit_up_vector_ * tan_y_half_fov_;
	auto right_vector = math::cross(unit_direction_, unit_up_vector_) * tan_x_half_fov_;
	
	auto near_plane = math::Plane3D(near_z_ * unit_direction_, up_vector, right_vector);
	auto far_plane = math::Plane3D(far_z_ * unit_direction_, right_vector, up_vector);
	auto plane1 = math::Plane3D(math::cross(unit_up_vector_, unit_direction_ + right_vector));
	auto plane2 = math::Plane3D(math::cross(-right_vector, unit_direction_ + up_vector));
	auto plane3 = math::Plane3D(math::cross(-unit_up_vector_, unit_direction_ - right_vector));
	auto plane4 = math::Plane3D(math::cross(right_vector, unit_direction_ - up_vector));
	
	return Visibility_frustum(position_, {plane1, plane2, plane3, plane4, near_plane, far_plane});
}
} // namespace nwd::geometry
