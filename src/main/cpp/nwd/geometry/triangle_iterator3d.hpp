#pragma once

#include <iterator>
#include <type_traits>
#include <ranges>

#include <nwd/geometry/triangle3d.hpp>
#include <nwd/utility/iterators.hpp>

namespace nwd::geometry
{
template<typename Vertex_iterator>
requires(std::forward_iterator<Vertex_iterator> and std::is_convertible_v<typename std::iterator_traits<Vertex_iterator>::value_type, Vector3D>)
struct Triangle_iterator3D
{
	using difference_type = typename std::iterator_traits<Vertex_iterator>::difference_type;
	using value_type = Triangle3D;
	using pointer = void;
	using reference = void;
	using iterator_category = std::forward_iterator_tag;
	
	bool shuffle_ = false;
	Vertex_iterator it_;
	
	Triangle_iterator3D() = default;
	
	constexpr Triangle_iterator3D(Vertex_iterator begin) noexcept
		:
		it_(begin)
	{
	}
	
	constexpr friend bool operator==(Triangle_iterator3D lhs, Vertex_iterator rhs) noexcept
	{
		return lhs.it_ == rhs;
	}
	
	constexpr friend bool operator==(Triangle_iterator3D lhs, Triangle_iterator3D rhs) noexcept
	{
		return lhs.it_ == rhs.it_;
	}
	
	constexpr value_type operator*() const noexcept
	{
		auto next = std::next(it_);
		auto nextnext = std::next(next);
		
		if (not shuffle_)
		{
			return Triangle3D::from_points(*it_, *next, *nextnext);
		}
		else
		{
			return Triangle3D::from_points(*it_, *nextnext, *next);
		}
	}
	
	constexpr Triangle_iterator3D& operator+=(difference_type diff) noexcept
	{
		it_ += diff;
		shuffle_ ^= (diff % 2);
		
		return *this;
	}
	
	constexpr friend Triangle_iterator3D operator+(Triangle_iterator3D it, difference_type diff) noexcept
	{
		return it += diff;
	}
	
	constexpr Triangle_iterator3D& operator++() noexcept
	{
		++it_;
		shuffle_ = not shuffle_;
		return *this;
	}
	
	constexpr Triangle_iterator3D operator++(int) noexcept
	{
		return utility::operators::post_increment(*this);
	}
};

////////////////////////////////////////////////////////////////////////////////

template<typename Vertex_iterator>
requires(std::forward_iterator<Vertex_iterator> and std::is_convertible_v<typename std::iterator_traits<Vertex_iterator>::value_type, Vector3D>)
inline constexpr std::ranges::subrange<Triangle_iterator3D<Vertex_iterator>, Vertex_iterator> triangle_range(Vertex_iterator begin, Vertex_iterator end) noexcept
{
	return std::ranges::subrange(Triangle_iterator3D(begin), std::prev(std::prev(end)));
}

template<typename Vertex_range_type, typename Vertex_iterator = decltype(std::begin(std::declval<Vertex_range_type>()))>
requires(std::is_convertible_v<decltype(*std::begin(std::declval<Vertex_range_type>())), Vector3D>)
inline constexpr std::ranges::subrange<Triangle_iterator3D<Vertex_iterator>, Vertex_iterator> triangle_range(const Vertex_range_type& range) noexcept
{
	return triangle_range(std::begin(range), std::end(range));
}
} // namespace nwd::geometry
