#pragma once

#include <algorithm>
#include <array>
#include <utility>
#include <ostream>
#include <stdexcept>

#include <nwd/geometry/vector3d.hpp>

namespace nwd::geometry
{
struct Bounding_box3D
{
	Bounding_box3D() = default;
	
	constexpr Bounding_box3D(Vector3D min, Vector3D max)
		:
		min_(min),
		max_(max)
	{
		if (min_[0] > max_[0] or min_[1] > max_[1] or min_[2] > max_[2])
		{
			std::unreachable();
		}
	}
	
	constexpr static Bounding_box3D empty() noexcept
	{
		auto result = Bounding_box3D();
		result.min_ = Vector3D(coord_t::max(), coord_t::max(), coord_t::max());
		result.max_ = Vector3D(coord_t::lowest(), coord_t::lowest(), coord_t::lowest());
		return result;
	}
	
	constexpr static Bounding_box3D full() noexcept
	{
		auto result = Bounding_box3D();
		result.min_ = Vector3D(coord_t::lowest(), coord_t::lowest(), coord_t::lowest());
		result.max_ = Vector3D(coord_t::max(), coord_t::max(), coord_t::max());
		return result;
	}
	
	constexpr friend bool operator==(const Bounding_box3D& lhs, const Bounding_box3D& rhs) noexcept = default;
	
	constexpr Vector3D min() const noexcept {return min_;}
	constexpr Vector3D max() const noexcept {return max_;}
	
	constexpr Bounding_box3D& move(Vector3D vector) noexcept
	{
		min_ += vector;
		max_ += vector;
		return *this;
	}
	
	constexpr Bounding_box3D& operator+=(Vector3D rhs) noexcept
	{
		min_[0] = std::min(min()[0], rhs[0]);
		min_[1] = std::min(min()[1], rhs[1]);
		min_[2] = std::min(min()[2], rhs[2]);
		
		max_[0] = std::max(max()[0], rhs[0]);
		max_[1] = std::max(max()[1], rhs[1]);
		max_[2] = std::max(max()[2], rhs[2]);
		
		return *this;
	}
	
	constexpr friend Bounding_box3D operator+(Vector3D lhs, Bounding_box3D rhs) noexcept
	{
		return rhs += lhs;
	}
	
	constexpr friend Bounding_box3D operator+(Bounding_box3D lhs, Vector3D rhs) noexcept
	{
		return lhs += rhs;
	}
	
	constexpr Bounding_box3D& operator+=(const Bounding_box3D& rhs) noexcept
	{
		min_[0] = std::min(min()[0], rhs.min()[0]);
		min_[1] = std::min(min()[1], rhs.min()[1]);
		min_[2] = std::min(min()[2], rhs.min()[2]);
		
		max_[0] = std::max(max()[0], rhs.max()[0]);
		max_[1] = std::max(max()[1], rhs.max()[1]);
		max_[2] = std::max(max()[2], rhs.max()[2]);
		
		return *this;
	}
	
	constexpr friend Bounding_box3D operator+(Bounding_box3D lhs, const Bounding_box3D& rhs) noexcept
	{
		return lhs += rhs;
	}
	
	constexpr Vector3D center() const noexcept
	{
		return midpoint(min(), max());
	}
	
	constexpr std::array<Vector3D, 8> vertices() const noexcept
	{
		return {{
			{min()[0], min()[1], min()[2]},
			{min()[0], min()[1], max()[2]},
			{min()[0], max()[1], min()[2]},
			{min()[0], max()[1], max()[2]},
			
			{max()[0], min()[1], min()[2]},
			{max()[0], min()[1], max()[2]},
			{max()[0], max()[1], min()[2]},
			{max()[0], max()[1], max()[2]},
		}};
	}
	
public:
	Vector3D min_;
	Vector3D max_;
};

static_assert(std::is_nothrow_copy_constructible_v<Bounding_box3D>);

inline constexpr bool contains(Bounding_box3D outer, Vector3D inner) noexcept
{
	return inner[0] >= outer.min()[0] and inner[0] <= outer.max()[0]
		and inner[1] >= outer.min()[1] and inner[1] <= outer.max()[1]
		and inner[2] >= outer.min()[2] and inner[2] <= outer.max()[2]
	;
}

inline constexpr bool contains(Bounding_box3D outer, Bounding_box3D inner) noexcept
{
	return outer.min()[0] <= inner.min()[0] and outer.min()[1] <= inner.min()[1]
		and outer.min()[2] <= inner.min()[2] and outer.max()[0] >= inner.max()[0]
		and outer.max()[1] >= inner.max()[1] and outer.max()[2] >= inner.max()[2]
	;
}

inline constexpr bool overlap(Vector3D lhs, Bounding_box3D rhs) noexcept
{
	return contains(rhs, lhs);
}

inline constexpr bool overlap(Bounding_box3D lhs, Vector3D rhs) noexcept
{
	return overlap(rhs, lhs);
}

inline constexpr bool overlap(Bounding_box3D lhs, Bounding_box3D rhs) noexcept
{
	return lhs.min()[0] <= rhs.max()[0] and lhs.min()[1] <= rhs.max()[1]
		and lhs.min()[2] <= rhs.max()[2] and rhs.min()[0] <= lhs.max()[0]
		and rhs.min()[1] <= lhs.max()[1] and rhs.min()[2] <= lhs.max()[2]
	;
}

inline constexpr Bounding_box3D box_of(Vector3D value) noexcept
{
	return Bounding_box3D(value - Vector3D::epsilon(), value + Vector3D::epsilon());
}

inline constexpr Bounding_box3D box_of(Bounding_box3D value) noexcept
{
	return value;
}

inline std::ostream& operator<<(std::ostream& os, const Bounding_box3D& box)
{
	return os << "Bounding_box3D {min: " << box.min() << ", max: " << box.max() << "}";
}
} // namespace nwd::geometry

namespace nwd::serialization
{
template<> inline constexpr std::ptrdiff_t serialized_size<geometry::Bounding_box3D>
= 2 * serialized_size<geometry::Vector3D>;

template<> inline constexpr auto& private_serialize<geometry::Bounding_box3D> =
*[](std::byte* target, geometry::Bounding_box3D value) -> void
{
	serialize_shift(target, value.min_);
	serialize_shift(target, value.max_);
};

template<> inline constexpr auto& private_deserialize<geometry::Bounding_box3D> =
*[](const std::byte* source, geometry::Bounding_box3D& result) -> void
{
	
	deserialize_shift(source, result.min_);
	deserialize_shift(source, result.max_);
};
} // namespace nwd::serialization
