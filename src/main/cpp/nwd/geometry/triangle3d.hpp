#pragma once

#include <array>
#include <ostream>

#include <nwd/geometry/vector3d.hpp>
#include <nwd/serialization/fwd.hpp>

namespace nwd::geometry
{
struct Triangle3D
{
private:
	Triangle3D(Vector3D point, Vector3D v1, Vector3D v2) noexcept
		:
		point_(point), v1_(v1), v2_(v2)
	{
	}
	
public:
	Triangle3D() = default;
	
	static Triangle3D from_vectors(Vector3D position, Vector3D v1, Vector3D v2) noexcept
	{
		return {position, v1, v2};
	}
	
	static Triangle3D from_points(Vector3D p1, Vector3D p2, Vector3D p3) noexcept
	{
		return {p1, p2 - p1, p3 - p1};
	}
	
	Triangle3D& move(Vector3D vector) noexcept
	{
		point_ -= vector;
		return *this;
	}
	
	template<typename Type>
	Triangle3D& rotate_xy(Type sin, Type cos) noexcept
	{
		v1_.rotate_xy(sin, cos);
		v2_.rotate_xy(sin, cos);
		return *this;
	}
	
	template<typename Type = double>
	Triangle3D& rotate_xy(angle_t angle) noexcept
	{
		return rotate_xy(std::sin(Type(angle), std::cos(Type(angle))));
	}
	
	Vector3D point() const noexcept
	{
		return point_;
	}
	
	std::array<Vector3D, 2> vectors() const noexcept
	{
		return {v1_, v2_};
	}
	
	std::array<Vector3D, 3> points() const noexcept
	{
		return {point_, point_ + v1_, point_ + v2_};
	}
	
	Vector3D normal() const noexcept
	{
		return normalize(cross(v1_, v2_));
	}
	
public:
	Vector3D point_;
	Vector3D v1_;
	Vector3D v2_;
};

inline std::ostream& operator<<(std::ostream& os, Triangle3D triangle)
{
	auto pts = triangle.points();
	return os << "Triangle3D {" << pts[0] << ", " << pts[1] << ", " << pts[2] << "}";
}
} // namespace nwd::geometry

namespace nwd::serialization
{
template<> inline constexpr std::ptrdiff_t serialized_size<geometry::Triangle3D>
= 3 * serialized_size<geometry::Vector3D>;

template<> inline constexpr auto& private_serialize<geometry::Triangle3D> =
*[](std::byte* target, geometry::Triangle3D value) -> void
{
	serialize_shift(target, value.point_);
	serialize_shift(target, value.v1_);
	serialize_shift(target, value.v2_);
};

template<> inline constexpr auto& private_deserialize<geometry::Triangle3D> =
*[](const std::byte* source, geometry::Triangle3D& result) -> void
{
	deserialize_shift(source, result.point_);
	deserialize_shift(source, result.v1_);
	deserialize_shift(source, result.v2_);
};
} // namespace nwd::serialization
