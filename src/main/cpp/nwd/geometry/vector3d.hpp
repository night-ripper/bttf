#pragma once

#include <cmath>

#include <array>
#include <ostream>

#include <nwd/geometry/units.hpp>
#include <nwd/serialization/fwd.hpp>
#include <nwd/utility/integer_cast.hpp>

namespace nwd::geometry
{
struct Vector3D : std::array<coord_t, 3>
{
	Vector3D() = default;
	
	constexpr Vector3D(const Vector3D&) = default;
	constexpr Vector3D& operator=(const Vector3D&) = default;
	
	constexpr Vector3D(coord_t x, coord_t y, coord_t z) noexcept
		:
		std::array<coord_t, 3> {x, y, z}
	{
	}
	
	constexpr static Vector3D zero() noexcept
	{
		return Vector3D(coord_t(0.f), coord_t(0.f), coord_t(0.f));
	}
	
	constexpr static Vector3D epsilon() noexcept
	{
		return Vector3D(coord_t::epsilon(), coord_t::epsilon(), coord_t::epsilon());
	}
	
	constexpr friend bool operator==(Vector3D lhs, Vector3D rhs) noexcept = default;
	
	constexpr static std::array<float, 3> floating(Vector3D vector) noexcept
	{
		return std::array<float, 3> {float(vector[0]), float(vector[1]), float(vector[2])};
	}
	
	constexpr friend Vector3D operator-(Vector3D vector) noexcept
	{
		return Vector3D(-vector[0], -vector[1], -vector[2]);
	}
	
	constexpr Vector3D& operator+=(Vector3D rhs) noexcept
	{
		(*this)[0] += rhs[0];
		(*this)[1] += rhs[1];
		(*this)[2] += rhs[2];
		return *this;
	}
	
	constexpr Vector3D& operator-=(Vector3D rhs) noexcept
	{
		return *this += -rhs;
	}
	
	constexpr Vector3D& operator*=(auto rhs) noexcept
	{
		(*this)[0] *= rhs;
		(*this)[1] *= rhs;
		(*this)[2] *= rhs;
		return *this;
	}
	
	constexpr Vector3D& operator/=(auto rhs) noexcept
	{
		return *this *= (1. / rhs);
	}
	
	constexpr Vector3D& move(Vector3D vector) noexcept
	{
		return *this += vector;
	}
	
	template<typename Type>
	constexpr Vector3D& rotate_xy(Type sin, Type cos) noexcept
	{
		return *this = {
			coord_t(Type((*this)[0]) * cos - Type((*this)[1]) * sin),
			coord_t(Type((*this)[1]) * cos + Type((*this)[0]) * sin),
			(*this)[2],
		};
	}
	
	template<typename Type = float>
	constexpr Vector3D& rotate_xy(angle_t angle) noexcept
	{
		return rotate_xy(std::sin(Type(angle)), std::cos(Type(angle)));
	}
	
	Vector3D& rotate_around(Vector3D unit_axis, angle_t angle) noexcept;
};

constexpr Vector3D operator+(Vector3D lhs, Vector3D rhs) noexcept
{
	return lhs += rhs;
}

constexpr Vector3D operator-(Vector3D lhs, Vector3D rhs) noexcept
{
	return lhs -= rhs;
}

constexpr Vector3D operator*(Vector3D lhs, auto rhs) noexcept
{
	return lhs *= rhs;
}

constexpr Vector3D operator/(Vector3D lhs, auto rhs) noexcept
{
	return lhs /= rhs;
}

template<typename Type = float>
inline Type length(Vector3D vector) noexcept
{
	return std::hypot(Type(vector[0]), Type(vector[1]), Type(vector[2]));
}

template<typename Type = float>
inline Type distance(Vector3D lhs, Vector3D rhs) noexcept
{
	return length<Type>(rhs -= lhs);
}

template<typename Type = float>
inline Type length_xy(Vector3D vector) noexcept
{
	return std::hypot(Type(vector[0]), Type(vector[1]));
}

template<typename Type = float>
inline Type distance_xy(Vector3D lhs, Vector3D rhs) noexcept
{
	return std::hypot(Type(rhs[0]) - Type(lhs[0]), Type(rhs[1]) - Type(lhs[1]));
}

constexpr Vector3D midpoint(Vector3D lhs, Vector3D rhs) noexcept
{
	return Vector3D(
		midpoint(lhs[0], rhs[0]),
		midpoint(lhs[1], rhs[1]),
		midpoint(lhs[2], rhs[2])
	);
}

template<typename Type = float>
requires(std::is_floating_point_v<Type>)
constexpr Type dot(Vector3D lhs, Vector3D rhs) noexcept
{
	using max_type = coord_t::max_type;
	
	auto result = max_type();
	result += max_type(lhs[0].value()) * max_type(rhs[0].value());
	result += max_type(lhs[1].value()) * max_type(rhs[1].value());
	result += max_type(lhs[2].value()) * max_type(rhs[2].value());
	result /= (max_type(1) << coord_t::point_position);
	
	return Type(result) * Type(coord_t::epsilon());
}

template<typename Type = float>
requires(std::is_floating_point_v<Type>)
constexpr Type dot_sqr(Vector3D vector) noexcept
{
	return dot<Type>(vector, vector);
}

constexpr Vector3D cross(Vector3D lhs, Vector3D rhs) noexcept
{
	using max_type = coord_t::max_type;
	
	auto x = max_type(lhs[1].value()) * max_type(rhs[2].value()) - max_type(lhs[2].value()) * max_type(rhs[1].value());
	auto y = max_type(lhs[2].value()) * max_type(rhs[0].value()) - max_type(lhs[0].value()) * max_type(rhs[2].value());
	auto z = max_type(lhs[0].value()) * max_type(rhs[1].value()) - max_type(lhs[1].value()) * max_type(rhs[0].value());
	
	x /= (max_type(1) << coord_t::point_position);
	y /= (max_type(1) << coord_t::point_position);
	z /= (max_type(1) << coord_t::point_position);
	
	return {coord_t::from(utility::checked_cast(x)), coord_t::from(utility::checked_cast(y)), coord_t::from(utility::checked_cast(z))};
}

inline Vector3D& Vector3D::rotate_around(Vector3D unit_axis, angle_t angle) noexcept
{
	/// https://en.wikipedia.org/wiki/Rodrigues%27_rotation_formula
	
	return *this = *this * std::cos(angle)
		+ cross(unit_axis, *this) * std::sin(angle)
		+ unit_axis * dot(unit_axis, *this) * (1.f - std::cos(angle))
	;
}

template<typename Type = float>
inline std::array<Type, 2> height_ratio(Vector3D vector) noexcept
{
	return {std::hypot(Type(vector[0]), Type(vector[1])), Type(vector[2])};
}

inline Vector3D normalize(Vector3D vector) noexcept
{
	return vector * (1.f / length<float>(vector));
}

inline std::ostream& operator<<(std::ostream& os, Vector3D vector)
{
	return os << "Vector3D {" << float(vector[0]) << ", " << float(vector[1]) << ", " << float(vector[2]) << "}";
}
} // namespace nwd::geometry

namespace nwd::serialization
{
template<> inline constexpr std::ptrdiff_t serialized_size<geometry::Vector3D>
= 3 * serialized_size<geometry::coord_t>;

template<> inline constexpr auto& private_serialize<geometry::Vector3D> =
*[](std::byte* target, geometry::Vector3D value) -> void
{
	serialize_shift(target, value[0]);
	serialize_shift(target, value[1]);
	serialize_shift(target, value[2]);
};

template<> inline constexpr auto& private_deserialize<geometry::Vector3D> =
*[](const std::byte* source, geometry::Vector3D& result) -> void
{
	deserialize_shift(source, result[0]);
	deserialize_shift(source, result[1]);
	deserialize_shift(source, result[2]);
};
} // namespace nwd::serialization
