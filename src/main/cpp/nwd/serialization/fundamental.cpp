#include <cstdint>
#include <cstring>

#include <type_traits>
#include <iostream>


#include <nwd/serialization/fundamental.hpp>

using namespace nwd::serialization;

int main()
{
	unsigned char buff[4] {250, 250, 1, 45};
	int val = deserialize<int>(buff);
	val = -1;
	std::cout << val << "\n";
	serialize(buff, val);
	
	{
		unsigned char bufff[4] {};
		std::memcpy(bufff, &val, 4);
		
		for (auto c : buff)
		{
			std::cout << int(c) << "\n";
		}
	}
	
	for (auto c : buff)
	{
		std::cout << int(c) << "\n";
	}
}
