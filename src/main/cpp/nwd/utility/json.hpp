#pragma once

#include <cstddef>

#include <ostream>

namespace nwd::utility
{
template<typename Iterator>
inline std::ostream& json_array(std::ostream& os, Iterator begin, Iterator end, auto&& writer)
{
	const char* delim = "";
	
	os << '[';
	
	while (begin != end)
	{
		os << delim;
		writer(os, *begin);
		delim = ",";
		++begin;
	}
	
	os << ']';
	
	return os;
}

template<typename Iterator>
inline std::ostream& json_array(std::ostream& os, Iterator begin, Iterator end)
{
	return json_array(os, begin, end, [](std::ostream& os, auto& value) -> void
	{
		os << value;
	});
}

template<typename Iterator>
inline std::ostream& json_array_n(std::ostream& os, Iterator begin, auto amount, auto&& writer)
{
	const char* delim = "";
	
	os << '[';
	
	while (amount != 0)
	{
		os << delim;
		writer(os, *begin);
		delim = ",";
		++begin;
		--amount;
	}
	
	os << ']';
	
	return os;
}

template<typename Iterator>
inline std::ostream& json_array_n(std::ostream& os, Iterator begin, auto amount)
{
	return json_array_n(os, begin, amount, [](std::ostream& os, auto& value) -> void
	{
		os << value;
	});
}
} // namespace nwd::utility
