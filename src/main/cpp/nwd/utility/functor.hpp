#pragma once

#include <type_traits>
#include <utility>
#include <version>

namespace nwd::utility
{
namespace privates
{
template<typename Result_type, auto Function, typename... Args>
struct Functor_impl
{
	using result_type = Result_type;
	
#ifdef __cpp_static_call_operator
	static
#endif
	constexpr Result_type operator()(Args... args)
#ifndef __cpp_static_call_operator
	const
#endif
	noexcept(noexcept(Function))
	{
		return Function(std::forward<Args>(args)...);
	}
};
} // namespace privates

//! Wraps a function pointer into a typed functor object.
template<auto Function>
struct Functor;

template<typename Result_type, typename... Args, Result_type(*Function)(Args...)>
struct Functor<Function> : privates::Functor_impl<Result_type, Function, Args...>
{
};
} // namespace nwd::utility
