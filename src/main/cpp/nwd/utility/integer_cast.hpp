#pragma once

#include <limits>
#include <type_traits>
#include <utility>

namespace nwd::utility
{
template<typename Target_type, typename Source_type = void>
struct Integer_cast
{
private:
	template<typename, typename>
	friend struct Integer_cast;
	
	constexpr static Target_type impl(Source_type value) noexcept
	requires(std::is_same_v<Target_type, Source_type>)
	{
		return value;
	}
	
	constexpr static Target_type impl(Source_type value) noexcept
	requires(std::is_signed_v<Target_type> and std::is_unsigned_v<Source_type>
		and sizeof(Target_type) == sizeof(Source_type))
	{
		return static_cast<Target_type>(value - (std::numeric_limits<Source_type>::max()
			/ static_cast<Source_type>(2) + static_cast<Source_type>(1))
		);
	}
	
	constexpr static Target_type impl(Source_type value) noexcept
	requires(std::is_unsigned_v<Target_type> and std::is_signed_v<Source_type>
		and sizeof(Target_type) == sizeof(Source_type))
	{
		return static_cast<Target_type>(value) + std::numeric_limits<Target_type>::max()
			/ static_cast<Target_type>(2) + static_cast<Target_type>(1)
		;
	}
	
	constexpr static Target_type impl(Source_type value) noexcept
	requires(std::is_signed_v<Target_type> == std::is_signed_v<Source_type>
		and std::is_unsigned_v<Target_type> == std::is_unsigned_v<Source_type>
		and sizeof(Target_type) < sizeof(Source_type))
	{
		return static_cast<Target_type>(value / (static_cast<Source_type>(1) << static_cast<Source_type>(
			(std::numeric_limits<Source_type>::digits - static_cast<Source_type>(std::numeric_limits<Target_type>::digits))
		)));
	}
	
	constexpr static Target_type impl(Source_type value) noexcept
	requires(std::is_signed_v<Target_type> == std::is_signed_v<Source_type>
		and std::is_unsigned_v<Target_type> == std::is_unsigned_v<Source_type>
		and sizeof(Target_type) > sizeof(Source_type))
	{
		return static_cast<Target_type>(value) * (static_cast<Target_type>(1) << static_cast<Target_type>(
			(std::numeric_limits<Target_type>::digits - static_cast<Target_type>(std::numeric_limits<Source_type>::digits))
		));
	}
	
public:
	constexpr Target_type operator()(Source_type value) noexcept
	{
		using Intermediate_type = std::conditional_t<std::is_signed_v<Target_type>, std::make_signed_t<Source_type>, std::make_unsigned_t<Source_type>>;
		return Integer_cast<Target_type, Intermediate_type>::impl(Integer_cast<Intermediate_type, Source_type>::impl(value));
	}
};

template<typename Target_type>
struct Integer_cast<Target_type, void>
{
	template<typename Source_type>
	constexpr Target_type operator()(Source_type value) noexcept
	{
		return Integer_cast<Target_type, Source_type>()(value);
	}
};

//! Casts an integer to another type preserving the value as relative to the
//! minimum and maximum value of @tparam Target_type.
template<typename Target_type, typename Source_type>
requires(std::is_integral_v<Target_type> and std::is_integral_v<Source_type>)
inline constexpr Target_type integer_cast(Source_type value) noexcept
{
	return Integer_cast<Target_type>()(value);
}

template<typename Type>
struct Checked_cast
{
	constexpr Checked_cast(Type value) noexcept
		:
		value_(value)
	{
	}
	
	template<typename Target_type>
	constexpr operator Target_type() noexcept
	{
		if (not std::in_range<Target_type>(value_))
		{
			std::unreachable();
		}
		
		return static_cast<Target_type>(value_);
	}
	
private:
	Type value_;
};

template<typename Target_type = void, typename Source_type>
requires((std::is_void_v<Target_type> or std::is_integral_v<Target_type>) and std::is_integral_v<Source_type>)
inline constexpr auto checked_cast(Source_type value) noexcept
-> std::conditional_t<std::is_void_v<Target_type>, Checked_cast<Source_type>, Target_type>
{
	return Checked_cast(value);
}
} // namespace nwd::utility
