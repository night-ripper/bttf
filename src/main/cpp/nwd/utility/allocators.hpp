#pragma once

#include <cstddef>

#include <span>

namespace nwd::utility
{
template<typename Value_type>
struct Buffer_allocator
{
	using value_type = Value_type;
	using pointer = Value_type*;
	using const_pointer = const Value_type*;
	using void_pointer = void*;
	using const_void_pointer = const void*;
	using size_type = std::size_t;
	using difference_type = std::ptrdiff_t;
	
	template<typename Type>
	struct rebind
	{
		using other = Buffer_allocator<Type>;
	};
	
	struct Buffer_allocation_overflow : std::bad_alloc
	{
	};
	
	Buffer_allocator() = default;
	
	Buffer_allocator(std::span<std::byte> data) noexcept
		:
		data_(data)
	{
	}
	
	pointer allocate(size_type n) const
	{
		if (n > data_.size())
		{
			throw Buffer_allocation_overflow();
		}
		
		return data_.data();
	}
	
	void deallocate(pointer, size_type) const noexcept
	{
	}
	
private:
	std::span<std::byte> data_;
};
} // namespace nwd::utility
