#pragma once

#include <cstddef>

#include <bit>
#include <memory>

namespace nwd::utility
{
struct Type_erased
{
	template<typename Type>
	using move_constructor_type = Type*(*)(Type*, Type&&);
	
	template<typename Type>
	using destructor_type = void(*)(Type*);
	
	template<typename Type>
	const static Type_erased of;
	
	std::int16_t alignment_power_;
	std::int32_t size_multiplier_;
	void(*move_constructor_)(void*, void*);
	void(*destructor_)(void*);
};

template<typename Type>
constexpr Type_erased Type_erased::of = Type_erased
{
	std::countr_zero(alignof(Type)),
	sizeof(Type) / alignof(Type),
	[](void* location, void* args) -> void
	{
		std::construct_at(static_cast<Type*>(location), std::move(*static_cast<Type*>(args)));
	},
	[](void* location) -> void
	{
		std::destroy_at(static_cast<Type*>(location));
	},
};

struct Type_erased_object : Type_erased
{
	template<typename Type>
	Type_erased_object(Type&& value)
		:
		Type_erased(Type_erased::of<Type>)
	{
		object_ = &value;
	}
	
	void* object_;
};
} // namespace nwd::utility
