#include <iostream>
#include <functional>

#include "invoker.hpp"

#include <memory>
#include <chrono>
#include <random>
#include <span>
#include <experimental/type_traits>

std::random_device rd;
std::mt19937 gen(rd());
std::uniform_int_distribution<> distrib(-1'000'000, 1'000'000);

void invoke_lambda(std::span<int> span, auto&& invoker)
{
	for (std::ptrdiff_t i = 0; i < ssize(span) - 1; ++i)
	{
		span[i] = invoker(span[i], span[i + 1]);
	}
}

void invoke_invoker(std::span<int> span, nwd::utility::Invoker<int(int, int)> invoker)
{
	for (std::ptrdiff_t i = 0; i < ssize(span) - 1; ++i)
	{
		span[i] = invoker(span[i], span[i + 1]);
	}
}

void invoke_function(std::span<int> span, std::function<int(int, int)> invoker)
{
	for (std::ptrdiff_t i = 0; i < ssize(span) - 1; ++i)
	{
		span[i] = invoker(span[i], span[i + 1]);
	}
}

void invoke_mo_function(std::span<int> span, std::move_only_function<int(int, int)> invoker)
{
	for (std::ptrdiff_t i = 0; i < ssize(span) - 1; ++i)
	{
		span[i] = invoker(span[i], span[i + 1]);
	}
}

int main()
{
	std::ptrdiff_t a = distrib(gen);
	std::ptrdiff_t b = distrib(gen);
	std::ptrdiff_t c = distrib(gen);
	std::ptrdiff_t d = distrib(gen);
	std::ptrdiff_t e = distrib(gen);
	std::ptrdiff_t f = distrib(gen);
	
	std::vector<int> vec(10'000'000);
	
	auto lbd = [&](int lhs, int rhs) noexcept -> int
	{
		if (lhs > a and lhs > b and lhs > c)
		{
			return lhs;
		}
		else if (rhs > d and rhs > e and rhs > f)
		{
			return rhs;
		}
		else
		{
			return std::midpoint(lhs, rhs);
		}
	};
	
	std::chrono::steady_clock::time_point start;
	
	for (auto& v : vec)
	{
		v = distrib(gen);
	}
	
	start = std::chrono::steady_clock::now();
	invoke_lambda(vec, lbd);
	std::cout << "lambda:             " << std::chrono::duration<double>(std::chrono::steady_clock::now() - start).count() << " s" << "\n";
	
	for (auto& v : vec)
	{
		v = distrib(gen);
	}
	
	start = std::chrono::steady_clock::now();
	invoke_invoker(vec, lbd);
	std::cout << "Invoker:            " << std::chrono::duration<double>(std::chrono::steady_clock::now() - start).count() << " s" << "\n";
	
	for (auto& v : vec)
	{
		v = distrib(gen);
	}
	
	start = std::chrono::steady_clock::now();
	invoke_mo_function(vec, lbd);
	std::cout << "move_only_function: " << std::chrono::duration<double>(std::chrono::steady_clock::now() - start).count() << " s" << "\n";
	
	for (auto& v : vec)
	{
		v = distrib(gen);
	}
	
	start = std::chrono::steady_clock::now();
	invoke_function(vec, lbd);
	std::cout << "function:           " << std::chrono::duration<double>(std::chrono::steady_clock::now() - start).count() << " s" << "\n";
}
