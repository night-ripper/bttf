#include <iostream>

#include <bitset>
#include <limits>
#include <ranges>
#include <algorithm>

#include <nwd/utility/bit_indexing.hpp>

using namespace nwd::utility::bit_indexing;

int main()
{
	std::uintmax_t arr[index_length<std::uintmax_t>(4097)] {};
// 	std::ranges::fill(arr, -1);
	
	erase(arr + index_length<std::uintmax_t>(4097), 500, 4097);
	
	for (auto v : arr)
	{
		std::cout << std::bitset<64>(v) << "\n";
	}
	
	std::cout << "##########" << "\n";
	
// 	std::cout << index_length<std::uintmax_t>(std::uintmax_t(-1) - 63) << "\n";
// 	std::cout << push_front(static_cast<std::uintmax_t*>(nullptr), (std::uintmax_t(-1) - 63)) << "\n";
	
	for (int i = 0; i != 4097; ++i)
	{
		if (i != push_front(arr, 4097))
		{
			throw;
		}
	}
	
	for (auto v : arr)
	{
		std::cout << std::bitset<64>(v) << "\n";
	}
	
	unsigned is[] = {1};
	std::cout << erase(is + std::size(is), 0, int(std::size(is))) << "\n";
	std::cout << is[0] << "\n";
// 	std::cout << is[1] << "\n";
	
	std::cout << index_length<unsigned>(0) << "\n";
	std::cout << index_length<unsigned>(1) << "\n";
	std::cout << index_length<unsigned>(2) << "\n";
	
	
	unsigned src = 987;
	unsigned dst = 0;
	
	copy(&src + 1, 1, &dst + 1, 1);
	
	std::cout << src << "\n";
	std::cout << dst << "\n";
	
	unsigned inp = 0;
	std::cout << push_front(&inp, 1) << "\n";
	std::cout << inp << "\n";
	std::cout << push_front(&inp, 1) << "\n";
	std::cout << inp << "\n";
}
