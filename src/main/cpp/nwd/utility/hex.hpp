#pragma once

#include <cstddef>
#include <cstdint>

#include <array>
#include <iterator>
#include <ranges>
#include <span>
#include <stdexcept>

#include <nwd/utility/iterators.hpp>

namespace nwd::utility::hex
{
template<typename Iterator_type>
struct Encoder
{
	using difference_type = std::ptrdiff_t;
	using value_type = char;
	
	Encoder() = default;

	explicit Encoder(auto&& it) noexcept
		:
		half_(false),
		it_(std::forward<decltype(it)>(it))
	{
	}
	
	friend bool operator==(const Encoder& lhs, const Encoder& rhs) noexcept = default;
	
	bool operator==(const Iterator_type& rhs) const noexcept
	{
		return it_ == rhs;
	}
	
	value_type operator*() const
	{
		auto it = std::to_integer<std::uint8_t>(*it_);
		it = std::uint8_t(it >> (std::uint8_t(4) * std::uint8_t(not half_)));
		it &= std::uint8_t(0x0f);
		
		if (it < 10)
		{
			return char(it) + '0';
		}
		else
		{
			return char(it) + 'A' - char(10);
		}
	}
	
	Encoder& operator++() noexcept
	{
		it_ += half_;
		half_ = not half_;
		return *this;
	}
	
	Encoder operator++(int) noexcept
	{
		return operators::post_increment(*this);
	}
	
private:
	[[no_unique_address]] bool half_;
	[[no_unique_address]] Iterator_type it_;
};

template<typename Iterator_type, typename End_type>
struct Decoder
{
	using difference_type = std::ptrdiff_t;
	using value_type = std::byte;
	
	Decoder() = default;
	
	explicit Decoder(auto&& it, auto&& end) noexcept
		:
		it_(std::forward<decltype(it)>(it)),
		end_(std::forward<decltype(end)>(end))
	{
	}
	
	bool operator==(std::default_sentinel_t) const noexcept
	{
		return it_ == end_;
	}
	
	value_type operator*()
	{
		char chars[2] {'0', '0'};
		
		chars[0] = *it_;
		++it_;
		
		if (it_ != end_)
		{
			chars[1] = *it_;
			++it_;
		}
		
		for (auto& c : chars)
		{
			if ('0' <= c and c <= '9')
			{
				c -= '0';
			}
			else if ('A' <= c and c <= 'F')
			{
				c += 10;
				c -= 'A';
			}
			else
			{
				throw std::domain_error(std::string() + "Character '" + c + "' is not a valid hexadecimal literal");
			}
		}
		
		auto result = std::byte();
		result |= std::byte(chars[0]);
		result <<= 4;
		result |= std::byte(chars[1]);
		
		return result;
	}
	
	Decoder& operator++() noexcept
	{
		return *this;
	}
	
	Decoder operator++(int) noexcept
	{
		return operators::post_increment(*this);
	}
	
private:
	[[no_unique_address]] Iterator_type it_;
	[[no_unique_address]] End_type end_;
};

inline auto encode_range(auto&& bytes) noexcept
-> std::ranges::subrange<Encoder<decltype(std::ranges::begin(bytes))>, decltype(std::ranges::end(bytes))>
{
	return std::ranges::subrange(Encoder<decltype(std::ranges::begin(bytes))>(std::ranges::begin(bytes)), std::ranges::end(bytes));
}

inline std::string_view encode(std::span<const std::byte> bytes, char* result) noexcept
{
	auto length = std::size_t(0);
	
	for (char c : encode_range(bytes))
	{
		result[length] = c;
		++length;
	}
	
	return std::string_view(result, length);
}

template<std::ptrdiff_t Size>
inline std::array<char, Size * 2> encode(const std::byte* bytes) noexcept
{
	std::array<char, Size * 2> result;
	encode(std::span(bytes, Size), result.data());
	return result;
}

inline auto decode_range(auto&& text) noexcept
-> std::ranges::subrange<Decoder<decltype(std::ranges::begin(text)), decltype(std::ranges::end(text))>, std::default_sentinel_t>
{
	return std::ranges::subrange(
		Decoder<decltype(std::ranges::begin(text)),
		decltype(std::ranges::end(text))>(std::ranges::begin(text), std::ranges::end(text)), std::default_sentinel
	);
}

inline std::span<std::byte> decode(std::string_view string, auto result_begin) noexcept
{
	auto result_end = result_begin;
	
	for (std::byte b : decode_range(string))
	{
		*result_end = b;
		++result_end;
	}
	
	return std::span(result_begin, result_end);
}

template<std::ptrdiff_t Size>
inline std::array<std::byte, Size / 2> decode(const char* string) noexcept
{
	std::array<std::byte, Size / 2> result;
	decode(std::string_view(string, Size), result.data());
	return result;
}
} // namespace nwd::utility::hex
