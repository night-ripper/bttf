#pragma once

#include <cstddef>
#include <cstdint>

#include <memory>
#include <memory_resource>
#include <span>

namespace nwd::utility
{
template<typename Value_type>
struct Shared_resources
{
	using value_type = Value_type;
	
	const value_type* data() const& noexcept
	{
		return objects_.get();
	}
	
	std::span<const value_type> span() const& noexcept
	{
		return std::span(data(), size_);
	}
	
	std::ptrdiff_t size() const& noexcept
	{
		return size_;
	}
	
	const value_type& operator[](std::ptrdiff_t index) const& noexcept
	{
		return data()[index];
	}
	
	Shared_resources() = default;
	
protected:
	template<typename Object_deleter>
	struct Deleter
	{
		std::ptrdiff_t size_;
		Object_deleter deleter_;
		
		Deleter(std::ptrdiff_t size, Object_deleter deleter)
			:
			size_(size),
			deleter_(std::move(deleter))
		{
		}
		
		void operator()(value_type* data)
		{
			deleter_(data, this->size_);
			delete[] data;
		}
	};
	
	/// <!-- TODO: use std::make_shared<unsigned char[]> when supported, NOTE: consider alignment -->
	template<typename Object_deleter>
	Shared_resources(std::ptrdiff_t size, Object_deleter deleter)
		:
		size_(size),
		objects_(new value_type[size], Deleter<Object_deleter>(size, std::move(deleter)))
	{
	}
	
protected:
	std::ptrdiff_t size_ = 0;
	std::shared_ptr<value_type[]> objects_;
};
} // namespace nwd::utility
