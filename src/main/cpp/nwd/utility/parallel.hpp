#pragma once

#include <iterator>
#include <functional>
#include <utility>
#include <thread>
#include <mutex>
#include <future>
#include <ranges>
#include <type_traits>
#include <latch>
#include <semaphore>

#include <nwd/containers/vector_queue.hpp>

namespace nwd::utility::parallel
{
struct Thread_pool
{
	using task_type = std::move_only_function<void()>;
	
	~Thread_pool()
	{
		for (auto& thread : threads_)
		{
			thread.request_stop();
		}
		
		semaphore_.release(std::ssize(threads_));
	}
	
	Thread_pool()
		:
		semaphore_(0)
	{
		tasks_.reserve(64);
		
		for (auto& thread : threads_)
		{
			thread = std::jthread(&loop, std::ref(*this));
		}
	}
	
public:
	void submit_task(task_type task)
	{
		{
			auto lg = std::lock_guard(mutex_);
			tasks_.emplace_back(std::move(task));
		}
		
		semaphore_.release();
	}
	
	template<typename Type>
	std::future<Type> submit(std::packaged_task<Type()> task)
	{
		auto result = task.get_future();
		submit_task(task_type([task = std::move(task)]() mutable noexcept -> void {task();}));
		return result;
	}
	
	template<typename Function>
	std::future<std::invoke_result_t<Function>> submit(Function function)
	{
		return submit(std::packaged_task(std::move(function)));
	}
	
	static auto dereference(auto it) noexcept
	{
		if constexpr (std::is_reference_v<decltype(*it)>)
		{
			return std::ref(*it);
		}
		else
		{
			struct Value
			{
				decltype(*it) value_;
				std::add_rvalue_reference_t<decltype(*it)> get() noexcept {return std::move(value_);}
			};
			
			return Value(*it);
		}
	}
	
	template<std::ranges::input_range Range, typename Function>
	void for_each(Range&& range, Function function)
	{
		using Function_ref = std::add_lvalue_reference_t<std::unwrap_reference_t<Function>>;
		
		if constexpr (std::ranges::sized_range<Range> and not std::ranges::disable_sized_range<Range>)
		{
			struct
			{
				std::latch latch_;
				Function_ref function_;
			}
			context {.latch_ = std::latch(std::ranges::ssize(range)), .function_ = function};
			
			for (auto it = std::ranges::begin(range); it != std::ranges::end(range); ++it)
			{
				submit_task([&context, value = dereference(it)]() mutable noexcept -> void
				{
					std::atomic_thread_fence(std::memory_order::acquire);
					context.function_(value.get());
					context.latch_.count_down();
				});
			}
			
			context.latch_.wait();
		}
		else
		{
			struct
			{
				std::atomic_flag finished_;
				std::atomic<std::size_t> task_count_;
				Function_ref function_;
			}
			context {.finished_ = 0, .task_count_ = 1, .function_ = function};
			
			for (auto it = std::ranges::begin(range); it != std::ranges::end(range); ++it)
			{
				context.task_count_.fetch_add(1, std::memory_order::relaxed);
				
				submit_task([&context, value = dereference(it)]() mutable noexcept -> void
				{
					std::atomic_thread_fence(std::memory_order::acquire);
					context.function_(value.get());
					
					if (context.task_count_.fetch_sub(1, std::memory_order::acq_rel) == 1)
					{
						context.finished_.test_and_set(std::memory_order::release);
						context.finished_.notify_one();
					}
				});
			}
			
			if (context.task_count_.fetch_sub(1, std::memory_order::acq_rel) != 1)
			{
				context.finished_.wait(0, std::memory_order::acquire);
			}
		}
	}
	
protected:
	static void loop(std::stop_token stop_token, Thread_pool& self)
	{
		while (self.semaphore_.acquire(), not stop_token.stop_requested())
		{
			auto task = task_type();
			
			{
				auto lg = std::lock_guard(self.mutex_);
				task = std::move(self.tasks_.front());
				self.tasks_.pop_front();
			}
			
			task();
		}
	}
	
private:
	std::mutex mutex_;
	std::counting_semaphore<std::numeric_limits<std::int32_t>::max()> semaphore_;
	std::vector<std::jthread> threads_ = std::vector<std::jthread>(std::max(1u, std::thread::hardware_concurrency()));
	containers::Vector_queue<task_type, std::int32_t> tasks_;
}
inline static thread_pool;

inline void for_each(std::ranges::input_range auto&& range, auto function)
{
	thread_pool.for_each(range, std::ref(function));
}
} // namespace nwd::utility::parallel
