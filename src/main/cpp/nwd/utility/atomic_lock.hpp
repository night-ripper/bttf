#pragma once

#include <cstdint>

#include <atomic>
#include <limits>
#include <thread>

namespace nwd::utility
{
struct Atomic_unique_lock
{
	bool try_lock() noexcept
	{
		if (not lock_.test_and_set(std::memory_order::relaxed))
		{
			lock_.test_and_set(std::memory_order::acquire);
			return true;
		}
		
		return false;
	}
	
protected:
	std::atomic_flag lock_;
};

struct Atomic_unique_spinlock : Atomic_unique_lock
{
	void lock() noexcept
	{
		while (not try_lock())
		{
			std::this_thread::sleep_for(std::chrono::microseconds(1));
		}
	}
	
	void unlock() noexcept
	{
		lock_.clear(std::memory_order::release);
	}
};

template<typename Type>
struct Atomic_shared_lock_base
{
	Atomic_shared_lock_base() noexcept
	{
		value_.store({}, std::memory_order::release);
	}
	
	bool try_lock() noexcept
	{
		while (true)
		{
			auto expected = value_.load(std::memory_order::relaxed);
			
			if (expected.writer_lock_ or expected.reader_lock_)
			{
				return false;
			}
			else if (value_.compare_exchange_weak(expected, {true, false, 0},
				std::memory_order::acquire, std::memory_order::relaxed))
			{
				return true;
			}
		}
	}
	
	void lock() noexcept
	{
		while (not try_lock())
		{
			std::this_thread::sleep_for(std::chrono::microseconds(1));
		}
	}
	
	void unlock() noexcept
	{
		while (true)
		{
			auto expected = value_.load(std::memory_order::relaxed);
			
			if (value_.compare_exchange_weak(expected,
				{false, expected.reader_lock_, expected.readers_},
				std::memory_order::release, std::memory_order::relaxed))
			{
				return;
			}
		}
	}
	
	bool try_lock_shared() noexcept
	{
		while (true)
		{
			auto expected = value_.load(std::memory_order::relaxed);
			const auto desired = Value {false, true, expected.readers_ + 1};
			
			if (expected.writer_lock_)
			{
				return false;
			}
			else if (expected.reader_lock_)
			{
				if (value_.compare_exchange_weak(expected, desired,
					std::memory_order::acquire, std::memory_order::relaxed))
				{
					return true;
				}
			}
			else
			{
				if (value_.compare_exchange_weak(expected, desired,
					std::memory_order::acquire, std::memory_order::relaxed))
				{
					return true;
				}
			}
		}
	}
	
	void lock_shared() noexcept
	{
		while (not try_lock_shared())
		{
			std::this_thread::sleep_for(std::chrono::microseconds(1));
		}
	}
	
	void unlock_shared() noexcept
	{
		while (true)
		{
			auto expected = value_.load(std::memory_order::relaxed);
			
			if (expected.readers_ == 1)
			{
				if (value_.compare_exchange_weak(expected, {false, false, 0},
					std::memory_order::relaxed, std::memory_order::relaxed))
				{
					return;
				}
			}
			else
			{
				if (value_.compare_exchange_weak(expected, {false, true, expected.readers_ - 1},
					std::memory_order::relaxed, std::memory_order::relaxed))
				{
					return;
				}
			}
		}
	}
	
private:
	struct Value
	{
		Type writer_lock_ : 1 = 0;
		Type reader_lock_ : 1 = 0;
		Type readers_ : (std::numeric_limits<unsigned char>::digits * sizeof(Type) - 2) = 0;
	};
	
private:
	std::atomic<Value> value_;
};

using Atomic_shared_lock_16 = Atomic_shared_lock_base<std::int16_t>;
using Atomic_shared_lock_32 = Atomic_shared_lock_base<std::int32_t>;
using Atomic_shared_lock_64 = Atomic_shared_lock_base<std::int64_t>;
} // namespace nwd::utility
