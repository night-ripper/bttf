#pragma once

#include <cstddef>

#include <array>
#include <memory>

namespace nwd::utility
{
template<typename Value_type, std::ptrdiff_t Array_size = 1>
requires(Array_size > 0)
struct Aligned_storage
{
	using value_type = Value_type;
	
	value_type* data() noexcept
	{
		return std::assume_aligned<alignof(value_type)>(reinterpret_cast<value_type*>(&data_[0]));
	}
	
	const value_type* data() const noexcept
	{
		return std::assume_aligned<alignof(value_type)>(reinterpret_cast<const value_type*>(&data_[0]));
	}
	
	value_type& operator[](std::ptrdiff_t index) noexcept requires(Array_size > 1) {return *(data() + index);}
	const value_type& operator[](std::ptrdiff_t index) const noexcept requires(Array_size > 1) {return *(data() + index);}
	
private:
	// For assertions
	alignas(value_type) std::array<std::byte, sizeof(value_type) * Array_size> data_;
};

template<typename Target_type, typename Source_type>
Target_type* reinterpret_aligned_cast(Source_type* value) noexcept
{
	return std::assume_aligned<alignof(Target_type)>(reinterpret_cast<Target_type*>(value));
}
} // namespace nwd::utility
