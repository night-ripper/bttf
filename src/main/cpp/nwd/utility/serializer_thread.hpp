#pragma once

#include <cstdint>

#include <chrono>
#include <exception>
#include <functional>
#include <mutex>
#include <thread>

#include <nwd/utility/invoker.hpp>

namespace nwd::utility
{
struct Serializer_thread
{
	~Serializer_thread()
	{
		jthread_.request_stop();
		execution_semaphore_.fetch_add(1, std::memory_order::acq_rel);
		execution_semaphore_.notify_one();
	}
	
	Serializer_thread() noexcept
	{
		jthread_ = std::jthread([this](std::stop_token stop_token) noexcept -> void
		{
			while (execution_semaphore_.wait(0, std::memory_order::acquire), not stop_token.stop_requested())
			{
				execution_semaphore_.fetch_sub(1, std::memory_order::acquire);
				
				try
				{
					function_();
				}
				catch (...)
				{
					exception_ = std::current_exception();
				}
				
				function_ = {};
				result_semaphore_.clear(std::memory_order::release);
				result_semaphore_.notify_one();
			}
		});
	}
	
	template<typename Function>
	void operator()(Function&& function) noexcept(noexcept(function()))
	requires(std::invocable<Function>)
	{
		auto lg = std::lock_guard(caller_lock_);
		function_ = std::ref(function);
		result_semaphore_.test_and_set(std::memory_order::acquire);
		execution_semaphore_.fetch_add(1, std::memory_order::acq_rel);
		execution_semaphore_.notify_one();
		result_semaphore_.wait(1, std::memory_order::acquire);
		
		if (not noexcept(function()) and exception_)
		{
			std::rethrow_exception(std::move(exception_));
		}
	}
	
private:
	std::jthread jthread_;
	std::move_only_function<void()> function_;
	std::exception_ptr exception_;
	std::mutex caller_lock_;
	std::atomic<std::int32_t> execution_semaphore_;
	std::atomic_flag result_semaphore_;
};
} // namespace nwd::utility
