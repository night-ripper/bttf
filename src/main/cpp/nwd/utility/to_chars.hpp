#pragma once

#include <cstddef>
#include <cstdint>

#include <algorithm>
#include <charconv>
#include <span>
#include <string_view>
#include <type_traits>

namespace nwd::utility
{
struct To_chars_view
{
	To_chars_view() = default;
	
	To_chars_view(std::span<char> text)
		:
		begin_(text.data()),
		capacity_(static_cast<std::int32_t>(text.size())),
		pos_(0)
	{
	}
	
	To_chars_view& operator<<(std::string_view text) noexcept
	{
		auto length = std::min(static_cast<std::int32_t>(std::ssize(text)), capacity_ - pos_);
		std::copy_n(text.begin(), length, begin_ + pos_);
		pos_ += length;
		return *this;
	}
	
	template<typename Type>
	requires(std::is_arithmetic_v<Type>)
	To_chars_view& operator<<(Type value) noexcept
	{
		pos_ = static_cast<std::int32_t>(std::to_chars(begin_ + pos_, begin_ + capacity_, value).ptr - begin_);
		return *this;
	}
	
	void reset() noexcept
	{
		pos_ = 0;
	}
	
	char* begin() noexcept {return begin_;}
	const char* begin() const noexcept {return begin_;}
	
	char* end() noexcept {return begin_ + pos_;}
	const char* end() const noexcept {return begin_ + pos_;}
	
	std::ptrdiff_t size() const noexcept
	{
		return pos_;
	}
	
	std::string_view view() const noexcept
	{
		return std::string_view(begin_, begin_ + pos_);
	}
	
	operator std::string_view() const noexcept
	{
		return view();
	}
	
private:
	char* begin_ = nullptr;
	std::int32_t capacity_;
	std::int32_t pos_;
};

template<std::ptrdiff_t Size>
struct To_chars_buffer : To_chars_view
{
	To_chars_buffer() noexcept
		:
		To_chars_view(buffer_)
	{
	}
	
private:
	char buffer_[Size];
};
} // namespace nwd::utility
