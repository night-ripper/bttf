#include "rendering/context.hpp"
#include "rendering/textures.hpp"
#include "rendering/png.hpp"
#include "rendering/freetype.hpp"
#include "rendering-component.hpp"
#include "audio-component.hpp"

#include "world.hpp"
#include "world-objects.hpp"

#include "rendering-coordinates.hpp"

#include "resources.hpp"

#include "audio/wav.hpp"
#include "audio/vorbis.hpp"
#include "audio/openal.hpp"

#include "utility/normalize.hpp"

#include <iostream>
#include <fstream>
#include <memory>

#include <random>
#include <experimental/random>
#include <vector>
#include <locale>
#include <set>
#include <algorithm>
#include <memory_resource>

#include <chrono>
#include <random>
#include <thread>
#include <filesystem>

#include <bit>

#include <epoxy/gl.h>

using namespace geometry;
using namespace rendering;

static void make_wall(bttf::World& map, geometry::Vector3D begin, geometry::Vector3D end)
{
	auto& wall = map.attach(std::allocate_shared<bttf::Wall>(
		std::pmr::polymorphic_allocator(&bttf::memory_resource()),
		begin, end,
		bttf::Floor::index_type(bttf::environment_binding,
			std::experimental::randint<short>(bttf::walls_first, bttf::walls_last)
		)
	));
	map.attach_collision(wall, wall.handle_, geometry::Bounding_box3D(wall));
	map.attach(static_cast<bttf::World::Rendering_object&>(wall), geometry::Bounding_box3D(wall));
}

static void make_wall_line(bttf::World& map, geometry::Vector3D begin, geometry::Vector3D end)
{
	auto dist = distance(begin, end);
	
	geometry::Vector3D step = (end - begin) / dist;
	
	for (int tiles = 0; tiles < std::ceil(dist); ++tiles)
	{
		make_wall(map, begin, begin + step);
		begin += step;
	}
}

static void make_floor(bttf::World& map, geometry::Vector3D begin)
{
	auto& floor = map.attach(std::allocate_shared<bttf::Floor>(
		std::pmr::polymorphic_allocator(&bttf::memory_resource()),
		begin,
		bttf::Floor::index_type(bttf::environment_binding,
			std::experimental::randint<short>(bttf::floors_first, bttf::floors_last)
		)
	));
	map.attach(static_cast<bttf::World::Rendering_object&>(floor), geometry::Bounding_box3D(floor));
}

static bttf::Crate& make_random_container(bttf::World& map, geometry::Vector3D position)
{
	std::random_device rd;
	std::mt19937 gen(rd());
	
	bttf::Crate::index_type indices[]
	{
		bttf::Crate::index_type(bttf::objects_binding, bttf::crate_index),
		bttf::Crate::index_type(bttf::objects_binding, bttf::barrel_index),
		bttf::Crate::index_type(bttf::objects_binding, bttf::chest_index),
	};
	
	bttf::Wavefront_obj_vertex_data* objects[]
	{
		bttf::crate_obj, bttf::barrel_obj, bttf::chest_obj,
	};
	
	auto index = std::uniform_int_distribution<int>(0, 2)(gen);
	auto angle = std::uniform_real_distribution<float>(0, angle_t::max())(gen);
	
	auto& p = map.attach(std::allocate_shared<bttf::Crate>(std::pmr::polymorphic_allocator(&bttf::memory_resource()),
		position, 0.6, angle, indices[index], objects[index]
	));
	map.attach(static_cast<bttf::World::Rendering_object&>(p), geometry::Bounding_box3D(p.sphere_));
	map.attach_collision(p, p.handle_, geometry::Bounding_box3D(p.sphere_));
	
	return p;
}

int main(int, const char**)
{
	/// Need to set locale before GLFW init for proper character encoding
	// TODO compiling for Trash Bindows® fails
	try
	{
		std::locale::global(std::locale(""));
	}
	catch (...)
	{
	}
	
	opengl::Glfw_library glfw_lib;
	opengl::Context context(960, 540, "𝙁𝙖𝙡𝙡𝙤𝙪𝙩 76", {opengl::Context::Hint::debug_profile});
	context.disable_cursor();
	
	{
		auto [x, y] = context.framebuffer_size();
		auto [xw, yw] = context.window_position();
		auto [xc, yc] = context.cursor_position();
		
		std::cout << x << " : " << y << "\n";
		std::cout << xw << " : " << yw << "\n";
		std::cout << xc << " : " << yc << "\n";
		
		context.cursor_position(x / 2, y / 2);
		
		for (int i = 0; i < 10; ++i)
		{
			context.poll_events();
			context.swap_buffers();
		}
		
		std::tie(xc, yc) = context.cursor_position();
		
		std::cout << xc << " : " << yc << "\n";
	}
	
	bttf::Rendering_component rcomp;
	bttf::Audio_component acomp;
	
	auto textures = bttf::load_textures_environment();
	textures.parameter(opengl::Texture::Wrapping::repeat_mirror);
	textures.parameter(opengl::Texture::Minifying::nearest);
	textures.parameter(opengl::Texture::Magnifying::nearest);
	
	auto cr_textures = bttf::load_textures_creatures();
	cr_textures.parameter(opengl::Texture::Wrapping::repeat_mirror);
	cr_textures.parameter(opengl::Texture::Minifying::nearest);
	cr_textures.parameter(opengl::Texture::Magnifying::nearest);
	
	auto anime_textures = bttf::load_textures_anime();
	anime_textures.parameter(opengl::Texture::Wrapping::repeat_mirror);
	anime_textures.parameter(opengl::Texture::Minifying::linear);
	anime_textures.parameter(opengl::Texture::Magnifying::linear);
	
	auto translucent_textures = bttf::load_textures_translucent();
	translucent_textures.parameter(opengl::Texture::Wrapping::repeat_mirror);
	translucent_textures.parameter(opengl::Texture::Minifying::linear);
	translucent_textures.parameter(opengl::Texture::Magnifying::linear);
	
	auto objects_textures = bttf::load_textures_objects();
	objects_textures.parameter(opengl::Texture::Wrapping::repeat_mirror);
	objects_textures.parameter(opengl::Texture::Minifying::nearest);
	objects_textures.parameter(opengl::Texture::Magnifying::nearest);
	
	auto objs = bttf::load_objs();
	
	auto sounds = bttf::load_sounds();
	
	bttf::World map;
	
	{
		for (int i = 0; i < 10; ++i)
		{
			map.attach(std::allocate_shared<bttf::Smonk_generator>(std::pmr::polymorphic_allocator(&bttf::memory_resource()),
				Cylinder3D::bottom_up({i * 2 + 0.6, 0, 0}, 3, 1), 8 * (i + 1)
			));
		}
		
		map.attach(std::allocate_shared<bttf::Smonk_generator>(std::pmr::polymorphic_allocator(&bttf::memory_resource()),
			Cylinder3D::bottom_up({37, 2.5, 0}, 3, 1), 0
		));
		map.attach(std::allocate_shared<bttf::Smonk_generator>(std::pmr::polymorphic_allocator(&bttf::memory_resource()),
			Cylinder3D::bottom_up({37, -2.5, 0}, 3, 1), 0
		));
	}
	
	for (int height = 0; height < 3; ++height)
	{
		{
			geometry::Vector3D corners[] {
				{0, -1, height},
				{0, 1, height},
				{8, 1, height},
				{8, 5, height},
				{13, 5, height},
				{13, 2, height},
				{9, 2, height},
				{9, 1, height},
				{20, 1, height},
			};
			
			for (std::ptrdiff_t i = 0; i < std::ssize(corners) - 1; ++i)
			{
				make_wall_line(map, corners[i], corners[i + 1]);
			}
		}
		{
			geometry::Vector3D corners[] {
				{20, -1, height},
				{14, -1, height},
				{14, -2, height},
				{17, -2, height},
				{17, -6, height},
				{10, -6, height},
				{10, -2, height},
				{13, -2, height},
				{13, -1, height},
				{0, -1, height},
			};
			
			for (std::ptrdiff_t i = 0; i < std::ssize(corners) - 1; ++i)
			{
				make_wall_line(map, corners[i], corners[i + 1]);
			}
		}
		{
			geometry::Vector3D corners[] {
				{20, 1, height},
				{20, 6, height},
				{40, 6, height},
				{40, -6, height},
				{20, -6, height},
				{20, -1, height},
			};
			
			for (std::ptrdiff_t i = 0; i < std::ssize(corners) - 1; ++i)
			{
				make_wall_line(map, corners[i], corners[i + 1]);
			}
		}
	}
	
	for (coord_t x = 8; x < 13; x += 1)
	{
		for (coord_t y = 3; y < 6; y += 1)
		{
			make_floor(map, Vector3D(x, y, 0));
		}
	}
	
	make_floor(map, Vector3D(8, 2, 0));
	
	for (coord_t x = 10; x < 17; x += 1)
	{
		for (coord_t y = -5; y < -1; y += 1)
		{
			make_floor(map, Vector3D(x, y, 0));
		}
	}
	
	make_floor(map, Vector3D(13, -1, 0));
	
	for (int i = 20; i < 40; ++i)
	{
		for (int j = -5; j < 7; ++j)
		{
			make_floor(map, Vector3D(i, j, 0));
		}
	}
	
	for (int i = 0; i < 20; ++i)
	{
		for (int j = 0; j < 2; ++j)
		{
			make_floor(map, Vector3D(i, j, 0));
		}
	}
	
	{
		auto& p = map.attach(std::allocate_shared<bttf::Light>(std::pmr::polymorphic_allocator(&bttf::memory_resource()),
			Sphere3D({25, -2, 3}, 50), 1,.2,.3
		));
		map.attach(static_cast<bttf::World::Rendering_object&>(p), geometry::Bounding_box3D(p.area_));
		
		p.set_time_handler([](auto* self, float seconds) -> void
		{
			self->angle_ += seconds;
			self->multiplier_ = (std::sin(self->angle_) + 1) / 2;
		});
	}
	{
		auto& p = map.attach(std::allocate_shared<bttf::Light>(std::pmr::polymorphic_allocator(&bttf::memory_resource()),
			Sphere3D({30, 6, 5}, 50), .2,.2,1
		));
		map.attach(static_cast<bttf::World::Rendering_object&>(p), geometry::Bounding_box3D(p.area_));
		p.cos_inner_cutoff_ = std::cos(10.f / 180 * pi);
		p.cos_outer_cutoff_ = std::cos(15.f / 180 * pi);
		
		Vector3D v(p.area_.center(), {30, 0, 0});
		v = normalize(v);
		glsl::vec3 towards_aloe(v.x(), v.y(), v.z());
		
		p.direction_ = towards_aloe;
		p.original_direction_ = p.direction_;
		p.multiplier_ = 2;
		p.set_time_handler([](auto* self, float seconds) -> void
		{
			self->angle_ += seconds * 2;
			
			auto q = Quaternion::from_unit_axis_angle(0, 0, -1, self->angle_);
			
			auto v = rotate(self->original_direction_, q);
			self->r_ = v.x;
			self->g_ = v.y;
			self->b_ = v.z;
		});
	}
	{
		auto& p = map.attach(std::allocate_shared<bttf::Light>(std::pmr::polymorphic_allocator(&bttf::memory_resource()),
			Sphere3D({35, 4, 3}, 50), .1,1,1
		));
		map.attach(static_cast<bttf::World::Rendering_object&>(p), geometry::Bounding_box3D(p.area_));
		p.cos_inner_cutoff_ = std::cos(20.f / 180 * pi);
		p.cos_outer_cutoff_ = std::cos(60.f / 180 * pi);
		p.original_direction_ = normalize(glsl::vec3(1, -1, -1));
		p.set_time_handler([](auto* self, float seconds) -> void
		{
			self->angle_ += seconds * 3;
			
			auto q = Quaternion::from_unit_axis_angle(0, 0, -1, self->angle_);
			
			self->direction_ = rotate(self->original_direction_, q);
		});
	}
	{
		auto& p = map.attach(std::allocate_shared<bttf::Light>(std::pmr::polymorphic_allocator(&bttf::memory_resource()),
			Sphere3D({37, -5, 3}, 50), 1,.1,1
		));
		map.attach(static_cast<bttf::World::Rendering_object&>(p), geometry::Bounding_box3D(p.area_));
		p.cos_inner_cutoff_ = std::cos(40.f / 180 * pi);
		p.cos_outer_cutoff_ = std::cos(60.f / 180 * pi);
		p.original_direction_ = glsl::vec3(0, 0, -1);
		p.set_time_handler([](auto* self, float seconds) -> void
		{
			self->angle_ += seconds * 9;
			
			auto q = Quaternion::from_unit_axis_angle(1, 0, 0, self->angle_);
			
			self->direction_ = rotate(self->original_direction_, q);
		});
	}
	
	{
		auto& p = map.attach(std::allocate_shared<bttf::Light>(std::pmr::polymorphic_allocator(&bttf::memory_resource()),
			Sphere3D({3, 0, 3}, 25), 0, 1, 0
		));
		map.attach(static_cast<bttf::World::Rendering_object&>(p), geometry::Bounding_box3D(p.area_));
	}
	{
		auto& p = map.attach(std::allocate_shared<bttf::Aloe>(std::pmr::polymorphic_allocator(&bttf::memory_resource()),
			Vector3D(30, 0, 0)
		));
		map.attach(static_cast<bttf::World::Rendering_object&>(p), geometry::Bounding_box3D(p));
	}
	
	{
		auto& p = map.attach(std::allocate_shared<bttf::Creature>(std::pmr::polymorphic_allocator(&bttf::memory_resource()),
			// Cylinder3D(Vector3D(1,0,.8), .8, .25), 0.4, 0, 16. / 12,
			Cylinder3D(Vector3D(27,0,.8), .8, .25), 0.4, 0, 16. / 12,
			geometry::angle_t(0.), bttf::Creature::index_type(bttf::creatures_binding, bttf::corey_first)
		));
		map.attach_collision(p, p.handle_, geometry::Bounding_box3D(p.cylinder_));
		map.attach(static_cast<bttf::World::Rendering_object&>(p), geometry::Bounding_box3D(p.cylinder_));
		p.control(context, rcomp);
		map.attach(p.light_, geometry::Bounding_box3D(p.light_.area_));
		map.attach(p.flashlight_, geometry::Bounding_box3D(p.flashlight_.area_));
	}
	{
		auto& amb = map.attach(std::allocate_shared<bttf::Audio_object>(
			std::pmr::polymorphic_allocator(&bttf::memory_resource()), bttf::ambient_videodrome)
		);
		amb.source_.loop(true);
		amb.source_.play();
	}
	
	{
		geometry::Vector3D positions[] {
			{12.4, 4.3, 0},
			{12, 2.5, 0},
			{11.5, 3.2, 0},
			{10.9, 4.2, 0},
			{10, 2.5, 0},
			{9.7, 4.4, 0},
			{8.7, 4.3, 0},
			
			{16.5, -3, 0},
			{16.2, -5, 0},
			{15.7, -4.3, 0},
			{15, -2.9, 0},
			{14.2, -4.1, 0},
			{14.6, -5, 0},
			{14, -3, 0},
			{13.7, -5.3, 0},
			{12.4, -4.6, 0},
			{12, -3, 0},
			{11.5, -5.2, 0},
			{10.9, -4.2, 0},
			
			{10, 0, 0},
		};
		
		std::vector<bttf::Crate*> crates(std::ssize(positions));
		
		for (std::ptrdiff_t i = 0; i < std::ssize(positions); ++i)
		{
			crates[i] = &make_random_container(map, positions[i]);
		}
		
		auto key_crate = std::experimental::randint<int>(0, std::ssize(positions) - 1);
		
		crates[key_crate]->key_ = std::allocate_shared<bttf::Key>(
			std::pmr::polymorphic_allocator(&bttf::memory_resource()),
			crates[key_crate]->sphere_.center()
		);
		
		auto& door = map.attach(std::allocate_shared<bttf::Door>(
			std::pmr::polymorphic_allocator(&bttf::memory_resource()),
			Vector3D(20, 1, 0), Vector3D(20, -1, 0)
		));
		map.attach_collision(door, door.handle_, geometry::Bounding_box3D(door));
		map.attach(static_cast<bttf::World::Rendering_object&>(door), geometry::Bounding_box3D(door));
		door.unlocked_by_ = crates[key_crate]->key_.get();
	}
	
	{
		make_random_container(map, Vector3D(30, 4.5, 0));
		make_random_container(map, Vector3D(31, -3, 0));
		make_random_container(map, Vector3D(25, 3, 0));
		make_random_container(map, Vector3D(27, -4, 0));
		make_random_container(map, Vector3D(23, 0, 0));
	}
	
	auto now = std::chrono::steady_clock::now();
	
	while (not context.should_close())
	{
		context.poll_events();
		
		auto new_now = std::chrono::steady_clock::now();
		const auto seconds_passed = std::chrono::duration<float>(new_now - now).count();
		map.time_pass(seconds_passed);
		now = new_now;
		
		map.draw(rcomp);
		
		// std::cout << seconds_passed << "\n";
		
		context.swap_buffers();
	};
}
