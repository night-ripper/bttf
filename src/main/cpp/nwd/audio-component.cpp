#include "audio-component.hpp"

namespace bttf
{
static Audio_component* current_comp = nullptr;

Audio_component* Audio_component::current()
{
	return current_comp;
}

Audio_component::~Audio_component()
{
	current_comp = nullptr;
}

Audio_component::Audio_component()
	:
	context_ {device_}
{
	current_comp = this;
	context_();
	context_.listener.orientation({1, 0, 0}, {0, 0, 1});
}
} // namespace bttf

