#pragma once

#include <cstddef>

namespace nwd::platform
{
std::ptrdiff_t page_size() noexcept;
} // namespace nwd::platform
