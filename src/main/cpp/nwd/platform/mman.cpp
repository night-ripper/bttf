#include <nwd/platform/mman.hpp>
#include <unistd.h>

namespace nwd::platform
{
static const std::ptrdiff_t page_size_value = getpagesize();

std::ptrdiff_t page_size() noexcept
{
	return page_size_value;
}
} // namespace nwd::platform
