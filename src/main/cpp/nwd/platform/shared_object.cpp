#include <cstring>

#include <iostream>
#include <memory>
#include <stdexcept>

#include <dlfcn.h>

#include <nwd/utility/functor.hpp>
#include <nwd/platform/shared_object.hpp>

namespace nwd::platform
{
Shared_object::Shared_object(const char* filepath)
{
	void* library = dlopen(filepath, RTLD_NOW);
	
	if (library == nullptr)
	{
		const char* error = dlerror();
		throw std::runtime_error(std::string("dlopen: Could not load shared object: ") + error);
	}
	
	handle_ = std::shared_ptr<void>(library, utility::Functor<dlclose>());
}

std::shared_ptr<void> Shared_object::private_symbol(const char* name, bool exception) const
{
	auto result = std::shared_ptr<void>();
	
	void* symbol = dlsym(handle_.get(), name);
	
	if (symbol != nullptr)
	{
		result = std::shared_ptr<void>(handle_, symbol);
	}
	else if (exception)
	{
		const char* error = dlerror();
		throw std::runtime_error(std::string("dlsym: An error occured while loading a symbol from library: ") + error);
	}
	
	return result;
}
} // namespace nwd::platform
