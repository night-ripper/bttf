#pragma once

#include <array>
#include <memory>
#include <stdexcept>
#include <ranges>
#include <string_view>

#include <nwd/audio/sound.hpp>

#include <nwd/utility/functor.hpp>
#include <nwd/utility/unique_resource.hpp>
#include <nwd/utility/iterators.hpp>

namespace nwd::audio::openal
{
namespace deleters
{
void device(void* object) noexcept;
void capture_device(void* object) noexcept;
void context(void* object) noexcept;
void buffer(std::uint32_t object) noexcept;
void source(std::uint32_t object) noexcept;
} // namespace deleters

struct Device_name_iterator : utility::Forward_iterator<Device_name_iterator>
{
	using value_type = std::string_view;
	using difference_type = std::ptrdiff_t;
	
	Device_name_iterator() noexcept = default;
	
	Device_name_iterator(const char* names) noexcept
		:
		names_(names)
	{
	}
	
	bool operator==(std::default_sentinel_t) const noexcept;
	value_type operator*() const noexcept;
	Device_name_iterator& operator++() noexcept;
	using Forward_iterator::operator++;
	
	friend bool operator==(Device_name_iterator lhs, Device_name_iterator rhs) noexcept
	{
		return lhs.names_ == rhs.names_;
	}
	
private:
	const char* names_ = nullptr;
};

struct Device : std::unique_ptr<void, utility::Functor<deleters::device>>
{
	friend struct Context;
	
	Device() = default;
	
	//! @param name nullptr to create a default device
	explicit Device(const char* name);
	
	//! OpenAL doesn't have to be initialized
	static std::ranges::subrange<Device_name_iterator, std::default_sentinel_t> names();
	
	std::string_view name() const;
};

struct Capture_device : protected std::unique_ptr<void, utility::Functor<deleters::capture_device>>
{
	friend struct Context;
	
	Capture_device() = default;
	
	Capture_device(const char* name, Sound_info info);
	
	//! OpenAL doesn't have to be initialized
	static std::ranges::subrange<Device_name_iterator, std::default_sentinel_t> names();
	
	void start();
	void stop();
	
	std::ptrdiff_t samples_ready() const;
	
	//! @return Number of samples.
	std::ptrdiff_t read(std::byte* target, std::ptrdiff_t samples) const;
	
	// TODO rethink
	//! @return Number of samples.
	std::ptrdiff_t operator>>(Sound_view sound) const;
};

struct Context : protected std::unique_ptr<void, utility::Functor<deleters::context>>
{
	Context() = default;
	
	explicit Context(Device& device);
	
	void make_current();
	
	struct Listener
	{
		void gain(float value);
		void position(std::array<float, 3> vector);
		void velocity(std::array<float, 3> vector);
		void orientation(std::array<float, 3> vector, std::array<float, 3> up_vector);
	}
	listener;
	
	const char* vendor() const;
	const char* version() const;
	const char* renderer() const;
	const char* extensions() const;
};

struct Buffer : utility::Unique_resource<std::uint32_t, utility::Functor<deleters::buffer>>
{
	Buffer() noexcept = default;
	Buffer(Buffer&&) noexcept = default;
	Buffer& operator=(Buffer&&) noexcept = default;
	
	static Buffer create();
	
	std::int32_t bit_depth() const;
	std::int32_t channels() const;
	std::int32_t frequency() const;
	
protected:
	using utility::Unique_resource<std::uint32_t, utility::Functor<deleters::buffer>>::Unique_resource;
};

Buffer& operator>>(Sound_view sound, Buffer& buffer);
Buffer& operator>>(Streaming_sound& sound, Buffer& buffer);

struct Source : utility::Unique_resource<std::uint32_t, utility::Functor<deleters::source>>
{
	Source() = default;
	Source(Source&&) noexcept = default;
	Source& operator=(Source&&) noexcept = default;
	
	static Source create();
	
	bool stopped() const;
	
	void play();
	void pause();
	void stop();
	void rewind();
	
	void loop(bool);
	
	void gain(float value);
	void reference_distance(float value);
	void position(std::array<float, 3> vector);
	void velocity(std::array<float, 3> vector);
	void orientation(std::array<float, 3> vector, std::array<float, 3> up_vector);
	
protected:
	using utility::Unique_resource<std::uint32_t, utility::Functor<deleters::source>>::Unique_resource;
};

Source& operator>>(const Buffer& buffer, Source& source);
Source& operator>>(std::unique_ptr<Streaming_sound> sound, Source& source);
} // namespace nwd::audio::openal
