#include <cstdint>
#include <cstdio>
#include <cstring>

#include <algorithm>
#include <fstream>
#include <utility>
#include <iostream>

#include <vorbis/vorbisfile.h>

#include <nwd/audio/vorbis.hpp>
#include <nwd/audio/privates/common.hpp>
#include <nwd/utility/integer_cast.hpp>
#include <nwd/utility/fileio.hpp>

namespace nwd::audio::vorbis
{
namespace
{
struct Libvorbis_error : std::runtime_error
{
	using std::runtime_error::runtime_error;
};

struct Basic_vorbis
{
	~Basic_vorbis()
	{
		ov_clear(&vorbis_file_);
	}
	
	Basic_vorbis(std::streambuf& streambuf, ov_callbacks callbacks)
	{
		auto result = ov_open_callbacks(&streambuf, &vorbis_file_, nullptr, 0, callbacks);
		
		if (result != 0)
		{
			throw Libvorbis_error("Input does not appear to be an Ogg bit stream");
		}
	}
	
	Sound_info read_info(std::int16_t bit_depth)
	{
		if (bit_depth != 8 and bit_depth != 16)
		{
			throw Libvorbis_error(
				"Bit depth " + std::to_string(bit_depth) + " not supported, "
				"only valid values are 8 or 16"
			);
		}
		
		auto* info = ov_info(&vorbis_file_, -1);
		
		return Sound_info(Format(utility::checked_cast(info->channels), bit_depth), utility::checked_cast(info->rate));
	}
	
	std::ptrdiff_t read_once(std::int16_t bit_depth, std::byte* target, std::ptrdiff_t num_bytes)
	{
		const int word_size = bit_depth / 8;
		const int is_signed = word_size - 1;
		const int endianness = bool(std::endian::native == std::endian::big);
		
		auto result = ov_read(&vorbis_file_, reinterpret_cast<char*>(target), utility::checked_cast<int>(num_bytes), endianness, word_size, is_signed, nullptr);
		
		switch (result)
		{
		case OV_HOLE:
			throw Libvorbis_error(
				"One of: garbage between pages, loss of sync followed "
				"by recapture, or a corrupt page"
			);
			break;
			
		case OV_EBADLINK:
			throw Libvorbis_error(
				"An invalid stream section was supplied to libvorbisfile, "
				"or the requested link is corrupt"
			);
			break;
			
		case OV_EINVAL:
			throw Libvorbis_error(
				"The initial file headers couldn't be read or are corrupt, "
				"or the initial open call for vorbis file failed"
			);
			break;
			
		default:
			break;
		}
		
		return result;
	}
	
protected:
	OggVorbis_File vorbis_file_;
};

struct Streaming_vorbis : Basic_vorbis, privates::Common_streaming_sound
{
	static std::size_t read_function(void* memory, std::size_t size, std::size_t count, void* user_pointer)
	{
		auto& stream = *static_cast<std::streambuf*>(user_pointer);
		return utility::checked_cast(stream.sgetn(static_cast<char*>(memory), utility::checked_cast(size * count)));
	}
	
	static int seek_function(void* user_pointer, std::int64_t offset, int whence)
	{
		auto& stream = *static_cast<std::streambuf*>(user_pointer);
		return utility::checked_cast(static_cast<std::streamoff>(stream.pubseekoff(offset, std::ios_base::seekdir(whence), std::ios_base::in)));
	}
	
	static long tell_function(void* user_pointer)
	{
		auto& stream = *static_cast<std::streambuf*>(user_pointer);
		return stream.pubseekoff(0, std::ios_base::cur, std::ios_base::in);
	}
	
	constexpr static ov_callbacks callbacks = {&read_function, &seek_function, nullptr, tell_function};
	constexpr static std::ptrdiff_t buffer_size = 2520;
	
	Streaming_vorbis(Format format, std::streambuf& streambuf) noexcept
		:
		Basic_vorbis(streambuf, callbacks),
		Common_streaming_sound(read_info(format.bit_depth())),
		streambuf_(&streambuf),
		data_begin_(streambuf_->pubseekoff(0, std::ios_base::cur, std::ios_base::in)),
		bytes_remaining_(ov_pcm_total(&vorbis_file_, -1) * channels() * format.byte_depth()),
		bytes_total_(bytes_remaining_),
		source_channels_(channels())
	{
		static_cast<Sound_info&>(*this) = Sound_info(format, frequency());
	}
	
	int sync() final override
	{
		return streambuf_->pubsync();
	}
	
	pos_type seekoff(off_type offset, std::ios_base::seekdir direction, std::ios_base::openmode) final override
	{
		if (offset == 0 and direction == std::ios_base::beg)
		{
			// streambuf_->pubseekoff(data_begin_, std::ios_base::beg, std::ios_base::in);
			if (auto result = ov_raw_seek(&vorbis_file_, 0))
			{
				throw Libvorbis_error("ow_raw_seek failed with: " + std::to_string(result));
			}
			
			bytes_remaining_ = bytes_total_;
			setg(nullptr, nullptr, nullptr);
			return 0;
		}
		
		return -1;
	}
	
	std::streamsize showmanyc() final override
	{
		if (bytes_remaining_ == 0)
		{
			return -1;
		}
		
		return bytes_remaining_ / source_channels_ * channels();
	}
	
	int_type underflow() final override
	{
		if (bytes_remaining_ == 0)
		{
			return traits_type::eof();
		}
		
		std::streamsize max_data_length = buffer_size;
		
		if (channels() > source_channels_)
		{
			max_data_length /= channels();
			max_data_length *= source_channels_;
		}
		
		const std::streamsize want_to_read = std::min(bytes_remaining_, max_data_length);
		
		std::byte* const buffer_end = buffer_.data() + buffer_size;
		std::byte* data_begin = buffer_end - max_data_length;
		
		std::streamsize data_length = read_once(bit_depth(), data_begin, want_to_read);
		
		if (bool(data_length == 0) != bool(bytes_remaining_ == 0))
		{
			throw Libvorbis_error("Read " + std::to_string(data_length) + " bytes, while " + std::to_string(bytes_remaining_) + " remain");
		}
		
		bytes_remaining_ -= data_length;
		
		if (source_channels_ == channels())
		{
		}
		else if (std::size_t index = utility::checked_cast(((source_channels_ - 1) * 2) + byte_depth() - 1);
			index < std::size(privates::channels_function_table))
		{
			privates::channels_function_table[index](std::span(data_begin, utility::checked_cast(data_length)), buffer_.data());
			data_begin = buffer_.data();
			data_length = data_length * channels() / source_channels_;
		}
		else
		{
			std::unreachable();
		}
		
		auto result_data = reinterpret_cast<char_type*>(data_begin);
		setg(result_data, result_data, result_data + data_length);
		
		return static_cast<unsigned char>(*result_data);
	}
	
private:
	std::streambuf* streambuf_;
	std::ptrdiff_t data_begin_;
	std::ptrdiff_t bytes_remaining_;
	std::ptrdiff_t bytes_total_;
	std::int16_t source_channels_;
	alignas(16) std::array<std::byte, buffer_size> buffer_;
};

struct Streaming_vorbis_file final : std::filebuf, Streaming_vorbis
{
	Streaming_vorbis_file(Format format, std::filebuf&& filebuf)
		:
		std::filebuf(std::move(filebuf)),
		Streaming_vorbis(format, static_cast<std::filebuf&>(*this))
	{
	}
};
} // namespace

std::unique_ptr<Streaming_sound> create_stream(Format format, std::streambuf& streambuf)
{
	return std::make_unique<Streaming_vorbis>(format, streambuf);
}

std::unique_ptr<Streaming_sound> create_stream(Format format, const std::filesystem::path& path)
{
	return std::make_unique<Streaming_vorbis_file>(format, utility::fileio::read_binary(path));
}
} // namespace nwd::audio::vorbis
