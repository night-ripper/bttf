#pragma once

#include <cstddef>
#include <cstdio>

#include <algorithm>
#include <span>
#include <stdexcept>
#include <streambuf>

#include <nwd/audio/sound.hpp>

#include <nwd/utility/storage.hpp>

namespace nwd::audio
{
struct Unsupported_format_error : std::runtime_error
{
	Unsupported_format_error(std::int16_t bit_depth)
		:
		std::runtime_error("Bit depth " + std::to_string(bit_depth) + " not supported, the only valid values are 8 or 16")
	{
	}
};

namespace privates
{
//! Duplicates channels
template<typename Type>
inline void channels_function_1_2(std::span<const std::byte> source, std::byte* target) noexcept
{
	for (std::size_t i = 0; i != source.size() / sizeof(Type); ++i)
	{
		utility::reinterpret_aligned_cast<Type>(target)[i * 2 + 0]
			= utility::reinterpret_aligned_cast<const Type>(source.data())[i]
		;
		utility::reinterpret_aligned_cast<Type>(target)[i * 2 + 1]
			= utility::reinterpret_aligned_cast<const Type>(source.data())[i]
		;
	}
}

//! Selects the first (left) channel
template<typename Type>
inline void channels_function_2_1(std::span<const std::byte> source, std::byte* target) noexcept
{
	for (std::size_t i = 0; i != source.size() / 2 / sizeof(Type); ++i)
	{
		utility::reinterpret_aligned_cast<Type>(target)[i] = utility::reinterpret_aligned_cast<const Type>(source.data())[i * 2];
	}
}

constexpr void(*channels_function_table[])(std::span<const std::byte> source, std::byte* target) noexcept
{
	&channels_function_1_2<std::uint8_t>,
	&channels_function_1_2<std::int16_t>,
	&channels_function_2_1<std::uint8_t>,
	&channels_function_2_1<std::int16_t>,
};

struct Common_streaming_sound : Streaming_sound
{
	using Streaming_sound::Streaming_sound;
	
	void imbue(const std::locale&) final override
	{
		return;
	}
	
	pos_type seekoff(off_type offset, std::ios_base::seekdir direction, std::ios_base::openmode) override = 0;
	std::streamsize showmanyc() override = 0;
	int_type underflow() override = 0;
	
	std::streamsize xsgetn(char_type* buffer, std::streamsize count) final override
	{
		auto result = std::streamsize(0);
		
		do
		{
			auto data_transferred = std::min(count - result, egptr() - gptr());
			std::copy_n(gptr(), data_transferred, buffer + result);
			result += data_transferred;
			setg(eback(), gptr() + data_transferred, egptr());
		}
		while (result != count and underflow() != traits_type::eof());
		
		return result;
	}
};
} // namespace privates
} // namespace nwd::audio
