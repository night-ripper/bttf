#pragma once

#include <iosfwd>
#include <filesystem>

#include <nwd/audio/sound.hpp>

namespace nwd::audio::vorbis
{
std::unique_ptr<Streaming_sound> create_stream(Format format, std::streambuf& streambuf);
std::unique_ptr<Streaming_sound> create_stream(Format format, const std::filesystem::path& path);
} // namespace nwd::audio::vorbis
