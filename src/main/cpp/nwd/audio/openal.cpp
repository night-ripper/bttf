#include <cstring>

#include <atomic>
#include <algorithm>
#include <array>
#include <stdexcept>
#include <string>
#include <tuple>
#include <thread>
#include <mutex>
#include <semaphore>

#include <AL/al.h>
#define AL_ALEXT_PROTOTYPES
#include <AL/alext.h>
#include <AL/alc.h>

#include <nwd/audio/openal.hpp>
#include <nwd/audio/sound.hpp>
#include <nwd/utility/integer_cast.hpp>
#include <nwd/utility/serializer_thread.hpp>

namespace nwd::audio::openal
{
struct OpenAL_error : std::runtime_error
{
	using std::runtime_error::runtime_error;
};

struct OpenAL_context_error : std::runtime_error
{
	using std::runtime_error::runtime_error;
};

namespace
{
std::int32_t format_of(std::int32_t channels, std::int32_t bit_depth)
{
	channels -= 1;
	bit_depth -= 8;
	bit_depth /= 4;
	
	/// channels: {0, 1}
	/// bit_depth: {0, 2}
	/// sum: {0, 1, 2, 3}
	
	constexpr auto formats = std::array<std::int32_t, 4>
	{
		AL_FORMAT_MONO8,
		AL_FORMAT_STEREO8,
		AL_FORMAT_MONO16,
		AL_FORMAT_STEREO16,
	};
	
	std::int32_t index = channels + bit_depth;
	
	if (index < 0 or std::ssize(formats) <= index)
	{
		std::unreachable();
	}
	
	return formats[static_cast<std::size_t>(index)];
}

std::int32_t format_of(Format format_value)
{
	return format_of(format_value.channels(), format_value.bit_depth());
}

[[maybe_unused]]
void check_error(ALenum error_code)
{
	switch (error_code)
	{
	case AL_NO_ERROR:
		return;
	case AL_INVALID_NAME:
		throw OpenAL_error("Invalid name (a bad name was passed to an OpenAL function)");
	case AL_INVALID_ENUM:
		throw OpenAL_error("Invalid enum (an invalid enum value was passed to an OpenAL function)");
	case AL_INVALID_VALUE:
		throw OpenAL_error("Invalid value (an invalid value was passed to an OpenAL function)");
	case AL_INVALID_OPERATION:
		throw OpenAL_error("Invalid operation (the requested operation is not valid)");
	case AL_OUT_OF_MEMORY:
		throw OpenAL_error("Out of memory");
	default:
		throw OpenAL_error("Passing a wrong error enum to error detection function");
	}
}

[[maybe_unused]]
void check_error()
{
	return check_error(alGetError());
}

template<typename Return_type, typename... Args>
Return_type call(Return_type(*function)(Args...), auto... args)
{
	alGetError();
	
	if constexpr (std::is_void_v<Return_type>)
	{
		function(args...);
		check_error();
	}
	else
	{
		Return_type result = function(args...);
		check_error();
		return result;
	}
}

[[maybe_unused]]
void check_context_error(ALenum error_code)
{
	switch (error_code)
	{
	case ALC_NO_ERROR:
		return;
	case ALC_INVALID_DEVICE:
		throw OpenAL_context_error("Invalid device (a bad device was passed to an OpenAL function)");
	case ALC_INVALID_CONTEXT:
		throw OpenAL_context_error("Invalid context (a bad context was passed to an OpenAL function)");
	case ALC_INVALID_ENUM:
		throw OpenAL_context_error("Invalid enum (an invalid enum value was passed to an OpenAL function)");
	case ALC_INVALID_VALUE:
		throw OpenAL_context_error("Invalid value (an invalid value was passed to an OpenAL function)");
	case ALC_OUT_OF_MEMORY:
		throw OpenAL_context_error("Out of memory");
	default:
		throw OpenAL_context_error("Passing a wrong error enum to error detection function");
	}
}

[[maybe_unused]]
void check_context_error(ALCdevice* device)
{
	return check_context_error(alcGetError(device));
}

struct Static_section
{
	struct Source_data
	{
		using resource_type = std::array<std::uint32_t, 3>;
		
		static void deleter(const resource_type& object) noexcept
		{
			call(alDeleteBuffers, utility::checked_cast(std::ssize(object)), std::data(object));
		}
		
		utility::Unique_resource<std::array<std::uint32_t, 3>, utility::Functor<deleter>> buffer_objects_;
		std::unique_ptr<Streaming_sound> data_source_;
		std::int8_t expected_rewinds_ = -1;
		
		void processed(std::uint32_t source, std::uint32_t buffers_processed);
		void source_state(std::uint32_t source, std::uint32_t new_state);
	};
	
	//! Supposedly AL functions cannot be called from the callback so we invoke
	//! them from within this worker and wait for the result.
	utility::Serializer_thread worker_;
	std::size_t capacity_ = 16;
	std::unique_ptr<Source_data[]> source_data_ = std::make_unique<Source_data[]>(capacity_);
	
	static void event_callback(ALenum event_type, ALuint object, ALuint param,
		ALsizei length, const ALchar *message, ALvoid *user_pointer);
	
	void register_source(std::uint32_t source_index)
	{
		worker_([&]
		{
			if (capacity_ <= source_index)
			{
				auto new_capacity = 8 + capacity_ + capacity_ / 2;
				auto new_sources = std::make_unique<Source_data[]>(new_capacity);
				std::move(source_data_.get(), source_data_.get() + capacity_, new_sources.get());
				capacity_ = new_capacity;
				source_data_ = std::move(new_sources);
			}
		});
	}
	
	void unregister_source(std::uint32_t source_index)
	{
		worker_([&]
		{
			source_data_[source_index] = {};
		});
	}
}
static static_section;
alignas(16) inline static thread_local std::array<std::byte, 2048> thread_local_data_buffer;

void Static_section::event_callback(ALenum event_type, ALuint object, ALuint param,
	[[maybe_unused]] ALsizei length, [[maybe_unused]] const ALchar *message, [[maybe_unused]] ALvoid *user_pointer)
{
	switch (event_type)
	{
	case AL_EVENT_TYPE_BUFFER_COMPLETED_SOFT:
		static_section.worker_([&]
		{
			static_section.source_data_[object].processed(object, param);
		});
		break;
	case AL_EVENT_TYPE_SOURCE_STATE_CHANGED_SOFT:
		static_section.worker_([&]
		{
			static_section.source_data_[object].source_state(object, param);
		});
		break;
	case AL_EVENT_TYPE_DISCONNECTED_SOFT:
		break;
	default:
		break;
	}
}

void Static_section::Source_data::processed(std::uint32_t source, std::uint32_t buffers_processed)
{
	if (expected_rewinds_ > 0)
	{
		return;
	}
	
	for (; buffers_processed > 0; --buffers_processed)
	{
		auto data_length = data_source_->sgetn(
			reinterpret_cast<char*>(thread_local_data_buffer.data()),
			std::ssize(thread_local_data_buffer)
				/ data_source_->channels() * data_source_->channels()
				/ data_source_->byte_depth() * data_source_->byte_depth()
		);
		
		std::uint32_t buffer = 0;
		call(alSourceUnqueueBuffers, source, 1, &buffer);
		
		if (data_length > 0)
		{
			call(alBufferData, buffer, format_of(*data_source_),
				thread_local_data_buffer.data(), utility::checked_cast(data_length), data_source_->frequency()
			);
			call(alSourceQueueBuffers, source, 1, &buffer);
		}
	}
}

void Static_section::Source_data::source_state(std::uint32_t source, std::uint32_t new_state)
{
	(void) source;
	
	switch (new_state)
	{
	case AL_INITIAL:
		--expected_rewinds_;
		break;
	case AL_PLAYING:
		break;
	case AL_PAUSED:
		break;
	case AL_STOPPED:
		if (not buffer_objects_)
		{
			call(alSourcei, source, AL_BUFFER, 0);
		}
		break;
	default:
		break;
	}
}
} // namespace

bool Device_name_iterator::operator==(std::default_sentinel_t) const noexcept
{
	return *names_ == '\0';
}

auto Device_name_iterator::operator*() const noexcept -> value_type
{
	return std::string_view(names_);
}

auto Device_name_iterator::operator++() noexcept -> Device_name_iterator&
{
	names_ += std::strlen(names_) + 1;
	return *this;
}

static_assert(std::forward_iterator<Device_name_iterator>);

void deleters::device(void* object) noexcept
{
	alcCloseDevice(static_cast<ALCdevice*>(object));
}

Device::Device(const char* name)
{
	auto* result = alcOpenDevice(name);
	check_context_error(result);
	
	if (not result)
	{
		throw OpenAL_error("Error opening device");
	}
	
	this->reset(result);
}

std::ranges::subrange<Device_name_iterator, std::default_sentinel_t> Device::names()
{
	auto result = alcGetString(nullptr, ALC_ALL_DEVICES_SPECIFIER);
	return std::ranges::subrange(Device_name_iterator(result), std::default_sentinel);
}

std::string_view Device::name() const
{
	return call(alcGetString, static_cast<ALCdevice*>(get()), ALC_DEVICE_SPECIFIER);
}

void deleters::capture_device(void* object) noexcept
{
	call(alcCaptureCloseDevice, static_cast<ALCdevice*>(object));
}

Capture_device::Capture_device(const char* name, Sound_info info)
{
	auto* result = call(alcCaptureOpenDevice, name, static_cast<std::uint32_t>(info.frequency()), format_of(info), 4096);
	check_context_error(result);
	
	if (not result)
	{
		throw OpenAL_error("Error opening capture device");
	}
	
	this->reset(result);
}

std::ranges::subrange<Device_name_iterator, std::default_sentinel_t> Capture_device::names()
{
	return std::ranges::subrange(Device_name_iterator(alcGetString(nullptr, ALC_CAPTURE_DEVICE_SPECIFIER)), std::default_sentinel);
}

void Capture_device::start()
{
	alcCaptureStart(static_cast<ALCdevice*>(this->get()));
	check_context_error(static_cast<ALCdevice*>(this->get()));
}

void Capture_device::stop()
{
	alcCaptureStop(static_cast<ALCdevice*>(this->get()));
	check_context_error(static_cast<ALCdevice*>(this->get()));
}

std::ptrdiff_t Capture_device::samples_ready() const
{
	ALCint result = 0;
	alcGetIntegerv(static_cast<ALCdevice*>(this->get()), ALC_CAPTURE_SAMPLES, 1, &result);
	check_context_error(static_cast<ALCdevice*>(this->get()));
	return result;
}

std::ptrdiff_t Capture_device::read(std::byte* target, std::ptrdiff_t samples) const
{
	auto result = this->samples_ready();
	result = std::min(result, samples);
	alcCaptureSamples(static_cast<ALCdevice*>(this->get()), target, utility::checked_cast(result));
	check_context_error(static_cast<ALCdevice*>(this->get()));
	return result;
}

std::ptrdiff_t Capture_device::operator>>(Sound_view sound) const
{
	return read(sound.view().data(), std::ssize(sound.view()) / sound.sample_size_bytes());
}

void deleters::context(void* object) noexcept
{
	if (call(alcGetCurrentContext) == object)
	{
		alcMakeContextCurrent(nullptr);
	}
	
	alcDestroyContext(static_cast<ALCcontext*>(object));
}

Context::Context(Device& device)
{
	check_context_error(static_cast<ALCdevice*>(device.get()));
	auto* result = alcCreateContext(static_cast<ALCdevice*>(device.get()), nullptr);
	check_context_error(static_cast<ALCdevice*>(device.get()));
	
	if (not result)
	{
		throw OpenAL_error("Error creating context");
	}
	
	this->reset(result);
}

void Context::make_current()
{
	call(alcMakeContextCurrent, static_cast<ALCcontext*>(this->get()));
	auto types = std::to_array<ALenum>({
		AL_EVENT_TYPE_BUFFER_COMPLETED_SOFT,
		AL_EVENT_TYPE_SOURCE_STATE_CHANGED_SOFT,
		AL_EVENT_TYPE_DISCONNECTED_SOFT,
	});
	
	call(alEventControlSOFT, utility::checked_cast(std::ssize(types)), types.data(), true);
	call(alEventCallbackSOFT, &Static_section::event_callback, nullptr);
}

const char* Context::vendor() const
{
	return call(alGetString, AL_VENDOR);
}

const char* Context::version() const
{
	return call(alGetString, AL_VERSION);
}

const char* Context::renderer() const
{
	return call(alGetString, AL_RENDERER);
}

const char* Context::extensions() const
{
	return call(alGetString, AL_EXTENSIONS);
}

void Context::Listener::gain(float value)
{
	alListenerf(AL_GAIN, value);
}

void Context::Listener::position(std::array<float, 3> vector)
{
	alListener3f(AL_POSITION, vector[0], vector[1], vector[2]);
}

void Context::Listener::velocity(std::array<float, 3> vector)
{
	alListener3f(AL_VELOCITY, vector[0], vector[1], vector[2]);
}

void Context::Listener::orientation(std::array<float, 3> vector, std::array<float, 3> up_vector)
{
	std::array<float, 6> value {
		vector[0], vector[1], vector[2],
		up_vector[0], up_vector[1], up_vector[2],
	};
	
	alListenerfv(AL_ORIENTATION, std::data(value));
}

void deleters::buffer(std::uint32_t object) noexcept
{
	call(alDeleteBuffers, 1, &object);
}

Buffer Buffer::create()
{
	std::uint32_t object;
	call(alGenBuffers, 1, &object);
	return Buffer(object);
}

Buffer& operator>>(Sound_view sound, Buffer& buffer)
{
	call(alBufferData, buffer.get(), format_of(sound),
		sound.view().data(), utility::checked_cast(std::ssize(sound.view())), sound.frequency()
	);
	return buffer;
}

#if 0
// This solution uses alBufferCallbackSOFT, which is not the preferred way
// since it should be real-time

static ALsizei buffer_callback(ALvoid* user_pointer, ALvoid* target, ALsizei num_bytes)
{
	return static_cast<Streaming_sound*>(user_pointer)->sgetn(static_cast<char*>(target), num_bytes);
}

Buffer& operator>>(Streaming_sound& sound, Buffer& buffer)
{
	call(alBufferCallbackSOFT, buffer.get(), format_of(sound), sound.frequency(), buffer_callback, &sound);
	return buffer;
}
#endif

Buffer& operator>>(Streaming_sound& sound, Buffer& buffer)
{
	auto size = sound.in_avail();
	
	if (size <= 0)
	{
		throw OpenAL_error("Cannot stream sound into a buffer: sound has unknown size");
	}
	
	auto data = std::make_unique<std::byte[]>(static_cast<std::size_t>(size));
	size = sound.sgetn(reinterpret_cast<char*>(data.get()), size);
	return Sound_view(sound, std::span(data.get(), utility::checked_cast(size))) >> buffer;
}

std::int32_t Buffer::bit_depth() const
{
	ALint result;
	alGetBufferi(this->get(), AL_BITS, &result);
	return result;
}

std::int32_t Buffer::channels() const
{
	ALint result;
	alGetBufferi(this->get(), AL_CHANNELS, &result);
	return result;
}
std::int32_t Buffer::frequency() const
{
	ALint result;
	alGetBufferi(this->get(), AL_FREQUENCY, &result);
	return result;
}

void deleters::source(std::uint32_t object) noexcept
{
	call(alDeleteSources, 1, &object);
	static_section.unregister_source(object);
}

Source Source::create()
{
	std::uint32_t object;
	call(alGenSources, 1, &object);
	return Source(object);
}

bool Source::stopped() const
{
	ALint state;
	alGetSourcei(this->get(), AL_SOURCE_STATE, &state);
	return state == AL_STOPPED;
}

void Source::play()
{
	call(alSourcePlay, this->get());
}

void Source::pause()
{
	call(alSourcePause, this->get());
}

void Source::stop()
{
	call(alSourceStop, this->get());
}

void Source::rewind()
{
	call(alSourceRewind, this->get());
}

void Source::loop(bool value)
{
	call(alSourcei, this->get(), AL_LOOPING, value);
}

Source& operator>>(const Buffer& buffer, Source& source)
{
	call(alSourcei, static_cast<std::uint32_t>(source.get()), AL_BUFFER, static_cast<std::int32_t>(buffer.get()));
	return source;
}

Source& operator>>(std::unique_ptr<Streaming_sound> sound, Source& source)
{
	call(alSourceStop, source.get());
	
	static_section.worker_([&]
	{
		++static_section.source_data_[source.get()].expected_rewinds_;
	});
	
	call(alSourceRewind, source.get());
	call(alSourcei, source.get(), AL_BUFFER, 0);
	auto buffers = std::array<std::uint32_t, 3>();
	call(alGenBuffers, utility::checked_cast(std::ssize(buffers)), buffers.data());
	
	int buffers_filled = 0;
	
	for (std::uint32_t buffer : buffers)
	{
		if (sound->sgetn(reinterpret_cast<char*>(thread_local_data_buffer.data()), thread_local_data_buffer.size()) == 0)
		{
			break;
		}
		
		call(alBufferData, buffer, format_of(*sound), thread_local_data_buffer.data(),
			utility::checked_cast(std::ssize(thread_local_data_buffer)), sound->frequency()
		);
		
		++buffers_filled;
	}
	
	static_section.worker_([&]
	{
		auto& buffer_objects = static_section.source_data_[source.get()].buffer_objects_;
		buffer_objects.reset(buffers);
		call(alSourceQueueBuffers, source.get(), buffers_filled, buffer_objects.get().data());
		static_section.source_data_[source.get()].data_source_ = std::move(sound);
	});
	
	return source;
}

void Source::gain(float value)
{
	alSourcef(this->get(), AL_GAIN, value);
}

void Source::reference_distance(float value)
{
	alSourcef(this->get(), AL_REFERENCE_DISTANCE, value);
}

void Source::position(std::array<float, 3> vector)
{
	alSource3f(this->get(), AL_POSITION, vector[0], vector[1], vector[2]);
}

void Source::velocity(std::array<float, 3> vector)
{
	alSource3f(this->get(), AL_VELOCITY, vector[0], vector[1], vector[2]);
}

void Source::orientation(std::array<float, 3> vector, std::array<float, 3> up_vector)
{
	std::array<float, 6> value {
		vector[0], vector[1], vector[2],
		up_vector[0], up_vector[1], up_vector[2],
	};
	
	alSourcefv(this->get(), AL_ORIENTATION, std::data(value));
}

static_assert(std::forward_iterator<Device_name_iterator>);
static_assert(std::ranges::forward_range<std::ranges::subrange<Device_name_iterator, std::default_sentinel_t>>);
} // namespace nwd::audio::openal
