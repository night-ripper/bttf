#pragma once

#include <nwd/containers/aa/privates/tree.hpp>

namespace nwd::containers::aa
{
template<typename Key_type, typename Mapped_type, typename Compare = std::less<Key_type>, typename Size_type = std::ptrdiff_t>
struct Map : privates::Common_interface<Key_type, Mapped_type, Compare, Size_type, true>
{
private:
	using Base = privates::Common_interface<Key_type, Mapped_type, Compare, Size_type, true>;
	
public:
	using mapped_type = Mapped_type;
	using typename Base::iterator;
	
private:
	template<typename... Args>
	std::pair<iterator, bool> private_try_emplace(const auto& key, Args&&... args)
	{
		auto [position, inserted] = this->Base::template try_emplace<true>(key,
			std::piecewise_construct,
			std::forward_as_tuple(key),
			std::forward_as_tuple(std::forward<Args>(args)...)
		);
		
		return std::pair(this->Base::get(position), inserted);
	}
	
public:
	template<typename... Args>
	std::pair<iterator, bool> try_emplace(const Key_type& key, Args&&... args)
	{
		return private_try_emplace(key, std::forward<Args>(args)...);
	}
	
	template<typename... Args>
	std::pair<iterator, bool> try_emplace(Key_type&& key, Args&&... args)
	{
		return private_try_emplace(std::move(key), std::forward<Args>(args)...);
	}
};
} // namespace nwd::containers::aa
