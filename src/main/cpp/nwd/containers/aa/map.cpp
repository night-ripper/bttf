#include <cassert>

#include <iostream>
#include <fstream>
#include <random>

#include <map>
#include <unordered_map>
#include <ranges>
#include <chrono>

#include <nwd/containers/aa/map.hpp>
#include <nwd/containers/repository.hpp>

std::mt19937 rd(1337);
std::uniform_int_distribution<> dist(0, 1'000'000'000);

int main()
{
	nwd::containers::aa::Map<int, int>
// 	std::pmr::map<int, int>
// 	std::pmr::unordered_map<int, int>
	m;
	
	for (auto& v : m)
	{
		
	}
	
	{
		auto start = std::chrono::steady_clock::now();
		for (int i = 0; i < 1'500'000; ++i)
// 		for (int i = 0; i < 5000; ++i)
		{
// 			m.try_emplace(dist(rd), i);
			m.try_emplace(i, i);
		}
		auto finish = std::chrono::steady_clock::now();
		std::cout << "it took: " << std::chrono::duration_cast<std::chrono::milliseconds>(finish - start).count() << "\n";
	}
	
	{
#if 1
		auto start = std::chrono::steady_clock::now();
		for (int i = 0; i < 3'500'000; ++i)
		{
// 			m.insert(dist(rd));
// 			nwd::containers::aa::to_dot(std::ofstream("gr-new.dot"), m) << "\n";
			auto it = m.find(i);
			if (it != m.end())
			{
				assert(std::get<0>(*it) == i);
			}
		}
		auto finish = std::chrono::steady_clock::now();
		std::cout << "it took: " << std::chrono::duration_cast<std::chrono::milliseconds>(finish - start).count() << "\n";
#endif
	}
	
	{
		auto start = std::chrono::steady_clock::now();
		auto it = m.begin();
		
		while (not m.empty())
		{
			m.erase(it++);
		}
		auto finish = std::chrono::steady_clock::now();
		std::cout << "it took: " << std::chrono::duration_cast<std::chrono::milliseconds>(finish - start).count() << "\n";
	}
	
	/*
	int max_height = 0;
	for (auto it = m.begin(); it != m.end(); ++it)
	{
		int height = 0;
		std::ptrdiff_t index = it.index_;
		while (index != -1)
		{
			if (m.nodes_[index].level_ == 0)
			{
				++height;
			}
			index = m.nodes_[index].parent_;
		}
// 		std::cout << std::get<0>(*it) << "\n";
// 		std::cout << height << "\n";
		if (height > max_height)
		{
			max_height = height;
		}
	}
	
	std::cout << max_height << "\n";
	*/
	
// 	nwd::containers::aa::to_dot(std::ofstream("gr-new.dot"), m) << "\n";
	
	std::cout << m.contains(3779) << "\n";
	std::cout << m.contains(3780) << "\n";
	std::cout << m.contains(3781) << "\n";
	
#if 0
	for (auto i : m | std::views::keys)
	{
		std::cout << "\t" << i << "\n";
	}
	std::cout << "\t" << std::get<0>(*--m.end()) << "\n";
#endif
	
	std::cout << sizeof(m.begin()) << "\n";
	std::cout << sizeof(m) << "\n";
	std::cout << sizeof(nwd::containers::privates::Vector_storage<int, int>) << "\n";
	std::cout << sizeof(nwd::containers::Repository<int, int>) << "\n";
}
