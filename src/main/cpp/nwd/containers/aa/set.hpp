#pragma once

#include <nwd/containers/aa/privates/tree.hpp>

namespace nwd::containers::aa
{
template<typename Key_type, typename Compare = std::less<Key_type>, typename Size_type = std::ptrdiff_t>
struct Set : privates::Common_interface<Key_type, void, Compare, Size_type, true>
{
private:
	using Base = privates::Common_interface<Key_type, void, Compare, Size_type, true>;
};
} // namespace nwd::containers::aa
