#pragma once

#include <nwd/containers/hash/hash_base.hpp>

namespace nwd::containers::hash
{
namespace privates
{
template<typename Key_type>
constexpr const Key_type& Hash_set_key_getter(const Key_type& key) noexcept
{
	return key;
}
} // namespace privates

template<typename Key_type, typename Hash = std::hash<Key_type>, typename Equal = std::equal_to<Key_type>>
struct Hash_set : protected privates::Hash_base<Key_type, Key_type, Hash, Equal, privates::Hash_set_key_getter<Key_type>>
{
private:
	using Base = privates::Hash_base<Key_type, Key_type, Hash, Equal, privates::Hash_set_key_getter<Key_type>>;
	
public:
	using key_type = Key_type;
	using value_type = Key_type;
	
	using size_type	= typename Base::size_type;
	using difference_type = typename Base::difference_type;
	
	using hasher = typename Base::hasher;
	using key_equal = typename Base::key_equal;
	using allocator_type = typename Base::allocator_type;
	
	using reference = value_type&;
	using const_reference = const value_type&;
	
	using iterator = typename Base::iterator;
	using const_iterator = typename Base::const_iterator;
	
	using node_type = typename Base::node_type;
	
private:
	std::pair<const_iterator, bool> insert_private(auto&& key)
	noexcept(Base::template is_emplace_noexcept<decltype(key)>)
	{
		auto [index, empty] = Base::find_to_insert(key);
		
		if (empty)
		{
			Base::emplace(index, std::forward<decltype(key)>(key));
		}
		
		return {{Base::table_, index}, empty};
	}
	
public:
	using Base::size;
	using Base::capacity;
	using Base::allocated_capacity;
	using Base::data;
	
	using Base::find;
	using Base::contains;
	
	using Base::clear;
	using Base::erase;
	
	key_type extract(const_iterator pos)
	noexcept(std::is_nothrow_move_constructible_v<value_type>)
	{
		return Base::extract(pos, [](auto& value) noexcept -> decltype(value) {return value;});
	}
	
	std::pair<const_iterator, bool> insert(const key_type& key)
	noexcept(noexcept(insert_private(key)))
	{
		return insert_private(key);
	}
	
	std::pair<const_iterator, bool> insert(key_type&& key)
	noexcept(noexcept(insert_private(std::move(key))))
	{
		return insert_private(std::move(key));
	}
};
} // namespace nwd::containers::hash
