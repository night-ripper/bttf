#pragma once

#include <cstddef>
#include <cstdint>

#include <limits>
#include <span>
#include <type_traits>
#include <utility>

#include <nwd/containers/privates/common.hpp>
#include <nwd/containers/privates/container.hpp>
#include <nwd/containers/privates/vector_storage.hpp>

#include <nwd/utility/bit_indexing.hpp>
#include <nwd/utility/iterators.hpp>
#include <nwd/utility/integer_cast.hpp>

namespace nwd::containers
{
namespace repositories
{
template<typename Index_type, typename Value_type, typename Size_type>
struct Iterator : utility::Forward_iterator<Iterator<Index_type, Value_type, Size_type>>
{
	using value_type = Value_type;
	using reference = value_type&;
	using difference_type = Size_type;
	
	Iterator() noexcept = default;
	
	Iterator(const Index_type* begin, value_type* data, Size_type length) noexcept
		:
		begin_(begin),
		data_(data),
		length_(length)
	{
		if (length_ != 0 and *begin_ % 2 == 0)
		{
			++*this;
		}
	}
	
	friend bool operator==(const Iterator& lhs, const Iterator& rhs) noexcept
	{
		return lhs.distance_ == rhs.distance_
			and lhs.shift_ == rhs.shift_
		;
	}
	
	bool operator==(std::default_sentinel_t) const noexcept
	{
		return distance_ == length_;
	}
	
	reference operator*() const noexcept
	{
		return data_[std::numeric_limits<Index_type>::digits * distance_ + shift_];
	}
	
	Iterator& operator++() noexcept
	{
		Index_type index = begin_[distance_];
		index >>= shift_;
		index >>= 1;
		++shift_;
		
		if (index != 0)
		{
			shift_ += static_cast<std::int16_t>(std::countr_zero(index));
			return *this;
		}
		
		shift_ = 0;
		
		while (++distance_, distance_ != length_)
		{
			index = begin_[distance_];
			
			if (index != 0)
			{
				shift_ = static_cast<std::int16_t>(std::countr_zero(index));
				return *this;
			}
		}
		
		return *this;
	}
	
	using utility::Forward_iterator<Iterator<Index_type, Value_type, Size_type>>::operator++;
	
private:
	const Index_type* begin_ = nullptr;
	value_type* data_ = nullptr;
	Size_type length_ = 0;
	Size_type distance_ = 0;
	std::int16_t shift_ = 0;
};
} // namespace repositories

template<
	typename Value_type,
	typename Size_type = std::ptrdiff_t,
	typename Header_type = void
>
struct Repository : protected privates::Container, privates::Vector_storage_data<Size_type>
{
	using typename privates::Container::allocator_type;
	
	using value_type = Value_type;
	using header_type = Header_type;
	
	using typename privates::Vector_storage_data<Size_type>::size_type;
	using difference_type = size_type;
	
	using reference = value_type&;
	using const_reference = const value_type&;
	
private:
	using index_type = std::uintmax_t;
	
	constexpr static std::ptrdiff_t alignment = std::max({
		privates::alignof_void_specialization<header_type>, alignof(index_type), alignof(value_type)
	});
	
	constexpr static std::ptrdiff_t index_offset = privates::trailer_offset(
		privates::sizeof_void_specialization<header_type>, alignof(index_type)
	);
	
	template<typename Type>
	constexpr static Type* static_header(std::byte* data)
	{
		return reinterpret_cast<header_type*>(data);
	}
	
	constexpr static std::span<index_type> index_span(std::byte* data, size_type index_size_bytes) noexcept
	{
		return std::span(reinterpret_cast<index_type*>(data + index_offset), std::size_t(index_size_bytes) / sizeof(index_type));
	}
	
protected:
	constexpr std::span<index_type> index_span() const noexcept
	{
		return index_span(this->data_, index_size_bytes_);
	}
	
private:
	constexpr static size_type index_size_bytes(size_type capacity) noexcept
	{
		return utility::bit_indexing::index_length<index_type>(capacity) * size_type(sizeof(index_type));
	}
	
	constexpr static size_type values_offset(size_type index_size_bytes) noexcept
	{
		return utility::checked_cast(index_offset + privates::trailer_offset(index_size_bytes, size_type(alignof(value_type))));
	}
	
	constexpr static value_type* static_data(std::byte* data, size_type index_size_bytes) noexcept
	{
		return reinterpret_cast<value_type*>(data + values_offset(index_size_bytes));
	}
	
	constexpr static size_type allocated_size_bytes(size_type capacity, size_type index_size_bytes) noexcept
	{
		return values_offset(index_size_bytes) + size_type(sizeof(value_type)) * capacity;
	}
	
	template<typename Iterator_type>
	static Iterator_type static_begin(auto& self) noexcept
	{
		auto* level_begin = self.index_span().data() + self.index_span().size();
		auto level_length = utility::bit_indexing::level_length<index_type>(self.capacity_);
		level_begin -= level_length;
		auto* data = self.data();
		
		return Iterator_type(level_begin, data, level_length);
	}
	
public:
	using iterator = repositories::Iterator<index_type, Value_type, Size_type>;
	using const_iterator = repositories::Iterator<index_type, const Value_type, Size_type>;
	
	header_type* header() noexcept requires(not std::is_void_v<header_type>)
	{
		return static_header<header_type>(this->data_);
	}
	
	const header_type* header() const noexcept requires(not std::is_void_v<header_type>)
	{
		return static_header<const header_type>(this->data_);
	}
	
	value_type* data() noexcept
	{
		if (this->data_ != nullptr)
		{
			return static_data(this->data_, index_size_bytes_);
		}
		
		return nullptr;
	}
	
	const value_type* data() const noexcept
	{
		if (this->data_ != nullptr)
		{
			return static_data(this->data_, index_size_bytes_);
		}
		
		return nullptr;
	}
	
	iterator begin() noexcept
	{
		return static_begin<iterator>(*this);
	}
	
	const_iterator begin() const noexcept
	{
		return static_begin<const_iterator>(*this);
	}
	
	constexpr static std::default_sentinel_t end() noexcept
	{
		return std::default_sentinel;
	}
	
	void clear() noexcept
	{
		if (size_ > 0)
		{
			for (value_type& value : *this)
			{
				std::destroy_at(&value);
			}
			
			std::ranges::fill(index_span(), 0);
			
			size_ = 0;
		}
	}
	
	~Repository()
	{
		if (size_ > 0)
		{
			for (value_type& value : *this)
			{
				std::destroy_at(&value);
			}
		}
		
		std::ranges::destroy(index_span());
		
		if (this->data_)
		{
			if constexpr (not std::is_void_v<header_type>)
			{
				std::destroy_at(header());
			}
			
			this->allocator_.deallocate_bytes(this->data_,
				std::size_t(allocated_size_bytes(this->capacity_, index_size_bytes_)),
				std::size_t(alignment)
			);
		}
	}
	
	Repository() noexcept = default;
	
	explicit Repository(allocator_type allocator) noexcept
		:
		Container(allocator)
	{
	}
	
	Repository(size_type capacity, allocator_type allocator = allocator_type())
		:
		Container(allocator)
	{
		if (capacity > 0)
		{
			auto this_index_size_bytes = index_size_bytes(capacity);
			this->data_ = reinterpret_cast<std::byte*>(this->allocator_.allocate_bytes(
				values_offset(this_index_size_bytes)
				+ static_cast<std::ptrdiff_t>(sizeof(value_type)) * capacity, alignment
			));
			index_size_bytes_ = this_index_size_bytes;
			this->capacity_ = capacity;
			std::ranges::uninitialized_fill(index_span(), 0);
			
			if constexpr (not std::is_void_v<header_type>)
			{
				this->allocator_.construct(header());
			}
		}
	}
	
	Repository(const Repository& other)
		:
		Repository(other.capacity(), other.allocator_.select_on_container_copy_construction())
	{
		if constexpr (not std::is_void_v<header_type>)
		{
			this->allocator_.construct(header(), *other.header());
		}
		
		auto* data = this->data();
		auto* other_data = other.data();
		
		if constexpr (std::is_nothrow_copy_constructible_v<value_type>)
		{
			for (const value_type& value : other)
			{
				this->allocator_.construct(data + (&value - other_data), value);
			}
		}
		else
		{
			const value_type* last_value = nullptr;
			
			try
			{
				for (const value_type& value : other)
				{
					last_value = &value;
					this->allocator_.construct(data + (&value - other_data), value);
				}
			}
			catch (...)
			{
				for (const value_type& value : other)
				{
					if (&value == last_value)
					{
						break;
					}
					
					std::destroy_at(data + (&value - other_data));
				}
				
				if constexpr (not std::is_void_v<header_type>)
				{
					std::destroy_at(header());
				}
				
				this->allocator_.deallocate_bytes(this->data_, allocated_size_bytes(this->capacity_, index_size_bytes_), alignment);
				
				throw;
			}
			
			
			std::ranges::copy(other.index_span(), index_span().begin());
		}
	}
	
	Repository& operator=(const Repository& other) noexcept
	{
		std::destroy_at(this);
		std::construct_at(this, other);
		return *this;
	}
	
	Repository(Repository&& other) noexcept
		:
		Container(static_cast<Container&&>(other)),
		privates::Vector_storage_data<Size_type>(static_cast<privates::Vector_storage_data<Size_type>&&>(other))
	{
		index_size_bytes_ = other.index_size_bytes_;
		other.index_size_bytes_ = 0;
		size_ = other.size_;
		other.size_ = 0;
	}
	
	Repository& operator=(Repository&& other) noexcept
	{
		std::destroy_at(this);
		std::construct_at(this, std::move(other));
		return *this;
	}
	
	void erase(size_type index) noexcept
	{
		auto this_span = index_span();
		
		if (not utility::bit_indexing::erase(this_span.data() + this_span.size(), index, this->capacity_))
		{
			std::unreachable();
		}
		
		std::destroy_at(data() + index);
		
		--size_;
	}
	
	template<typename... Args>
	[[nodiscard]] size_type emplace(Args&&... args)
	{
		if (size_ == this->capacity_)
		{
			auto new_capacity = this->default_capacity_growth(this->capacity_);
			auto new_index_size_bytes = index_size_bytes(new_capacity);
			
			auto new_data = reinterpret_cast<std::byte*>(this->allocator_.allocate_bytes(
				std::size_t(values_offset(new_index_size_bytes)) + sizeof(value_type) * std::size_t(new_capacity),
				std::size_t(alignment)
			));
			
			auto new_index_span = index_span(new_data, new_index_size_bytes);
			std::ranges::uninitialized_fill(new_index_span, 0);
			
			if (this->data_)
			{
				if constexpr (not std::is_void_v<header_type>)
				{
					privates::construct_and_destroy(this->allocator_, static_header<header_type>(new_data), std::move(*header()));
				}
				auto this_span = index_span();
				utility::bit_indexing::copy(this_span.data() + this_span.size(),
					this->capacity_, new_index_span.data() + new_index_span.size(), new_capacity
				);
				
				auto* data = this->data();
				auto* new_value_data = static_data(new_data, new_index_size_bytes);
				
				for (value_type& value : *this)
				{
					privates::construct_and_destroy(this->allocator_, new_value_data + (&value - data), std::move(value));
				}
				
				this->allocator_.deallocate_bytes(
					this->data_, std::size_t(allocated_size_bytes(this->capacity_, index_size_bytes_)), alignment
				);
			}
			else
			{
				if constexpr (not std::is_void_v<header_type>)
				{
					this->allocator_.construct(static_header<header_type>(new_data));
				}
			}
			
			this->capacity_ = new_capacity;
			this->data_ = new_data;
			index_size_bytes_ = new_index_size_bytes;
		}
		
		auto span = index_span();
		
		size_type result = utility::bit_indexing::push_front(span.data(), this->capacity_);
		
		try
		{
			this->allocator_.construct(data() + result, std::forward<Args>(args)...);
		}
		catch (...)
		{
			utility::bit_indexing::erase(span.data() + span.size(), result, this->capacity_);
			throw;
		}
		
		++size_;
		
		return result;
	}
	
	[[nodiscard]] size_type insert(const value_type& value)
	{
		return emplace(value);
	}
	
	[[nodiscard]] size_type insert(value_type&& value)
	{
		return emplace(std::move(value));
	}
	
	reference operator[](size_type index) noexcept
	{
		return data()[index];
	}
	
	const_reference operator[](size_type index) const noexcept
	{
		return data()[index];
	}
	
	constexpr bool empty() const noexcept
	{
		return size_ == 0;
	}
	
private:
	[[no_unique_address]] size_type index_size_bytes_ = 0;
	
protected:
	[[no_unique_address]] size_type size_ = 0;
};
} // namespace nwd::containers
