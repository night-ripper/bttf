#include <iostream>

#include <thread>
#include <vector>
#include "concurrent_queue.hpp"

int main()
{
	
	nwd::containers::Concurrent_queue<std::unique_ptr<int>> cq;
	
	/// Test energy consumption
	/*
	std::vector<std::jthread> poppers(20);
	
	for (auto& popper : poppers)
	{
		popper = std::jthread([&]() -> void
		{
			while (true)
			{
				if (auto v = cq.try_pop())
				{
					std::string text = "popped " + std::to_string(**v);
					std::cout << text << "\n";
				}
				else
				{
					std::this_thread::sleep_for(std::chrono::microseconds(1));
				}
			}
		});
	}
	*/
	
	/*
	for (std::size_t i = 0; true; ++i)
	{
		if (cq.try_emplace(std::make_unique<int>(i)))
		{
		}
	}
	*/
	
	
	std::atomic<int> writers_finished = 0;
	std::atomic_thread_fence(std::memory_order::acq_rel);
	
	std::vector<std::jthread> jts;
	jts.reserve(100);
	constexpr int num_writers = 20;
	
	auto now = std::chrono::steady_clock::now();
	
	for (int i = 0; i != num_writers; ++i)
	{
		jts.emplace_back([i, &cq, &writers_finished]() -> void
		{
			for (int j = 0; j != 100000; ++j)
			{
				auto value = std::make_unique<int>(i);
				
#if 0
				while (not cq.try_emplace(std::move(value)))
				{
					std::this_thread::sleep_for(std::chrono::microseconds(1));
				}
#else
				cq.emplace(std::move(value));
#endif
			}
			
			writers_finished.fetch_add(1, std::memory_order::acq_rel);
		});
	}
	
	std::atomic<int> anums[num_writers] = {};
	std::atomic_thread_fence(std::memory_order::acq_rel);
	
	for (int i = 0; i != num_writers; ++i)
	{
		jts.emplace_back([&cq, &anums, &writers_finished]() -> void
		{
			int nums[num_writers] = {};
			
			while (writers_finished.load(std::memory_order::relaxed) != num_writers)
			{
				if (auto op = cq.try_pop())
				{
					++nums[**op];
				}
				else
				{
					std::this_thread::sleep_for(std::chrono::microseconds(500));
				}
			}
			
			while (auto op = cq.try_pop())
			{
				++nums[**op];
			}
			
			for (int i = 0; i != num_writers; ++i)
			{
				anums[i].fetch_add(nums[i], std::memory_order::acq_rel);
			}
		});
	}
	
	jts.clear();
	std::atomic_thread_fence(std::memory_order::acq_rel);
	
	auto new_now = std::chrono::steady_clock::now();
	
	for (int i : anums)
	{
		std::cout << i << "\n";
	}
	
	std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(new_now - now).count() << "\n";
}
