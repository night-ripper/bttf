#pragma once

#include <cstddef>
#include <cstdint>

#include <array>
#include <functional>
#include <type_traits>
#include <utility>

namespace nwd::containers::nodes::linked
{
template<typename Value_type, typename Size_type>
struct Node
{
	using value_type = Value_type;
	using size_type = Size_type;
	
	Node() noexcept(std::is_nothrow_default_constructible_v<value_type>) = default;
	
	template<typename... Args>
	Node(Args... args) noexcept(std::is_nothrow_constructible_v<value_type, Args...>)
		:
		value_(std::forward<Args>(args)...)
	{
	}
	
	Node(const Node&) noexcept(std::is_nothrow_copy_constructible_v<value_type>) = default;
	Node& operator=(const Node&) noexcept(std::is_nothrow_copy_assignable_v<value_type>) = default;
	
	Node(Node&&) noexcept(std::is_nothrow_move_constructible_v<value_type>) = default;
	Node& operator=(Node&&) noexcept(std::is_nothrow_move_assignable_v<value_type>) = default;
	
public:
	size_type lsib_ = -1;
	size_type rsib_ = -1;
	value_type value_;
};

template<typename Value_type, typename Size_type>
inline Size_type get_prev(const Node<Value_type, Size_type>& node) noexcept
{
	return node.lsib_;
}

template<typename Value_type, typename Size_type>
inline void set_prev(Node<Value_type, Size_type>& node, Size_type value) noexcept
{
	node.lsib_ = value;
}

template<typename Value_type, typename Size_type>
inline Size_type get_next(const Node<Value_type, Size_type>& node) noexcept
{
	return node.rsib_;
}

template<typename Value_type, typename Size_type>
inline void set_next(Node<Value_type, Size_type>& node, Size_type value) noexcept
{
	node.rsib_ = value;
}

template<typename Value_type, typename Size_type>
inline Value_type& value_of(Node<Value_type, Size_type>& node) noexcept
{
	return node.value_;
}

template<typename Value_type, typename Size_type>
inline const Value_type& value_of(const Node<Value_type, Size_type>& node) noexcept
{
	return node.value_;
}

template<typename Size_type>
inline std::array<Size_type, 2> unlink(auto nodes, Size_type index) noexcept
{
	Size_type prev = get_prev(nodes[index]);
	Size_type next = get_next(nodes[index]);
	
	if (prev != -1)
	{
		set_next(nodes[prev], next);
	}
	
	if (next != -1)
	{
		set_prev(nodes[next], prev);
	}
	
	return std::array {prev, next};
}

template<typename Size_type>
inline void link_back(auto nodes, Size_type index, Size_type last) noexcept
{
	set_prev(nodes[index], last);
	
	if (last != -1)
	{
		set_next(nodes[last], index);
	}
}
} // namespace nwd::containers::nodes::linked
