#pragma once

#include <algorithm>
#include <tuple>

#include <nwd/containers/repository.hpp>
#include <nwd/utility/iterators.hpp>

namespace nwd::containers::nodes::aa
{
namespace privates
{
template<typename Type>
requires(std::is_signed_v<Type>)
constexpr Type valid_index(Type value) noexcept
{
	if (value < 0)
	{
		std::unreachable();
	}
	
	return value;
}
} // namespace privates

template<typename Value_type, typename Size_type>
struct Node
{
	using value_type = Value_type;
	using size_type = Size_type;
	
	template<typename... Args>
	Node(Args&&... args) noexcept(std::is_nothrow_constructible_v<value_type, Args...>)
		:
		value_(std::forward<Args>(args)...)
	{
	}
	
public:
	size_type parent_ = -1;
	size_type descendants_[2] = {-1, -1};
	std::int16_t level_ = 0;
	value_type value_;
};

template<typename Value_type, typename Size_type>
inline Size_type get_parent(const Node<Value_type, Size_type>& node) noexcept
{
	return node.parent_;
}

template<typename Value_type, typename Size_type>
inline void set_parent(Node<Value_type, Size_type>& node, Size_type value) noexcept
{
	node.parent_ = value;
}

template<typename Value_type, typename Size_type>
inline Size_type get_descendant(const Node<Value_type, Size_type>& node, std::int8_t index) noexcept
{
	return node.descendants_[privates::valid_index(index)];
}

template<typename Value_type, typename Size_type>
inline void set_descendant(Node<Value_type, Size_type>& node, std::int8_t index, Size_type value) noexcept
{
	node.descendants_[privates::valid_index(index)] = value;
}

template<typename Value_type, typename Size_type>
inline std::int16_t get_level(const Node<Value_type, Size_type>& node) noexcept
{
	return node.level_;
}

template<typename Value_type, typename Size_type>
inline void set_level(Node<Value_type, Size_type>& node, std::int16_t value) noexcept
{
	node.level_ = value;
}

template<typename Value_type, typename Size_type>
inline Value_type& value_of(Node<Value_type, Size_type>& node) noexcept
{
	return node.value_;
}

template<typename Value_type, typename Size_type>
inline const Value_type& value_of(const Node<Value_type, Size_type>& node) noexcept
{
	return node.value_;
}

template<typename Size_type>
inline std::int8_t get_parent_index(auto nodes, Size_type index, Size_type parent) noexcept
{
	for (std::int8_t result = 0; result != 2; ++result)
	{
		if (get_descendant(nodes[privates::valid_index(parent)], result) == index)
		{
			return result;
		}
	}
	
	return -1;
}

////////////////////////////////////////////////////////////////////////////////

template<typename Real_type, typename Node_range_type, typename Value_type,
	typename Difference_type, typename Reference_type, typename Pointer_type>
struct Iterator
{
	using difference_type = Difference_type;
	using value_type = Value_type;
	using reference = Reference_type;
	using pointer = Pointer_type;
	using iterator_category = std::bidirectional_iterator_tag;
	
	constexpr Iterator() noexcept = default;
	
	constexpr Iterator(Node_range_type nodes, difference_type index, bool is_end) noexcept
		:
		nodes_(nodes),
		index_(index),
		is_end_(is_end)
	{
	}
	
	friend bool operator==(Iterator, Iterator) noexcept = default;
	
	reference operator*() const noexcept
	{
		return reference(value_of(nodes_[index_]));
	}
	
	pointer operator->() const noexcept
	{
		return pointer(&value_of(nodes_[index_]));
	}
	
protected:
	void walk_up(bool desc_index) noexcept
	{
		difference_type parent = -1;
		difference_type index = index_;
		
		while ((parent = get_parent(nodes_[index]), parent != -1)
			and get_parent_index(nodes_, index, parent) == desc_index)
		{
			index = parent;
		}
		
		if (parent != -1)
		{
			index_ = parent;
		}
		else
		{
			is_end_ = true;
		}
	}
	
	void walk_down(bool direction) noexcept
	{
		difference_type descendant = -1;
		
		while (descendant = get_descendant(nodes_[index_], direction), descendant != -1)
		{
			index_ = descendant;
		}
	}
	
public:
	Real_type& operator++() noexcept
	{
		if (is_end_)
		{
			is_end_ = false;
		}
		else
		{
			if (auto rdes = get_descendant(nodes_[index_], 1); rdes != -1)
			{
				index_ = rdes;
				walk_down(0);
			}
			else
			{
				walk_up(1);
			}
		}
		
		return static_cast<Real_type&>(*this);
	}
	
	Real_type& operator--() noexcept
	{
		if (is_end_)
		{
			is_end_ = false;
		}
		else
		{
			if (auto ldes = get_descendant(nodes_[index_], 0); ldes != -1)
			{
				index_ = ldes;
				walk_down(1);
			}
			else
			{
				walk_up(0);
			}
		}
		
		return static_cast<Real_type&>(*this);
	}
	
	Real_type operator++(int) noexcept
	{
		return nwd::utility::operators::post_increment(static_cast<Real_type&>(*this));
	}
	
	Real_type operator--(int) noexcept
	{
		return nwd::utility::operators::post_decrement(static_cast<Real_type&>(*this));
	}
	
public:
	Node_range_type nodes_;
	difference_type index_;
	bool is_end_;
};

////////////////////////////////////////////////////////////////////////////////

template<typename Size_type>
inline Size_type skew(auto nodes, Size_type index) noexcept
{
	if (auto l_index = get_descendant(nodes[index], 0); l_index == -1)
	{
		return index;
	}
	else if (get_level(nodes[index]) == get_level(nodes[l_index]))
	{
		if (auto lrdesc = get_descendant(nodes[l_index], 1); lrdesc != -1)
		{
			set_parent(nodes[lrdesc], index);
		}
		
		set_parent(nodes[l_index], get_parent(nodes[index]));
		set_parent(nodes[index], l_index);
		
		set_descendant(nodes[index], 0, get_descendant(nodes[l_index], 1));
		set_descendant(nodes[l_index], 1, index);
		
		return l_index;
	}
	
	return index;
}

template<typename Size_type>
inline Size_type split(auto nodes, Size_type index) noexcept
{
	if (auto r_index = get_descendant(nodes[index], 1);
		r_index == -1 or get_descendant(nodes[r_index], 1) == -1)
	{
		return index;
	}
	else if (get_level(nodes[index]) == get_level(nodes[get_descendant(nodes[r_index], 1)]))
	{
		if (auto rldesc = get_descendant(nodes[r_index], 0); rldesc != -1)
		{
			set_parent(nodes[rldesc], index);
		}
		
		set_parent(nodes[r_index], get_parent(nodes[index]));
		set_parent(nodes[index], r_index);
		
		set_descendant(nodes[index], 1, get_descendant(nodes[r_index], 0));
		set_descendant(nodes[r_index], 0, index);
		set_level(nodes[r_index], get_level(nodes[r_index]) + std::int8_t(1));
		
		return r_index;
	}
	
	return index;
}

template<typename Size_type>
inline std::tuple<Size_type, Size_type, std::int8_t> find(auto nodes, Size_type root,
	const auto& key, auto compare) noexcept
{
	Size_type desc = root;
	Size_type parent = -1;
	std::int8_t parent_index = 0;
	
	do
	{
		parent = desc;
		
		// NOTE need assert?
		// static_assert(noexcept(compare(std::as_const(key), std::as_const(nodes[desc]))));
		
		if (compare(std::as_const(key), std::as_const(nodes[desc])))
		{
			parent_index = 0;
			desc = get_descendant(nodes[desc], parent_index);
		}
		else if (compare(std::as_const(nodes[desc]), std::as_const(key)))
		{
			parent_index = 1;
			desc = get_descendant(nodes[desc], parent_index);
		}
		else
		{
			break;
		}
	}
	while (desc != -1);
	
	return std::tuple(desc, parent, parent_index);
}

template<typename Size_type>
inline void swap_nodes(auto nodes, Size_type index, Size_type successor) noexcept
{
	if (index == successor)
	{
		std::unreachable();
	}
	
	auto parent = get_parent(nodes[index]);
	auto successor_rdes = get_descendant(nodes[successor], 1);
	if (get_descendant(nodes[index], 1) == successor)
	{
		set_parent(nodes[index], successor);
		set_descendant(nodes[successor], 1, index);
	}
	else
	{
		set_parent(nodes[index], get_parent(nodes[successor]));
		set_descendant(nodes[successor], 1, get_descendant(nodes[index], 1));
		auto successor_parent = get_parent(nodes[successor]);
		set_descendant(nodes[successor_parent], 0, index);
		set_parent(nodes[index], successor_parent);
		set_parent(nodes[get_descendant(nodes[index], 1)], successor);
	}
	
	set_parent(nodes[successor], parent);
	if (parent != -1)
	{
		set_descendant(nodes[parent], get_parent_index(nodes, index, parent), successor);
	}
	
	auto index_ldes = get_descendant(nodes[index], 0);
	set_descendant(nodes[successor], 0, index_ldes);
	set_descendant(nodes[index], 0, Size_type(-1));
	if (index_ldes != -1)
	{
		set_parent(nodes[index_ldes], successor);
	}
	
	set_descendant(nodes[index], 1, successor_rdes);
	
	{
		auto level = get_level(nodes[index]);
		set_level(nodes[index], get_level(nodes[successor]));
		set_level(nodes[successor], level);
	}
	
// 	NOTE unnecessary, this is set in `erase_rebalance`
// 	if (successor_rdes != -1)
// 	{
// 		set_parent(nodes[successor_rdes], index);
// 	}
}

constexpr std::int8_t change_propagation_distance = 3;

template<typename Size_type>
inline bool insert_rebalance(auto nodes, Size_type parent,
	std::int8_t parent_index, Size_type index) noexcept
{
	set_parent(nodes[index], parent);
	set_descendant(nodes[parent], parent_index, index);
	
	auto changes = change_propagation_distance;
	
	while (index = parent, parent = get_parent(nodes[parent]),
		parent != -1 and changes > 0)
	{
		parent_index = get_parent_index(nodes, index, parent);
		
		--changes;
		
		if (auto nv = skew(nodes, index); nv != index)
		{
			index = nv;
			changes = change_propagation_distance;
		}
		
		if (auto nv = split(nodes, index); nv != index)
		{
			index = nv;
			changes = change_propagation_distance;
		}
		
		set_parent(nodes[index], parent);
		set_descendant(nodes[parent], parent_index, index);
	}
	
	return changes > 0;
}

template<typename Size_type>
inline Size_type erase_rebalance_leaf(auto nodes, Size_type index) noexcept
{
	if (get_level(nodes[index]) != 0)
	{
		std::unreachable();
	}
	
	auto rdes = get_descendant(nodes[index], 1);
	auto parent = get_parent(nodes[index]);
	
	if (rdes != -1)
	{
		set_parent(nodes[rdes], parent);
	}
	
	if (parent == -1)
	{
		return get_descendant(nodes[index], 1);
	}
	
	set_descendant(nodes[parent], get_parent_index(nodes, index, parent), rdes);
	
	auto changes = change_propagation_distance;
	
	do
	{
		--changes;
		index = parent;
		parent = get_parent(nodes[parent]);
		std::int8_t parent_index = -1;
		
		if (parent != -1)
		{
			parent_index = get_parent_index(nodes, index, parent);
		}
		
		std::int16_t level = -1;
		
		if (auto ldes = get_descendant(nodes[index], 0); ldes != -1)
		{
			level = get_level(nodes[ldes]);
		}
		
		rdes = get_descendant(nodes[index], 1);
		
		if (rdes != -1)
		{
			if (auto rlevel = get_level(nodes[rdes]); rlevel < level)
			{
				level = rlevel;
			}
		}
		else
		{
			level = -1;
		}
		
		++level;
		
		if (level < get_level(nodes[index]))
		{
			changes = change_propagation_distance;
			
			set_level(nodes[index], level);
			
			if (rdes != -1 and level < get_level(nodes[rdes]))
			{
				set_level(nodes[rdes], level);
			}
		}
		
		if (auto new_index = skew(nodes, index); new_index != index)
		{
			index = new_index;
			changes = change_propagation_distance;
		}
		
		if (auto rdes = get_descendant(nodes[index], 1); rdes != -1)
		{
			rdes = skew(nodes, rdes);
			
			if (rdes != get_descendant(nodes[index], 1))
			{
				set_descendant(nodes[index], 1, rdes);
				changes = change_propagation_distance;
			}
			
			if (auto rrdes = get_descendant(nodes[rdes], 1); rrdes != -1)
			{
				rrdes = skew(nodes, rrdes);
				
				if (rrdes != get_descendant(nodes[rdes], 1))
				{
					set_descendant(nodes[rdes], 1, rrdes);
					changes = change_propagation_distance;
				}
			}
		}
		
		if (auto new_index = split(nodes, index); new_index != index)
		{
			index = new_index;
			changes = change_propagation_distance;
		}
		
		if (auto rdes = get_descendant(nodes[index], 1); rdes != -1)
		{
			rdes = split(nodes, rdes);
			
			if (rdes != get_descendant(nodes[index], 1))
			{
				set_descendant(nodes[index], 1, rdes);
				changes = change_propagation_distance;
			}
		}
		
		if (parent != -1)
		{
			set_descendant(nodes[parent], parent_index, index);
		}
	}
	while (parent != -1 and changes > 0);
	
	return parent == -1 ? index : -1;
}

template<typename Size_type>
inline Size_type find_successor(auto nodes, Size_type index) noexcept
{
	if (get_level(nodes[index]) == 0)
	{
		return -1;
	}
	
	index = get_descendant(nodes[index], 1);
	
	while (get_level(nodes[index]) > 0)
	{
		index = get_descendant(nodes[index], 0);
	}
	
	return index;
}

template<typename Size_type>
inline Size_type erase_rebalance(auto nodes, Size_type index) noexcept
{
	if (auto successor = find_successor(nodes, index); successor != -1)
	{
		swap_nodes(nodes, index, successor);
	}
	
	return erase_rebalance_leaf(nodes, index);
}
} // namespace nwd::containers::nodes::aa
