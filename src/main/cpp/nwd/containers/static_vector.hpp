#pragma once

#include <cstddef>
#include <cstdint>

#include <array>
#include <limits>

#include <nwd/containers/privates/common.hpp>

#include <nwd/utility/storage.hpp>

namespace nwd::containers
{
template<typename Value_type, std::ptrdiff_t Capacity, typename Size_type = std::ptrdiff_t>
struct Static_vector
{
	using value_type = Value_type;
	using size_type = Size_type;
	
	~Static_vector()
	{
		std::destroy_n(data_.data(), size_);
	}
	
	[[nodiscard]] constexpr bool empty() const noexcept
	{
		return size_ == 0;
	}
	
	constexpr size_type size() const noexcept
	{
		return size_;
	}
	
	constexpr size_type max_size() const noexcept
	{
		return Capacity;
	}
	
	constexpr size_type capacity() const noexcept
	{
		return Capacity;
	}
	
	constexpr void clear() noexcept
	{
		std::destroy_n(data_.data(), size_);
		size_ = 0;
	}
	
	value_type* begin() noexcept {return data_.data();}
	const value_type* begin() const noexcept {return data_.data();}
	
	value_type* end() noexcept {return data_.data() + size_;}
	const value_type* end() const noexcept {return data_.data() + size_;}
	
	value_type& front() noexcept {return *begin();}
	const value_type& front() const noexcept {return *begin();}
	
	value_type& back() noexcept {return *(end() - 1);}
	const value_type& back() const noexcept {return *(end() - 1);}
	
	value_type& operator[](size_type index) noexcept {return *(begin() + index);}
	const value_type& operator[](size_type index) const noexcept {return *(begin() + index);}
	
	void pop_back() noexcept
	{
		std::destroy_at(&back());
		--size_;
	}
	
	void erase(size_type index) noexcept
	{
		std::destroy_at(data_.data() + index);
		
		while (index != size_ - 1)
		{
			privates::construct_and_destroy(data_.data() + index, std::move(*(data_.data() + index + 1)));
			++index;
		}
		
		--size_;
	}
	
	void push_back(const value_type& value)
	{
		emplace_back(value);
	}
	
	void push_back(value_type&& value)
	{
		emplace_back(std::move(value));
	}
	
	template<typename... Args>
	value_type& emplace_back(Args... args)
	{
		std::construct_at(end(), std::forward<Args>(args)...);
		++size_;
		return back();
	}
	
private:
	size_type size_ = 0;
	utility::Aligned_storage<value_type, Capacity> data_;
};
} // namespace nwd::containers
