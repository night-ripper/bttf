#pragma once

#include <memory>

namespace nwd::containers::privates
{
template<typename Allocator_type, typename Type>
inline Type& construct_and_destroy(Allocator_type& allocator, Type* constructed, Type&& destroyed)
{
	std::uninitialized_construct_using_allocator(constructed, allocator, std::move(destroyed));
	std::destroy_at(&destroyed);
	return *constructed;
}

template<typename Allocator_type, typename Src_it, typename Dst_it>
inline Dst_it construct_and_destroy(Allocator_type& allocator, Src_it source, Src_it source_end, Dst_it destination)
{
	// auto result = std::uninitialized_move(source, source_end, destination);
	while (source != source_end)
	{
		std::uninitialized_construct_using_allocator(&*destination, allocator, std::move(*source));
		++source;
		++destination;
	}
	std::destroy(source, source_end);
	return destination;
}

template<typename Allocator_type, typename Src_it, typename Dst_it>
inline std::pair<Src_it, Dst_it> construct_and_destroy_n(Allocator_type& allocator, Src_it source, std::ptrdiff_t amount, Dst_it destination)
{
	// auto result = std::uninitialized_move_n(source, amount, destination);
	auto source_copy = source;
	
	for (std::ptrdiff_t i = 0; i != amount; ++i)
	{
		std::uninitialized_construct_using_allocator(&*destination, allocator, std::move(*source_copy));
		++source_copy;
		++destination;
	}
	std::destroy_n(source, amount);
	return std::pair(source_copy, destination);
}

//! Equivalent to sizeof(struct {Header; Trailer[];})
inline constexpr std::ptrdiff_t trailer_offset(std::ptrdiff_t header_size, std::ptrdiff_t trailer_alignment) noexcept
{
	return (header_size + trailer_alignment - 1) / trailer_alignment * trailer_alignment;
}
} // namespace nwd::containers::privates
