#pragma once

#include <utility>
#include <type_traits>

namespace nwd::containers::privates
{
template<typename Key_type, typename Mapped_type>
struct Map_reference : std::pair<const Key_type&, Mapped_type&>
{
	constexpr explicit Map_reference(auto&& value) noexcept
		:
		std::pair<const Key_type&, Mapped_type&>(value.first, value.second)
	{
	}
};

template<typename Key_type, typename Mapped_type>
struct Map_pointer : Map_reference<Key_type, Mapped_type>
{
	constexpr explicit Map_pointer(auto* value) noexcept
		:
		Map_reference<Key_type, Mapped_type>(*value)
	{
	}
	
	constexpr std::pair<const Key_type&, Mapped_type&>* operator->() noexcept
	{
		return this;
	}
	
	constexpr const std::pair<const Key_type&, Mapped_type&>* operator->() const noexcept
	{
		return this;
	}
};

template<typename Key_type, typename Mapped_type>
struct Associative_entry_traits
{
	using key_type = Key_type;
	using value_type = std::pair<Key_type, Mapped_type>;
	using reference = Map_reference<Key_type, Mapped_type>;
	using const_reference = Map_reference<Key_type, const Mapped_type>;
	using pointer = Map_pointer<Key_type, Mapped_type>;
	using const_pointer = Map_pointer<Key_type, const Mapped_type>;
	
	constexpr static const Key_type& get_key(const value_type& value) noexcept
	{
		return value.first;
	}
	
	template<typename Key_compare>
	struct value_compare : Key_compare
	{
		bool operator()(const value_type& lhs, const value_type& rhs) const
		noexcept(noexcept(static_cast<const Key_compare&>(*this)(get_key(lhs), get_key(rhs))))
		{
			return static_cast<const Key_compare&>(*this)(get_key(lhs), get_key(rhs));
		}
	};
};

template<typename Key_type>
struct Associative_entry_traits<Key_type, void>
{
	using key_type = Key_type;
	using value_type = Key_type;
	using reference = const Key_type&;
	using const_reference = const Key_type&;
	using pointer = const Key_type*;
	using const_pointer = const Key_type*;
	
	constexpr static const Key_type& get_key(const value_type& value) noexcept
	{
		return value;
	}
	
	template<typename Key_compare>
	using value_compare = Key_compare;
};
} // namespace nwd::containers::privates

template<typename Key_type, typename Mapped_type>
struct std::tuple_size<nwd::containers::privates::Map_reference<Key_type, Mapped_type>>
	:
	std::tuple_size<std::pair<const Key_type&, Mapped_type&>>
{
};

template<std::size_t Index, typename Key_type, typename Mapped_type>
struct std::tuple_element<Index, nwd::containers::privates::Map_reference<Key_type, Mapped_type>>
	:
	std::tuple_element<Index, std::pair<const Key_type&, Mapped_type&>>
{
};
