#pragma once

#include <cstddef>

#include <type_traits>

#include <nwd/containers/privates/common.hpp>
#include <nwd/containers/privates/container.hpp>

#include <nwd/utility/storage.hpp>

namespace nwd::containers::privates
{
template<typename Size_type = std::ptrdiff_t>
requires(std::is_signed_v<Size_type>)
struct Vector_storage_data
{
	using size_type = Size_type;
	
protected:
	~Vector_storage_data() = default;
	
public:
	Vector_storage_data() noexcept = default;
	
	Vector_storage_data(const Vector_storage_data& other) noexcept = delete;
	Vector_storage_data& operator=(const Vector_storage_data& other) noexcept = delete;
	
	Vector_storage_data(Vector_storage_data&& other) noexcept
		:
		data_(other.data_),
		capacity_(other.capacity_)
	{
		other.data_ = nullptr;
		other.capacity_ = 0;
	}
	
	Vector_storage_data& operator=(Vector_storage_data&& other) noexcept = delete;
	
	size_type capacity() const noexcept
	{
		return capacity_;
	}
	
	static size_type default_capacity_growth(size_type capacity) noexcept
	{
		return 8 + capacity + capacity / 2;
	}
	
public:
	[[no_unique_address]] std::byte* data_ = nullptr;
	[[no_unique_address]] size_type capacity_ = 0;
};

template<std::size_t Value_size, std::size_t Value_alignment, typename Size_type = std::ptrdiff_t,
	std::size_t Header_size = 0, std::size_t Header_alignment = 0>
struct Vector_storage_base : Container, Vector_storage_data<Size_type>
{
	using typename Vector_storage_data<Size_type>::size_type;
	using typename Container::allocator_type;
	
protected:
	constexpr static std::ptrdiff_t values_offset = trailer_offset(Header_size, Value_alignment);
	
	constexpr std::ptrdiff_t size_for(std::ptrdiff_t capacity) noexcept
	{
		return values_offset + std::ptrdiff_t(Value_size) * capacity;
	}
	
	constexpr static std::ptrdiff_t alignment = std::max(Value_alignment, Header_alignment);
	
public:
	~Vector_storage_base()
	{
		if (this->data_)
		{
			this->allocator_.deallocate_bytes(this->data_, std::size_t(size_for(this->capacity_)), std::size_t(alignment));
		}
	}
	
	explicit Vector_storage_base(allocator_type allocator) noexcept
		:
		Container(allocator)
	{
	}
	
	Vector_storage_base() noexcept = default;
	
	Vector_storage_base(size_type capacity, allocator_type allocator = allocator_type())
		:
		Container(allocator)
	{
		if (capacity > 0)
		{
			this->data_ = static_cast<std::byte*>(this->allocator_.allocate_bytes(size_for(capacity), alignment));
			this->capacity_ = capacity;
		}
	}
	
	Vector_storage_base(Vector_storage_base&& other) noexcept
		:
		Container(static_cast<Container&&>(other)),
		Vector_storage_data<Size_type>(static_cast<Vector_storage_data<Size_type>&&>(other))
	{
	}
	
	Vector_storage_base& operator=(Vector_storage_base&& other) noexcept
	{
		std::destroy_at(this);
		std::construct_at(this, std::move(other));
		return *this;
	}
};

template<typename Type>
inline constexpr std::size_t sizeof_void_specialization = sizeof(Type);

template<>
inline constexpr std::size_t sizeof_void_specialization<void> = 0;

template<typename Type>
inline constexpr std::size_t alignof_void_specialization = alignof(Type);

template<>
inline constexpr std::size_t alignof_void_specialization<void> = 0;

template<typename Value_type, typename Size_type = std::ptrdiff_t, typename Header_type = void>
struct Vector_storage : Vector_storage_base<sizeof(Value_type), alignof(Value_type), Size_type,
	sizeof_void_specialization<Header_type>, alignof_void_specialization<Header_type>>
{
protected:
	using Base = Vector_storage_base<sizeof(Value_type), alignof(Value_type), Size_type,
		sizeof_void_specialization<Header_type>, alignof_void_specialization<Header_type>
	>;
	
	template<typename Type>
	static Type* static_header(auto* data) noexcept
	{
		return utility::reinterpret_aligned_cast<Type>(data);
	}
	
	template<typename Type>
	static Type* static_data(auto* data) noexcept
	{
		if (data)
		{
			return utility::reinterpret_aligned_cast<Type>(data + Base::values_offset);
		}
		
		return nullptr;
	}
	
public:
	using typename Base::size_type;
	using value_type = Value_type;
	using header_type = Header_type;
	
	using reference = value_type&;
	using const_reference = const value_type&;
	
	using Base::Base;
	
	header_type* header() noexcept requires(not std::is_void_v<Header_type>) {return static_header<header_type>(this->data_);}
	const header_type* header() const noexcept requires(not std::is_void_v<Header_type>) {return static_header<const header_type>(this->data_);}
	
	value_type* data() noexcept {return static_data<value_type>(this->data_);}
	const value_type* data() const noexcept {return static_data<const value_type>(this->data_);}
	
	[[nodiscard]]
	std::byte* allocate_storage(size_type new_capacity)
	{
		return static_cast<std::byte*>(this->allocator_.allocate_bytes(std::size_t(Base::size_for(new_capacity)), Base::alignment));
	}
	
	void deallocate_storage(std::byte* data, size_type capacity)
	{
		this->allocator_.deallocate_bytes(data, std::size_t(Base::size_for(capacity)), Base::alignment);
	}
	
	void relocate_storage(size_type new_capacity)
	{
		auto* new_data = allocate_storage(new_capacity);
		
		if (this->data_)
		{
			if constexpr (not std::is_void_v<Header_type>)
			{
				privates::construct_and_destroy(this->allocator_, static_header<header_type>(new_data), std::move(*header()));
			}
			privates::construct_and_destroy_n(this->allocator_, data(), this->capacity_, static_data<value_type>(new_data));
			deallocate_storage(this->data_, Base::size_for(this->capacity_));
		}
		
		this->capacity_ = new_capacity;
		this->data_ = new_data;
	}
};
} // namespace nwd::containers::privates
