#pragma once

#include <cstddef>
#include <cstdint>

#include <limits>
#include <iosfwd>

#include <nwd/containers/repository.hpp>

namespace nwd::containers
{
namespace repositories
{
template<typename Value_type, typename Size_type>
struct Heap_node
{
	[[no_unique_address]] Size_type parent_ = -1;
	[[no_unique_address]] signed char balance_ = 0;
	[[no_unique_address]] Node<Value_type, Size_type> base_;
	
	explicit operator Node<Value_type, Size_type>&() noexcept {return base_;}
	explicit operator const Node<Value_type, Size_type>&() const noexcept {return base_;}
};
} // namespace repositories

template<typename Value_type, typename Size_type = std::ptrdiff_t, typename Compare = std::less<Value_type>>
struct Heap_repository : protected Repository<Value_type, Size_type, repositories::Heap_node<Value_type, Size_type>>
{
private:
	using Base = Repository<Value_type, Size_type, repositories::Heap_node<Value_type, Size_type>>;
	using typename Base::node_type;
	
public:
	using typename Base::size_type;
	using typename Base::value_type;
	using typename Base::allocator_type;
	using value_compare = Compare;
	
	using Base::empty;
	using Base::swap;
	
	Heap_repository() noexcept = default;
	
	explicit Heap_repository(allocator_type allocator, value_compare compare = value_compare()) noexcept
		:
		Base(allocator),
		compare_(compare)
	{
	}
	
	Heap_repository(size_type capacity, allocator_type allocator = std::pmr::get_default_resource(),
		value_compare compare = value_compare())
		:
		Base(capacity, allocator),
		compare_(compare)
	{
	}
	
private:
	auto& static_top(auto& self)
	{
		return self[self.top_];
	}
	
	bool compare(size_type lhs, size_type rhs) const noexcept
	{
		return compare_(*this->node_at(lhs).value_.data(), *this->node_at(rhs).value_.data());
	}
	
	bool insert(size_type inserted, size_type root)
	{
		bool result = compare(root, inserted);
		
		while (true)
		{
			if (result)
			{
				if (auto parent = this->data_at(root).parent_; parent != -1)
				{
					if (auto& prev = this->node_at(parent).indices_[0]; prev == root)
					{
						prev = inserted;
					}
					else if (auto& next = this->node_at(parent).indices_[1]; next == root)
					{
						next = inserted;
					}
				}
				
				if (auto prev = this->node_at(root).indices_[0]; prev != -1)
				{
					this->data_at(prev).parent_ = inserted;
				}
				
				if (auto next = this->node_at(root).indices_[1]; next != -1)
				{
					this->data_at(next).parent_ = inserted;
				}
				
				this->node_at(inserted).indices_[0] = this->node_at(root).indices_[0];
				this->node_at(inserted).indices_[1] = this->node_at(root).indices_[1];
				
				this->data_at(inserted).parent_ = this->data_at(root).parent_;
				this->data_at(inserted).balance_ = this->data_at(root).balance_;
				
				std::swap(inserted, root);
			}
			
			bool desc_index = 1;
			
			if (this->data_at(root).balance_ < 0)
			{
				++this->data_at(root).balance_;
			}
			else
			{
				desc_index = 0;
				--this->data_at(root).balance_;
			}
			
			if (this->node_at(root).indices_[desc_index] == -1)
			{
				this->node_at(root).indices_[desc_index] = inserted;
				
				this->node_at(inserted).indices_[0] = -1;
				this->node_at(inserted).indices_[1] = -1;
				
				this->data_at(inserted).parent_ = root;
				this->data_at(inserted).balance_ = 0;
				
				break;
			}
			else
			{
				root = this->node_at(root).indices_[desc_index];
			}
		}
		
		return result;
	}
	
	size_type move_down(size_type new_top)
	{
		size_type result = new_top;
		
		size_type parent = -1;
		bool parent_larger_index = 0;
		
		if (this->node_at(new_top).indices_[0] != -1 and this->node_at(new_top).indices_[1] != -1)
		{
			if (compare(this->node_at(new_top).indices_[0], this->node_at(new_top).indices_[1]))
			{
				parent_larger_index = 1;
			}
			
			if (auto larger_desc = this->node_at(new_top).indices_[parent_larger_index];
				compare(new_top, larger_desc))
			{
				result = larger_desc;
				
				this->data_at(larger_desc).parent_ = parent;
				
				for (bool i : {0, 1})
				{
					if (auto desc = this->node_at(larger_desc).indices_[i]; desc != -1)
					{
						this->data_at(desc).parent_ = new_top;
					}
				}
				
				{
					auto& smaller_desc = this->node_at(new_top).indices_[not parent_larger_index];
					this->data_at(smaller_desc).parent_ = larger_desc;
					std::swap(smaller_desc, this->node_at(larger_desc).indices_[not parent_larger_index]);
				}
				
				this->node_at(new_top).indices_[parent_larger_index] = this->node_at(larger_desc).indices_[parent_larger_index];
				this->node_at(larger_desc).indices_[parent_larger_index] = new_top;
				this->data_at(new_top).parent_ = larger_desc;
				std::swap(this->data_at(new_top).balance_, this->data_at(larger_desc).balance_);
				
				parent = larger_desc;
			}
		}
		
		while (this->node_at(new_top).indices_[0] != -1 and this->node_at(new_top).indices_[1] != -1)
		{
			bool larger_index = 0;
			
			if (compare(this->node_at(new_top).indices_[0], this->node_at(new_top).indices_[1]))
			{
				larger_index = 1;
			}
			
			if (auto larger_desc = this->node_at(new_top).indices_[larger_index];
				compare(new_top, larger_desc))
			{
				this->data_at(larger_desc).parent_ = parent;
				
				for (bool i : {0, 1})
				{
					if (auto desc = this->node_at(larger_desc).indices_[i]; desc != -1)
					{
						this->data_at(desc).parent_ = new_top;
					}
				}
				
				{
					auto& smaller_desc = this->node_at(new_top).indices_[not larger_index];
					this->data_at(smaller_desc).parent_ = larger_desc;
					std::swap(smaller_desc, this->node_at(larger_desc).indices_[not larger_index]);
				}
				
				this->node_at(new_top).indices_[larger_index] = this->node_at(larger_desc).indices_[larger_index];
				this->node_at(larger_desc).indices_[larger_index] = new_top;
				this->data_at(new_top).parent_ = larger_desc;
				this->node_at(parent).indices_[parent_larger_index] = larger_desc;
				std::swap(this->data_at(new_top).balance_, this->data_at(larger_desc).balance_);
				
				parent_larger_index = larger_index;
				parent = larger_desc;
				
				continue;
			}
			else
			{
				return result;
			}
		}
		
		for (bool i : {0, 1})
		{
			if (auto& desc = this->node_at(new_top).indices_[i]; desc != -1)
			{
				if (compare(new_top, desc))
				{
					if (result == new_top)
					{
						result = desc;
					}
					
					if (parent != -1)
					{
						this->node_at(parent).indices_[parent_larger_index] = desc;
					}
					
					this->data_at(new_top).parent_ = desc;
					this->data_at(desc).parent_ = parent;
					
					this->node_at(desc).indices_[0] = new_top;
					
					std::swap(this->data_at(new_top).balance_, this->data_at(desc).balance_);
					
					desc = -1;
				}
				
				break;
			}
		}
		
		return result;
	}
	
public:
	value_type& top() noexcept {return static_top(*this);}
	const value_type& top() const noexcept {return static_top(*this);}
	
	void push(const value_type& value)
	{
		emplace(value);
	}
	
	void push(value_type&& value)
	{
		emplace(std::move(value));
	}
	
	template<typename... Args>
	void emplace(Args&&... args)
	{
		auto index = Base::emplace(std::forward<Args>(args)...);
		
		if (top_ == -1)
		{
			top_ = index;
		}
		else
		{
			if (insert(index, top_))
			{
				top_ = index;
				this->data_at(top_).parent_ = -1;
			}
		}
		
		++size_;
	}
	
	void pop()
	{
		size_type successor_parent = -1;
		auto successor = top_;
		
		while (true)
		{
			if (this->data_at(successor).balance_ < 0)
			{
				if (this->node_at(successor).indices_[0] != -1)
				{
					++this->data_at(successor).balance_;
					successor_parent = successor;
					successor = this->node_at(successor).indices_[0];
					continue;
				}
			}
			else
			{
				if (this->node_at(successor).indices_[1] != -1)
				{
					--this->data_at(successor).balance_;
					successor_parent = successor;
					successor = this->node_at(successor).indices_[1];
					continue;
				}
			}
			
			break;
		}
		
		if (successor_parent != -1)
		{
			if (this->data_at(successor_parent).balance_ < 0)
			{
				this->node_at(successor_parent).indices_[1] = -1;
			}
			else
			{
				this->node_at(successor_parent).indices_[0] = -1;
			}
			
			this->node_at(successor).indices_[0] = this->node_at(top_).indices_[0];
			this->node_at(successor).indices_[1] = this->node_at(top_).indices_[1];
			
			this->data_at(successor).parent_ = -1;
			this->data_at(successor).balance_ = this->data_at(top_).balance_;
			
			if (auto& prev = this->node_at(successor).indices_[0]; prev != -1)
			{
				this->data_at(prev).parent_ = successor;
			}
			
			if (auto& next = this->node_at(successor).indices_[1]; next != -1)
			{
				this->data_at(next).parent_ = successor;
			}
			
			successor_parent = move_down(successor);
		}
		
		Base::erase(top_);
		
		top_ = successor_parent;
		
		--size_;
	}
	
	size_type size() const noexcept {return size_;}
	
	value_compare& value_comp() noexcept {return compare_;}
	const value_compare& value_comp() const noexcept {return compare_;}
	
	std::ostream& to_dot(std::ostream& os)
	{
		os << "digraph nodes {" << "\n";
		
		const auto print_node = [&](auto i) -> void
		{
			const auto& node = this->data_at(i);
			const auto& node_base = static_cast<const typename Base::Node_base&>(node);
			
			os
			<< "node" << i
			<< "[shape=record,label=\"{"
			<< "value: " << *node_base.value_.data() << "\\n"
			<< "position: " << i << "\\n"
			<< "parent: " << node.parent_ << "\\n"
			<< "balance: " << int(node.balance_) << "\\n"
			<< "|{<l>" << node_base.indices_[0] << "|<r>" << node_base.indices_[1] << "}"
			<< "}\"];\n"
			;
		};
		
		if (top_ != -1)
		{
			print_node(top_);
		}
		
		for (std::ptrdiff_t i = 0; i != this->extent(); ++i)
		{
			if (i != top_ and this->get_if(i))
			{
				print_node(i);
			}
		}
		
		for (std::ptrdiff_t i = 0; i != this->extent(); ++i)
		{
			if (this->get_if(i))
			{
				const auto& node = this->data_at(i);
				const auto& node_base = static_cast<const typename Base::Node_base&>(node);
				
				if (auto parent = node.parent_; parent != -1)
				{
					os << "node" << i << " -> node" << parent << " [style=dotted];\n";
				}
				
				for (std::tuple<size_type, char> array[] = {{node_base.indices_[0], 'l'}, {node_base.indices_[1], 'r'}}; auto [desc, ch] : array)
				{
					if (desc != -1)
					{
						os << "node" << i << ":" << ch << " -> node" << desc << ";\n";
					}
				}
			}
		}
		
		os << "}" << "\n";
		
		return os;
	}
	
	std::ostream&& to_dot(std::ostream&& os)
	{
		return std::move(to_dot(os));
	}
	
private:
	[[no_unique_address]] size_type top_ = -1;
	[[no_unique_address]] size_type size_ = 0;
	[[no_unique_address]] value_compare compare_ = value_compare();
};
} // namespace nwd::containers
