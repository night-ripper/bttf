#pragma once

#include <array>
#include <ranges>
#include <span>

#include <nwd/containers/privates/vector_storage.hpp>

#include <nwd/math/common.hpp>
#include <nwd/utility/integer_cast.hpp>
#include <nwd/utility/iterators.hpp>

namespace nwd::containers
{
template<typename Value_type, typename Size_type = std::ptrdiff_t>
struct Vector_queue : protected privates::Vector_storage<Value_type, Size_type>
{
private:
	using Base = privates::Vector_storage<Value_type, Size_type>;
	
public:
	using value_type = Value_type;
	using size_type = Size_type;
	using typename Base::allocator_type;
	
	using typename Base::reference;
	using typename Base::const_reference;
	
private:
	static auto& static_front(auto& self) noexcept
	{
		return self.data()[self.begin_];
	}
	
	static auto& static_back(auto& self) noexcept
	{
		size_type end = self.end_ == -1 ? self.begin_ : self.end_;
		return self.data()[math::mod_decr(end, self.capacity_)];
	}
	
	void simple_clear() noexcept
	{
		std::ranges::destroy(*this);
	}
	
protected:
	std::array<std::span<value_type>, 2> spans() noexcept
	{
		auto result = std::array<std::span<value_type>, 2>();
		
		if (not empty())
		{
			result[0] = std::span(this->data() + begin_, std::size_t(this->capacity_ - begin_));
			
			if (begin_ <= end_)
			{
				result[0] = result[0].subspan(0, std::size_t(end_ - begin_));
			}
			else
			{
				result[1] = std::span(this->data(), std::size_t(begin_));
				
				if (end_ != -1)
				{
					result[1] = result[1].subspan(0, std::size_t(end_));
				}
			}
		}
		
		return result;
	}
	
public:
	~Vector_queue()
	{
		simple_clear();
	}
	
	Vector_queue() noexcept = default;
	
	explicit Vector_queue(allocator_type allocator) noexcept
		:
		Base(allocator)
	{
	}
	
	Vector_queue(size_type capacity, allocator_type allocator = allocator_type())
		:
		Base(capacity, allocator)
	{
		if (capacity > 0)
		{
			begin_ = 0;
			end_ = 0;
		}
	}
	
	Vector_queue(Vector_queue&& other) noexcept
		:
		Base(static_cast<Base&&>(other))
	{
		this->begin_ = other.begin_;
		other.begin_ = -1;
		this->end_ = other.end_;
		other.end_ = -1;
	}
	
	Vector_queue& operator=(Vector_queue&& other) noexcept
	{
		std::destroy_at(this);
		std::construct_at(this, std::move(other));
		return *this;
	}
	
	void clear()
	{
		simple_clear();
		
		if (begin_ != -1)
		{
			begin_ = 0;
		}
		
		end_ = begin_;
	}
	
	using Base::capacity;
	
	[[nodiscard]] bool empty() const noexcept
	{
		return begin_ == end_;
	}
	
	size_type size() const noexcept
	{
		if (end_ == -1)
		{
			return this->capacity_;
		}
		
		if (end_ < begin_)
		{
			return end_ - (begin_ - this->capacity_);
		}
		
		return end_ - begin_;
	}
	
	reference front() noexcept
	{
		return static_front(*this);
	}
	
	const_reference front() const noexcept
	{
		return static_front(*this);
	}
	
	reference back() noexcept
	{
		return static_back(*this);
	}
	
	const_reference back() const noexcept
	{
		return static_back(*this);
	}
	
	void pop_front(size_type count = 1) noexcept
	{
		if (end_ == -1)
		{
			end_ = begin_;
		}
		
		while (count-- > 0)
		{
			std::destroy_at(this->data() + begin_);
			begin_ = math::mod_incr(begin_, this->capacity_);
		}
	}
	
	void pop_back() noexcept
	{
		auto end = end_;
		
		if (end == -1)
		{
			end = begin_;
		}
		
		end = math::mod_decr(end, this->capacity_);
		
		std::destroy_at(&this->data()[end]);
		
		end_ = end;
	}
	
	void reserve(size_type capacity)
	{
		if (capacity > this->capacity_)
		{
			auto* new_storage = this->Base::allocate_storage(capacity);
			
			if (begin_ != -1)
			{
				privates::construct_and_destroy_n(this->allocator_, begin(), this->size(),
					iterator(begin_, std::span(Base::template static_data<value_type>(new_storage), std::size_t(capacity)))
				);
				end_ = (begin_ + this->size()) % capacity;
			}
			else
			{
				begin_ = 0;
				end_ = 0;
			}
			
			if (this->data_)
			{
				this->Base::deallocate_storage(this->data_, this->capacity_);
			}
			
			this->data_ = new_storage;
			this->capacity_ = capacity;
		}
	}
	
	template<typename... Args>
	value_type& emplace_back(Args&&... args)
	{
		if (end_ == -1)
		{
			reserve(this->Base::default_capacity_growth(this->capacity_));
		}
		
		std::uninitialized_construct_using_allocator(this->data() + end_, this->allocator_, std::forward<Args>(args)...);
		auto& result = this->data()[end_];
		
		end_ = math::mod_incr(end_, this->capacity_);
		
		if (end_ == begin_)
		{
			end_ = -1;
		}
		
		return result;
	}
	
	void push_back(const value_type& value)
	{
		emplace_back(value);
	}
	
	void push_back(value_type&& value)
	{
		emplace_back(std::move(value));
	}
	
	struct iterator : utility::Random_access_iterator<iterator, std::ptrdiff_t>
	{
		using value_type = Value_type;
		using typename utility::Random_access_iterator<iterator, std::ptrdiff_t>::difference_type;
		
		constexpr iterator() noexcept = default;
		
		constexpr iterator(size_type index, std::span<value_type> data) noexcept
			:
			index_(index),
			data_(data)
		{
		}
		
		size_type index_;
		std::span<value_type> data_;
		
		friend bool operator==(iterator lhs, iterator rhs) noexcept
		{
			return lhs.index_ == rhs.index_;
		}
		
		friend auto operator<=>(iterator lhs, iterator rhs) noexcept
		{
			return lhs - rhs <=> 0;
		}
		
		value_type& operator*() const noexcept
		{
			return data_[std::size_t(index_ - bool(index_ >= std::ssize(data_)) * std::ssize(data_))];
		}
		
		iterator& operator+=(difference_type difference) noexcept
		{
			index_ += static_cast<size_type>(difference);
			return *this;
		}
		
		friend difference_type operator-(iterator lhs, iterator rhs) noexcept
		{
			return lhs.index_ - rhs.index_;
		}
	};
	
	iterator begin() noexcept
	{
		return iterator(begin_, std::span<value_type>(this->data(), std::size_t(this->capacity_)));
	}
	
	iterator end() noexcept
	{
		return iterator(begin_ + this->size(), std::span<value_type>(this->data(), std::size_t(this->capacity_)));
	}
	
	constexpr friend size_type erase_if(Vector_queue& object, auto predicate) noexcept
	{
		size_type result = 0;
		auto new_end = object.begin();
		auto end = object.end();
		
		if (new_end != end)
		{
			for (auto it = new_end; ++it != end;)
			{
				if (predicate(*new_end))
				{
					++result;
					*new_end = std::move(*it);
				}
				else
				{
					++new_end;
				}
			}
		}
		
		if (result != 0)
		{
			++new_end;
			object.end_ = new_end.index_;
			
			for (size_type i = 0; i != result; ++i)
			{
				std::destroy_at(&*new_end);
				++new_end;
			}
		}
		
		return result;
	}
	
	constexpr friend size_type erase(Vector_queue& object, const value_type& value) noexcept
	{
		return erase_if(object, [&value](const value_type& other) -> bool
		{
			return value == other;
		});
	}
	
protected:
	size_type begin_ = -1;
	size_type end_ = -1;
};
} // namespace nwd::containers
