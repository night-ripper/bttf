#pragma once

#include <cstddef>
#include <cstdint>

#include <memory>
#include <span>
#include <string_view>

#include <nwd/utility/functor.hpp>
#include <nwd/utility/enum_base.hpp>

namespace nwd::databases
{
namespace deleters
{
void sqlite(void* object) noexcept;
void sqlite_statement(void* object) noexcept;
} // namespace deleters

struct SQLite : protected std::unique_ptr<void, utility::Functor<deleters::sqlite>>
{
	SQLite() = default;
	
	static SQLite create_from_file(const char8_t* filepath);
	static SQLite create_from_memory();
	
	struct Result;
	
	struct Statement : protected std::unique_ptr<void, utility::Functor<deleters::sqlite_statement>>
	{
	protected:
		friend SQLite;
		
		using std::unique_ptr<void, utility::Functor<deleters::sqlite_statement>>::unique_ptr;
		
	public:
		[[nodiscard]]
		auto step() -> Result;
		
		void reset();
		
		auto column_int32(std::int32_t column) const -> std::int32_t;
		auto column_int64(std::int32_t column) const -> std::int64_t;
		auto column_double(std::int32_t column) const -> double;
		
		auto column_text(std::int32_t column) const -> const char*;
		auto column_blob(std::int32_t column) const -> const std::byte*;
		auto column_blob_span(std::int32_t column) const -> std::span<const std::byte>;
		
		//! null
		void bind(std::int32_t index);
		
		void bind(std::int32_t index, std::int32_t value);
		void bind(std::int32_t index, std::int64_t value);
		void bind(std::int32_t index, double value);
		
		//! text
		void bind(std::int32_t index, std::string_view value);
		//! text
		void bind_copy(std::int32_t index, std::string_view value);
		
		//! blob
		void bind(std::int32_t index, std::span<const std::byte> value);
		//! blob
		void bind_copy(std::int32_t index, std::span<const std::byte> value);
	};
	
	Statement prepare(std::string_view& statement) const;
	Statement prepare_single(std::string_view&& statement) const;
	void execute(std::string_view program) const;
};

struct SQLite::Result : utility::Enum_base
{
	using utility::Enum_base::Enum_base;
	
	struct Primary : utility::Enum_base
	{
		using utility::Enum_base::Enum_base;
		
		friend bool operator==(Primary lhs, Result rhs) noexcept;
	};
	
	const static Primary ok;
	const static Primary generic_error;
	const static Primary internal_error;
	const static Primary permission_denied;
	const static Primary callback_abort;
	const static Primary database_busy;
	const static Primary table_locked;
	const static Primary out_of_memory;
	const static Primary database_readonly;
	const static Primary operation_interrupted;
	const static Primary io_error;
	const static Primary database_corrupted;
	const static Primary unknown_opcode;
	const static Primary database_full;
	const static Primary database_opening_error;
	const static Primary lock_protocol_error;
	
	const static Primary database_schema_changed;
	const static Primary value_too_big;
	const static Primary constraint_violation;
	const static Primary data_type_mismatch;
	const static Primary library_misuse;
	const static Primary feature_unsupported;
	const static Primary authorization_denied;
	
	const static Primary bind_parameter_out_of_range;
	const static Primary file_is_not_a_database;
	const static Primary notification;
	const static Primary warning;
	const static Primary row_ready;
	const static Primary step_done;
	
	struct Constraint;
};

struct SQLite::Result::Constraint : SQLite::Result
{
	using Result::Result;
	
	const static Constraint check;
	const static Constraint commit_hook;
	const static Constraint foreign_key;
	const static Constraint extension_function;
	const static Constraint not_null;
	const static Constraint primary_key;
	const static Constraint trigger;
	const static Constraint unique;
	const static Constraint virtual_table;
	const static Constraint rowid_not_unique;
	const static Constraint pinned;
	const static Constraint strict_data_type;
};
} // namespace nwd::databases
