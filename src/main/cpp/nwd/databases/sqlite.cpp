#include <stdexcept>
#include <string>
#include <filesystem>

#include <sqlite3.h>

#include <nwd/databases/sqlite.hpp>
#include <nwd/utility/integer_cast.hpp>

namespace nwd::databases
{
struct SQLite_error : std::runtime_error
{
	using std::runtime_error::runtime_error;
};

bool operator==(SQLite::Result::Primary lhs, SQLite::Result rhs) noexcept
{
	return +lhs == (rhs & 0xff);
}

const SQLite::Result::Primary SQLite::Result::ok = SQLITE_OK;
const SQLite::Result::Primary SQLite::Result::generic_error = SQLITE_ERROR;
const SQLite::Result::Primary SQLite::Result::internal_error = SQLITE_INTERNAL;
const SQLite::Result::Primary SQLite::Result::permission_denied = SQLITE_PERM;
const SQLite::Result::Primary SQLite::Result::callback_abort = SQLITE_ABORT;
const SQLite::Result::Primary SQLite::Result::database_busy = SQLITE_BUSY;
const SQLite::Result::Primary SQLite::Result::table_locked = SQLITE_LOCKED;
const SQLite::Result::Primary SQLite::Result::out_of_memory = SQLITE_NOMEM;
const SQLite::Result::Primary SQLite::Result::database_readonly = SQLITE_READONLY;
const SQLite::Result::Primary SQLite::Result::operation_interrupted = SQLITE_INTERRUPT;
const SQLite::Result::Primary SQLite::Result::io_error = SQLITE_IOERR;
const SQLite::Result::Primary SQLite::Result::database_corrupted = SQLITE_CORRUPT;
const SQLite::Result::Primary SQLite::Result::unknown_opcode = SQLITE_NOTFOUND;
const SQLite::Result::Primary SQLite::Result::database_full = SQLITE_FULL;
const SQLite::Result::Primary SQLite::Result::database_opening_error = SQLITE_CANTOPEN;
const SQLite::Result::Primary SQLite::Result::lock_protocol_error = SQLITE_PROTOCOL;

const SQLite::Result::Primary SQLite::Result::database_schema_changed = SQLITE_SCHEMA;
const SQLite::Result::Primary SQLite::Result::value_too_big = SQLITE_TOOBIG;
const SQLite::Result::Primary SQLite::Result::constraint_violation = SQLITE_CONSTRAINT;
const SQLite::Result::Primary SQLite::Result::data_type_mismatch = SQLITE_MISMATCH;
const SQLite::Result::Primary SQLite::Result::library_misuse = SQLITE_MISUSE;
const SQLite::Result::Primary SQLite::Result::feature_unsupported = SQLITE_NOLFS;
const SQLite::Result::Primary SQLite::Result::authorization_denied = SQLITE_AUTH;

const SQLite::Result::Primary SQLite::Result::bind_parameter_out_of_range = SQLITE_RANGE;
const SQLite::Result::Primary SQLite::Result::file_is_not_a_database = SQLITE_NOTADB;
const SQLite::Result::Primary SQLite::Result::notification = SQLITE_NOTICE;
const SQLite::Result::Primary SQLite::Result::warning = SQLITE_WARNING;
const SQLite::Result::Primary SQLite::Result::row_ready = SQLITE_ROW;
const SQLite::Result::Primary SQLite::Result::step_done = SQLITE_DONE;

const SQLite::Result::Constraint SQLite::Result::Constraint::check = SQLITE_CONSTRAINT_CHECK;
const SQLite::Result::Constraint SQLite::Result::Constraint::commit_hook = SQLITE_CONSTRAINT_COMMITHOOK;
const SQLite::Result::Constraint SQLite::Result::Constraint::foreign_key = SQLITE_CONSTRAINT_FOREIGNKEY;
const SQLite::Result::Constraint SQLite::Result::Constraint::extension_function = SQLITE_CONSTRAINT_FUNCTION;
const SQLite::Result::Constraint SQLite::Result::Constraint::not_null = SQLITE_CONSTRAINT_NOTNULL;
const SQLite::Result::Constraint SQLite::Result::Constraint::primary_key = SQLITE_CONSTRAINT_PRIMARYKEY;
const SQLite::Result::Constraint SQLite::Result::Constraint::trigger = SQLITE_CONSTRAINT_TRIGGER;
const SQLite::Result::Constraint SQLite::Result::Constraint::unique = SQLITE_CONSTRAINT_UNIQUE;
const SQLite::Result::Constraint SQLite::Result::Constraint::virtual_table = SQLITE_CONSTRAINT_VTAB;
const SQLite::Result::Constraint SQLite::Result::Constraint::rowid_not_unique = SQLITE_CONSTRAINT_ROWID;
const SQLite::Result::Constraint SQLite::Result::Constraint::pinned = SQLITE_CONSTRAINT_PINNED;
#ifdef SQLITE_CONSTRAINT_DATATYPE
const SQLite::Result::Constraint SQLite::Result::Constraint::strict_data_type = SQLITE_CONSTRAINT_DATATYPE;
#endif

void deleters::sqlite(void* object) noexcept
{
	sqlite3_close(static_cast<sqlite3*>(object));
}

void deleters::sqlite_statement(void* object) noexcept
{
	sqlite3_finalize(static_cast<sqlite3_stmt*>(object));
}

auto SQLite::Statement::step() -> Result
{
	return sqlite3_step(static_cast<sqlite3_stmt*>(get()));
}

void SQLite::Statement::reset()
{
	if (auto error = sqlite3_reset(static_cast<sqlite3_stmt*>(get()));
		error != SQLITE_OK)
	{
		throw SQLite_error("Error during statement reset, code: " + std::to_string(error));
	}
}

static void check_return_bind(std::int32_t status)
{
	switch (status)
	{
	case SQLITE_OK:
		return;
		
	case SQLITE_RANGE:
		throw SQLite_error("Error during bind: bind parameter out of range");
		
	default:
		throw SQLite_error("Error during bind, code: " + std::to_string(status));
	}
}

void SQLite::Statement::bind(std::int32_t index)
{
	check_return_bind(sqlite3_bind_null(static_cast<sqlite3_stmt*>(get()), index));
}

void SQLite::Statement::bind(std::int32_t index, std::int32_t value)
{
	check_return_bind(sqlite3_bind_int(static_cast<sqlite3_stmt*>(get()), index, value));
}

void SQLite::Statement::bind(std::int32_t index, std::int64_t value)
{
	check_return_bind(sqlite3_bind_int64(static_cast<sqlite3_stmt*>(get()), index, value));
}

void SQLite::Statement::bind(std::int32_t index, double value)
{
	check_return_bind(sqlite3_bind_double(static_cast<sqlite3_stmt*>(get()), index, value));
}

void SQLite::Statement::bind(std::int32_t index, std::string_view value)
{
	check_return_bind(sqlite3_bind_text(static_cast<sqlite3_stmt*>(get()), index,
		value.data(), utility::checked_cast(std::ssize(value)), SQLITE_STATIC)
	);
}

void SQLite::Statement::bind_copy(std::int32_t index, std::string_view value)
{
	check_return_bind(sqlite3_bind_text(static_cast<sqlite3_stmt*>(get()), index,
		value.data(), utility::checked_cast(std::ssize(value)), SQLITE_TRANSIENT)
	);
}

void SQLite::Statement::bind(std::int32_t index, std::span<const std::byte> value)
{
	check_return_bind(sqlite3_bind_blob(static_cast<sqlite3_stmt*>(get()), index,
		value.data(), utility::checked_cast(std::ssize(value)), SQLITE_STATIC)
	);
}

void SQLite::Statement::bind_copy(std::int32_t index, std::span<const std::byte> value)
{
	check_return_bind(sqlite3_bind_blob(static_cast<sqlite3_stmt*>(get()), index,
		value.data(), utility::checked_cast(std::ssize(value)), SQLITE_TRANSIENT)
	);
}

auto SQLite::Statement::column_int32(std::int32_t column) const -> std::int32_t
{
	return sqlite3_column_int(static_cast<sqlite3_stmt*>(get()), column);
}

auto SQLite::Statement::column_int64(std::int32_t column) const -> std::int64_t
{
	return sqlite3_column_int64(static_cast<sqlite3_stmt*>(get()), column);
}

auto SQLite::Statement::column_double(std::int32_t column) const -> double
{
	return sqlite3_column_double(static_cast<sqlite3_stmt*>(get()), column);
}

auto SQLite::Statement::column_text(std::int32_t column) const -> const char*
{
	return reinterpret_cast<const char*>(sqlite3_column_text(static_cast<sqlite3_stmt*>(get()), column));
}

auto SQLite::Statement::column_blob(std::int32_t column) const -> const std::byte*
{
	return static_cast<const std::byte*>(sqlite3_column_blob(static_cast<sqlite3_stmt*>(get()), column));
}

auto SQLite::Statement::column_blob_span(std::int32_t column) const -> std::span<const std::byte>
{
	return std::span(column_blob(column), utility::checked_cast(sqlite3_column_bytes(static_cast<sqlite3_stmt*>(get()), column)));
}

SQLite SQLite::create_from_file(const char8_t* filepath)
{
	sqlite3* resource = nullptr;
	
	if (auto error = sqlite3_open_v2(reinterpret_cast<const char*>(filepath), &resource,
		SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_FULLMUTEX | SQLITE_OPEN_EXRESCODE, nullptr);
		error != SQLITE_OK)
	{
		throw SQLite_error("Could not open database file, code: " + std::to_string(error));
	}
	
	if (resource == nullptr)
	{
		throw SQLite_error("Database handle is nullptr");
	}
	
	auto result = SQLite();
	result.reset(resource);
	
	return result;
}

SQLite SQLite::create_from_memory()
{
	return create_from_file(u8":memory:");
}

auto SQLite::prepare(std::string_view& statement) const -> Statement
{
	sqlite3_stmt* result = nullptr;
	const char* remainder = statement.data();
	
	auto error = sqlite3_prepare_v2(static_cast<sqlite3*>(get()), statement.data(),
		utility::checked_cast(std::ssize(statement)), &result, &remainder
	);
	
	if (error != SQLITE_OK)
	{
		throw SQLite_error("Error during statement preparation, code: "
			+ std::to_string(error) + ", statement: \"" + std::string(statement) + "\""
		);
	}
	
	statement = std::string_view(remainder, statement.end());
	
	return Statement(result);
}

auto SQLite::prepare_single(std::string_view&& statement) const -> Statement
{
	return prepare(statement);
}

void SQLite::execute(std::string_view program) const
{
	while (not program.empty())
	{
		auto statement = prepare(program);
		
		if (statement)
		{
			if (auto result = statement.step(); result != Result::step_done)
			{
				throw SQLite_error("Error during execution, primary code: "
					+ std::to_string(Result::Primary(result))
					+ ", extended code: " + std::to_string(result)
				);
			}
		}
	}
}
} // namespace nwd::databases
