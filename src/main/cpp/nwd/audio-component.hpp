#pragma once

#include "audio/openal.hpp"

#include "geometry/vector3d.hpp"
#include "glue/glsl.hpp"

#include <limits>

namespace bttf
{
struct Audio_component
{
	~Audio_component();
	Audio_component();
	
	Audio_component(Audio_component&&) noexcept = delete;
	Audio_component& operator=(Audio_component&&) noexcept = delete;
	
	static Audio_component* current();
	
	audio::openal::Device device_;
	audio::openal::Context context_;
	geometry::Vector3D listener_position_;
	float orienation_x_ = std::numeric_limits<float>::quiet_NaN();
	float orienation_y_ = std::numeric_limits<float>::quiet_NaN();
};
} // namespace bttf
