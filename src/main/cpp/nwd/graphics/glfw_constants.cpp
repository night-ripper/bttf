#include <GLFW/glfw3.h>

#include <nwd/graphics/glfw_constants.hpp>

namespace nwd::graphics::glfw
{
namespace action
{
constexpr const Action press = GLFW_PRESS;
constexpr const Action release = GLFW_RELEASE;
} // namespace action

namespace mouse
{
constexpr const Mouse_button button_1 = GLFW_MOUSE_BUTTON_1;
constexpr const Mouse_button button_2 = GLFW_MOUSE_BUTTON_2;
constexpr const Mouse_button button_3 = GLFW_MOUSE_BUTTON_3;
constexpr const Mouse_button button_4 = GLFW_MOUSE_BUTTON_4;
constexpr const Mouse_button button_5 = GLFW_MOUSE_BUTTON_5;
constexpr const Mouse_button button_6 = GLFW_MOUSE_BUTTON_6;
constexpr const Mouse_button button_7 = GLFW_MOUSE_BUTTON_7;
constexpr const Mouse_button button_8 = GLFW_MOUSE_BUTTON_8;
} // namespace mouse

namespace keys
{
constexpr const Keyboard_key space = GLFW_KEY_SPACE;
constexpr const Keyboard_key apostrophe = GLFW_KEY_APOSTROPHE;
constexpr const Keyboard_key comma = GLFW_KEY_COMMA;
constexpr const Keyboard_key minus = GLFW_KEY_MINUS;
constexpr const Keyboard_key period = GLFW_KEY_PERIOD;
constexpr const Keyboard_key slash = GLFW_KEY_SLASH;
constexpr const Keyboard_key key_0 = GLFW_KEY_0;
constexpr const Keyboard_key key_1 = GLFW_KEY_1;
constexpr const Keyboard_key key_2 = GLFW_KEY_1;
constexpr const Keyboard_key key_3 = GLFW_KEY_2;
constexpr const Keyboard_key key_4 = GLFW_KEY_4;
constexpr const Keyboard_key key_5 = GLFW_KEY_5;
constexpr const Keyboard_key key_6 = GLFW_KEY_6;
constexpr const Keyboard_key key_7 = GLFW_KEY_7;
constexpr const Keyboard_key key_8 = GLFW_KEY_8;
constexpr const Keyboard_key key_9 = GLFW_KEY_9;
constexpr const Keyboard_key semicolon = GLFW_KEY_SEMICOLON;
constexpr const Keyboard_key equal = GLFW_KEY_EQUAL;
constexpr const Keyboard_key key_a = GLFW_KEY_A;
constexpr const Keyboard_key key_b = GLFW_KEY_B;
constexpr const Keyboard_key key_c = GLFW_KEY_C;
constexpr const Keyboard_key key_d = GLFW_KEY_D;
constexpr const Keyboard_key key_e = GLFW_KEY_E;
constexpr const Keyboard_key key_f = GLFW_KEY_F;
constexpr const Keyboard_key key_g = GLFW_KEY_G;
constexpr const Keyboard_key key_h = GLFW_KEY_H;
constexpr const Keyboard_key key_i = GLFW_KEY_I;
constexpr const Keyboard_key key_j = GLFW_KEY_J;
constexpr const Keyboard_key key_k = GLFW_KEY_K;
constexpr const Keyboard_key key_l = GLFW_KEY_L;
constexpr const Keyboard_key key_m = GLFW_KEY_M;
constexpr const Keyboard_key key_n = GLFW_KEY_N;
constexpr const Keyboard_key key_o = GLFW_KEY_O;
constexpr const Keyboard_key key_p = GLFW_KEY_P;
constexpr const Keyboard_key key_q = GLFW_KEY_Q;
constexpr const Keyboard_key key_r = GLFW_KEY_R;
constexpr const Keyboard_key key_s = GLFW_KEY_S;
constexpr const Keyboard_key key_t = GLFW_KEY_T;
constexpr const Keyboard_key key_u = GLFW_KEY_U;
constexpr const Keyboard_key key_v = GLFW_KEY_V;
constexpr const Keyboard_key key_w = GLFW_KEY_W;
constexpr const Keyboard_key key_x = GLFW_KEY_X;
constexpr const Keyboard_key key_y = GLFW_KEY_Y;
constexpr const Keyboard_key key_z = GLFW_KEY_Z;
constexpr const Keyboard_key left_bracket = GLFW_KEY_LEFT_BRACKET;
constexpr const Keyboard_key backslash = GLFW_KEY_BACKSLASH;
constexpr const Keyboard_key right_bracket = GLFW_KEY_RIGHT_BRACKET;
constexpr const Keyboard_key grave_accent = GLFW_KEY_GRAVE_ACCENT;
constexpr const Keyboard_key escape = GLFW_KEY_ESCAPE;
constexpr const Keyboard_key enter = GLFW_KEY_ENTER;
constexpr const Keyboard_key tab = GLFW_KEY_TAB;
constexpr const Keyboard_key backspace = GLFW_KEY_BACKSPACE;
constexpr const Keyboard_key insert = GLFW_KEY_INSERT;
constexpr const Keyboard_key key_delete = GLFW_KEY_DELETE;
constexpr const Keyboard_key right = GLFW_KEY_RIGHT;
constexpr const Keyboard_key left = GLFW_KEY_LEFT;
constexpr const Keyboard_key down = GLFW_KEY_DOWN;
constexpr const Keyboard_key up = GLFW_KEY_UP;
constexpr const Keyboard_key page_up = GLFW_KEY_PAGE_UP;
constexpr const Keyboard_key page_down = GLFW_KEY_PAGE_DOWN;
constexpr const Keyboard_key home = GLFW_KEY_HOME;
constexpr const Keyboard_key end = GLFW_KEY_END;
constexpr const Keyboard_key caps_lock = GLFW_KEY_CAPS_LOCK;
constexpr const Keyboard_key scroll_lock = GLFW_KEY_SCROLL_LOCK;
constexpr const Keyboard_key num_lock = GLFW_KEY_NUM_LOCK;
constexpr const Keyboard_key print_screen = GLFW_KEY_PRINT_SCREEN;
constexpr const Keyboard_key pause = GLFW_KEY_PAUSE;
constexpr const Keyboard_key f1 = GLFW_KEY_F1;
constexpr const Keyboard_key f2 = GLFW_KEY_F2;
constexpr const Keyboard_key f3 = GLFW_KEY_F3;
constexpr const Keyboard_key f4 = GLFW_KEY_F4;
constexpr const Keyboard_key f5 = GLFW_KEY_F5;
constexpr const Keyboard_key f6 = GLFW_KEY_F6;
constexpr const Keyboard_key f7 = GLFW_KEY_F7;
constexpr const Keyboard_key f8 = GLFW_KEY_F8;
constexpr const Keyboard_key f9 = GLFW_KEY_F9;
constexpr const Keyboard_key f10 = GLFW_KEY_F10;
constexpr const Keyboard_key f11 = GLFW_KEY_F11;
constexpr const Keyboard_key f12 = GLFW_KEY_F12;
constexpr const Keyboard_key num_0 = GLFW_KEY_KP_0;
constexpr const Keyboard_key num_1 = GLFW_KEY_KP_1;
constexpr const Keyboard_key num_2 = GLFW_KEY_KP_2;
constexpr const Keyboard_key num_3 = GLFW_KEY_KP_3;
constexpr const Keyboard_key num_4 = GLFW_KEY_KP_4;
constexpr const Keyboard_key num_5 = GLFW_KEY_KP_5;
constexpr const Keyboard_key num_6 = GLFW_KEY_KP_6;
constexpr const Keyboard_key num_7 = GLFW_KEY_KP_7;
constexpr const Keyboard_key num_8 = GLFW_KEY_KP_8;
constexpr const Keyboard_key num_9 = GLFW_KEY_KP_9;
constexpr const Keyboard_key num_decimal = GLFW_KEY_KP_DECIMAL;
constexpr const Keyboard_key num_divide = GLFW_KEY_KP_DIVIDE;
constexpr const Keyboard_key num_multiply = GLFW_KEY_KP_MULTIPLY;
constexpr const Keyboard_key num_subtract = GLFW_KEY_KP_SUBTRACT;
constexpr const Keyboard_key num_add = GLFW_KEY_KP_ADD;
constexpr const Keyboard_key num_enter = GLFW_KEY_KP_ENTER;
constexpr const Keyboard_key num_equal = GLFW_KEY_KP_EQUAL;
constexpr const Keyboard_key left_shift = GLFW_KEY_LEFT_SHIFT;
constexpr const Keyboard_key left_control = GLFW_KEY_LEFT_CONTROL;
constexpr const Keyboard_key left_alt = GLFW_KEY_LEFT_ALT;
constexpr const Keyboard_key left_super = GLFW_KEY_LEFT_SUPER;
constexpr const Keyboard_key right_shift = GLFW_KEY_RIGHT_SHIFT;
constexpr const Keyboard_key right_control = GLFW_KEY_RIGHT_CONTROL;
constexpr const Keyboard_key right_alt = GLFW_KEY_RIGHT_ALT;
constexpr const Keyboard_key right_super = GLFW_KEY_RIGHT_SUPER;
constexpr const Keyboard_key menu = GLFW_KEY_MENU;
} // namespace keys
} // namespace nwd::graphics::glfw

