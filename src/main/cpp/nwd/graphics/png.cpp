#include <iostream>
#include <streambuf>
#include <memory>
#include <stdexcept>
#include <type_traits>

#include <png.h>

#include <nwd/graphics/png.hpp>
#include <nwd/utility/integer_cast.hpp>

/// For now we only support loading RGBA8 PNG files
namespace nwd::graphics::png
{
struct Libpng_error : std::runtime_error
{
	using std::runtime_error::runtime_error;
};

namespace
{
std::int32_t row_stride(std::int32_t width)
{
	/// RGBA8
	constexpr int bit_depth = 8;
	constexpr int channels = 4;
	
	return width * (bit_depth * channels / 8);
}

void read_data(png_struct* png_struct_p, png_byte* data, png_size_t length)
{
	auto* streambuf = static_cast<std::streambuf*>(png_get_io_ptr(png_struct_p));
	
	streambuf->sgetn(reinterpret_cast<char*>(data), utility::checked_cast(length));
}

void png_error_callback(png_struct*, const char* error_message)
{
	throw Libpng_error(error_message);
}

void png_warning_callback(png_struct*, const char* warning_message)
{
	std::clog << "Libpng warning: " << warning_message << "\n";
}

struct Image_PNG
{
	constexpr static int png_signature_size = 8;
	
	~Image_PNG()
	{
		png_destroy_read_struct(&struct_p_,
			info_p_ ? &info_p_ : nullptr,
			end_info_p_ ? &end_info_p_ : nullptr
		);
	}
	
	Image_PNG()
	{
		struct_p_ = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
		
		if (not struct_p_)
		{
			throw Libpng_error("Could not create a PNG read struct");
		}
		
		png_set_error_fn(struct_p_, nullptr, &png_error_callback, &png_warning_callback);
		
		info_p_ = png_create_info_struct(struct_p_);
		
		if (not info_p_)
		{
			png_destroy_read_struct(&struct_p_, nullptr, nullptr);
			throw Libpng_error("Could not create a PNG info struct");
		}
		
		end_info_p_ = png_create_info_struct(struct_p_);
		
		if (not end_info_p_)
		{
			png_destroy_read_struct(&struct_p_, &info_p_, nullptr);
			throw Libpng_error("Could not create a PNG end info struct");
		}
	}
	
	Image_PNG(const Image_PNG&) = delete;
	Image_PNG& operator=(const Image_PNG&) = delete;
	
	Image_PNG(Image_PNG&& other) noexcept
		:
		width_(other.width_),
		height_(other.height_),
		struct_p_(other.struct_p_),
		info_p_(other.info_p_),
		end_info_p_(other.end_info_p_)
	{
		other.struct_p_ = nullptr;
		other.info_p_ = nullptr;
		other.end_info_p_ = nullptr;
	}
	
	static void read_signature(std::streambuf& streambuf)
	{
		png_byte png_signature[png_signature_size];
		
		streambuf.sgetn(reinterpret_cast<char*>(png_signature), png_signature_size);
		
		if (png_sig_cmp(png_signature, 0, png_signature_size))
		{
			throw Libpng_error("Signature does not match");
		}
	}
	
	void read_info(std::streambuf& streambuf)
	{
		png_set_read_fn(struct_p_, &streambuf, read_data);
		
		png_set_sig_bytes(struct_p_, png_signature_size);
		
		png_read_info(struct_p_, info_p_);
		
		width_ = utility::checked_cast(png_get_image_width(struct_p_, info_p_));
		height_ = utility::checked_cast(png_get_image_height(struct_p_, info_p_));
	}
	
	void set_rgba_8()
	{
		auto bit_depth = png_get_bit_depth(struct_p_, info_p_);
		[[maybe_unused]] auto channels = png_get_channels(struct_p_, info_p_);
		auto color_type = png_get_color_type(struct_p_, info_p_);
		
		if (color_type == PNG_COLOR_TYPE_PALETTE)
		{
			png_set_palette_to_rgb(struct_p_);
		}
		
		if (not (color_type & PNG_COLOR_MASK_ALPHA))
		{
			png_set_add_alpha(struct_p_, -1u, PNG_FILLER_AFTER);
		}
		
		if (color_type == PNG_COLOR_TYPE_GRAY)
		{
			if (bit_depth < 8)
			{
				png_set_expand_gray_1_2_4_to_8(struct_p_);
			}
		}
		
		if (png_get_valid(struct_p_, info_p_, PNG_INFO_tRNS))
		{
			png_set_tRNS_to_alpha(struct_p_);
		}
		
		if (bit_depth == 16)
		{
			png_set_strip_16(struct_p_);
		}
		
		png_read_update_info(struct_p_, info_p_);
		
		/// Ensure that the data are in RGBA8 format
		if (png_get_bit_depth(struct_p_, info_p_) != 8 or png_get_channels(struct_p_, info_p_) != 4)
		{
			throw Libpng_error("Image_PNG is not in RGBA8 format");
		}
	}
	
	void read_row(std::byte* target)
	{
		png_read_row(struct_p_, reinterpret_cast<unsigned char*>(target), nullptr);
	}
	
	template<typename Row_index_function>
	std::byte* read_rows(std::byte* target, Row_index_function row_index_function)
	{
		for (std::int32_t i = 0; i < height_; ++i)
		{
			std::int32_t offset = utility::checked_cast(row_stride(width_) * row_index_function(i));
			read_row(target + offset);
		}
		
		return target + row_stride(width_) * height_;
	}
	
public:
	std::int32_t width_ = 0;
	std::int32_t height_ = 0;
	
	png_structp struct_p_ = nullptr;
	png_infop info_p_ = nullptr;
	png_infop end_info_p_ = nullptr;
};
} // namespace

static Image_PNG read_png_begin(std::streambuf& streambuf)
{
	Image_PNG::read_signature(streambuf);
	Image_PNG image;
	image.read_info(streambuf);
	image.set_rgba_8();
	
	return image;
}

std::byte* read_bottom_up(std::streambuf&& streambuf, std::byte* result)
{
	auto image = read_png_begin(streambuf);
	
	image.read_rows(result,
	[height = image.height_](std::ptrdiff_t index) -> std::ptrdiff_t
	{
		return height - index - 1;
	});
	
	return result;
}

std::byte* read_top_down(std::streambuf&& streambuf, std::byte* result)
{
	auto image = read_png_begin(streambuf);
	
	image.read_rows(result, [](std::ptrdiff_t index) -> std::ptrdiff_t
	{
		return index;
	});
	
	return result;
}

Image_rgba_u_8 read_bottom_up(std::streambuf&& streambuf)
{
	auto image = read_png_begin(streambuf);
	auto result = Image_rgba_u_8(image.width_, image.height_);
	result.data() = std::shared_ptr<std::byte[]>(new std::byte[utility::checked_cast<std::size_t>(result.size_bytes())]);
	
	image.read_rows(result.begin(),
	[height = result.height()](std::ptrdiff_t index) -> std::ptrdiff_t
	{
		return height - index - 1;
	});
	
	return result;
}

Image_rgba_u_8 read_top_down(std::streambuf&& streambuf)
{
	auto image = read_png_begin(streambuf);
	auto result = Image_rgba_u_8(image.width_, image.height_);
	result.data() = std::shared_ptr<std::byte[]>(new std::byte[utility::checked_cast<std::size_t>(result.size_bytes())]);
	
	image.read_rows(result.begin(), [](std::ptrdiff_t index) -> std::ptrdiff_t
	{
		return index;
	});
	
	return result;
}
} // namespace nwd::graphics::png
