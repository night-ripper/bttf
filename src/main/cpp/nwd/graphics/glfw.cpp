#include <climits>
#include <cuchar>
#include <cstring>
#include <cstddef>

#include <iostream>
#include <vector>

#include <GLFW/glfw3.h>

#include <nwd/graphics/glfw.hpp>

namespace nwd::graphics::glfw
{
struct Glfw_error : std::runtime_error
{
	using std::runtime_error::runtime_error;
};

[[gnu::cold]]
static void error_callback(
	[[maybe_unused]] std::int32_t code,
	[[maybe_unused]] const char* message)
{
	throw Glfw_error(message);
}

static bool static_init = []() -> bool
{
	glfwSetErrorCallback(&error_callback);
	return true;
}();

void deleters::context(void* object) noexcept
{
	glfwDestroyWindow(static_cast<GLFWwindow*>(object));
}

void deleters::cursor(void* object) noexcept
{
	glfwDestroyCursor(static_cast<GLFWcursor*>(object));
}

////////////////////////////////////////////////////////////////////////////////

struct Implementation
{
	static GLFWwindow* get(const Context& context) {return static_cast<GLFWwindow*>(context.get());}
	
	static Monitor::Video_mode expose(const GLFWvidmode* video_mode)
	{
		return
		{
			.width_ = video_mode->width,
			.height_ = video_mode->height,
			.red_bits_ = video_mode->redBits,
			.green_bits_ = video_mode->greenBits,
			.blue_bits_ = video_mode->blueBits,
			.refresh_rate_ = video_mode->refreshRate,
		};
	}
	
	static GLFWimage to_glfw_image(const Image_rgba_u_8& image) noexcept
	{
		auto result = GLFWimage();
		result.width = static_cast<int>(image.width());
		result.height = static_cast<int>(image.height());
		result.pixels = reinterpret_cast<unsigned char*>(image.data().get());
		return result;
	}
	
	static Context* user_pointer(GLFWwindow* window)
	{
		return static_cast<Context*>(glfwGetWindowUserPointer(window));
	}
	
	static void window_position_callback(GLFWwindow* window, std::int32_t xpos, std::int32_t ypos)
	{
		user_pointer(window)->window_position_handler_(xpos, ypos);
	}
	
	static void framebuffer_size_callback(GLFWwindow* window, std::int32_t width, std::int32_t height)
	{
		user_pointer(window)->framebuffer_size_handler_(width, height);
	}
	
	static void key_callback(GLFWwindow* window, std::int32_t key, std::int32_t scancode, std::int32_t action, std::int32_t mods)
	{
		user_pointer(window)->key_handler_(key, scancode, action, mods);
	}
	
	static void character_callback(GLFWwindow* window, std::uint32_t codepoint)
	{
		user_pointer(window)->character_handler_(codepoint);
	}
	
	static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
	{
		user_pointer(window)->cursor_position_handler_(xpos, ypos);
	}
	
	static void mouse_button_callback(GLFWwindow* window, std::int32_t button, std::int32_t action, std::int32_t mods)
	{
		user_pointer(window)->mouse_button_handler_(button, action, mods);
	}
	
	static void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
	{
		user_pointer(window)->scroll_handler_(xoffset, yoffset);
	}
};

////////////////////////////////////////////////////////////////////////////////

Cursor::Cursor(const Image_rgba_u_8& image, int xhot, int yhot)
{
	auto glfw_image = Implementation::to_glfw_image(image);
	reset(glfwCreateCursor(&glfw_image, xhot, yhot));
}

std::array<std::int32_t, 2> Monitor::physical_size_mm() const
{
	auto result = std::array<std::int32_t, 2>();
	glfwGetMonitorPhysicalSize(static_cast<GLFWmonitor*>(monitor_), &result[0], &result[1]);
	return result;
}

std::array<float, 2> Monitor::scale() const
{
	auto result = std::array<float, 2>();
	glfwGetMonitorContentScale(static_cast<GLFWmonitor*>(monitor_), &result[0], &result[1]);
	return result;
}

auto Monitor::content() const -> Content
{
	auto result = Content();
	glfwGetMonitorWorkarea(static_cast<GLFWmonitor*>(monitor_), &result.x_, &result.y_, &result.width_, &result.height_);
	return result;
}

std::string_view Monitor::name() const
{
	return glfwGetMonitorName(static_cast<GLFWmonitor*>(monitor_));
}

auto Monitor::Video_mode_iterator::operator*() const noexcept -> value_type
{
	return Implementation::expose(static_cast<const GLFWvidmode*>(video_mode_));
}

auto Monitor::Video_mode_iterator::operator++() noexcept -> Video_mode_iterator&
{
	video_mode_ = static_cast<const GLFWvidmode*>(video_mode_) + 1;
	return *this;
}

auto Monitor::current_video_mode() const -> Video_mode
{
	return Implementation::expose(glfwGetVideoMode(static_cast<GLFWmonitor*>(monitor_)));
}

auto Monitor::video_modes() const -> std::ranges::subrange<Video_mode_iterator, Video_mode_iterator>
{
	std::int32_t count = 0;
	const GLFWvidmode* video_modes = glfwGetVideoModes(static_cast<GLFWmonitor*>(monitor_), &count);
	return std::ranges::subrange(Video_mode_iterator(video_modes), Video_mode_iterator(video_modes + count));
}

static_assert(std::forward_iterator<Monitor::Video_mode_iterator>);

Library::~Library()
{
	glfwTerminate();
}

Library::Library()
{
	if (not glfwInit())
	{
		throw Glfw_error("Could not initialize library");
	}
}

const Library::Version Library::version_compiled = {GLFW_VERSION_MAJOR, GLFW_VERSION_MINOR, GLFW_VERSION_REVISION};

auto Library::version_runtime() -> Version
{
	Version result;
	glfwGetVersion(&result.major_, &result.minor_, &result.revision_);
	return result;
}

auto Library::version_runtime_string() -> std::string_view
{
	return glfwGetVersionString();
}

auto Library::Monitor_iterator::operator*() const noexcept -> value_type
{
	return Monitor(*static_cast<GLFWmonitor* const*>(monitor_));
}

auto Library::Monitor_iterator::operator++() noexcept -> Monitor_iterator&
{
	monitor_ = static_cast<const GLFWmonitor* const*>(monitor_) + 1;
	return *this;
}

auto Library::primary_monitor() const -> Monitor
{
	return glfwGetPrimaryMonitor();
}

auto Library::monitors() const -> std::ranges::subrange<Monitor_iterator, Monitor_iterator>
{
	std::int32_t count = 0;
	GLFWmonitor** monitors = glfwGetMonitors(&count);
	return std::ranges::subrange(Monitor_iterator(monitors), Monitor_iterator(monitors + count));
}

void Library::window_hint(Context::Hint hint)
{
	glfwWindowHint(hint.hint_, hint.value_);
}

auto Library::create_context(std::int32_t width, std::int32_t height, const char* title) -> Context
{
	auto* result = glfwCreateWindow(width, height, title, nullptr, nullptr);
	
	if (not result)
	{
		throw Glfw_error("Could not create a window");
	}
	
	return Context(result);
}

const Context::Hint Context::Hint::no_api = {GLFW_CLIENT_API, GLFW_NO_API};

const Context::Hint Context::Hint::opengl_debug_profile = {GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE};
const Context::Hint Context::Hint::opengl_no_error_profile = {GLFW_CONTEXT_NO_ERROR, GLFW_TRUE};

const Context::Hint Context::Hint::opengl_forward_compatibility = {GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE};
const Context::Hint Context::Hint::opengl_core_profile = {GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE};

const Context::Hint Context::Hint::srgb_capable = {GLFW_SRGB_CAPABLE, GLFW_TRUE};

Context::Hint Context::Hint::visible(bool value)
{
	return {GLFW_VISIBLE, value};
}

Context::Hint Context::Hint::maximized(bool value)
{
	return {GLFW_MAXIMIZED, value};
}

Context::Hint Context::Hint::samples(std::int32_t samples)
{
	return {GLFW_SAMPLES, samples};
}

Context::Hint Context::Hint::opengl_major_version(std::int32_t major)
{
	return {GLFW_CONTEXT_VERSION_MAJOR, major};
}

Context::Hint Context::Hint::opengl_minor_version(std::int32_t minor)
{
	return {GLFW_CONTEXT_VERSION_MINOR, minor};
}

Context::Context(void* context) noexcept
	:
	unique_ptr(context)
{
	glfwSetWindowUserPointer(Implementation::get(*this), this);
}

void Context::make_current() const
{
	glfwMakeContextCurrent(Implementation::get(*this));
}

std::array<std::int32_t, 2> Context::window_position() const
{
	std::int32_t xpos, ypos;
	glfwGetWindowPos(Implementation::get(*this), &xpos, &ypos);
	return {xpos, ypos};
}

std::array<std::int32_t, 4> Context::frame_size() const
{
	std::int32_t left, top, right, bottom;
	glfwGetWindowFrameSize(Implementation::get(*this), &left, &top, &right, &bottom);
	return {left, top, right, bottom};
}

std::array<std::int32_t, 2> Context::framebuffer_size() const
{
	std::int32_t width, height;
	glfwGetFramebufferSize(Implementation::get(*this), &width, &height);
	return {width, height};
}

void Context::poll_events()
{
	glfwPollEvents();
}

void Context::swap_buffers()
{
	glfwSwapBuffers(Implementation::get(*this));
}

bool Context::should_close() const
{
	return glfwWindowShouldClose(Implementation::get(*this));
}

std::string Context::clipboard() const
{
	return glfwGetClipboardString(Implementation::get(*this));
}

void Context::clipboard(const char* string)
{
	glfwSetClipboardString(Implementation::get(*this), string);
}

std::array<double, 2> Context::cursor_position() const
{
	double x, y;
	glfwGetCursorPos(Implementation::get(*this), &x, &y);
	return {x, y};
}

void Context::cursor_position(double x, double y)
{
	glfwSetCursorPos(Implementation::get(*this), x, y);
}

void Context::enable_cursor()
{
	glfwSetInputMode(Implementation::get(*this), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

void Context::disable_cursor()
{
	glfwSetInputMode(Implementation::get(*this), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void Context::set_fullscreen(Monitor monitor)
{
	const GLFWvidmode* mode = glfwGetVideoMode(static_cast<GLFWmonitor*>(monitor.monitor_));
	
	glfwSetWindowMonitor(Implementation::get(*this), static_cast<GLFWmonitor*>(monitor.monitor_), 0, 0, mode->width, mode->height, mode->refreshRate);
}

void Context::set_windowed(std::int32_t x, std::int32_t y, std::int32_t width, std::int32_t height)
{
	glfwSetWindowMonitor(Implementation::get(*this), nullptr, x, y, width, height, {});
}

void Context::set_icon(const std::span<const Image_rgba_u_8> icons)
{
	auto images = std::vector<GLFWimage>(icons.size());
	
	for (std::size_t i = 0; i != std::size(icons); ++i)
	{
		images[i] = Implementation::to_glfw_image(icons[i]);
	}
	
	glfwSetWindowIcon(Implementation::get(*this), static_cast<int>(std::ssize(icons)), std::data(images));
}


void Context::set_cursor(Cursor&& cursor)
{
	glfwSetCursor(Implementation::get(*this), static_cast<GLFWcursor*>(cursor.get()));
	cursor_ = std::move(cursor);
}

////////////////////////////////////////////////////////////////////////////////

template<typename Handler_type, typename Glfw_function, typename Static_function>
static Handler_type handler_helper(const Context& context, Handler_type&& new_handler,
	Handler_type& old_handler, Glfw_function glfw_function, Static_function static_function)
{
	glfw_function(Implementation::get(context), new_handler ? static_function : nullptr);
	
	return std::exchange(old_handler, std::move(new_handler));
}

auto Context::window_position_handler(Handler::Window_position handler) -> Handler::Window_position
{
	return handler_helper(*this, std::move(handler),
		window_position_handler_,
		glfwSetWindowPosCallback,
		Implementation::window_position_callback
	);
}

auto Context::framebuffer_size_handler(Handler::Framebuffer_size handler) -> Handler::Framebuffer_size
{
	return handler_helper(*this, std::move(handler),
		framebuffer_size_handler_,
		glfwSetFramebufferSizeCallback,
		Implementation::framebuffer_size_callback
	);
}

auto Context::key_handler(Handler::Key handler) -> Handler::Key
{
	return handler_helper(*this, std::move(handler),
		key_handler_,
		glfwSetKeyCallback,
		Implementation::key_callback
	);
}

auto Context::character_handler(Handler::Character handler) -> Handler::Character
{
	return handler_helper(*this, std::move(handler),
		character_handler_,
		glfwSetCharCallback,
		Implementation::character_callback
	);
}

auto Context::cursor_position_handler(Handler::Cursor_position handler) -> Handler::Cursor_position
{
	return handler_helper(*this, std::move(handler),
		cursor_position_handler_,
		glfwSetCursorPosCallback,
		Implementation::cursor_position_callback
	);
}

auto Context::mouse_button_handler(Handler::Mouse_button handler) -> Handler::Mouse_button
{
	return handler_helper(*this, std::move(handler),
		mouse_button_handler_,
		glfwSetMouseButtonCallback,
		Implementation::mouse_button_callback
	);
}

auto Context::scroll_handler(Handler::Scroll handler) -> Handler::Scroll
{
	return handler_helper(*this, std::move(handler),
		scroll_handler_,
		glfwSetScrollCallback,
		Implementation::scroll_callback
	);
}

static_assert(std::forward_iterator<Monitor::Video_mode_iterator>);
static_assert(std::ranges::forward_range<std::ranges::subrange<Monitor::Video_mode_iterator, Monitor::Video_mode_iterator>>);

static_assert(std::forward_iterator<Library::Monitor_iterator>);
static_assert(std::ranges::forward_range<std::ranges::subrange<Library::Monitor_iterator, Library::Monitor_iterator>>);
} // namespace nwd::graphics::glfw
