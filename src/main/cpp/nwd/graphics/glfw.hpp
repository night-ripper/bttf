#pragma once

#include <array>
#include <functional>
#include <memory>
#include <memory_resource>
#include <ranges>
#include <span>
#include <string>
#include <vector>

#include <nwd/graphics/image.hpp>

#include <nwd/utility/functor.hpp>
#include <nwd/utility/iterators.hpp>
#include <nwd/utility/enum_base.hpp>

namespace nwd::graphics::glfw
{
namespace deleters
{
void context(void* object) noexcept;
void cursor(void* object) noexcept;
} // namespace deleters

struct Library;
struct Context;

struct Action : utility::Enum_base
{
	using Enum_base::Enum_base;
};

struct Mouse_button : utility::Enum_base
{
	using Enum_base::Enum_base;
};

struct Keyboard_key : utility::Enum_base
{
	using Enum_base::Enum_base;
};

struct Cursor : protected std::unique_ptr<void, utility::Functor<deleters::cursor>>
{
	friend Context;
	
	Cursor() = default;
	
	Cursor(const Image_rgba_u_8& image, int xhot = 0, int yhot = 0);
};

struct Monitor
{
	std::array<std::int32_t, 2> physical_size_mm() const;
	std::array<float, 2> scale() const;
	
	// The area of a monitor not occupied by global task bars or menu bars
	struct Content
	{
		std::int32_t x_;
		std::int32_t y_;
		std::int32_t width_;
		std::int32_t height_;
	};
	
	struct Video_mode
	{
		std::int32_t width_;
		std::int32_t height_;
		std::int32_t red_bits_;
		std::int32_t green_bits_;
		std::int32_t blue_bits_;
		std::int32_t refresh_rate_;
	};
	
	struct Video_mode_iterator : utility::Forward_iterator<Video_mode_iterator>
	{
		using value_type = Video_mode;
		using difference_type = std::ptrdiff_t;
		
		Video_mode_iterator() noexcept = default;
		Video_mode_iterator(const void* video_modes) noexcept
			:
			video_mode_(video_modes)
		{
		}
		
		value_type operator*() const noexcept;
		Video_mode_iterator& operator++() noexcept;
		using Forward_iterator::operator++;
		
		friend bool operator==(Video_mode_iterator lhs, Video_mode_iterator rhs) noexcept
		{
			return lhs.video_mode_ == rhs.video_mode_;
		}
		
	private:
		const void* video_mode_ = nullptr;
	};
	
	Content content() const;
	std::string_view name() const;
	Video_mode current_video_mode() const;
	std::ranges::subrange<Video_mode_iterator, Video_mode_iterator> video_modes() const;
	
private:
	Monitor(void* monitor) noexcept
		:
		monitor_(monitor)
	{
	}
	
private:
	friend Library;
	friend Context;
	friend struct Implementation;
	
private:
	void* monitor_ = nullptr;
};

struct Context : protected std::unique_ptr<void, utility::Functor<deleters::context>>
{
	struct Hint
	{
		std::int32_t hint_;
		std::int32_t value_;
		
		const static Hint no_api;
		
		const static Hint opengl_debug_profile;
		const static Hint opengl_no_error_profile;
		
		const static Hint opengl_forward_compatibility;
		const static Hint opengl_core_profile;
		
		const static Hint srgb_capable;
		
		static Hint visible(bool value);
		static Hint maximized(bool value);
		
		static Hint samples(std::int32_t samples);
		
		static Hint opengl_major_version(std::int32_t major);
		static Hint opengl_minor_version(std::int32_t minor);
	};
	
	void make_current() const;
	
	std::array<std::int32_t, 2> window_position() const;
	std::array<std::int32_t, 4> frame_size() const;
	std::array<std::int32_t, 2> framebuffer_size() const;
	
	void poll_events();
	void swap_buffers();
	bool should_close() const;
	
	std::string clipboard() const;
	void clipboard(const char* string);
	
	std::array<double, 2> cursor_position() const;
	void cursor_position(double x, double y);
	
	void enable_cursor();
	void disable_cursor();
	
	void set_fullscreen(Monitor monitor);
	void set_windowed(std::int32_t x, std::int32_t y, std::int32_t width, std::int32_t height);
	
	void set_icon(std::span<const Image_rgba_u_8> icons);
	void set_cursor(Cursor&& cursor);
	
	// Namespacing struct
	struct Handler
	{
		using Window_position = std::move_only_function<void(std::int32_t xpos, std::int32_t ypos)>;
		using Framebuffer_size = std::move_only_function<void(std::int32_t width, std::int32_t height)>;
		using Key = std::move_only_function<void(Keyboard_key key, std::int32_t scancode, Action action, std::int32_t modifier_keys)>;
		using Character = std::move_only_function<void(std::uint32_t codepoint)>;
		using Cursor_position = std::move_only_function<void(double xpos, double ypos)>;
		using Mouse_button = std::move_only_function<void(glfw::Mouse_button button, Action action, std::int32_t mods)>;
		using Scroll = std::move_only_function<void(double xoffset, double yoffset)>;
	};
	
	Handler::Window_position window_position_handler(Handler::Window_position handler = {});
	Handler::Framebuffer_size framebuffer_size_handler(Handler::Framebuffer_size handler = {});
	Handler::Key key_handler(Handler::Key handler = {});
	Handler::Character character_handler(Handler::Character handler = {});
	Handler::Cursor_position cursor_position_handler(Handler::Cursor_position handler = {});
	Handler::Mouse_button mouse_button_handler(Handler::Mouse_button handler = {});
	Handler::Scroll scroll_handler(Handler::Scroll handler = {});
	
private:
	friend Library;
	friend struct Implementation;
	
	Context(void* context) noexcept;
	
	Context() noexcept = delete;
	Context(Context&& other) noexcept = delete;
	Context& operator=(Context&& other) noexcept = delete;
	
private:
	Handler::Window_position window_position_handler_;
	Handler::Framebuffer_size framebuffer_size_handler_;
	Handler::Key key_handler_;
	Handler::Character character_handler_;
	Handler::Cursor_position cursor_position_handler_;
	Handler::Mouse_button mouse_button_handler_;
	Handler::Scroll scroll_handler_;
	Cursor cursor_;
};

struct Library
{
	~Library();
	
	Library();
	
	struct Version
	{
		std::int32_t major_;
		std::int32_t minor_;
		std::int32_t revision_;
	};
	
	static const Version version_compiled;
	static Version version_runtime();
	static std::string_view version_runtime_string();
	
	struct Monitor_iterator : utility::Forward_iterator<Monitor_iterator>
	{
		using value_type = Monitor;
		using difference_type = std::ptrdiff_t;
		
		Monitor_iterator() noexcept = default;
		Monitor_iterator(const void* monitors) noexcept
			:
			monitor_(monitors)
		{
		}
		
		value_type operator*() const noexcept;
		Monitor_iterator& operator++() noexcept;
		using Forward_iterator::operator++;
		
		friend bool operator==(Monitor_iterator lhs, Monitor_iterator rhs) noexcept
		{
			return lhs.monitor_ == rhs.monitor_;
		}
		
	private:
		const void* monitor_ = nullptr;
	};
	
	Monitor primary_monitor() const;
	std::ranges::subrange<Monitor_iterator, Monitor_iterator> monitors() const;
	
	void window_hint(Context::Hint);
	
	Context create_context(std::int32_t width, std::int32_t height, const char* title);
};
} // namespace nwd::graphics::glfw
