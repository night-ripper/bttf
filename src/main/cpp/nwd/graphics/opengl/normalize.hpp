#pragma once

#include <cmath>
#include <cstdint>

#include <limits>
#include <type_traits>

/// OpenGL defines the range as [-MAX, +MAX], see
/// https://www.khronos.org/opengl/wiki/Normalized_Integer
namespace nwd::graphics::opengl
{
namespace privates
{
struct Normalized_max
{
	template<typename Target_type>
	constexpr operator Target_type() && noexcept
	{
		return std::numeric_limits<Target_type>::max();
	}
};

struct Normalized_min
{
	template<typename Target_type>
	constexpr operator Target_type() && noexcept
	{
		if constexpr (std::is_signed_v<Target_type>)
		{
			return -std::numeric_limits<Target_type>::max();
		}
		else
		{
			return 0;
		}
	}
};

struct Normalized
{
	template<typename Type> constexpr static float max_value = float(Type(Normalized_max()));
	template<typename Type> constexpr static float min_value = float(Type(Normalized_min()));
	
	Normalized() = default;
	
	constexpr Normalized(float value) noexcept
		:
		value_(value)
	{
	}
	
	template<typename Target_type>
	constexpr operator Target_type() && noexcept
	{
		return Target_type(value_ * float(std::numeric_limits<Target_type>::max()));
	}
	
private:
	float value_;
};
} // namespace privates

template<typename Type = privates::Normalized_max>
constexpr Type nrm_max() noexcept
{
	return Type(privates::Normalized_max());
}

template<typename Type = privates::Normalized_min>
constexpr Type nrm_min() noexcept
{
	return Type(privates::Normalized_min());
}

template<typename Type = privates::Normalized>
constexpr Type nrm(float value) noexcept
{
	return Type(privates::Normalized(value));
}
} // namespace nwd::graphics::opengl
