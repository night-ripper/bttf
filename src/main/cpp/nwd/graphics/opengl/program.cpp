#include <cstring>

#include <iostream>
#include <memory>
#include <memory_resource>
#include <fstream>
#include <string>

#include <epoxy/gl.h>

#include <nwd/graphics/opengl/program.hpp>
#include <nwd/utility/integer_cast.hpp>

namespace nwd::graphics::opengl
{
namespace
{
struct Program_error : std::runtime_error
{
	using std::runtime_error::runtime_error;
};
} // namespace

////////////////////////////////////////////////////////////////////////////////

template<>
void Program::uniform(std::int32_t location, float v1)
{glProgramUniform1f(this->get(), location, v1);}

template<>
void Program::uniform(std::int32_t location, float v1, float v2)
{glProgramUniform2f(this->get(), location, v1, v2);}

template<>
void Program::uniform(std::int32_t location, float v1, float v2, float v3)
{glProgramUniform3f(this->get(), location, v1, v2, v3);}

template<>
void Program::uniform(std::int32_t location, float v1, float v2, float v3, float v4)
{glProgramUniform4f(this->get(), location, v1, v2, v3, v4);}

template<>
void Program::uniform(std::int32_t location, std::int32_t v1)
{glProgramUniform1i(this->get(), location, v1);}

template<>
void Program::uniform(std::int32_t location, std::int32_t v1, std::int32_t v2)
{glProgramUniform2i(this->get(), location, v1, v2);}

template<>
void Program::uniform(std::int32_t location, std::int32_t v1, std::int32_t v2, std::int32_t v3)
{glProgramUniform3i(this->get(), location, v1, v2, v3);}

template<>
void Program::uniform(std::int32_t location, std::int32_t v1, std::int32_t v2, std::int32_t v3, std::int32_t v4)
{glProgramUniform4i(this->get(), location, v1, v2, v3, v4);}

template<>
void Program::uniform(std::int32_t location, std::uint32_t v1)
{glProgramUniform1ui(this->get(), location, v1);}

template<>
void Program::uniform(std::int32_t location, std::uint32_t v1, std::uint32_t v2)
{glProgramUniform2ui(this->get(), location, v1, v2);}

template<>
void Program::uniform(std::int32_t location, std::uint32_t v1, std::uint32_t v2, std::uint32_t v3)
{glProgramUniform3ui(this->get(), location, v1, v2, v3);}

template<>
void Program::uniform(std::int32_t location, std::uint32_t v1, std::uint32_t v2, std::uint32_t v3, std::uint32_t v4)
{glProgramUniform4ui(this->get(), location, v1, v2, v3, v4);}

////////////////////////////////////////////////////////////////////////////////

template<>
void Program::uniform_matrix<2, 2>(std::int32_t location, const float* matrices, std::int32_t count, bool transpose)
{glProgramUniformMatrix2fv(this->get(), location, count, transpose, matrices);}

template<>
void Program::uniform_matrix<2, 3>(std::int32_t location, const float* matrices, std::int32_t count, bool transpose)
{glProgramUniformMatrix2x3fv(this->get(), location, count, transpose, matrices);}

template<>
void Program::uniform_matrix<2, 4>(std::int32_t location, const float* matrices, std::int32_t count, bool transpose)
{glProgramUniformMatrix2x4fv(this->get(), location, count, transpose, matrices);}

template<>
void Program::uniform_matrix<3, 2>(std::int32_t location, const float* matrices, std::int32_t count, bool transpose)
{glProgramUniformMatrix3x2fv(this->get(), location, count, transpose, matrices);}

template<>
void Program::uniform_matrix<3, 3>(std::int32_t location, const float* matrices, std::int32_t count, bool transpose)
{glProgramUniformMatrix3fv(this->get(), location, count, transpose, matrices);}

template<>
void Program::uniform_matrix<3, 4>(std::int32_t location, const float* matrices, std::int32_t count, bool transpose)
{glProgramUniformMatrix3x4fv(this->get(), location, count, transpose, matrices);}

template<>
void Program::uniform_matrix<4, 2>(std::int32_t location, const float* matrices, std::int32_t count, bool transpose)
{glProgramUniformMatrix4x2fv(this->get(), location, count, transpose, matrices);}

template<>
void Program::uniform_matrix<4, 3>(std::int32_t location, const float* matrices, std::int32_t count, bool transpose)
{glProgramUniformMatrix4x3fv(this->get(), location, count, transpose, matrices);}

template<>
void Program::uniform_matrix<4, 4>(std::int32_t location, const float* matrices, std::int32_t count, bool transpose)
{glProgramUniformMatrix4fv(this->get(), location, count, transpose, matrices);}

void Program::uniform_handle(std::int32_t location, std::uint64_t value)
{
	glProgramUniformHandleui64ARB(this->get(), location, value);
}

////////////////////////////////////////////////////////////////////////////////

void deleters::shader_base(std::uint32_t object) noexcept
{
	glDeleteShader(object);
}

void Shader_base::compile(std::span<std::string_view> sources)
{
	const std::size_t count = std::size(sources);
	
	auto source_strings = std::make_unique<const char*[]>(static_cast<std::size_t>(count));
	auto lengths = std::make_unique<std::int32_t[]>(static_cast<std::size_t>(count));
	
	for (std::size_t i = 0; i < count; ++i)
	{
		source_strings[i] = sources[i].data();
		lengths[i] = utility::checked_cast(std::ssize(sources[i]));
	}
	
	glShaderSource(this->get(), utility::checked_cast(count), source_strings.get(), lengths.get());
	
	std::int32_t info_log_length = 0;
	const char* prefix = "";
	std::string info_log;
	
	try
	{
		GLint success = 0;
		glCompileShader(this->get());
		glGetShaderiv(this->get(), GL_COMPILE_STATUS, &success);
		glGetShaderiv(this->get(), GL_INFO_LOG_LENGTH, &info_log_length);
		info_log.append(utility::checked_cast(info_log_length), '\0');
		
		if (not success)
		{
			prefix = "OpenGL program compile error: ";
			
			glGetShaderInfoLog(this->get(), info_log_length, nullptr, info_log.data());
			
			throw std::runtime_error(prefix + info_log);
		}
		else if (info_log_length > 0)
		{
			std::clog << "OpenGL compile message: " << info_log;
		}
	}
	catch (...)
	{
		throw;
	}
}

void Shader_base::compile(std::initializer_list<std::istream*> iss)
{
	const auto count = std::size(iss);
	
	auto sources = std::make_unique<std::string[]>(count);
	auto source_views = std::make_unique<std::string_view[]>(count);
	
	for (std::size_t i = 0; i < count; ++i)
	{
		if (not std::begin(iss)[i]->good())
		{
			throw Program_error("Opening an empty stream");
		}
		
		sources[i] = std::string(
			std::istreambuf_iterator(*std::begin(iss)[i]),
			std::istreambuf_iterator<char>()
		);
		source_views[i] = sources[i];
	}
	
	return compile(std::span(source_views.get(), count));
}

namespace
{
template<typename> extern const std::uint32_t shader_enum;

template<> constexpr std::uint32_t shader_enum<Vertex_shader> = GL_VERTEX_SHADER;
template<> constexpr std::uint32_t shader_enum<Geometry_shader> = GL_GEOMETRY_SHADER;
template<> constexpr std::uint32_t shader_enum<Fragment_shader> = GL_FRAGMENT_SHADER;
} // namespace

template<typename Type>
Shader<Type> Shader<Type>::create()
{
	auto result = Shader<Type>();
	result.reset(glCreateShader(shader_enum<Type>));
	return result;
}

template struct Shader<Vertex_shader>;
template struct Shader<Geometry_shader>;
template struct Shader<Fragment_shader>;

Program Program::create()
{
	auto result = Program();
	result.reset(glCreateProgram());
	return result;
}

void deleters::program(std::uint32_t object) noexcept
{
	glDeleteProgram(object);
}

Program::Program(std::span<const Shader_base* const> span)
	:
	Program(Program::create())
{
	for (const auto* shader : span)
	{
		glAttachShader(this->get(), shader->get());
	}
	
	std::int32_t info_log_length = 0;
	
	try
	{
		GLint success = 0;
		glLinkProgram(this->get());
		glGetProgramiv(this->get(), GL_LINK_STATUS, &success);
		glGetProgramiv(this->get(), GL_INFO_LOG_LENGTH, &info_log_length);
		
		if (not success)
		{
			throw std::runtime_error("");
		}
	}
	catch (std::exception& ex)
	{
		char message[256] = "OpenGL program link error: ";
		
		auto length = std::strlen(message);
		
		glGetProgramInfoLog(this->get(),
			utility::checked_cast((std::ssize(message) - length) * sizeof(*message)),
			nullptr, &message[length]
		);
		
		throw Program_error(std::string(ex.what()) + message);
	}
	
	for (const auto* shader : span)
	{
		glDetachShader(this->get(), shader->get());
	}
}

void Program::use() const
{
	glUseProgram(this->get());
}
} // namespace nwd::graphics::opengl
