#pragma once

#include <cstdint>

namespace nwd::graphics::opengl
{
template<typename> extern const std::uint32_t enum_of;
} // namespace nwd::graphics::opengl
