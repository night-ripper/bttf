#include <epoxy/gl.h>

#include <nwd/graphics/opengl/buffers.hpp>

namespace nwd::graphics::opengl
{
static std::uint32_t create_buffer()
{
	auto object = std::uint32_t(0);
	glCreateBuffers(1, &object);
	return object;
}

void deleters::buffer(std::uint32_t object) noexcept
{
	glDeleteBuffers(1, &object);
}

Buffer Buffer::create()
{
	auto result = Buffer();
	result.reset(create_buffer());
	return result;
}

void Buffer::data(std::span<const std::byte> data)
{
	glNamedBufferData(get(), std::size(data), std::data(data), GL_DYNAMIC_DRAW);
}

void deleters::vertex_array(std::uint32_t object) noexcept
{
	glDeleteVertexArrays(1, &object);
}

Vertex_array Vertex_array::create()
{
	auto object = std::uint32_t(0);
	glCreateVertexArrays(1, &object);
	auto result = Vertex_array();
	result.reset(object);
	return result;
}

void Vertex_array::format_bind_data_buffer(std::uint32_t attribute_index, std::int32_t size,
	std::uint32_t type, bool normalize, std::uint32_t relative_offset,
	const Buffer& buffer, std::intptr_t offset, std::int32_t stride) const
{
	glEnableVertexArrayAttrib(get(), attribute_index);
	glVertexArrayAttribBinding(get(), attribute_index, attribute_index);
	glVertexArrayAttribFormat(get(), attribute_index, size, type, normalize, relative_offset);
	glVertexArrayVertexBuffer(get(), attribute_index, buffer.get(), offset, stride);
}

void Vertex_array::format_i_bind_data_buffer(std::uint32_t attribute_index, std::int32_t size,
	std::uint32_t type, std::uint32_t relative_offset,
	const Buffer& buffer, std::intptr_t offset, std::int32_t stride) const
{
	glEnableVertexArrayAttrib(get(), attribute_index);
	glVertexArrayAttribBinding(get(), attribute_index, attribute_index);
	glVertexArrayAttribIFormat(get(), attribute_index, size, type, relative_offset);
	glVertexArrayVertexBuffer(get(), attribute_index, buffer.get(), offset, stride);
}

void Vertex_array::format_l_bind_data_buffer(std::uint32_t attribute_index, std::int32_t size,
	std::uint32_t type, std::uint32_t relative_offset,
	const Buffer& buffer, std::intptr_t offset, std::int32_t stride) const
{
	glEnableVertexArrayAttrib(get(), attribute_index);
	glVertexArrayAttribBinding(get(), attribute_index, attribute_index);
	glVertexArrayAttribLFormat(get(), attribute_index, size, type, relative_offset);
	glVertexArrayVertexBuffer(get(), attribute_index, buffer.get(), offset, stride);
}

void Vertex_array::bind_element_buffer(const Buffer& buffer) const
{
	glVertexArrayElementBuffer(get(), buffer.get());
}

void Vertex_array::bind() const
{
	glBindVertexArray(this->get());
}

Shader_storage::Shader_storage(std::uint32_t binding)
	:
	Unique_resource(create_buffer()),
	binding_(binding)
{
}

void Shader_storage::bind() const
{
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, binding_, this->get());
}
} // namespace nwd::graphics::opengl
