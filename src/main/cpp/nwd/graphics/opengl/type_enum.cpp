#include <nwd/graphics/opengl/type_enum.hpp>

#include <epoxy/gl.h>

namespace nwd::graphics::opengl
{
template<> std::uint32_t enum_of<GLbyte> = GL_BYTE;
template<> std::uint32_t enum_of<GLubyte> = GL_UNSIGNED_BYTE;
template<> std::uint32_t enum_of<GLshort> = GL_SHORT;
template<> std::uint32_t enum_of<GLushort> = GL_UNSIGNED_SHORT;
template<> std::uint32_t enum_of<GLint> = GL_INT;
template<> std::uint32_t enum_of<GLuint> = GL_UNSIGNED_INT;
template<> std::uint32_t enum_of<GLint64> = GL_INT64_ARB;
template<> std::uint32_t enum_of<GLuint64> = GL_UNSIGNED_INT64_ARB;
template<> std::uint32_t enum_of<_Float16> = GL_HALF_FLOAT;
template<> std::uint32_t enum_of<GLfloat> = GL_FLOAT;
template<> std::uint32_t enum_of<GLdouble> = GL_DOUBLE;
} // namespace nwd::graphics::opengl
