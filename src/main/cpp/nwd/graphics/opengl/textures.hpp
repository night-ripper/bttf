#pragma once

#include <cstddef>
#include <cstdint>

#include <vector>
#include <span>
#include <functional>

#include <nwd/utility/functor.hpp>
#include <nwd/utility/unique_resource.hpp>
#include <nwd/utility/enum_base.hpp>
#include <nwd/graphics/image.hpp>

namespace nwd::graphics::opengl
{
namespace deleters
{
void texture(std::uint32_t object) noexcept;
void renderbuffer(std::uint32_t object) noexcept;
void framebuffer(std::uint32_t object) noexcept;
void bound_framebuffer(utility::Enum_base object) noexcept;
void resident_handle(std::uint64_t object) noexcept;
} // namespace deleters

struct Framebuffer;
struct Renderbuffer;
struct Texture_handle;

struct Texture : protected utility::Unique_resource<std::uint32_t, utility::Functor<deleters::texture>>
{
	struct Format
	{
		std::int8_t r = 0;
		std::int8_t g = 0;
		std::int8_t b = 0;
		std::int8_t a = 0;
		
		/// Number of channels, 1 : 4
		std::int8_t channels = 0;
		
		/// Number of bytes
		std::int8_t size = 0;
		
		bool is_signed = 0;
		
		std::uint32_t internal = 0;
		std::uint32_t external = 0;
		std::uint32_t type = 0;
		
		Format() noexcept = default;
		
		Format(bool is_signed,
			std::int8_t r, std::int8_t g, std::int8_t b, std::int8_t a,
			std::uint32_t internal, std::uint32_t external, std::uint32_t type) noexcept
			:
			r(r),
			g(g),
			b(b),
			a(a),
			channels(static_cast<std::int8_t>((not not r) + (not not g) + (not not b) + (not not a))),
			size(static_cast<std::int8_t>((int(r) + int(g) + int(b) + int(a)) / 8)),
			is_signed(is_signed),
			internal(internal),
			external(external),
			type(type)
		{
		}
		
		static const Format rgb_u_3_3_2;
		static const Format rgb_u_5_6_5;
		static const Format rgba_u_4_4_4_4;
		static const Format rgba_u_5_5_5_1;
		static const Format rgba_u_8_8_8_8;
		static const Format rgba_u_10_10_10_2;
		
		static const Format r_s_8;
		static const Format r_u_8;
		static const Format rg_s_8;
		static const Format rg_u_8;
		static const Format rgb_s_8;
		static const Format rgb_u_8;
		static const Format rgba_s_8;
		static const Format rgba_u_8;
		
		static const Format r_s_16;
		static const Format r_u_16;
		static const Format rg_s_16;
		static const Format rg_u_16;
		static const Format rgb_s_16;
		static const Format rgb_u_16;
		static const Format rgba_s_16;
		static const Format rgba_u_16;
		
		static const Format r_s_32;
		static const Format r_u_32;
		static const Format rg_s_32;
		static const Format rg_u_32;
		static const Format rgb_s_32;
		static const Format rgb_u_32;
		static const Format rgba_s_32;
		static const Format rgba_u_32;
		
		static const Format r_f_16;
		static const Format rg_f_16;
		static const Format rgb_f_16;
		static const Format rgba_f_16;
		
		static const Format r_f_32;
		static const Format rg_f_32;
		static const Format rgb_f_32;
		static const Format rgba_f_32;
		
		static const Format depth_16;
		static const Format depth_24;
		static const Format depth_32;
		static const Format depth_24_stencil_8;
		static const Format stencil_8;
		static const Format stencil_16;
	};
	
	////////////////////////////////////////////////////////////////////////////
	
	using Parameter = std::move_only_function<void(Texture&) const>;
	
	struct Wrapping
	{
		static const Texture::Parameter repeat;
		static const Texture::Parameter repeat_mirror;
		static const Texture::Parameter clamp_to_edge;
		static const Texture::Parameter clamp_to_edge_mirror;
		static const Texture::Parameter clamp_to_border_black;
		static const Texture::Parameter clamp_to_border_transparent;
		static Parameter clamp_to_border(float r, float g, float b, float a) noexcept;
	};

	struct Minifying
	{
		static const Texture::Parameter nearest;
		static const Texture::Parameter linear;
	};

	struct Magnifying
	{
		static const Texture::Parameter nearest;
		static const Texture::Parameter linear;
	};
	
	////////////////////////////////////////////////////////////////////////////
	
	std::int32_t levels() const {return levels_;}
	
	void bind(std::int32_t binding);
	
	void parameter(const Parameter& param)
	{
		param(*this);
	}
	
	std::int32_t width() const {return width_;}
	std::int32_t height() const {return height_;}
	
protected:
	Texture() noexcept = default;
	
	Texture(std::uint32_t target, std::int32_t levels = 1);
	
	friend Framebuffer;
	friend Texture_handle;
	
protected:
	std::int32_t levels_ = 0;
	std::int32_t width_ = 0;
	std::int32_t height_ = 0;
};

struct Texture_2D : Texture
{
	Texture_2D() noexcept = default;
	
	Texture_2D(std::int32_t width, std::int32_t height, std::int32_t levels = 1);
	
	void storage(Format internal_format);
	void upload(Format external_format, const void* data);
};

struct Texture_2D_multisample : Texture
{
	Texture_2D_multisample() noexcept = default;
	
	Texture_2D_multisample(std::int32_t width, std::int32_t height,
		std::int32_t samples, bool fixed_sample_locations = true
	);
	
	void storage(Format internal_format);
	
protected:
	bool fixed_sample_locations_ = false;
};

struct Texture_2D_array : Texture
{
	Texture_2D_array() noexcept = default;
	
	Texture_2D_array(std::int32_t width, std::int32_t height, std::int32_t levels = 1);
	
	void storage(Format internal_format, std::int32_t layers);
	
	void upload(std::int32_t layer_begin, std::int32_t amount,
		Format external_format, const void* data);
	
	void upload(std::span<const Image_rgba_u_8> span, std::int32_t layer_begin = 0);
	
	void upload(std::initializer_list<Image_rgba_u_8> list, std::int32_t layer_begin = 0)
	{
		return upload(std::span(list), layer_begin);
	}
	
	void upload(const Image_rgba_u_8& image, std::int32_t layer = 0)
	{
		return upload(layer, 1, Format::rgba_u_8, image.begin());
	}
	
	std::int32_t layers() const {return layers_;}
	
protected:
	std::int32_t layers_ = 0;
};

////////////////////////////////////////////////////////////////////////////////

struct Renderbuffer : protected utility::Unique_resource<std::uint32_t, utility::Functor<deleters::renderbuffer>>
{
	Renderbuffer() = default;
	
	Renderbuffer(std::int32_t width, std::int32_t height, std::int32_t samples = 0);
	
	void storage(Texture::Format internal_format);
	
	std::int32_t width() const {return width_;}
	std::int32_t height() const {return height_;}
	std::int32_t samples() const {return samples_;}
	
	friend Framebuffer;
	
protected:
	std::int32_t width_ = 0;
	std::int32_t height_ = 0;
	std::int32_t samples_ = 0;
};

struct Framebuffer : protected utility::Unique_resource<std::uint32_t, utility::Functor<deleters::framebuffer>>
{
	Framebuffer() = default;
	Framebuffer(Framebuffer&&) noexcept = default;
	Framebuffer& operator=(Framebuffer&&) noexcept = default;
	
	static Framebuffer create();
	
	struct Attachment : utility::Enum_base
	{
		static const Attachment depth;
		static const Attachment stencil;
		static const Attachment depth_stencil;
		static const Attachment color;
	};
	
	struct Target : utility::Enum_base
	{
		static const Target read;
		static const Target draw;
		static const Target read_draw;
	};

	struct Draw_buffer : utility::Enum_base
	{
		static const Draw_buffer none;
		static const Draw_buffer front_left;
		static const Draw_buffer front_right;
		static const Draw_buffer back_left;
		static const Draw_buffer back_right;
		static const Draw_buffer front;
		static const Draw_buffer back;
		static const Draw_buffer left;
		static const Draw_buffer right;
		static const Draw_buffer front_back;
	};
	
	void attach(const Texture& texture, Attachment attachment) const;
	void attach(const Renderbuffer& renderbuffer, Attachment attachment) const;
	
	void draw_buffer(Draw_buffer draw_buffer);
	
	struct Bound : utility::Unique_resource<Target, utility::Functor<deleters::bound_framebuffer>>
	{
		explicit Bound(const Framebuffer& framebuffer, Target target);
	};
	
	Bound bind(Target target) const;
	
protected:
	using utility::Unique_resource<std::uint32_t, utility::Functor<deleters::framebuffer>>::Unique_resource;
	
protected:
	Draw_buffer draw_buffer_ = Draw_buffer::none;
};

struct Texture_handle
{
	struct Resident : utility::Unique_resource<std::uint64_t, utility::Functor<deleters::resident_handle>>
	{
		Resident() = default;
		explicit Resident(const Texture_handle& handle);
	};
	
	Texture_handle(const Texture& texture);
	Texture_handle(const Resident& resident);
	
	std::uint64_t get() const noexcept {return value_;}
	
protected:
	std::uint64_t value_;
};
} // namespace nwd::graphics::opengl
