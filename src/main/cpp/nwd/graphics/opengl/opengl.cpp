#include <iostream>

#include <epoxy/gl.h>

namespace nwd::graphics::opengl
{
struct OpenGL_error : std::runtime_error
{
	using std::runtime_error::runtime_error;
};

[[gnu::cold]]
[[maybe_unused]]
static void opengl_debug_callback(
	[[maybe_unused]] GLenum source,
	[[maybe_unused]] GLenum type,
	[[maybe_unused]] GLuint id,
	[[maybe_unused]] GLenum severity,
	[[maybe_unused]] GLsizei length,
	[[maybe_unused]] const char* message,
	[[maybe_unused]] const void* user_parameter)
{
	const char* source_string = "(unknown)";
	
	switch (source)
	{
	case GL_DEBUG_SOURCE_API:
		source_string = "api";
		break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
		source_string = "window system";
		break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER:
		source_string = "shader compiler";
		break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:
		source_string = "third party";
		break;
	case GL_DEBUG_SOURCE_APPLICATION:
		source_string = "application";
		break;
	case GL_DEBUG_SOURCE_OTHER:
		source_string = "other";
		break;
	}
	
	const char* type_string = "(unknown)";
	
	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:
		type_string = "error";
		break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
		type_string = "deprecated behaviour";
		break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
		type_string = "undefined behaviour";
		break;
	case GL_DEBUG_TYPE_PORTABILITY:
		type_string = "portability";
		break;
	case GL_DEBUG_TYPE_PERFORMANCE:
		type_string = "performance";
		break;
	case GL_DEBUG_TYPE_MARKER:
		type_string = "marker";
		break;
	case GL_DEBUG_TYPE_PUSH_GROUP:
		type_string = "push group";
		break;
	case GL_DEBUG_TYPE_POP_GROUP:
		type_string = "pop group";
		break;
	case GL_DEBUG_TYPE_OTHER:
		type_string = "other";
		break;
	}
	
	const char* severity_string = "(unknown)";
	
	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:
		severity_string = "high";
		break;
	case GL_DEBUG_SEVERITY_MEDIUM:
		severity_string = "medium";
		break;
	case GL_DEBUG_SEVERITY_LOW:
		severity_string = "low";
		break;
	case GL_DEBUG_SEVERITY_NOTIFICATION:
		severity_string = "notification";
		break;
	}
	
	std::string message_middle = std::string("(source: ") + source_string + ", " +
		"type: " + type_string + ", " + "severity: " + severity_string + "): "
	;
	
	if (severity == GL_DEBUG_SEVERITY_HIGH)
	{
		throw OpenGL_error(message_middle + std::string(message, length));
	}
	else
	{
		std::clog << "OpenGL warning ";
		std::clog << message_middle;
		std::clog.write(message, length);
		std::clog << "\n";
	}
}
} // namespace nwd::graphics::opengl
