#pragma once

#include <cstddef>
#include <cstdint>
#include <cstring>

#include <array>

namespace nwd::config
{
constexpr auto version = []() -> auto
{
    constexpr const char value[] = "0.0.1";
    // NOTE we also want the trailing \0
    auto result = std::array<std::byte, sizeof(value)>();
    for (auto i = 0uz; i != std::size(result); ++i)
    {
        result[i] = std::byte(value[i]);
    }
    return result;
}();

constexpr std::int32_t username_length = 64;
constexpr std::int32_t salt_length = 8;
constexpr std::int32_t password_length = 256 - salt_length;
constexpr std::int32_t max_udp_length = 128 + 256;
} //nwd::config
