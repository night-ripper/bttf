#pragma once

#include <cstdint>

namespace nwd::shared::messages
{
enum struct Common : std::uint8_t
{
	invalid,
	
	heartbeat,
	
	initial_connect,
};
} // namespace nwd::shared::messages
