#pragma once

#include <nwd/utility/executor.hpp>

namespace nwd::shared::globals
{
inline static auto executor = utility::Executor(std::chrono::milliseconds(1));
} // namespace nwd::shared::globals
