#include <iostream>
#include <thread>
#include <chrono>

#include <AL/al.h>
#include <AL/alc.h>

#include <nwd/audio/openal.hpp>
#include <nwd/graphics/glfw.hpp>

using namespace nwd;

int main()
{
	for (auto name : audio::openal::Capture_device::names())
	{
		std::cout << '|' << name << '|' << "\n";
	}
	
	for (auto name : audio::openal::Device::names())
	{
		std::cout << '|' << name << '|' << "\n";
	}
	
	graphics::glfw::Library glfw3_library;
	auto ctx = glfw3_library.create_context(564, 123, "asdasad");
	
	auto m = glfw3_library.primary_monitor();
	
	std::cout << m.name() << "\n";
	std::cout << m.physical_size_mm()[0] << "\n";
	std::cout << m.physical_size_mm()[1] << "\n";
	
	for (auto vm : m.video_modes())
	{
		std::cout << '|' << vm.height_ << '|' << "\n";
		std::cout << '|' << vm.width_ << '|' << "\n";
		std::cout << '|' << vm.refresh_rate_ << '|' << "\n";
	}
	
	std::cout << '|' << m.current_video_mode().height_ << '|' << "\n";
	std::cout << '|' << m.current_video_mode().width_ << '|' << "\n";
	std::cout << '|' << m.current_video_mode().refresh_rate_ << '|' << "\n";
	
	for (auto vm : glfw3_library.monitors())
	{
		std::cout << vm.name() << "\n";
	}
	
	/*
	auto format = nwd::audio::Sound_info(1, 8, 44100);
	auto cdev = nwd::audio::openal::Capture_device("Logitech USB Headset Mono", format);
	auto us = std::move(nwd::audio::Unique_sound(format).load(4096, std::make_unique<unsigned char[]>(4096)));
	
	auto jt = std::jthread([&](std::stop_token stop_token) -> void
	{
		while (not stop_token.stop_requested())
		{
			std::cout << (cdev >> us) << "\n";
			std::this_thread::sleep_for(std::chrono::milliseconds(20));
		}
	});
	
	cdev.start();
	
	std::this_thread::sleep_for(std::chrono::seconds(1));
	jt.get_stop_source().request_stop();
	*/
}
