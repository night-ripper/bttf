#pragma once

#include <cstddef>
#include <cstdint>
#include <cmath>

#include <limits>
#include <type_traits>

namespace nwd::math
{
template<typename Underlying_type, typename Minimum>
requires(std::is_unsigned_v<Underlying_type>)
struct Fixed_cyclic
{
	using underlying_type = Underlying_type;
	
private:
	constexpr static long double minimum_value = Minimum::value;
	constexpr static long double maximum_value =
		std::numeric_limits<underlying_type>::max() * minimum_value
	;
	
	constexpr static long double inv_minimum_value = 1.L / minimum_value;
	constexpr static long double inv_maximum_value = 1.L / maximum_value;
	
public:
	Fixed_cyclic() = default;
	
	template<typename Type>
	requires(std::is_floating_point_v<Type>)
	constexpr explicit Fixed_cyclic(Type value) noexcept
		:
		value_(static_cast<underlying_type>(std::round(
			(value - std::floor(value * static_cast<Type>(inv_maximum_value)) * maximum_value)
			* static_cast<Type>(inv_minimum_value)
		)))
	{
	}
	
	constexpr static Fixed_cyclic from(underlying_type value) noexcept
	{
		auto result = Fixed_cyclic();
		result.value_ = value;
		return result;
	}
	
	template<typename Type>
	requires(std::is_floating_point_v<Type>)
	constexpr explicit operator Type() const noexcept
	{
		return static_cast<Type>(value_) * static_cast<Type>(minimum_value);
	}
	
	constexpr friend auto operator<=>(Fixed_cyclic lhs, Fixed_cyclic rhs) = default;
	
	constexpr friend Fixed_cyclic operator-(Fixed_cyclic value) noexcept
	{
		value.value_ = -value.value_;
		return value;
	}
	
	constexpr Fixed_cyclic& operator+=(Fixed_cyclic rhs) noexcept
	{
		value_ += rhs.value_;
		return *this;
	}
	
	constexpr friend Fixed_cyclic operator+(Fixed_cyclic lhs, Fixed_cyclic rhs) noexcept
	{
		return lhs += rhs;
	}
	
	constexpr Fixed_cyclic& operator-=(Fixed_cyclic rhs) noexcept
	{
		value_ -= rhs.value_;
		return *this;
	}
	
	constexpr friend Fixed_cyclic operator-(Fixed_cyclic lhs, Fixed_cyclic rhs) noexcept
	{
		return lhs -= rhs;
	}
	
	constexpr underlying_type& value() noexcept {return value_;}
	constexpr const underlying_type& value() const noexcept {return value_;}
	
private:
	underlying_type value_;
};
} // namespace nwd::math
