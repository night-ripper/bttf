#pragma once

#include <cmath>
#include <cstdint>

#include <array>
#include <numeric>
#include <numbers>
#include <type_traits>

#include <nwd/utility/arrays.hpp>

namespace nwd::math
{
template<typename Type, std::size_t Size>
struct Vec : std::array<Type, Size>
{
	constexpr Vec() = default;
	
	template<typename Arg_type>
	constexpr Vec(std::array<Arg_type, Size> array) noexcept
		:
		std::array<Type, Size>(utility::arrays::cast<Type>(std::move(array)))
	{
	}
	
	template<typename... Args>
	requires(sizeof...(Args) == Size and (std::convertible_to<Args, Type> and ...))
	constexpr explicit Vec(Args&&... args) noexcept
		:
		std::array<Type, Size> {Type(std::forward<Args>(args))...}
	{
	}
	
	template<std::size_t Index> constexpr Type& get() noexcept {return std::get<Index>(*this);}
	template<std::size_t Index> constexpr const Type& get() const noexcept {return std::get<Index>(*this);}
};

template<typename Type>
using Vec3 = Vec<Type, 3>;
using Vec3f = Vec3<float>;
using Vec3d = Vec3<double>;

template<typename Type, std::size_t Size>
constexpr Vec<Type, Size>& operator+=(Vec<Type, Size>& lhs, Vec<Type, Size> rhs) noexcept
requires(std::is_arithmetic_v<Type>)
{
	for (std::size_t i = 0; i != std::size(rhs);)
	{
		lhs[i] += rhs[i];
	}
	
	return lhs;
}

template<typename Type, std::size_t Size>
constexpr Vec<Type, Size>& operator-=(Vec<Type, Size>& lhs, Vec<Type, Size> rhs) noexcept
requires(std::is_arithmetic_v<Type>)
{
	for (std::size_t i = 0; i != std::size(rhs);)
	{
		lhs[i] -= rhs[i];
	}
	
	return lhs;
}

template<typename Type, std::size_t Size, typename Arg_type>
constexpr Vec<Type, Size>& operator*=(Vec<Type, Size>& lhs, Arg_type rhs) noexcept
requires(std::is_arithmetic_v<Type> and std::is_arithmetic_v<Arg_type>)
{
	for (std::size_t i = 0; i != std::size(lhs);)
	{
		lhs[i] *= rhs;
	}
	
	return lhs;
}

template<typename Type, std::size_t Size, typename Arg_type>
constexpr Vec<Type, Size>& operator/=(Vec<Type, Size>& lhs, Arg_type rhs) noexcept
requires(std::is_arithmetic_v<Type> and std::is_arithmetic_v<Arg_type>)
{
	for (std::size_t i = 0; i != std::size(lhs);)
	{
		lhs[i] /= rhs;
	}
	
	return lhs;
}

template<typename Type, std::size_t Size>
requires(std::is_arithmetic_v<Type>)
inline constexpr Vec<Type, Size> operator-(Vec<Type, Size> vector) noexcept
{
	for (std::size_t i = 0; i != std::size(vector);)
	{
		vector[i] = -vector[i];
	}
	
	return vector;
}

template<typename Type, std::size_t Size>
constexpr Vec<Type, Size> operator+(Vec<Type, Size> lhs, Vec<Type, Size> rhs) noexcept
requires(std::is_arithmetic_v<Type>)
{
	return lhs += rhs;
}

template<typename Type, std::size_t Size>
constexpr Vec<Type, Size> operator-(Vec<Type, Size> lhs, Vec<Type, Size> rhs) noexcept
requires(std::is_arithmetic_v<Type>)
{
	return lhs -= rhs;
}

template<typename Type, std::size_t Size, typename Arg_type>
requires(std::is_arithmetic_v<Type> and std::is_arithmetic_v<Arg_type>)
inline constexpr Vec<Type, Size> operator*(Vec<Type, Size> vector, Arg_type value) noexcept
{
	return vector *= value;
}

template<typename Type, std::size_t Size, typename Arg_type>
requires(std::is_arithmetic_v<Type> and std::is_arithmetic_v<Arg_type>)
inline constexpr Vec<Type, Size> operator*(Arg_type value, Vec<Type, Size> vector) noexcept
{
	return vector *= value;
}

template<typename Type, std::size_t Size, typename Arg_type>
requires(std::is_arithmetic_v<Type> and std::is_arithmetic_v<Arg_type>)
inline constexpr Vec<Type, Size> operator/(Vec<Type, Size> vector, Arg_type value) noexcept
{
	return vector /= value;
}

template<typename Type, std::size_t Size>
requires(std::is_arithmetic_v<Type>)
inline constexpr Vec<Type, Size> difference(Vec<Type, Size> lhs, Vec<Type, Size> rhs) noexcept
{
	return rhs - lhs;
}

template<typename Type>
requires(std::is_floating_point_v<Type>)
inline constexpr Type length(std::array<Type, 2> vector) noexcept
{
	return std::hypot(vector[0], vector[1]);
}

template<typename Type>
requires(std::is_floating_point_v<Type>)
inline constexpr Type length(Vec3<Type> vector) noexcept
{
	return std::hypot(vector[0], vector[1], vector[2]);
}

template<typename Type, std::size_t Size>
requires(std::is_floating_point_v<Type>)
inline constexpr Type distance(Vec<Type, Size> lhs, Vec<Type, Size> rhs) noexcept
{
	return length(difference(lhs, rhs));
}

template<typename Type, std::size_t Size>
requires(std::is_floating_point_v<Type>)
inline constexpr Vec<Type, Size> normalize(Vec<Type, Size> vector) noexcept
{
	return vector /= length(vector);
}

template<typename Type, std::size_t Size>
requires(std::is_floating_point_v<Type>)
inline constexpr Type dot(Vec<Type, Size> lhs, Vec<Type, Size> rhs) noexcept
{
	auto result = Type();
	
	for (std::size_t i = 0; i != std::size(rhs);)
	{
		result += lhs[i] * rhs[i];
	}
	
	return result;
}

template<typename Type>
requires(std::is_floating_point_v<Type>)
inline constexpr Vec3<Type> cross(Vec3<Type> lhs, Vec3<Type> rhs) noexcept
{
	return Vec3<Type> {
		lhs[1] * rhs[2] - lhs[2] * rhs[1],
		lhs[2] * rhs[0] - lhs[0] * rhs[2],
		lhs[0] * rhs[1] - lhs[1] * rhs[0],
	};
}

template<typename Type, std::size_t Size>
requires(std::is_floating_point_v<Type>)
inline constexpr Vec<Type, Size> midpoint(Vec<Type, Size> lhs, Vec<Type, Size> rhs) noexcept
{
	for (std::size_t i = 0; i != std::size(lhs);)
	{
		lhs[i] = std::midpoint(lhs[i], rhs[i]);
	}
	
	return lhs;
}

template<typename Type, std::size_t Size>
requires(std::is_floating_point_v<Type>)
inline constexpr Vec<Type, Size> lerp(Vec<Type, Size> lhs, Vec<Type, Size> rhs, Type t) noexcept
{
	for (std::size_t i = 0; i != std::size(lhs);)
	{
		lhs[i] = std::lerp(lhs[i], rhs[i], t);
	}
	
	return lhs;
}

template<typename Type, std::size_t Size>
requires(std::is_floating_point_v<Type>)
inline constexpr Vec<Type, Size> pow(Vec<Type, Size> base, Type exponent) noexcept
{
	for (std::size_t i = 0; i != std::size(base);)
	{
		base[i] = std::pow(base[i], exponent);
	}
	
	return base;
}

template<typename Type>
requires(std::is_floating_point_v<Type>)
inline constexpr Vec3<Type> rotate(Vec3<Type> vector, Type sin, Type cos, Vec3<Type> unit_axis)
{
	// https://en.wikipedia.org/wiki/Rodrigues%27_rotation_formula
	return vector * cos
		+ cross(unit_axis, vector) * sin
		+ unit_axis * (dot(unit_axis, vector) * (1.f - cos))
	;
}

template<typename Type>
requires(std::is_floating_point_v<Type>)
inline constexpr Vec3<Type> rotate(Vec3<Type> vector, Type angle, Vec3<Type> unit_axis)
{
	return rotate(vector, std::sin(angle), std::cos(angle), unit_axis);
}

//! @return The first 3 solutions and their divisor
//! The real result is obtained by division of each of the 3 results
//! by the 4-th one.
//! The divisor always has non-negative value
template<typename Type>
requires(std::is_floating_point_v<Type>)
Vec<Type, 4> solve(std::array<Vec3<Type>, 4> columns) noexcept
{
	constexpr auto determinant_3 = [](const Vec3<Type>* __restrict__ vs) noexcept -> Type
	{
		/*
		return (
			Type(vs[0][0]) * Type(vs[1][1]) * Type(vs[2][2]) +
			Type(vs[1][0]) * Type(vs[2][1]) * Type(vs[0][2]) +
			Type(vs[2][0]) * Type(vs[0][1]) * Type(vs[1][2])
		) - (
			Type(vs[2][0]) * Type(vs[1][1]) * Type(vs[0][2]) +
			Type(vs[1][0]) * Type(vs[0][1]) * Type(vs[2][2]) +
			Type(vs[0][0]) * Type(vs[2][1]) * Type(vs[1][2])
		);
		*/
		return Type(vs[0][0]) * (Type(vs[1][1]) * Type(vs[2][2]) - Type(vs[2][1]) * Type(vs[1][2]))
			+ Type(vs[1][0]) * (Type(vs[2][1]) * Type(vs[0][2]) - Type(vs[0][1]) * Type(vs[2][2]))
			+ Type(vs[2][0]) * (Type(vs[0][1]) * Type(vs[1][2]) - Type(vs[1][1]) * Type(vs[0][2]))
		;
	};
	
	// We use Cramer's rule
	
	auto result = Vec<Type, 4>();
	
	const auto divisor = determinant_3(std::data(columns));
	
	for (std::size_t i = 0; i < 3; ++i)
	{
		std::swap(columns[i], columns[3]);
		
		result[i] = determinant_3(std::data(columns)) * std::copysign(Type(1), divisor);
		
		// Return to the original state
		std::swap(columns[i], columns[3]);
	}
	
	result[3] = std::copysign(divisor, Type(1));
	
	return result;
}

//! @param angle_xy zero angle points to {1, 0, 0}, usual counterclockwise direction.
//! @param angle_z the value of (pi / 2) points upwards
//! @return The vector pointing in that direction and a perpendicular vector
//! pointing in the upwards direction
template<typename Type>
requires(std::is_floating_point_v<Type>)
std::array<Vec3<Type>, 2> direction_up_unit_vectors(Type angle_xy, Type angle_z)
{
	auto direction_xy = rotate(Vec3<Type>(1, 0, 0), angle_xy, Vec3<Type>(0, 0, 1));
	auto normal_xy = cross(direction_xy, Vec3<Type>(0, 0, 1));
	auto direction = normalize(rotate(direction_xy, angle_z, normal_xy));
	auto up = normalize(rotate(direction, std::numbers::pi_v<Type> / 2, normal_xy));
	return {direction, up};
}

//! @return The vector pointing in that direction and a perpendicular vector
//! pointing in the upwards direction
template<typename Type>
requires(std::is_floating_point_v<Type>)
Vec3<Type> unit_vector(Type angle_xy, Type angle_z)
{
	Vec3<Type> vector_xy = rotate(Vec3<Type>(1, 0, 0), angle_xy, Vec3<Type>(0, 0, 1));
	Vec3<Type> perpendicular_xy = rotate(Vec3<Type>(1, 0, 0), (angle_xy - std::numbers::pi_v<Type> / 2), Vec3<Type>(0, 0, 1));
	return normalize(rotate(vector_xy, angle_z, perpendicular_xy));
}
} // namespace nwd::math

template<typename Type, std::size_t Size>
struct std::tuple_size<nwd::math::Vec<Type, Size>> : std::tuple_size<std::array<Type, Size>>
{
};

template<std::size_t Index, typename Type, std::size_t Size>
struct std::tuple_element<Index, nwd::math::Vec<Type, Size>> : std::tuple_element<Index, std::array<Type, Size>>
{
};
