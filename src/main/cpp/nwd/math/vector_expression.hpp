#pragma once

#include <cmath>
#include <cstdint>

#include <array>
#include <utility>
#include <type_traits>

namespace nwd::math
{
template<typename Type>
struct Vector_expression
{
	constexpr Type& operator[](std::ptrdiff_t index) noexcept {return static_cast<Type&>(*this)[index];}
	constexpr const Type& operator[](std::ptrdiff_t index) const noexcept {return static_cast<const Type&>(*this)[index];}
    constexpr std::ptrdiff_t size() const noexcept {return std::ssize(static_cast<const Type&>(*this));}
};

template<typename Type, std::ptrdiff_t Size>
struct Vector : std::array<Type, Size>, Vector_expression<Vector<Type, Size>>
{
	constexpr Vector() = default;
	
	template<typename Arg_type>
	constexpr Vector(const Arg_type(&array)[Size]) noexcept
	{
		for (std::ptrdiff_t i = 0; i != std::ssize(array); ++i)
		{
			this->data()[i] = array[i];
		}
	}
	
	template<typename Arg_type>
	constexpr Vector(std::array<Arg_type, Size> array) noexcept
	{
		for (std::ptrdiff_t i = 0; i != array.size(); ++i)
		{
			this->data()[i] = array[i];
		}
	}
	
	template <typename Arg_type>
	constexpr Vector(const Vector_expression<Arg_type>& expression) noexcept
	{
		if (std::ssize(*this) != std::ssize(expression))
		{
			std::unreachable();
		}
		
		for (std::ptrdiff_t i = 0; i != expression.size(); ++i)
		{
			this->data()[i] = expression[i];
		}
	}
	
	using std::array<Type, Size>::operator[];
	using std::array<Type, Size>::size;
};

namespace vector_expressions
{
template<typename Type>
struct Unary_minus
{
	constexpr Unary_minus(const Type& value) noexcept
		:
		value_(value)
	{
	}
	
	constexpr auto operator[](std::ptrdiff_t index) const noexcept {return -value_[index];}
    constexpr std::ptrdiff_t size() const noexcept {return value_.size();}
	
private:
	const Type& value_;
};

template<typename L_type, typename R_type>
struct Addition : Vector_expression<Addition<L_type, R_type>>
{
	constexpr Addition(const L_type& lhs, const R_type& rhs) noexcept
		:
		lhs_(lhs), rhs_(rhs)
	{
		if (std::ssize(lhs) != std::ssize(rhs))
		{
			std::unreachable();
		}
	}
	
	constexpr auto operator[](std::ptrdiff_t index) const noexcept {return lhs_[index] + rhs_[index];}
    constexpr std::ptrdiff_t size() const noexcept {return lhs_.size();}
	
private:
	const L_type& lhs_;
	const R_type& rhs_;
};

template<typename L_type, typename R_type>
struct Subtraction : Vector_expression<Subtraction<L_type, R_type>>
{
	constexpr Subtraction(const L_type& lhs, const R_type& rhs) noexcept
		:
		lhs_(lhs), rhs_(rhs)
	{
		if (lhs.size() != rhs.size())
		{
			std::unreachable();
		}
	}
	
	constexpr auto operator[](std::ptrdiff_t index) const noexcept {return lhs_[index] - rhs_[index];}
    constexpr std::ptrdiff_t size() const noexcept {return lhs_.size();}
	
private:
	const L_type& lhs_;
	const R_type& rhs_;
};

template<typename L_type, typename R_type>
struct Multiplication_lhs : Vector_expression<Multiplication_lhs<L_type, R_type>>
{
	constexpr Multiplication_lhs(const L_type& lhs, const R_type& rhs) noexcept
		:
		lhs_(lhs), rhs_(rhs)
	{
	}
	
	constexpr auto operator[](std::ptrdiff_t index) const noexcept {return lhs_[index] * rhs_;}
    constexpr std::ptrdiff_t size() const noexcept {return lhs_.size();}
	
private:
	const L_type& lhs_;
	const R_type& rhs_;
};

template<typename L_type, typename R_type>
struct Multiplication_rhs : Vector_expression<Multiplication_rhs<L_type, R_type>>
{
	constexpr Multiplication_rhs(const L_type& lhs, const R_type& rhs) noexcept
		:
		lhs_(lhs), rhs_(rhs)
	{
	}
	
	constexpr auto operator[](std::ptrdiff_t index) const noexcept {return lhs_ * rhs_[index];}
    constexpr std::ptrdiff_t size() const noexcept {return rhs_.size();}
	
private:
	const L_type& lhs_;
	const R_type& rhs_;
};

template<typename L_type, typename R_type>
struct Division_lhs : Vector_expression<Division_lhs<L_type, R_type>>
{
	constexpr Division_lhs(const L_type& lhs, const R_type& rhs) noexcept
		:
		lhs_(lhs), rhs_(rhs)
	{
	}
	
	constexpr auto operator[](std::ptrdiff_t index) const noexcept {return lhs_[index] / rhs_;}
    constexpr std::ptrdiff_t size() const noexcept {return lhs_.size();}
	
private:
	const L_type& lhs_;
	const R_type& rhs_;
};
} // namespace vector_expressions

template <typename Type>
constexpr vector_expressions::Unary_minus<Type>
operator-(const Vector_expression<Type>& value) noexcept
{
   return vector_expressions::Unary_minus<Type>(static_cast<const Type&>(value));
}

template <typename L_type, typename R_type>
constexpr vector_expressions::Addition<L_type, R_type>
operator+(const Vector_expression<L_type>& lhs, const Vector_expression<R_type>& rhs) noexcept
{
   return vector_expressions::Addition<L_type, R_type>(static_cast<const L_type&>(lhs), static_cast<const R_type&>(rhs));
}

template <typename L_type, typename R_type>
constexpr vector_expressions::Subtraction<L_type, R_type>
operator-(const Vector_expression<L_type>& lhs, const Vector_expression<R_type>& rhs) noexcept
{
   return vector_expressions::Subtraction<L_type, R_type>(static_cast<const L_type&>(lhs), static_cast<const R_type&>(rhs));
}

template <typename L_type, typename R_type>
constexpr vector_expressions::Subtraction<L_type, R_type>
operator*(const Vector_expression<L_type>& lhs, const Vector_expression<R_type>& rhs) noexcept = delete;

template <typename L_type, typename R_type>
constexpr vector_expressions::Multiplication_lhs<L_type, R_type>
operator*(const Vector_expression<L_type>& lhs, const R_type& rhs) noexcept
{
   return vector_expressions::Multiplication_lhs<L_type, R_type>(static_cast<const L_type&>(lhs), rhs);
}

template <typename L_type, typename R_type>
constexpr vector_expressions::Multiplication_rhs<L_type, R_type>
operator*(const L_type& lhs, const Vector_expression<R_type>& rhs) noexcept
{
   return vector_expressions::Multiplication_rhs<L_type, R_type>(lhs, static_cast<const R_type&>(rhs));
}

template <typename L_type, typename R_type>
constexpr vector_expressions::Division_lhs<L_type, R_type>
operator/(const Vector_expression<L_type>& lhs, const R_type& rhs) noexcept
{
   return vector_expressions::Division_lhs<L_type, R_type>(static_cast<const L_type&>(lhs), rhs);
}
} // namespace nwd::math
