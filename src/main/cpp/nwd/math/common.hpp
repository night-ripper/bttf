#pragma once

namespace nwd::math
{
template<typename Type>
constexpr inline Type mod_incr(Type value, Type modulus) noexcept
{
	++value;
	value *= bool(value != modulus);
	return value;
}

template<typename Type>
constexpr inline Type mod_decr(Type value, Type modulus) noexcept
{
	--value;
	value += modulus * bool(value < Type(0) or modulus < value);
	return value;
}
} // namespace nwd::math
