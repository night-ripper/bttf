#pragma once

#include <cstddef>
#include <cstdint>
#include <cmath>

#include <concepts>
#include <charconv>
#include <limits>
#include <numeric>
#include <type_traits>
#include <utility>
#include <ostream>

namespace nwd::math
{
template<typename Underlying_type, std::ptrdiff_t Point_position>
requires(Point_position <= std::numeric_limits<Underlying_type>::digits)
struct Fixed_point
{
	using underlying_type = Underlying_type;
	using max_type = std::conditional_t<std::is_signed_v<Underlying_type>, std::intmax_t, std::uintmax_t>;
	
	constexpr static auto point_position = Point_position;
	constexpr static auto digits = std::numeric_limits<Underlying_type>::digits;
	
private:
	constexpr static underlying_type min_value = std::numeric_limits<underlying_type>::min();
	constexpr static underlying_type lowest_value = std::numeric_limits<underlying_type>::lowest();
	constexpr static underlying_type max_value = std::numeric_limits<underlying_type>::max();
	constexpr static underlying_type epsilon_value = underlying_type(1);
	constexpr static underlying_type one_value = (Point_position == digits) ? max_value : (underlying_type(1) << Point_position);
	
	constexpr Fixed_point(std::in_place_t, underlying_type value) noexcept
		:
		value_(value)
	{
	}
	
public:
	constexpr static Fixed_point min() {return {std::in_place, min_value};}
	constexpr static Fixed_point lowest() {return {std::in_place, lowest_value};}
	constexpr static Fixed_point max() {return {std::in_place, max_value};}
	constexpr static Fixed_point epsilon() {return {std::in_place, epsilon_value};}
	constexpr static Fixed_point one() {return {std::in_place, one_value};}
	
	Fixed_point() = default;
	
	template<typename Type>
	requires(std::is_floating_point_v<Type>)
	constexpr explicit Fixed_point(Type value) noexcept
		:
		value_(static_cast<underlying_type>(value * static_cast<Type>(one_value)))
	{
		if (value * static_cast<Type>(one_value) > static_cast<Type>(max_value))
		{
			std::unreachable();
		}
		
		if (value * static_cast<Type>(one_value) < static_cast<Type>(lowest_value))
		{
			std::unreachable();
		}
	}
	
	constexpr static Fixed_point from(underlying_type value) noexcept
	{
		return Fixed_point(std::in_place, value);
	}
	
	template<typename Type>
	requires(std::is_floating_point_v<Type>)
	constexpr static Fixed_point from_ceil(Type value) noexcept
	{
		return Fixed_point::from(static_cast<underlying_type>(std::ceil(value * Type(one_value))));
	}
	
	template<typename Type>
	requires(std::is_floating_point_v<Type>)
	constexpr static Fixed_point from_round(Type value) noexcept
	{
		return Fixed_point::from(static_cast<underlying_type>(std::round(value * Type(one_value))));
	}
	
	template<typename Type>
	requires(std::is_floating_point_v<Type>)
	constexpr explicit operator Type() const noexcept
	{
		constexpr auto multiplier = static_cast<Type>(1.L / static_cast<long double>(one_value));
		return multiplier * static_cast<Type>(value_);
	}
	
	constexpr friend auto operator<=>(Fixed_point, Fixed_point) noexcept = default;
	
	constexpr friend Fixed_point operator-(Fixed_point value) noexcept
	{
		value.value_ = -value.value_;
		return value;
	}
	
	constexpr Fixed_point& operator+=(Fixed_point rhs) noexcept
	{
		if (__builtin_add_overflow(value_, rhs.value_, &value_))
		{
			std::unreachable();
		}
		
		return *this;
	}
	
	constexpr friend Fixed_point operator+(Fixed_point lhs, Fixed_point rhs) noexcept
	{
		return lhs += rhs;
	}
	
	constexpr Fixed_point& operator-=(Fixed_point rhs) noexcept
	{
		if (__builtin_sub_overflow(value_, rhs.value_, &value_))
		{
			std::unreachable();
		}
		
		return *this;
	}
	
	constexpr friend Fixed_point operator-(Fixed_point lhs, Fixed_point rhs) noexcept
	{
		return lhs -= rhs;
	}
	
	template<typename Type>
	requires(std::is_integral_v<Type>)
	constexpr Fixed_point& operator*=(Type rhs) noexcept
	{
		if (__builtin_mul_overflow(value_, rhs, &value_))
		{
			std::unreachable();
		}
		
		return *this;
	}
	
	template<typename R_type, std::ptrdiff_t R_pos>
	constexpr Fixed_point& operator*=(Fixed_point<R_type, R_pos> rhs) noexcept
	{
		static_assert(
			std::numeric_limits<underlying_type>::digits +
			std::numeric_limits<R_type>::digits <
			std::numeric_limits<max_type>::digits,
			"Multiplication of large numbers not implemented"
		);
		
		auto result = max_type(value_);
		result *= rhs.value();
		result /= (max_type(1) << R_pos);
		
		if (not std::in_range<underlying_type>(result))
		{
			std::unreachable();
		}
		
		value_ = static_cast<underlying_type>(result);
		return *this;
	}
	
	template<typename Type>
	requires(std::is_floating_point_v<Type>)
	constexpr Fixed_point& operator*=(Type rhs) noexcept
	{
		rhs = static_cast<Type>(value_) * rhs;
		value_ = static_cast<underlying_type>(rhs);
		return *this;
	}
	
	template<typename Type>
	constexpr friend Fixed_point operator*(Fixed_point lhs, Type rhs) noexcept
	{
		return lhs *= rhs;
	}
	
	template<typename Type>
	requires(std::is_integral_v<Type>)
	constexpr Fixed_point& operator/=(Type rhs) noexcept
	{
		value_ /= rhs;
		return *this;
	}
	
	template<typename Type>
	requires(std::is_floating_point_v<Type>)
	constexpr Fixed_point& operator/=(Type rhs) noexcept
	{
		rhs = static_cast<Type>(value_) / rhs;
		
		if (rhs > max_value)
		{
			std::unreachable();
		}
			
		if (rhs < lowest_value)
		{
			std::unreachable();
		}
		
		value_ = static_cast<underlying_type>(rhs);
		return *this;
	}
	
	template<typename Type>
	constexpr friend Fixed_point operator/(Fixed_point lhs, Type rhs) noexcept
	{
		return lhs /= rhs;
	}
	
	constexpr friend Fixed_point midpoint(Fixed_point lhs, Fixed_point rhs) noexcept
	{
		return Fixed_point::from(std::midpoint(lhs.value_, rhs.value_));
	}
	
	constexpr underlying_type& value() noexcept {return value_;}
	constexpr const underlying_type& value() const noexcept {return value_;}
	
private:
	underlying_type value_;
};

template<typename Underlying_type, std::ptrdiff_t Point_position>
std::ostream& operator<<(std::ostream& os, Fixed_point<Underlying_type, Point_position> value)
{
	using unsigned_type = std::make_unsigned_t<Underlying_type>;
	
	char result[1 + std::numeric_limits<Underlying_type>::digits10 +  Point_position + 1] {};
	char* result_end = result;
	
	auto underlying_value = value.value();
	auto unsigned_value = unsigned_type(underlying_value);
	
	if (underlying_value < 0)
	{
		unsigned_value = -unsigned_value;
		*result_end = '-';
		++result_end;
	}
	
	result_end = std::to_chars(result_end, std::end(result), underlying_value / value.one().value()).ptr;
	
	underlying_value %= value.one().value();
	
	unsigned_value <<= std::numeric_limits<unsigned_type>::digits - Point_position;
	
	auto upper = unsigned_value / (unsigned_type(1) << std::numeric_limits<unsigned_type>::digits / 2);
	auto lower = unsigned_value % (unsigned_type(1) << std::numeric_limits<unsigned_type>::digits / 2);
	
	if constexpr (Point_position > 0)
	{
		*result_end = '.';
		++result_end;
		
		for (std::ptrdiff_t i = 0; i != Point_position; ++i, ++result_end)
		{
			lower *= 10;
			upper *= 10;
			auto digit = upper / (unsigned_type(1) << std::numeric_limits<unsigned_type>::digits / 2);
			std::to_chars(result_end, result_end + 1, digit);
			upper += lower / (unsigned_type(1) << std::numeric_limits<unsigned_type>::digits / 2);
			upper %= (unsigned_type(1) << std::numeric_limits<unsigned_type>::digits / 2);
			lower %= (unsigned_type(1) << std::numeric_limits<unsigned_type>::digits / 2);
		}
	}
	
	os.write(result, result_end - result);
	
	return os;
}

template<std::ptrdiff_t Power, typename Underlying_type, std::ptrdiff_t Point_position>
inline constexpr Fixed_point<Underlying_type, Point_position>
pow([[maybe_unused]] Fixed_point<Underlying_type, Point_position> value) noexcept
{
	if constexpr (Power == 0)
	{
		return Fixed_point<Underlying_type, Point_position>::one();
	}
	else if constexpr (Power == 1)
	{
		return value;
	}
	else if constexpr (Power % 2 == 0)
	{
		return pow<Power / 2>(value * value);
	}
	else if constexpr (Power % 2 == 1)
	{
		return value * pow<(Power - 1) / 2>(value * value);
	}
}
} // namespace nwd::math
