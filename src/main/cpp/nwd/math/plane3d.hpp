#pragma once

#include <cstdint>

#include <utility>

#include <nwd/math/vector.hpp>

namespace nwd::math
{
struct Plane3D : std::array<double, 4>
{
	constexpr Plane3D() noexcept = default;
	
	constexpr Plane3D(const Plane3D&) = default;
	constexpr Plane3D& operator=(const Plane3D&) = default;
	
	constexpr Plane3D(Plane3D&&) noexcept = default;
	constexpr Plane3D& operator=(Plane3D&&) noexcept = default;
	
	constexpr Plane3D(double a, double b, double c, double d) noexcept
		:
		std::array<double, 4> {a, b, c, d}
	{
	}
	
	/// @param distance The distance from origin.
	constexpr Plane3D(Vec3d normal, double distance = {}) noexcept
		:
		Plane3D(normal[0], normal[1], normal[2], distance)
	{
		if (length(normal) == 0)
		{
			std::unreachable();
		}
	}
	
	constexpr Plane3D(Vec3d normal, Vec3d inside) noexcept
		:
		Plane3D(normal, -dot(inside, normal))
	{
	}
	
	/// @param point The point that lies inside the plane.
	/// @param vector1 @param vector2 The vectors relative to the @param point
	/// that lie inside the plane.
	constexpr Plane3D(Vec3d point, Vec3d vector1, Vec3d vector2) noexcept
		:
		Plane3D(cross(vector1, vector2), point)
	{
		if (length(vector1) == 0)
		{
			std::unreachable();
		}
		
		if (length(vector2) == 0)
		{
			std::unreachable();
		}
		
		if (distance(vector1, vector2) == 0)
		{
			std::unreachable();
		}
	}
	
	constexpr double substitute(Vec3d vector) const noexcept
	{
		return (*this)[0] * vector[0] + (*this)[1] * vector[1] + (*this)[2] * vector[2] + (*this)[3];
	}
	
	constexpr bool half_space(Vec3d vector) const noexcept
	{
		return substitute(vector) >= 0;
	}
};
} // namespace nwd::math
