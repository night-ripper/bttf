#pragma once

#include <cmath>
#include <numbers>

#include <random>
#include <numeric>
#include <type_traits>

namespace nwd::math
{
template<typename Result_type>
requires(std::is_floating_point_v<Result_type>)
struct Semicircle_distribution
{
	using result_type = Result_type;
	
	struct param_type
	{
		using distribution_type = Semicircle_distribution<result_type>;
		
		constexpr param_type(result_type radius = 1, result_type center = 1) noexcept
			:
			radius_(radius),
			center_(center)
		{
		}
		
		constexpr result_type radius() const noexcept {return radius_;}
		constexpr result_type center() const noexcept {return center_;}
		
		constexpr friend bool operator==(param_type, param_type) noexcept = default;
		
	private:
		result_type radius_;
		result_type center_;
	};
	
	constexpr Semicircle_distribution(param_type param = param_type()) noexcept
		:
		param_(param)
	{
	}
	
	constexpr static Semicircle_distribution radius_center(result_type radius, result_type center = 0) noexcept
	{
		return Semicircle_distribution(param_type(radius, center));
	}
	
	constexpr static Semicircle_distribution min_max(result_type min, result_type max) noexcept
	{
		auto middle = std::midpoint(min, max);
		return Semicircle_distribution(param_type(std::min({middle - min, max - middle}), middle));
	}
	
	constexpr Semicircle_distribution(const Semicircle_distribution&) noexcept = default;
	constexpr Semicircle_distribution& operator=(const Semicircle_distribution&) noexcept = default;
	
	void reset() const noexcept
	{
		return;
	}
	
	param_type param() const noexcept {return param_;}
	void param(param_type param) noexcept {param_ = param;}
	
	template<typename Generator_type>
	result_type operator()(Generator_type&& generator)
	{
		return (*this)(generator, param());
	}
	
	template<typename Generator_type>
	result_type operator()(Generator_type&& generator, param_type param)
	{
		auto radius = std::sqrt(std::generate_canonical<result_type, 1>(generator));
		
		auto angle = std::generate_canonical<result_type, 1>(generator) *
			std::numbers::pi_v<result_type>
		;
		
		auto value = std::cos(angle) * radius;
		
		return value * param.radius() + param.center();
	}
	
	constexpr result_type min() const noexcept {return param_.center() - param_.radius();}
	constexpr result_type max() const noexcept {return param_.center() + param_.radius();}
	
	constexpr friend bool operator==(Semicircle_distribution, Semicircle_distribution) noexcept = default;
	
private:
	param_type param_;
};
} // namespace nwd::math
