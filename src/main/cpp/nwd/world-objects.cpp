#include "world-objects.hpp"
#include "rendering-component.hpp"
#include "audio-component.hpp"

#include "resources.hpp"

#include "geometry/geometry.hpp"
#include "rendering/context.hpp"

#include "utility/normalize.hpp"

#include "rendering-coordinates-obj.hpp"

#include "glue/glsl.hpp"

#include <algorithm>
#include <iostream>
#include <random>
#include <experimental/random>

#include <GLFW/glfw3.h>

using utility::nrm;

namespace bttf
{
static std::pmr::unsynchronized_pool_resource resource_;
std::pmr::memory_resource& memory_resource() {return resource_;}

static std::random_device random_device;
static std::mt19937 random_gen {random_device()};

static std::vector<Wall*> walls;
static std::vector<Crate*> crates;
static std::vector<Door*> doors;
static std::vector<Key*> keys;

static Creature* player;

static std::array<geometry::Vector3D, 4> wall_vertices(const Wall_chunk& wall)
{
	std::array<geometry::Vector3D, 4> result = {{
		wall.begin_,
		wall.begin_ + rotate(geometry::Vector3D(wall.scale_, 0, 0), wall.quaternion_),
		wall.begin_ + geometry::Vector3D(0, 0, wall.scale_height_),
		wall.begin_ + rotate(geometry::Vector3D(wall.scale_, 0, wall.scale_height_), wall.quaternion_),
	}};
	
	return result;
}

static std::array<geometry::Vector3D, 10> crate_vertices(const Crate& crate)
{
	std::array<geometry::Vector3D, 10> result {};
	
	for (int i = 0; i < 8; ++i)
	{
		auto& vert = result[i];
		const geometry::coord_t half_side = crate.side_length_ / 2;
		
		if (i % 2 == 0)
		{
			vert.z() += half_side;
		}
		else
		{
			vert.z() -= half_side;
		}
		
		if (i < 4)
		{
			vert.y() -= half_side;
		}
		else
		{
			vert.y() += half_side;
		}
		
		if (i < 2 or i >= 6)
		{
			vert.x() -= half_side;
		}
		else
		{
			vert.x() += half_side;
		}
		
		vert = rotate(vert, crate.quaternion_),
		
		vert += crate.sphere_.center();
	}
	
	result[8] = result[0];
	result[9] = result[1];
	
	return result;
}

template<bool Constant = false, typename Iterator>
static Iterator overlap_divide(Iterator begin, Iterator end,
	geometry::Cylinder3D cylinder, geometry::Vector3D vector)
{
	const auto line = geometry::Line_segment3D::point_vec(cylinder.center(), vector);
	cylinder.move(vector);
	
	const auto lambda = [&](const auto& triangle) -> bool
	{
		return overlap(cylinder, triangle) or overlap(line, triangle);
	};
	
	if constexpr (Constant)
	{
		return std::find_if(begin, end, lambda);
	}
	else
	{
		return std::partition(begin, end, std::not_fn(lambda));
	}
}

template<bool Constant = false, typename Iterator>
static std::tuple<Iterator, geometry::Vector3D>
safe_vector(Iterator begin, Iterator end, geometry::Cylinder3D cylinder, geometry::Vector3D vector)
{
	utility::assert_that([&]() -> bool
	{
		auto result = std::find_if(begin, end, [&](const auto& triangle) -> bool
		{
			return overlap(cylinder, triangle);
		});
		
		return result == end;
	});
	
	Iterator collision = overlap_divide<Constant>(begin, end, cylinder, vector);
	
	if (collision != end)
	{
		geometry::Vector3D safe {};
		geometry::Vector3D mid = midpoint(safe, vector);
		
		for (int i = 0; i < 3; ++i)
		{
			if (auto new_result = overlap_divide<Constant>(begin, end, cylinder, mid);
				new_result != end)
			{
				vector = mid;
				collision = new_result;
			}
			else
			{
				safe = mid;
			}
			
			if (auto new_mid = midpoint(safe, vector);
				new_mid != safe)
			{
				mid = new_mid;
			}
			else
			{
				break;
			}
		}
		
		vector = safe;
	}
	
	utility::assert_that([&]() -> bool
	{
		auto moved_cylinder = cylinder;
		moved_cylinder.move(vector);
		
		return end == std::find_if(begin, end, [&](const auto& triangle) -> bool
		{
			return overlap(moved_cylinder, triangle);
		});
	});
	
	return {collision, vector};
}

static std::tuple<geometry::Cylinder3D, geometry::Vector3D>
move_slide(geometry::Cylinder3D cylinder, geometry::Vector3D move, World& world)
{
	std::vector<geometry::Triangle3D> environment;
	
	geometry::Sphere3D area(cylinder.center(),
		(std::hypot(cylinder.radius(), double(cylinder.height())) + length(move)) * 1.05
	);
	
	world.rtree_collision_.visit_immediate(
	[&area](const geometry::Bounding_box3D& box) -> bool
	{
		return overlap(area, box);
	},
	[&environment, &area](const World::Object_base& object) -> void
	{
		if (auto [it, out] = std::equal_range(std::begin(walls), std::end(walls), &object);
			it != std::end(walls) and it + 1 == out)
		{
			for (auto tri : geometry::triangles(wall_vertices(**it)))
			{
				if (overlap(area, tri))
				{
					environment.push_back(tri);
				}
			}
		}
		else if (auto [it, out] = std::equal_range(std::begin(crates), std::end(crates), &object);
			it != std::end(crates) and it + 1 == out)
		{
			for (auto tri : geometry::triangles(crate_vertices(**it)))
			{
				if (overlap(area, tri))
				{
					environment.push_back(tri);
				}
			}
		}
	});
	
	geometry::Cylinder3D result = cylinder;
	
	for (int i = 0; i < 2; ++i)
	{
		if (length(move) <= 2 * double(geometry::coord_t::epsilon()))
		{
			break;
		}
		
		auto [collision, safe] = safe_vector(std::begin(environment), std::end(environment), result, move);
		result.move(safe);
		move -= safe;
		
		if (collision != std::end(environment))
		{
			/// Do not move completely towards the collision, but stay slighly away
			/// so that further sliding has enough space
			
			{
				/// TODO
				auto move_away = std::accumulate(collision, std::end(environment),
				geometry::Vector3D {}, [&](auto out_vector, const auto& tri) -> geometry::Vector3D
				{
					return out_vector += tri.normal();
				});
				move_away = normalize(move_away) * 2 * double(geometry::coord_t::epsilon());
				auto moved_result = result;
				moved_result.move(move_away);
				
				auto can_move = std::find_if(std::begin(environment), std::end(environment),
				[&](const auto& tri) -> bool
				{
					return overlap(moved_result, tri);
				});
				
				if (can_move == std::end(environment))
				{
					result.move(move_away);
					move -= move_away;
				}
			}
			
			geometry::Vector3D new_move {};
			double new_length = 0;
			
			/// The sum of all collisions' normals
			geometry::Vector3D accumulated_normal;
			
			const auto max_move = [&](geometry::Vector3D corrected_move) -> void
			{
				if (length(corrected_move) > 2 * double(geometry::coord_t::epsilon()))
				{
					corrected_move = std::get<geometry::Vector3D>(safe_vector<true>(
						std::begin(environment), std::end(environment), result, corrected_move
					));
					
					if (auto len = length(corrected_move); new_length < len)
					{
						new_move = corrected_move;
						new_length = len;
					}
				}
			};
			
			std::for_each(collision, std::end(environment), [&](const auto& tri) -> void
			{
				auto normal = cross(tri.vectors()[0], tri.vectors()[1]);
				normal.z() = 0;
				normal = normalize(normal);
				accumulated_normal += normal;
				
				/// Multiplying by that number makes the sliding smoother
				/// It pushes the object slightly more away from the collision
				auto corrected_move = move - normal * dot(move, normal) * 1.01;
				
				max_move(corrected_move);
			});
			
			if (const auto num_collisions = std::distance(collision, std::end(environment));
				num_collisions > 1)
			{
				accumulated_normal = normalize(accumulated_normal);
				
				auto corrected_move = move - accumulated_normal * dot(move, accumulated_normal) * 1.01;
				
				max_move(corrected_move);
			}
			
			result.move(new_move);
			move -= new_move;
		}
		else
		{
			break;
		}
	}
	
	return {result, move};
}

Wall_chunk::Wall_chunk(geometry::Vector3D begin, geometry::Vector3D end, index_type tex_index, float height)
	:
	Rendering_object(this, [](decltype(this) self, Rendering_component& comp) -> void
	{
		return self->write_buffers(comp);
	}),
	tex_index_(tex_index),
	begin_ {begin},
	scale_ {float(distance(begin, end))},
	scale_height_ {height}
{
	const auto diff = geometry::Vector3D(begin, end);
	const auto angle = std::atan2(float(diff.y()), float(diff.x()));
	quaternion_ = geometry::Quaternion::from_unit_axis_angle(0, 0, 1, angle);
}

void Wall_chunk::write_buffers(Rendering_component& comp)
{
	const auto ns = Vertex_data::Normal_coordinates(0, -1, 0);
	
	comp.vdata_.insert(std::end(comp.vdata_), {
		{begin_, {0, 0}, tex_index_, {0, 0, 0}, {scale_, 1, scale_height_}, quaternion_, {}, {}, {}},
		{begin_, {nrm(1.), 0}, tex_index_, {nrm(1.), 0, 0}, {scale_, 1, scale_height_}, quaternion_, {}, {}, {}},
		{begin_, {0, nrm(1.)}, tex_index_, {0, 0, nrm(1.)}, {scale_, 1, scale_height_}, quaternion_, ns, {}, {}},
		{begin_, {nrm(1.), nrm(1.)}, tex_index_, {nrm(1.), 0, nrm(1.)}, {scale_, 1, scale_height_}, quaternion_, ns, {}, {}},
	});
	
	auto& end = comp.last_index_;
	comp.vindices_.insert(std::end(comp.vindices_), {
		end + 0, end + 1, end + 2, end + 3, Element_index::restart
	});
	end += 4;
}

Wall_chunk::operator geometry::Bounding_box3D() const
{
	const auto vertices = wall_vertices(*this);
	
	return {std::begin(vertices), std::end(vertices)};
}

Wall::~Wall()
{
	if (auto it = std::lower_bound(std::begin(walls), std::end(walls), this);
		it != std::end(walls))
	{
		walls.erase(it);
	}
}

Wall::Wall(geometry::Vector3D begin, geometry::Vector3D end, index_type tex_index, float height)
	:
	Object(this),
	Wall_chunk(begin, end, tex_index, height)
{
	walls.insert(std::lower_bound(std::begin(walls), std::end(walls), this), this);
}

Floor_chunk::Floor_chunk(geometry::Vector3D origin, index_type tex_index)
	:
	Rendering_object(this, [](decltype(this) self, Rendering_component& comp) -> void
	{
		return self->write_buffers(comp);
	}),
	tex_index_(tex_index),
	center_ {origin}
{
	center_.x() += 0.5;
	center_.y() -= 0.5;
}

void Floor_chunk::write_buffers(Rendering_component& comp)
{
	comp.vdata_.insert(std::end(comp.vdata_), {
		{center_, {0, 0}, tex_index_, {nrm(-0.5), nrm(0.5), 0}, {}, {}, {}, {}, {}},
		{center_, {nrm(1.), 0}, tex_index_, {nrm(-0.5), nrm(-0.5), 0}, {}, {}, {}, {}, {}},
		{center_, {0, nrm(1.)}, tex_index_, {nrm(0.5), nrm(0.5), 0}, {}, {}, {0, 0, nrm(1.)}, {}, {}},
		{center_, {nrm(1.), nrm(1.)}, tex_index_, {nrm(0.5), nrm(-0.5), 0}, {}, {}, {0, 0, nrm(1.)}, {}, {}},
	});
	
	auto& end = comp.last_index_;
	comp.vindices_.insert(std::end(comp.vindices_), {
		end + 0, end + 1, end + 2, end + 3, Element_index::restart
	});
	end += 4;
}

Floor_chunk::operator geometry::Bounding_box3D() const
{
	return {center_ - geometry::Vector3D(0.5, 0.5, 0), center_ + geometry::Vector3D(0.5, 0.5, 0)};
}

Floor::Floor(geometry::Vector3D origin, index_type tex_index)
	:
	Object(this),
	Floor_chunk(origin, tex_index)
{
}

Smonk::~Smonk()
{
}

Smonk::Smonk(geometry::Cylinder3D cylinder)
	:
	Object(this, [](decltype(this) self, float seconds) -> void
	{
		self->angle_ += seconds;
		self->center_.z() += seconds;
		self->transparency_ += seconds / 3;
		
		if (self->transparency_ > 1)
		{
			self->disable();
		}
	}),
	Rendering_object(this, [](decltype(this) self, Rendering_component& comp) -> void
	{
		return self->write_buffers(comp);
	}),
	tex_index_(bttf::Smonk::index_type(bttf::translucent_binding, bttf::smonk)),
	cylinder_ {cylinder},
	center_ {cylinder.center() - geometry::Vector3D(0, 0, cylinder.height())},
	rotation_vector_ {[]() -> glsl::vec3
	{
		std::normal_distribution<float> ndistr(0, 1);
		glsl::vec3 random_unit_vector(ndistr(random_gen), ndistr(random_gen), ndistr(random_gen));
		return normalize(random_unit_vector);
	}()},
	angle_ {std::uniform_real_distribution<float> {0, geometry::angle_t::max()}(random_gen)}
{
}

void Smonk::write_buffers(Rendering_component& comp)
{
	const auto last_index = comp.last_index_transparent_;
	const auto vdata_begin = std::ssize(comp.vdata_transparent_);
	const auto vindices_begin = std::ssize(comp.vindices_transparent_);
	
	comp.vdata_transparent_.resize(vdata_begin + std::ssize(bttf::smonk_obj->vdata_));
	comp.vindices_transparent_.resize(vindices_begin + std::ssize(bttf::smonk_obj->vindices_));
	comp.last_index_transparent_ += bttf::smonk_obj->size();
	
	bttf::smonk_obj->fill_indices(&comp.vindices_transparent_[vindices_begin], last_index);
	
	std::transform(bttf::smonk_obj->vbegin(), bttf::smonk_obj->vend(), &comp.vdata_transparent_[vdata_begin],
	[&](Vertex_data vd) -> Vertex_data
	{
		vd.vertex_coordinates = center_;
		vd.texture_indices = {bttf::translucent_binding, bttf::smonk};
		// vd.texture_indices = Vertex_data::Texture_indices::color_only();
		
		float scale = cylinder_.radius() + transparency_ / 2;
		vd.scale = {scale, scale, scale};
		vd.quaternion  = geometry::Quaternion::from_unit_axis_angle(
			rotation_vector_.x, rotation_vector_.y, rotation_vector_.z, angle_
		);
		
		{
			float value = transparency_ + 1;
			value = (1 + 1. / 3) / (value * value) - (1. / 3);
			vd.color = {nrm(.2), nrm(.5), nrm(.2), nrm(value)};
		}
		vd.ambient_color = {nrm(.2), nrm(.5), nrm(.2)};
		
		return vd;
	});
}

Smonk::operator geometry::Bounding_box3D() const
{
	return geometry::Bounding_box3D(cylinder_);
}

Smonk_generator::Smonk_generator(geometry::Cylinder3D cylinder, float delay)
	:
	Object(this, [](decltype(this) self, float seconds) -> void
	{
		self->delay_ -= seconds;
		
		if (self->delay_ < 0)
		{
			self->set_time_handler([](decltype(this) self, float seconds) -> void
			{
				if (overlap(self->cylinder_, player->cylinder_))
				{
					player->health_ -= seconds;
					
					if (player->health_ <= 0)
					{
						throw std::runtime_error("You died of dank air poisoning");
					}
				}
				
				self->time_to_generate_ -= seconds;
				
				if (self->time_to_generate_ < 0)
				{
					self->time_to_generate_ = .10;
					
					auto& smok = self->world().attach(std::allocate_shared<bttf::Smonk>(
						std::pmr::polymorphic_allocator(&bttf::memory_resource()), self->cylinder_
					));
					self->world().attach(static_cast<bttf::World::Rendering_object&>(smok), geometry::Bounding_box3D(smok));
				}
			});
		}
	}),
	cylinder_ {cylinder},
	light_ {geometry::Sphere3D({cylinder_.center() + geometry::Vector3D(0, 0, cylinder_.height())}, 10), 0, 1, 0},
	time_to_generate_ {.10},
	delay_ {delay}
{
}

struct Punch : bttf::World::Object<Punch>
{
	Creature& owner_;
	float begin_;
	float duration_;
	float passed_;
	bool punched_ = false;
	
	std::optional<std::array<geometry::Cylinder3D, 2>> collision_;
	
	audio::openal::Source source_;
	audio::openal::Source source_punch_;
	const audio::openal::Buffer* buffer_;
	
	~Punch()
	{
	}
	
	Punch(Creature& owner, float duration)
		:
		Object(this, [](decltype(this) self, float seconds) -> void
		{
			self->source_.play();
			
			self->set_time_handler([](decltype(this) self, float seconds) -> void
			{
				self->time_handle(seconds);
			});
			
			self->time_handle(seconds);
		}),
		owner_ {owner},
		duration_ {duration},
		passed_ {0}
	{
	}
	
	void time_handle(float seconds)
	{
		passed_ += seconds;
		
		if (passed_ > duration_)
		{
			owner_.punch_ = nullptr;
			owner_.disown(this);
			owner_.tex_index_.array_index = bttf::corey_first;
			return;
		}
		
		float index = passed_ / duration_ * (bttf::corey_last - bttf::corey_first) + 1;
		owner_.tex_index_.array_index = bttf::corey_first + index;
		
		if (index < bttf::corey_first + 2 or punched_)
		{
			return;
		}
		
		punched_ = true;
		
		if (not collision_)
		{
			collision_.emplace();
			(*collision_)[0] = owner_.cylinder_;
			(*collision_)[0].move(geometry::Vector3D(owner_.cylinder_.radius(), 0, 0).rotate_xy(owner_.angle_xy_));
			
			(*collision_)[1] = owner_.cylinder_;
			(*collision_)[1].move(geometry::Vector3D(2 * owner_.cylinder_.radius(), 0, 0).rotate_xy(owner_.angle_xy_));
		}
		
		std::vector<Crate*> crates_hit;
		
		owner_.world().rtree_collision_.visit_immediate(
		[&](const geometry::Bounding_box3D& box) -> bool
		{
			return overlap((*collision_)[0], box) or overlap((*collision_)[1], box);
		},
		[&](World::Object_base& object) -> void
		{
			if (auto [it, out] = std::equal_range(std::begin(doors), std::end(doors), &object);
				it != std::end(doors) and it + 1 == out)
			{
				for (auto tri : geometry::triangles(wall_vertices(**it)))
				{
					if (overlap((*collision_)[0], tri) or overlap((*collision_)[1], tri))
					{
						if ((*it)->locked_)
						{
							for (const auto& shared : owner_.keys_)
							{
								if (shared.get() == (*it)->unlocked_by_)
								{
									(*it)->locked_ = false;
									
									auto& sound = owner_.world().attach(std::allocate_shared<Audio_object>(
										std::pmr::polymorphic_allocator(&bttf::memory_resource()),
										bttf::door_unlocked_sound, geometry::Bounding_box3D(**it).center()
									));
									sound.source_.play();
									
									(*it)->tex_index_ = {bttf::creatures_binding, bttf::door_open};
									
									walls.erase(std::lower_bound(std::begin(walls), std::end(walls), (*it)));
									return;
								}
							}
							
							auto& sound = owner_.world().attach(std::allocate_shared<Audio_object>(
								std::pmr::polymorphic_allocator(&bttf::memory_resource()),
								bttf::door_locked_sound, geometry::Bounding_box3D(**it).center()
							));
							sound.source_.play();
						}
					}
				}
			}
			
			if (auto [it, out] = std::equal_range(std::begin(crates), std::end(crates), &object);
				it != std::end(crates) and it + 1 == out)
			{
				if (overlap((*collision_)[0], (*it)->sphere_) or overlap((*collision_)[1], (*it)->sphere_))
				{
					crates_hit.push_back((*it));
				}
			}
		});
		
		for (auto* c : crates_hit)
		{
			c->get_hit();
		}
	}
};

struct Controller
{
	Rendering_component* rcomp_ = nullptr;
	rendering::opengl::Context* context_ = nullptr;
	Creature* player_;
	
	float cursor_coefficient_xy_;
	float cursor_coefficient_z_;
	float scroll_coefficient_;
	
	geometry::coord_t scroll_min_;
	geometry::coord_t scroll_max_;
	
	float xpos_;
	float ypos_;
	
	geometry::angle_t angle_z_;
	
	Camera& camera() const {return player_->world().camera_;}
	
	void framebuffer_size([[maybe_unused]] int width, [[maybe_unused]] int height)
	{
		glViewport(0, 0, width, height);
		
		camera().aspect_ratio(float(width) / float(height));
		camera() = locked_camera(camera().position());
	}
	
	void key([[maybe_unused]] int key, [[maybe_unused]] int scancode,
		[[maybe_unused]] int action, [[maybe_unused]] int modifier_keys)
	{
		using namespace geometry;
		
		if (false);
		else if (key == GLFW_KEY_W)
		{
			auto v = Vector3D(1, 0, 0);
			if (action == GLFW_PRESS)
			{
				player_->moving_ += v;
			}
			else if (action == GLFW_RELEASE)
			{
				player_->moving_ -= v;
			}
		}
		else if (key == GLFW_KEY_S)
		{
			auto v = -Vector3D(1, 0, 0);
			if (action == GLFW_PRESS)
			{
				player_->moving_ += v;
			}
			else if (action == GLFW_RELEASE)
			{
				player_->moving_ -= v;
			}
		}
		else if (key == GLFW_KEY_A)
		{
			auto v = Vector3D(0, 1, 0);
			if (action == GLFW_PRESS)
			{
				player_->moving_ += v;
			}
			else if (action == GLFW_RELEASE)
			{
				player_->moving_ -= v;
			}
		}
		else if (key == GLFW_KEY_D)
		{
			auto v = -Vector3D(0, 1, 0);
			if (action == GLFW_PRESS)
			{
				player_->moving_ += v;
			}
			else if (action == GLFW_RELEASE)
			{
				player_->moving_ -= v;
			}
		}
		else if (key == GLFW_KEY_L)
		{
			if (action == GLFW_PRESS)
			{
				rcomp_->rescale(rcomp_->framebuffer_width_ * 4 / 3, rcomp_->framebuffer_height_ * 4 / 3);
				std::cout << rcomp_->framebuffer_width_ << " : " << rcomp_->framebuffer_height_ << "\n";
			}
		}
		else if (key == GLFW_KEY_P)
		{
			if (action == GLFW_PRESS)
			{
				rcomp_->rescale(rcomp_->framebuffer_width_ * 3 / 4, rcomp_->framebuffer_height_ * 3 / 4);
				std::cout << rcomp_->framebuffer_width_ << " : " << rcomp_->framebuffer_height_ << "\n";
			}
		}
		else if (key == GLFW_KEY_F)
		{
			if (action == GLFW_PRESS)
			{
				player_->flashlight_.multiplier_ = not (player_->flashlight_.multiplier_);
				auto& ref = player_->world().attach(std::allocate_shared<bttf::Audio_object>(
					std::pmr::polymorphic_allocator(&bttf::memory_resource()),
					bttf::flashlight
				));
				
				ref.source_.play();
			}
		}
		else if (key == GLFW_KEY_K)
		{
			if (action == GLFW_PRESS)
			{
				rcomp_->gamma_ /= 1.1;
				rcomp_->set_gamma();
				std::cout << "gamma: " << rcomp_->gamma_ << "\n";
			}
		}
		else if (key == GLFW_KEY_O)
		{
			if (action == GLFW_PRESS)
			{
				rcomp_->gamma_ *= 1.1;
				rcomp_->set_gamma();
				std::cout << "gamma: " << rcomp_->gamma_ << "\n";
			}
		}
		else if (key == GLFW_KEY_J)
		{
			if (action == GLFW_PRESS)
			{
				rcomp_->exposure_ /= 1.1;
				rcomp_->set_exposure();
				std::cout << "exposure: " << rcomp_->exposure_ << "\n";
			}
		}
		else if (key == GLFW_KEY_I)
		{
			if (action == GLFW_PRESS)
			{
				rcomp_->exposure_ *= 1.1;
				rcomp_->set_exposure();
				std::cout << "exposure: " << rcomp_->exposure_ << "\n";
			}
		}
	}
	
	Camera locked_camera(geometry::Vector3D origin)
	{
		using namespace geometry;
		
		auto half_fov_y = angle_t(camera().fov() / 2) / camera().aspect_ratio();
		angle_z_ = angle_t(-pi / 2) + half_fov_y -
			angle_t(std::atan(player_->cylinder_.radius() / double(origin.z())))
		;
		std::cout << (player_->cylinder_.radius() / double(origin.z())) << "\n";
		
		return Camera::upwards_camera(origin, player_->angle_xy_, angle_z_, camera().perspective());
	}
	
	void cursor([[maybe_unused]] double xpos, [[maybe_unused]] double ypos)
	{
		auto xdiff = xpos - xpos_;
		[[maybe_unused]] auto ydiff = ypos - ypos_;
		
		context_->cursor_position(xpos_, ypos_);
		
		player_->angle_xy_ += cursor_coefficient_xy_ * xdiff;
		// angle_z_ += cursor_coefficient_z_ * ydiff;
		camera().rotated(player_->angle_xy_, angle_z_);
		
		Audio_component::current()->orienation_x_ = std::cos(-player_->angle_xy_);
		Audio_component::current()->orienation_y_ = std::sin(-player_->angle_xy_);
		
		player_->flashlight_.direction_ = {std::cos(float(player_->angle_xy_)), std::sin(float(player_->angle_xy_)), 0};
		
		/*
		if (camera().unit_up_vector().z < 0)
		{
			const signed char sign = camera().unit_direction().z > 0 ? 1 : -1;
			
			angle_z_ = pi / 2 * sign;
			opengl::vec3 direction = camera().unit_direction() + camera().unit_up_vector();
			direction.z = 0;
			direction = normalize(direction) * -sign;
			angle_t angle_xy = std::atan2(direction.y, direction.x);
			
			camera().rotated(angle_xy, angle_z_);
		}
		*/
	}
	
	void scroll([[maybe_unused]] double xoffset, [[maybe_unused]] double yoffset)
	{
		using namespace geometry;
		
		auto new_position = camera().position();
		new_position.z() = std::clamp(
			new_position.z() - coord_t(yoffset * scroll_coefficient_),
			scroll_min_, scroll_max_
		);
		
		camera() = locked_camera(new_position);
	}
	
	void mouse_button([[maybe_unused]] int button, [[maybe_unused]] int action, [[maybe_unused]] int mods)
	{
		if (button == GLFW_MOUSE_BUTTON_RIGHT)
		{
			if (action == GLFW_PRESS)
			{
				player_->light_.multiplier_ = not (player_->light_.multiplier_);
			}
		}
		else if (button == GLFW_MOUSE_BUTTON_LEFT)
		{
			if (action == GLFW_PRESS and not player_->punch_)
			{
				auto punch = std::allocate_shared<bttf::Punch>(
					std::pmr::polymorphic_allocator(&bttf::memory_resource()),
					*player_, 0.4
				);
				
				punch->buffer_ = bttf::punch_wind;
				punch->source_ = audio::openal::Source::create();
				punch->source_ << *punch->buffer_;
				
				player_->punch_ = punch.get();
				player_->own(std::move(punch));
			}
		}
	}
}
static controller;

Creature::~Creature()
{
}

Creature::Creature(geometry::Cylinder3D collision, float tex_center_x, float tex_center_y, float tex_scale,
	geometry::angle_t angle_xy, index_type tex_index)
	:
	Object(this, [](decltype(this) self, float seconds) -> void
	{
		return self->time_handle(seconds);
	}),
	Rendering_object(this, [](decltype(this) self, Rendering_component& comp) -> void
	{
		return self->write_buffers(comp);
	}),
	cylinder_ {collision},
	tex_center_x_ {tex_center_x},
	tex_center_y_ {tex_center_y},
	tex_scale_ {tex_scale},
	tex_index_ {tex_index},
	angle_xy_ {angle_xy},
	light_ {geometry::Sphere3D(cylinder_.center() +
		geometry::Vector3D(0, 0, cylinder_.height() + geometry::coord_t(0.1)), 20),
		0.5, 0.5, 0.5
	},
	flashlight_ {geometry::Sphere3D(cylinder_.center(), 50), 2, 2, 2}
{
	flashlight_.direction_ = {std::cos(float(angle_xy_)), std::sin(float(angle_xy_)), 0};
	
	flashlight_.cos_inner_cutoff_ = std::cos(20.f / 180 * geometry::pi);
	flashlight_.cos_outer_cutoff_ = std::cos(60.f / 180 * geometry::pi);
	
	bttf::player = this;
}

void Creature::control(rendering::opengl::Context& context, Rendering_component& rcomp)
{
	Audio_component::current()->listener_position_ = cylinder_.center();
	Audio_component::current()->orienation_x_ = std::cos(-angle_xy_);
	Audio_component::current()->orienation_y_ = std::sin(-angle_xy_);
	
	controller.rcomp_ = &rcomp;
	controller.context_ = &context;
	controller.player_ = this;
	
	controller.cursor_coefficient_xy_ = -0.0015;
	controller.cursor_coefficient_z_ = -0.0015;
	controller.scroll_coefficient_ = 0.35;
	
	controller.scroll_min_ = 4;
	controller.scroll_max_ = 10;
	
	using namespace geometry;
	
	Camera::Perspective per {0.01, 1000, pi / 2, 16.f / 9.f};
	float height = controller.scroll_min_ + cylinder_.center().z();
	auto half_fov_y = angle_t(per.x_fov / 2) / per.aspect_ratio;
	
	controller.angle_z_ = angle_t(-pi / 2) + half_fov_y -
		angle_t(std::atan(cylinder_.radius() / height))
	;
	
	world().camera_ = Camera::upwards_camera(
		{cylinder_.center().x(), cylinder_.center().y(), height},
		angle_xy_,
		controller.angle_z_,
		per
	);
	
	{
		auto [xpos, ypos] = context.cursor_position();
		controller.xpos_ = xpos;
		controller.ypos_ = ypos;
	}
	
	// context.window_position_handler([](auto&&... args) -> void {controller.window_position(args...);});
	context.framebuffer_size_handler([](auto&&... args) -> void {controller.framebuffer_size(args...);});
	context.key_handler([](auto&&... args) -> void {controller.key(args...);});
	context.cursor_position_handler([](auto&&... args) -> void {controller.cursor(args...);});
	context.scroll_handler([](auto&&... args) -> void {controller.scroll(args...);});
	context.mouse_button_handler([](auto&&... args) -> void {controller.mouse_button(args...);});
}

static audio::openal::Source run_sound;
static int anime_index = -1;

void Creature::time_handle(float seconds)
{
	auto moving = moving_;
	
	if (auto state = glfwGetKey(static_cast<GLFWwindow*>(rendering::opengl::Context::window()), GLFW_KEY_LEFT_SHIFT);
		moving != geometry::Vector3D {} and state == GLFW_PRESS)
	{
		moving *= 3;
		
		if (not run_sound)
		{
			run_sound = audio::openal::Source::create();
			run_sound << *bttf::run;
			run_sound.loop(true);
			run_sound.play();
		}
		
		if (anime_index == -1 or anime_index == bttf::anime_last + 1)
		{
			anime_index = bttf::anime_first;
		}
		
		++anime_index;
	}
	else if (run_sound)
	{
		run_sound = {};
		anime_index = -1;
	}
	
	velocity_ += geometry::Vector3D(moving).rotate_xy(angle_xy_);
	
	if (auto velocity = velocity_ * seconds; velocity != geometry::Vector3D())
	{
		auto [cylinder, vector] = move_slide(cylinder_, velocity, world());
		
		const auto vector_diff = cylinder.center() - cylinder_.center();
		
		cylinder_ = cylinder;
		velocity_ = {};
		
		handle_.transform(geometry::Bounding_box3D(cylinder_));
		static_cast<Rendering_object&>(*this).handle_.transform(geometry::Bounding_box3D(cylinder_));
		
		world().camera_.position(world().camera_.position() + vector_diff);
		
		Audio_component::current()->listener_position_ = cylinder_.center();
		
		light_.area_.move(vector_diff);
		light_.handle_.transform(geometry::Bounding_box3D(light_.area_));
		
		flashlight_.area_.move(vector_diff);
		flashlight_.handle_.transform(geometry::Bounding_box3D(flashlight_.area_));
	}
}

void Creature::write_buffers(Rendering_component& comp)
{
	const auto center = cylinder_.center();
	const float r = cylinder_.radius();
	const float h = cylinder_.center().z();
	const auto q = geometry::Quaternion::from_unit_axis_angle(0, 0, 1, angle_xy_);
	const auto sc = Vertex_data::Scale(tex_scale_, tex_scale_, h);
	const float tx = tex_center_x_ * r;
	const float ty = tex_center_y_ * r;
	
	comp.vdata_.insert(std::end(comp.vdata_), {
		{center, {0, nrm(1.)}, tex_index_, {nrm(tx - r), nrm(ty + r), nrm(1.)}, sc, q, {}, {}, {}},
		{center, {0, 0}, tex_index_, {nrm(tx - r), nrm(ty - r), nrm(1.)}, sc, q, {}, {}, {}},
		{center, {nrm(1.), nrm(1.)}, tex_index_, {nrm(tx + r), nrm(ty + r), nrm(1.)}, sc, q, {0, 0, nrm(1.)}, {}, {}},
		{center, {nrm(1.), 0}, tex_index_, {nrm(tx + r), nrm(ty - r), nrm(1.)}, sc, q, {0, 0, nrm(1.)}, {}, {}},
	});
	
	auto& end = comp.last_index_;
	comp.vindices_.insert(std::end(comp.vindices_), {
		end + 0, end + 1, end + 2, end + 3, Element_index::restart
	});
	end += 4;
	
	float cx[] {-.9, std::lerp(-.9f, -.2f, health_)};
	float cy[] {.8, .9};
	
	const Post_data::Texture_indices health_indices = {bttf::environment_binding, bttf::health};
	
	comp.post_triangles_.insert(std::end(comp.post_triangles_), {
		{{nrm(cx[0]), nrm(cy[0])}, {nrm(0.f), nrm(0.f)}, health_indices, {}},
		{{nrm(cx[1]), nrm(cy[0])}, {nrm(1.f), nrm(0.f)}, health_indices, {}},
		{{nrm(cx[0]), nrm(cy[1])}, {nrm(0.f), nrm(1.f)}, health_indices, {}},
		
		{{nrm(cx[0]), nrm(cy[1])}, {nrm(0.f), nrm(1.f)}, health_indices, {}},
		{{nrm(cx[1]), nrm(cy[0])}, {nrm(1.f), nrm(0.f)}, health_indices, {}},
		{{nrm(cx[1]), nrm(cy[1])}, {nrm(1.f), nrm(1.f)}, health_indices, {}},
	});
	
	/// Currently sprinting
	if (anime_index != -1)
	{
		const Post_data::Texture_indices anime_indices = {
			bttf::anime_binding, Post_data::Texture_indices::element_type(anime_index - 1)
		};
		
		comp.post_triangles_.insert(std::end(comp.post_triangles_), {
			{{nrm(-1.f), nrm(-1.f)}, {nrm(0.f), nrm(0.f)}, anime_indices, {}},
			{{nrm(1.f), nrm(-1.f)}, {nrm(1.f), nrm(0.f)}, anime_indices, {}},
			{{nrm(-1.f), nrm( 1.f)}, {nrm(0.f), nrm(1.f)}, anime_indices, {}},
			
			{{nrm(-1.f), nrm( 1.f)}, {nrm(0.f), nrm(1.f)}, anime_indices, {}},
			{{nrm(1.f), nrm(-1.f)}, {nrm(1.f), nrm(0.f)}, anime_indices, {}},
			{{nrm(1.f), nrm( 1.f)}, {nrm(1.f), nrm(1.f)}, anime_indices, {}},
		});
	}
}

Light_chunk::Light_chunk(geometry::Sphere3D area, float r, float g, float b)
	:
	Rendering_object(this, [](decltype(this) self, Rendering_component& comp) -> void
	{
		return self->write_buffers(comp);
	}),
	area_ {area},
	r_ {r}, g_ {g}, b_ {b}
{
}

void Light_chunk::write_buffers(bttf::Rendering_component& comp)
{
	if (multiplier_ == 0)
	{
		return;
	}
	
	comp.vlights_.push_back({area_.center(), float(area_.radius()),
		{multiplier_ * r_, multiplier_ * g_, multiplier_ * b_},
		cos_inner_cutoff_, direction_, cos_outer_cutoff_
	});
}

Light::Light(geometry::Sphere3D area, float r, float g, float b)
	:
	Object(this),
	Light_chunk(area, r, g, b)
{
}

Audio_chunk::~Audio_chunk()
{
}

Audio_chunk::Audio_chunk(const audio::openal::Buffer* buffer, geometry::Vector3D position)
	:
	source_ {audio::openal::Source::create()},
	buffer_ {buffer},
	position_ {position}
{
	update_position();
	source_ << *buffer_;
}

Audio_chunk::Audio_chunk(const audio::openal::Buffer* buffer)
	:
	source_ {audio::openal::Source::create()},
	buffer_ {buffer}
{
	source_.position({0, 0, 0});
	source_ << *buffer_;
}

void Audio_chunk::update_position()
{
	auto result = position_ - bttf::Audio_component::current()->listener_position_;
	result.rotate_xy(bttf::Audio_component::current()->orienation_y_, bttf::Audio_component::current()->orienation_x_);
	source_.position({float(result.x()), float(result.y()), float(result.z())});
}

Audio_object::~Audio_object()
{
}

Audio_object::Audio_object(const audio::openal::Buffer* buffer, geometry::Vector3D position)
	:
	Object(this, [](decltype(this) self, float) -> void
	{
		if (self->source_.finished())
		{
			self->disable();
		}
		else
		{
			self->update_position();
		}
	}),
	Audio_chunk(buffer, position)
{
}

Audio_object::Audio_object(const audio::openal::Buffer* buffer)
	:
	Object(this, [](decltype(this) self, float) -> void
	{
		if (self->source_.finished())
		{
			self->disable();
		}
	}),
	Audio_chunk(buffer)
{
}

Crate::~Crate()
{
	crates.erase(std::lower_bound(std::begin(crates), std::end(crates), this));
}

Crate::Crate(geometry::Vector3D bottom, float side_length, geometry::angle_t angle, index_type tex_index, bttf::Wavefront_obj_vertex_data* model)
	:
	Object(this),
	Rendering_object(this, [](decltype(this) self, bttf::Rendering_component& comp) -> void
	{
		self->write_buffers(comp);
	}),
	side_length_ {side_length},
	sphere_ {geometry::Sphere3D(bottom + geometry::Vector3D(0, 0, side_length / 2),
		std::sqrt(3. / 2 * (side_length * side_length)))},
	quaternion_ {geometry::Quaternion::from_unit_axis_angle(0, 0, 1, angle)},
	tex_index_ {tex_index},
	model_ {model},
	life_ {std::experimental::randint(3, 6)}
{
	crates.insert(std::lower_bound(std::begin(crates), std::end(crates), this), this);
}

void Crate::get_hit()
{
	--life_;
	
	if (life_ <= 0)
	{
		if (key_)
		{
			auto& p = world().attach(std::move(key_));
			world().attach(static_cast<bttf::World::Rendering_object&>(p), geometry::Bounding_box3D(p.collision_));
			world().attach(p.light_, geometry::Bounding_box3D(p.light_.area_));
		}
		
		auto& ref = world().attach(std::allocate_shared<bttf::Audio_object>(
			std::pmr::polymorphic_allocator(&bttf::memory_resource()),
			bttf::wood_break
		));
		
		ref.source_.play();
		
		disable();
	}
	else
	{
		auto& ref = world().attach(std::allocate_shared<bttf::Audio_object>(
			std::pmr::polymorphic_allocator(&bttf::memory_resource()),
			(bttf::wood_hit_first + std::experimental::randint<int>(0, bttf::wood_hit_last - bttf::wood_hit_first))
		));
		
		ref.source_.play();
	}
}

void Crate::write_buffers(bttf::Rendering_component& comp)
{
	const auto last_index = comp.last_index_;
	const auto vdata_begin = std::ssize(comp.vdata_);
	const auto vindices_begin = std::ssize(comp.vindices_);
	
	comp.vdata_.resize(vdata_begin + std::ssize(model_->vdata_));
	comp.vindices_.resize(vindices_begin + std::ssize(model_->vindices_));
	comp.last_index_ += model_->size();
	
	model_->fill_indices(&comp.vindices_[vindices_begin], last_index);
	
	std::transform(model_->vbegin(), model_->vend(), &comp.vdata_[vdata_begin],
	[&](Vertex_data vd) -> Vertex_data
	{
		vd.vertex_coordinates = sphere_.center() - geometry::Vector3D(0, 0, side_length_ / 2);
		vd.texture_indices = tex_index_;
		// vd.texture_indices = Vertex_data::Texture_indices::color_only();
		
		vd.scale = {side_length_, side_length_, side_length_};
		vd.quaternion = quaternion_;
		
		vd.color = {};
		
		return vd;
	});
}

Key::~Key()
{
}

Key::Key(geometry::Vector3D origin)
	:
	Object(this, [](decltype(this) self, float seconds) -> void
	{
		self->quaternion_ *= geometry::Quaternion::from_unit_axis_angle(1, 0, 0, -seconds);
		
		if (overlap(self->collision_, player->cylinder_))
		{
			auto& ref = self->world().attach(std::allocate_shared<bttf::Audio_object>(
				std::pmr::polymorphic_allocator(&bttf::memory_resource()),
				bttf::key_collect
			));
			
			ref.source_.play();
			
			player->keys_.push_back(self->shared_this());
			
			self->Rendering_object::handle_.detach();
			self->light_.Rendering_object::handle_.detach();
			
			self->disable();
		}
	}),
	Rendering_object(this, [](decltype(this) self, bttf::Rendering_component& comp) -> void
	{
		self->write_buffers(comp);
	}),
	scale_ {0.4},
	collision_ {origin, scale_  * 3, scale_},
	quaternion_ {geometry::Quaternion::from_unit_axis_angle(0, 1, 0, geometry::pi / 2)},
	light_ {{origin, 20}, 212. / 255, 175. / 255, 55. / 255}
{
}

void Key::write_buffers(bttf::Rendering_component& comp)
{
	const auto last_index = comp.last_index_;
	const auto vdata_begin = std::ssize(comp.vdata_);
	const auto vindices_begin = std::ssize(comp.vindices_);
	
	comp.vdata_.resize(vdata_begin + std::ssize(bttf::key_obj->vdata_));
	comp.vindices_.resize(vindices_begin + std::ssize(bttf::key_obj->vindices_));
	comp.last_index_ += bttf::key_obj->size();
	
	bttf::key_obj->fill_indices(&comp.vindices_[vindices_begin], last_index);
	
	std::transform(bttf::key_obj->vbegin(), bttf::key_obj->vend(), &comp.vdata_[vdata_begin],
	[&](Vertex_data vd) -> Vertex_data
	{
		vd.vertex_coordinates = collision_.center();;
		vd.texture_indices = Vertex_data::Texture_indices::color_only();
		
		vd.scale = {scale_, scale_, scale_};
		vd.quaternion = quaternion_;
		
		vd.color = {nrm(212. / 255), nrm(175. / 255), nrm(55. / 255), nrm(1.)};
		vd.ambient_color = {nrm(212. / 255), nrm(175. / 255), nrm(55. / 255)};
		
		return vd;
	});
}

Door::~Door()
{
	doors.erase(std::lower_bound(std::begin(doors), std::end(doors), this));
}

Door::Door(geometry::Vector3D begin, geometry::Vector3D end)
	:
	Wall(begin, end, {bttf::creatures_binding, bttf::door_closed}, 3)
{
	doors.insert(std::lower_bound(std::begin(doors), std::end(doors), this), this);
}

Aloe::Aloe(geometry::Vector3D position)
	:
	Object(this),
	Rendering_object(this, [](decltype(this) self, bttf::Rendering_component& comp) -> void
	{
		self->write_buffers(comp);
	}),
	position_ {position}
{
}

void Aloe::write_buffers(bttf::Rendering_component& comp)
{
	{
		const auto last_index = comp.last_index_;
		const auto vdata_begin = std::ssize(comp.vdata_);
		const auto vindices_begin = std::ssize(comp.vindices_);
		
		comp.vdata_.resize(vdata_begin + std::ssize(bttf::aloeplant_obj->vdata_));
		comp.vindices_.resize(vindices_begin + std::ssize(bttf::aloeplant_obj->vindices_));
		comp.last_index_ += bttf::aloeplant_obj->size();
		
		bttf::aloeplant_obj->fill_indices(&comp.vindices_[vindices_begin], last_index);
		
		std::transform(bttf::aloeplant_obj->vbegin(), bttf::aloeplant_obj->vend(), &comp.vdata_[vdata_begin],
		[&](Vertex_data vd) -> Vertex_data
		{
			vd.vertex_coordinates = position_;
			vd.texture_indices = {bttf::objects_binding, bttf::aloeplant_index};
			
			vd.scale = {5, 5, 5};
			
			return vd;
		});
	}
	{
		const auto last_index = comp.last_index_;
		const auto vdata_begin = std::ssize(comp.vdata_);
		const auto vindices_begin = std::ssize(comp.vindices_);
		
		comp.vdata_.resize(vdata_begin + std::ssize(bttf::aloepot_obj->vdata_));
		comp.vindices_.resize(vindices_begin + std::ssize(bttf::aloepot_obj->vindices_));
		comp.last_index_ += bttf::aloepot_obj->size();
		
		bttf::aloepot_obj->fill_indices(&comp.vindices_[vindices_begin], last_index);
		
		std::transform(bttf::aloepot_obj->vbegin(), bttf::aloepot_obj->vend(), &comp.vdata_[vdata_begin],
		[&](Vertex_data vd) -> Vertex_data
		{
			vd.vertex_coordinates = position_;
			vd.texture_indices = {bttf::objects_binding, bttf::aloepot_index};
			
			vd.scale = {5, 5, 5};
			
			return vd;
		});
	}
}

Aloe::operator geometry::Bounding_box3D() const
{
	geometry::Vector3D diff(1, 1, 1);
	
	return {position_ - diff, position_ + diff};
}
} // namespace bttf
