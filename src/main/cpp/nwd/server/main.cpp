#include <iostream>
#include <syncstream>

#include <filesystem>
#include <algorithm>
#include <random>
#include <bitset>
#include <utility>
#include <semaphore>

#include <bit>
#include <set>
#include <map>
#include <string>

#include <fstream>
#include <syncstream>

#include <sqlite3.h>

#include <nwd/containers/vector_queue.hpp>
#include <nwd/databases/sqlite.hpp>
#include <nwd/shared/globals/random.hpp>
#include <nwd/containers/vector_queue.hpp>
#include <nwd/crypto/keypair.hpp>
#include <nwd/crypto/hash.hpp>
#include <nwd/crypto/shared_key.hpp>
#include <nwd/utility/algorithms.hpp>
#include <nwd/utility/random.hpp>
#include <nwd/utility/hex.hpp>
#include <nwd/utility/streams.hpp>
#include <nwd/utility/endian.hpp>
#include <nwd/shared/config.hpp>
#include <nwd/shared/messages/common.hpp>
#include <nwd/geometry/units.hpp>
#include <nwd/networking/event.hpp>
#include <nwd/networking/socket.hpp>
#include <nwd/networking/messages/from_client.hpp>
#include <nwd/networking/session.hpp>
#include <nwd/networking/heartbeat_session.hpp>
#include <nwd/serialization/fwd.hpp>
#include <nwd/serialization/common.hpp>
#include <nwd/shared/globals/executor.hpp>
#include <nwd/utility/parallel.hpp>

#include <nwd/audio/wav.hpp>
#include <nwd/audio/vorbis.hpp>

using namespace nwd;

databases::SQLite get_or_create_logins_db(std::filesystem::path directory)
{
	auto path_db_logins = directory / "logins.sqlite3";
	bool db_logins_exists = std::filesystem::exists(path_db_logins);
	auto db_logins = databases::SQLite::create_from_file(path_db_logins.u8string().c_str());
	
	if (not db_logins_exists)
	{
		std::filesystem::permissions(path_db_logins, std::filesystem::perms::owner_read | std::filesystem::perms::owner_write);
		
		try
		{
			db_logins.execute(R"""(
				-- pragma strict = on;
				
				create table logins(
					username text unique not null
					,
					salt blob(8) not null
					,
					password blob(32) not null
				);
				
				create unique index logins_index on logins(username);
			)""");
		}
		catch (...)
		{
			std::filesystem::remove(path_db_logins);
			throw;
		}
		
		std::clog << "Created database file: " << path_db_logins << "\n";
	}
	
	return db_logins;
}

std::unique_ptr<crypto::keypair::keypair_type> get_or_create_keypair(std::filesystem::path directory)
{
	std::string_view pub_name = "x25519-pub.pem";
	std::string_view priv_name = "x25519-priv.pem";
	
	auto result = std::make_unique<crypto::keypair::keypair_type>();
	
	if (not std::filesystem::exists(directory / pub_name)
		or not std::filesystem::exists(directory / priv_name))
	{
		*result = crypto::keypair::create();
		
		{
			auto ofs = std::ofstream(directory / pub_name);
			std::filesystem::permissions(directory / pub_name,
				std::filesystem::perms::owner_read | std::filesystem::perms::owner_write
			);
			ofs << "-----BEGIN PUBLIC KEY-----" << "\n";
			ofs.write(utility::hex::encode<crypto::keypair::Public::length>(result->first.data()).data(), 
				crypto::keypair::Public::length * 2
			) << "\n";
			ofs << "-----END PUBLIC KEY-----" << "\n";
		}
		{
			auto ofs = std::ofstream(directory / priv_name);
			std::filesystem::permissions(directory / priv_name,
				std::filesystem::perms::owner_read | std::filesystem::perms::owner_write
			);
			ofs << "-----BEGIN PRIVATE KEY-----" << "\n";
			ofs.write(utility::hex::encode<crypto::keypair::Private::length>(result->second.data()).data(), 
				crypto::keypair::Private::length * 2
			) << "\n";
			ofs << "-----END PRIVATE KEY-----" << "\n";
		}
	}
	else
	{
		{
			auto ifs = std::ifstream(directory / pub_name);
			while (ifs.good() and ifs.get() != '\n');
			utility::hex::decode(utility::read_array<crypto::keypair::Public::length * 2>(ifs), result->first.data());
		}
		{
			auto ifs = std::ifstream(directory / priv_name);
			while (ifs.good() and ifs.get() != '\n');
			utility::hex::decode(utility::read_array<crypto::keypair::Private::length * 2>(ifs), result->second.data());
		}
	}
	
	return result;
}

bool try_register(databases::SQLite& db_logins, std::string_view username, std::string_view password)
{
	auto select_statement = db_logins.prepare_single("select count(*) from logins where username = ?;");
	select_statement.bind(1, username);
	
	if (auto result = select_statement.step(); result != databases::SQLite::Result::row_ready)
	{
		throw std::runtime_error(std::to_string(result));
	}
	else
	{
		if (select_statement.column_int32(0) != 0)
		{
			return false;
		}
	}
	
	std::byte salted_password[config::salt_length + config::password_length];
	std::memcpy(salted_password, utility::random_data<config::salt_length>(shared::globals::random_device).data(), config::salt_length);
	std::memcpy(salted_password + config::salt_length, password.data(), password.length());
	
	auto insert_statement = db_logins.prepare_single("insert into logins values(?, ?, ?);");
	insert_statement.bind(1, username);
	insert_statement.bind(2, std::span(salted_password, config::salt_length));
	insert_statement.bind(3, crypto::hash32(std::span(salted_password, config::salt_length + password.length())));
	
	if (auto result = insert_statement.step(); result != databases::SQLite::Result::step_done)
	{
		throw std::runtime_error(std::to_string(result));
	}
	
	return true;
}

bool try_login(databases::SQLite& db_logins, std::string_view username, std::string_view password)
{
	auto select_statement = db_logins.prepare_single("select * from logins where username = ?;");
	select_statement.bind(1, username);
	
	auto result = select_statement.step();
	
	if (result != databases::SQLite::Result::row_ready)
	{
		return false;
	}
	
	std::byte salted_password[config::salt_length + config::password_length];
	std::memcpy(salted_password, select_statement.column_blob(1), config::salt_length);
	std::memcpy(salted_password + config::salt_length, password.data(), password.length());
	auto hashed = crypto::hash32(std::span(salted_password, config::salt_length + password.length()));
	auto decoded = std::span(select_statement.column_blob(2), 32);
	
	return std::ranges::equal(hashed, decoded);
}

struct Server_session final : networking::Heartbeat_session, virtual networking::Session
{
	inline const static crypto::keypair::keypair_type* keypair = nullptr;
	
	Server_session(const networking::messages::From_client_initial& initial, auto&&... args)
		:
		Session(std::forward<decltype(args)>(args)...)
	{
		this->receive_key_ = initial.client_send_key_;
		this->send_key_ = initial.server_send_key_;
		this->receive_nonce_ = initial.client_send_nonce_;
		this->send_nonce_ = initial.server_send_nonce_;
		std::osyncstream(std::clog) << "YES" << "\n";
	}
	
	Receive_result do_receive(std::span<std::byte> data) final override
	{
		std::cout << remote_endpoint_.port().value() << ": " << data.size() << "\n";
		return Receive_result::ok;
	}
	
	void handle_out_of_order(sequence_type sequence, std::span<std::byte> message) override
	{
		std::osyncstream(std::cout) << "handle_out_of_order" << "\n";
	}
	
	std::ptrdiff_t handle_in_order(std::span<std::byte> message) override
	{
		std::osyncstream(std::cout) << "handle_in_order" << "\n";
		return std::ssize(message);
	}
};

template<typename Function>
struct Sessions_handler : Function
{
	Sessions_handler(Function&& function)
		:
		Function(std::move(function))
	{
	}
	
	void operator()() & noexcept
	{
		auto time_point = std::chrono::time_point_cast<utility::Executor::duration>(std::chrono::steady_clock::now());
		static_cast<Function&>(*this)();
		shared::globals::executor.submit(time_point + std::chrono::milliseconds(500), std::ref(*this));
	}
};

int main([[maybe_unused]] int argc, [[maybe_unused]] const char** argv)
{
	auto server_path = std::filesystem::path(std::getenv("HOME"));
	server_path /= ".nwd_server";
	std::filesystem::create_directories(server_path);
	auto keypair = get_or_create_keypair(server_path);
	Server_session::keypair = keypair.get();
	
	auto local_endpoint = networking::Endpoint(networking::Address::localhost, networking::Port(25565));
	auto socket = networking::Unique_socket_udp::create();
	socket.get().bind(local_endpoint);
	
	auto sessions_lock = std::mutex();
	auto sessions = containers::aa::Map<networking::Endpoint, std::shared_ptr<Server_session>>();
	auto handler = std::jthread([&](std::stop_token stop_token) -> void
	{
		auto received_data = std::array<std::byte, config::max_udp_length>();
		
		while (true)
		{
			auto [endpoint, size] = socket.get().receive(received_data);
			
			if (stop_token.stop_requested())
			{
				return;
			}
			
			bool disconnect = false;
			auto session = std::shared_ptr<Server_session>();
			
			if (size > std::ssize(received_data))
			{
				// Ignore
				std::clog << endpoint.address().to_chars().view() << ", " << endpoint.port().value() << ": message too long: " << size << "\n";
				disconnect = true;
			}
			else
			{
				auto received_data_span = std::span(std::data(received_data), static_cast<std::size_t>(size));
				
				{
					auto lg = std::lock_guard(sessions_lock);
					auto [it, inserted] = sessions.try_emplace(endpoint, nullptr);
					
					if (inserted)
					{
						auto initial = networking::messages::From_client_initial();
						auto result = initial.receive(received_data_span, keypair->first, keypair->second);
						
						if (result == decltype(result)::ok)
						{
							it->second = std::make_shared<Server_session>(initial, endpoint, socket.get());
						}
						else
						{
							sessions.erase(it);
						}
						
						continue;
					}
					
					session = it->second;
				}
				
				if (session)
				{
					auto receive_result = session->receive(received_data_span);
					
					if (receive_result != decltype(receive_result)::ok)
					{
						disconnect = true;
					}
				}
			}
			
			if (disconnect)
			{
				auto lg = std::lock_guard(sessions_lock);
				sessions.erase(endpoint);
			}
		}
	});
	
	auto session_handler = Sessions_handler([&]() noexcept -> void
	{
		auto sessions_to_handle = std::vector<std::shared_ptr<Server_session>>();
		
		{
			auto lg = std::lock_guard(sessions_lock);
			
			for (const auto& ssn : sessions)
			{
				sessions_to_handle.emplace_back(ssn.second);
			}
		}
		
		utility::parallel::for_each(sessions_to_handle, [](auto& ssn) -> void
		{
			auto lg = ssn->get_lock();
			ssn->send_from_buffer(lg);
			ssn->handle(lg);
		});
		
		sessions_to_handle.clear();
	});
	
	shared::globals::executor.submit_now(std::ref(session_handler));
	
	std::osyncstream(std::cout) << "Server started..." << "\n";
	
	std::cin.get();
	handler.request_stop();
	socket.get().send(local_endpoint, {});
	handler.join();
	
#if 0
// 	socket.get().set_nonblocking();
	
	auto keypair = get_or_create_keypair(server_path);
	
	auto datagram = socket.get().receive();
	auto decrypted = utility::Byte_array<256>();
	std::cout << datagram.data().size() << "\n";
	auto decrypted_span = crypto::keypair::decrypt(datagram.data().content().subspan(1), decrypted.data(), keypair.first, keypair.second);
	
	auto end = decrypted_span->data();
	
	auto shared_key = crypto::Shared_key();
	end = std::ranges::copy_n(end, crypto::Shared_key::length, shared_key.data()).in;
	auto nonce = crypto::Nonce();
	end = std::ranges::copy_n(end, crypto::Nonce::length, nonce.data()).in;
	
	auto session = Server_session(socket.get(), datagram.endpoint(), nonce, ++auto(nonce), shared_key);
	
	std::cout << utility::endian::be_uint16_t(session.receive_nonce().end() - 2).value() << "\n";
	
	std::ranges::copy(utility::hex::encode_range(session.send_nonce()), std::ostreambuf_iterator(std::cout));
	std::cout << "\n";
	std::ranges::copy(utility::hex::encode_range(session.receive_nonce()), std::ostreambuf_iterator(std::cout));
	std::cout << "\n";
	std::ranges::copy(utility::hex::encode_range(session.shared_key()), std::ostreambuf_iterator(std::cout));
	std::cout << "\n";
	
	session.start_heartbeat();
	
	struct Lbd
	{
		networking::Session* session_;
		std::ptrdiff_t i = 0;
		
		void operator()(utility::Executor::function_type& self, utility::Executor& executor, utility::Executor::time_point time_point) noexcept
		{
			std::byte bytes[std::numeric_limits<std::ptrdiff_t>::digits10 + 1];
			
			auto end = std::to_chars(reinterpret_cast<char*>(bytes), reinterpret_cast<char*>(bytes) + std::ssize(bytes), i).ptr;
			++i;
			
// 			for (int i = 0; i != 50; ++i)
			{
				session_->Session::send(std::span(std::begin(bytes), reinterpret_cast<std::byte*>(end)));
			}
			
			executor.submit(time_point + std::chrono::milliseconds(10), std::move(self));
		}
	};
	
	session.executor_.submit_now(Lbd(&session));
	
	while (true)
	// for (int i = 0; i != 20; ++i)
	{
		std::cout << "???" << "\n";
		session.receive();
	}
	
	// auto evloop = networking::Event_loop::create(socket.get(), serv);
#endif
}
