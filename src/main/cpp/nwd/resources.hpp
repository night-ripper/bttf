#pragma once

#include "rendering/textures.hpp"
#include "rendering-coordinates.hpp"
#include "rendering-coordinates-obj.hpp"
#include "rendering/wavefront-obj.hpp"

#include "audio/openal.hpp"

#include <memory>

namespace bttf
{
rendering::opengl::Texture_2D_array load_textures_environment();
rendering::opengl::Texture_2D_array load_textures_creatures();
rendering::opengl::Texture_2D_array load_textures_translucent();
rendering::opengl::Texture_2D_array load_textures_anime();
rendering::opengl::Texture_2D_array load_textures_objects();

std::unique_ptr<Wavefront_obj_vertex_data[]> load_objs();

std::unique_ptr<audio::openal::Buffer[]> load_sounds();

extern Vertex_data::Texture_indices::element_type environment_binding;

extern Vertex_data::Texture_indices::element_type floors_first;
extern Vertex_data::Texture_indices::element_type floors_last;

extern Vertex_data::Texture_indices::element_type walls_first;
extern Vertex_data::Texture_indices::element_type walls_last;

extern Vertex_data::Texture_indices::element_type health;

////////////////////////////////////////////////////////////////////////////////

extern Vertex_data::Texture_indices::element_type creatures_binding;
extern Vertex_data::Texture_indices::element_type corey_first;
extern Vertex_data::Texture_indices::element_type corey_last;

extern Vertex_data::Texture_indices::element_type door_closed;
extern Vertex_data::Texture_indices::element_type door_open;

////////////////////////////////////////////////////////////////////////////////

extern Vertex_data::Texture_indices::element_type anime_binding;
extern Vertex_data::Texture_indices::element_type anime_first;
extern Vertex_data::Texture_indices::element_type anime_last;

////////////////////////////////////////////////////////////////////////////////

extern Vertex_data::Texture_indices::element_type objects_binding;
extern Vertex_data::Texture_indices::element_type crate_index;
extern Vertex_data::Texture_indices::element_type chest_index;
extern Vertex_data::Texture_indices::element_type barrel_index;
extern Vertex_data::Texture_indices::element_type aloeplant_index;
extern Vertex_data::Texture_indices::element_type aloepot_index;

////////////////////////////////////////////////////////////////////////////////

extern Vertex_data::Texture_indices::element_type translucent_binding;
extern Vertex_data::Texture_indices::element_type smonk;

////////////////////////////////////////////////////////////////////////////////

extern audio::openal::Buffer* flashlight;
extern audio::openal::Buffer* run;

extern audio::openal::Buffer* punch_wind;
extern audio::openal::Buffer* punch_hit_first;
extern audio::openal::Buffer* punch_hit_last;
extern audio::openal::Buffer* wood_hit_first;
extern audio::openal::Buffer* wood_hit_last;
extern audio::openal::Buffer* wood_break;

extern audio::openal::Buffer* ambient_videodrome;

extern audio::openal::Buffer* key_collect;

extern audio::openal::Buffer* door_locked_sound;
extern audio::openal::Buffer* door_unlocked_sound;

extern Wavefront_obj_vertex_data* crate_obj;
extern Wavefront_obj_vertex_data* chest_obj;
extern Wavefront_obj_vertex_data* barrel_obj;

extern Wavefront_obj_vertex_data* smonk_obj;
extern Wavefront_obj_vertex_data* key_obj;

extern Wavefront_obj_vertex_data* aloeplant_obj;
extern Wavefront_obj_vertex_data* aloepot_obj;
} // namespace bttf
