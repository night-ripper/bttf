#include "resources.hpp"
#include "rendering/png.hpp"

#include "audio/vorbis.hpp"
#include "audio/wav.hpp"
#include "utility/files.hpp"
#include "rendering/image.hpp"

#include <vector>
#include <filesystem>
#include <fstream>
#include <iostream>

#include <bit>

#include <string_view>

namespace bttf
{
using element_type = Vertex_data::Texture_indices::element_type;

using namespace std::string_literals;
using namespace std::string_view_literals;

element_type environment_binding = -1;
element_type floors_first = -1;
element_type floors_last = -1;

element_type walls_first = -1;
element_type walls_last = -1;

element_type health = -1;

element_type creatures_binding = -1;
element_type corey_first = -1;
element_type corey_last = -1;

element_type door_closed = -1;
element_type door_open = -1;

element_type anime_binding = -1;
element_type anime_first = -1;
element_type anime_last = -1;

element_type objects_binding = -1;
element_type crate_index = -1;
element_type chest_index = -1;
element_type barrel_index = -1;
element_type aloeplant_index = -1;
element_type aloepot_index = -1;

element_type translucent_binding = -1;
element_type smonk = -1;

audio::openal::Buffer* flashlight = nullptr;
audio::openal::Buffer* vine_boom = nullptr;
audio::openal::Buffer* alia = nullptr;
audio::openal::Buffer* run = nullptr;

audio::openal::Buffer* punch_wind = nullptr;
audio::openal::Buffer* punch_hit_first = nullptr;
audio::openal::Buffer* punch_hit_last = nullptr;
audio::openal::Buffer* wood_hit_first = nullptr;
audio::openal::Buffer* wood_hit_last = nullptr;
audio::openal::Buffer* wood_break = nullptr;

audio::openal::Buffer* ambient_videodrome = nullptr;

audio::openal::Buffer* key_collect = nullptr;
audio::openal::Buffer* door_locked_sound = nullptr;
audio::openal::Buffer* door_unlocked_sound = nullptr;

Wavefront_obj_vertex_data* crate_obj = nullptr;
Wavefront_obj_vertex_data* chest_obj = nullptr;
Wavefront_obj_vertex_data* barrel_obj = nullptr;

Wavefront_obj_vertex_data* smonk_obj = nullptr;
Wavefront_obj_vertex_data* key_obj = nullptr;

Wavefront_obj_vertex_data* aloeplant_obj = nullptr;
Wavefront_obj_vertex_data* aloepot_obj = nullptr;

rendering::opengl::Texture_2D_array load_textures_environment()
{
	environment_binding = 0;
	
	rendering::opengl::Texture_2D_array result(environment_binding, rendering::opengl::Texture::Format::rgb_u_3_3_2, 32, 32, std::bit_width(32u));
	std::vector<rendering::Image_rgba_u_8> ia;
	
	int index = 0;
	
	{
		floors_first = index;
		
		std::vector<std::filesystem::path> floors(
			std::filesystem::directory_iterator("resources2/floors/cobble"),
			std::filesystem::directory_iterator()
		);
		std::sort(std::begin(floors), std::end(floors));
		
		for (const auto& path : floors)
		{
			ia.push_back(rendering::png::read_bottom_up(utility::open(path.c_str(), std::ios::binary)));
			++index;
		}
		
		floors_last = index - 1;
	}
	{
		walls_first = index;
		
		std::vector<std::filesystem::path> walls(
			std::filesystem::directory_iterator("resources2/walls/catacombs"),
			std::filesystem::directory_iterator()
		);
		std::sort(std::begin(walls), std::end(walls));
		
		for (const auto& path : walls)
		{
			ia.push_back(rendering::png::read_bottom_up(utility::open(path.c_str(), std::ios::binary)));
			++index;
		}
		
		walls_last = index - 1;
	}
	{
		ia.push_back(rendering::png::read_bottom_up(utility::open("resources2/health.png", std::ios::binary)));
		health = index++;
	}
	
	result.storage(std::ssize(ia));
	result.upload(ia);
	
	return result;
}

rendering::opengl::Texture_2D_array load_textures_creatures()
{
	creatures_binding = 1;
	
	rendering::opengl::Texture_2D_array result(creatures_binding, rendering::opengl::Texture::Format::rgba_u_5_5_5_1, 32, 32, std::bit_width(32u));
	std::vector<rendering::Image_rgba_u_8> ia;
	
	int index = 0;
	
	{
		corey_first = index;
		
		std::vector<std::filesystem::path> corey(
			std::filesystem::directory_iterator("resources2/corey"),
			std::filesystem::directory_iterator()
		);
		std::sort(std::begin(corey), std::end(corey));
		
		for (const auto& path : corey)
		{
			ia.push_back(rendering::png::read_bottom_up(utility::open(path.c_str(), std::ios::binary)));
			++index;
		}
		
		corey_last = index - 1;
	}
	{
		ia.push_back(rendering::png::read_bottom_up(utility::open("resources2/doors/door-closed.png", std::ios::binary)));
		door_closed = index++;
		ia.push_back(rendering::png::read_bottom_up(utility::open("resources2/doors/door-open.png", std::ios::binary)));
		door_open = index++;
	}
	
	result.storage(std::ssize(ia));
	result.upload(ia);
	
	return result;
}

rendering::opengl::Texture_2D_array load_textures_translucent()
{
	translucent_binding = 2;
	std::ptrdiff_t index = 0;
	
	rendering::opengl::Texture_2D_array result(translucent_binding, rendering::opengl::Texture::Format::rgba_u_4_4_4_4, 256, 256, std::bit_width(32u));
	std::vector<rendering::Image_rgba_u_8> ia;
	
	ia.push_back(rendering::png::read_bottom_up(utility::open("resources2/objects/smonk.png", std::ios::binary)));
	smonk = index++;
	
	result.storage(std::ssize(ia));
	result.upload(ia);
	
	return result;
}

rendering::opengl::Texture_2D_array load_textures_anime()
{
	/// Hardcoded in anime.frag
	anime_binding = 3;
	
	rendering::opengl::Texture_2D_array result(anime_binding, rendering::opengl::Texture::Format::rgba_u_4_4_4_4, 640, 360, 1);
	std::vector<rendering::Image_rgba_u_8> ia;
	
	{
		std::vector<std::filesystem::path> animes(
			std::filesystem::directory_iterator("resources2/anime"),
			std::filesystem::directory_iterator()
		);
		std::sort(std::begin(animes), std::end(animes));
		
		for (const auto& path : animes)
		{
			ia.push_back(rendering::png::read_bottom_up(utility::open(path.c_str(), std::ios::binary)));
		}
		
		anime_first = 0;
		anime_last = anime_first + std::ssize(animes) - 1;
	}
	
	result.storage(std::ssize(ia));
	result.upload(ia);
	
	return result;
}

rendering::opengl::Texture_2D_array load_textures_objects()
{
	objects_binding = 4;
	
	rendering::opengl::Texture_2D_array result(objects_binding, rendering::opengl::Texture::Format::rgb_u_3_3_2, 1024, 1024, std::bit_width(1024u));
	std::vector<rendering::Image_rgba_u_8> ia;
	
	int index = 0;
	
	ia.push_back(rendering::png::read_bottom_up(utility::open("resources2/objects/crate.png", std::ios::binary)));
	crate_index = index++;
	
	ia.push_back(rendering::png::read_bottom_up(utility::open("resources2/objects/chest.png", std::ios::binary)));
	chest_index = index++;
	
	ia.push_back(rendering::png::read_bottom_up(utility::open("resources2/objects/barrel.png", std::ios::binary)));
	barrel_index = index++;
	
	ia.push_back(rendering::png::read_bottom_up(utility::open("resources2/objects/aloeplant.png", std::ios::binary)));
	aloeplant_index = index++;
	
	ia.push_back(rendering::png::read_bottom_up(utility::open("resources2/objects/aloepot.png", std::ios::binary)));
	aloepot_index = index++;
	
	result.storage(std::ssize(ia));
	result.upload(ia);
	
	return result;
}

std::unique_ptr<Wavefront_obj_vertex_data[]> load_objs()
{
	auto result = std::make_unique<Wavefront_obj_vertex_data[]>(100);
	
	int index = 0;
	
	crate_obj = &result[index++];
	*crate_obj = Wavefront_obj_vertex_data(rendering::Wavefront_obj(utility::open("resources2/objects/crate.obj")));
	
	chest_obj = &result[index++];
	*chest_obj = Wavefront_obj_vertex_data(rendering::Wavefront_obj(utility::open("resources2/objects/chest.obj")));
	
	barrel_obj = &result[index++];
	*barrel_obj = Wavefront_obj_vertex_data(rendering::Wavefront_obj(utility::open("resources2/objects/barrel.obj")));
	
	smonk_obj = &result[index++];
	*smonk_obj = Wavefront_obj_vertex_data(rendering::Wavefront_obj(utility::open("resources2/objects/smonk.obj")));
	
	key_obj = &result[index++];
	*key_obj = Wavefront_obj_vertex_data(rendering::Wavefront_obj(utility::open("resources2/objects/key.obj")));
	
	aloeplant_obj = &result[index++];
	*aloeplant_obj = Wavefront_obj_vertex_data(rendering::Wavefront_obj(utility::open("resources2/objects/aloeplant.obj")));
	
	aloepot_obj = &result[index++];
	*aloepot_obj = Wavefront_obj_vertex_data(rendering::Wavefront_obj(utility::open("resources2/objects/aloepot.obj")));
	
	return result;
}

static audio::openal::Buffer* load_sound(std::string_view filename, audio::openal::Buffer& buffer)
{
	buffer = audio::openal::Buffer::create();
	
	if (filename.ends_with(".ogg"))
	{
		buffer << audio::vorbis::read(utility::open(filename.data(), std::ios::binary), 8);
		
		return &buffer;
	}
	else if (filename.ends_with(".wav"))
	{
		auto sound = audio::wav::read(utility::open(filename.data(), std::ios::binary));
		sound.bit_depth(8);
		buffer << sound;
		return &buffer;
	}
	
	throw;
}

std::unique_ptr<audio::openal::Buffer[]> load_sounds()
{
	auto result = std::make_unique<audio::openal::Buffer[]>(100);
	
	int index = 0;
	
	flashlight = load_sound("resources2/flashlight.wav"sv, result[index++]);
	run = load_sound("resources2/run.wav"sv, result[index++]);
	punch_wind = load_sound("resources2/punch/WA11XXX1.wav"sv, result[index++]);
	
	punch_hit_first = load_sound("resources2/punch/WH32WXX2.wav"sv, result[index++]);
	load_sound("resources2/punch/WH62FXX1.wav"sv, result[index++]);
	load_sound("resources2/punch/WH71FXX1.wav"sv, result[index++]);
	punch_hit_last = load_sound("resources2/punch/WH72FXX1.wav"sv, result[index++]);
	
	wood_hit_first = load_sound("resources2/punch/WH82SXX1.wav"sv, result[index++]);
	wood_hit_last = load_sound("resources2/punch/WH82WXX1.wav"sv, result[index++]);
	
	wood_break = load_sound("resources2/punch/WHD2WXX1.wav"sv, result[index++]);
	
	ambient_videodrome = load_sound("resources2/videodrome.ogg"sv, result[index++]);
	
	key_collect = load_sound("resources2/key-pick.wav"sv, result[index++]);
	
	door_locked_sound = load_sound("resources2/door-closed.wav"sv, result[index++]);
	door_unlocked_sound = load_sound("resources2/door-open.wav"sv, result[index++]);
	
	return result;
}
} // namespace bttf
