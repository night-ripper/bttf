#include <iostream>

#include <random>
#include <chrono>
#include <charconv>

#include <nwd/networking/common.hpp>
#include <nwd/networking/socket.hpp>
#include <nwd/utility/executor.hpp>
#include <nwd/shared/globals/random.hpp>

namespace nwd::networking
{
struct UDP_forwarder
{
	UDP_forwarder(Endpoint source, Endpoint target)
		:
		num_datagrams_(0.5f, 0.4f),
		latency_ms_(1, 5),
		executor_(std::chrono::milliseconds(1)),
		socket_(Unique_socket_udp::create()),
		target_(target)
	{
		socket_.get().bind(source);
	}
	
	void operator()()
	{
		auto data = socket_.get().receive();
		data.endpoint_ = target_;
		
		auto num_datagrams = num_datagrams_(shared::globals::random_device);
		num_datagrams = std::ceil(std::max(0.f, num_datagrams));
		
		for (int i = 0; i != int(num_datagrams); ++i)
		{
			auto time_to_execute = std::chrono::time_point_cast<utility::Executor::duration>(std::chrono::steady_clock::now())
				+ std::chrono::milliseconds(latency_ms_(shared::globals::random_device))
			;
			executor_.submit(time_to_execute, [&, data](auto&&...) noexcept -> void
			{
				socket_.get().send(data);
			});
		}
	}
	
	std::normal_distribution<float> num_datagrams_;
	std::uniform_int_distribution<std::int32_t> latency_ms_;
	utility::Executor executor_;
	Unique_socket_udp socket_;
	Endpoint target_;
};
} // namespace nwd::networking

int main(int argc, const char** argv)
{
	if (argc != 3)
	{
		std::cout << "Usage: ./udp_forwarder <receive port> <send port>" << "\n";
		return 1;
	}
	
	using namespace nwd::networking;
	
	std::uint16_t receive_port;
	
	if (std::from_chars(argv[1], argv[1] + std::strlen(argv[1]), receive_port).ec != std::errc())
	{
		std::cout << "Could not interpret receive port" << "\n";
		return 2;
	}
	
	std::uint16_t send_port;
	
	if (std::from_chars(argv[2], argv[2] + std::strlen(argv[2]), send_port).ec != std::errc())
	{
		std::cout << "Could not interpret send port" << "\n";
		return 3;
	}
	
	auto udp = UDP_forwarder(Endpoint(Address::localhost, Port(receive_port)), Endpoint(Address::localhost, Port(send_port)));
	while (true)
	{
		udp();
	}
}
