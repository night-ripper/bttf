#pragma once

#include <cstdint>

#include <nwd/networking/messages/message.hpp>

namespace nwd::networking::messages
{
enum struct From_server : std::uint8_t
{
	unused = shared_last,
	
	connect,
	disconnect,
};
} // namespace nwd::networking::messages
