#include <cstdint>
#include <type_traits>
#include <utility>
#include <stdexcept>

#include <nwd/networking/messages/from_server.hpp>
#include <nwd/serialization/fundamental.hpp>

namespace nwd::networking::messages::from_server
{
} // namespace nwd::networking::messages::from_server
