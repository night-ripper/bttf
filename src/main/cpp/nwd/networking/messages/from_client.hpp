#pragma once

#include <cstdint>
#include <type_traits>

#include <nwd/shared/config.hpp>
#include <nwd/crypto/shared_key.hpp>
#include <nwd/crypto/keypair.hpp>
#include <nwd/networking/messages//message.hpp>

namespace nwd::networking::messages
{
struct From_client_initial
{
	constexpr static std::size_t length = config::version.size() + 2 * crypto::Shared_key::length + 2 * crypto::Nonce::length;
	
	[[nodiscard]]
	std::array<std::byte, crypto::keypair::seal_size + length> prepare(
		const crypto::keypair::Public& public_key
	);
	
	enum struct Receive_result
	{
		ok,
		wrong_size,
		wrong_encryption,
		wrong_version,
	};
	
	[[nodiscard]]
	Receive_result receive(std::span<const std::byte> data, const crypto::keypair::Public& public_key,
		const crypto::keypair::Private& private_key
	);
	
	crypto::Shared_key client_send_key_;
	crypto::Shared_key server_send_key_;
	crypto::Nonce client_send_nonce_;
	crypto::Nonce server_send_nonce_;
};

enum struct From_client : std::uint8_t
{
	unused = shared_last,
	
	connect,
	disconnect,
};
} // namespace nwd::networking::messages
