#pragma once

#include <utility>

#include <nwd/utility/endian.hpp>

namespace nwd::networking::messages
{
enum struct Shared : std::uint8_t
{
	invalid,
	stream_message,
	heartbeat,
};

constexpr std::uint8_t shared_last = std::to_underlying(Shared::heartbeat);
} // namespace nwd::networking::messages
