#include <cstring>

#include <iostream>
#include <memory>
#include <bitset>
#include <thread>
#include <chrono>

#include <arpa/inet.h>

#include <event2/event.h>
#include <event2/thread.h>

#include <fcntl.h>
#include <unistd.h>

void event_log_callback(int severity, const char* message)
{
	switch (severity)
	{
	case EVENT_LOG_DEBUG:
	case EVENT_LOG_MSG:
	case EVENT_LOG_WARN:
	case EVENT_LOG_ERR:
		std::cout << message << "\n";
	}
}

struct Event_base
{
	static Event_base create()
	{
		if (auto* value = event_base_new(); value != nullptr)
		{
			Event_base result;
			result.value_.reset(value);
			return result;
		}
		
		throw std::runtime_error("libevent: Could not initialize a new event_base");
	}
	
	struct Deleter
	{
		void operator()(event_base* value)
		{
			event_base_free(value);
		}
	};
	
	operator event_base*() const {return value_.get();}
	
	std::unique_ptr<event_base, Deleter> value_;
};

constexpr in_addr ipv4_of(unsigned char v1, unsigned char v2, unsigned char v3, unsigned char v4)
{
	in_addr_t result = 0;
	result += in_addr_t(v1) << 0;
	result += in_addr_t(v2) << 8;
	result += in_addr_t(v3) << 16;
	result += in_addr_t(v4) << 24;
	return {result};
}

void event_callback(evutil_socket_t fd, short events, void*)
{
	std::cout << "### rofl ###" << "\n";
	std::cout << fd << "\n";
	std::cout << events << "\n";
	std::ptrdiff_t len;
	char buf[2048];
	
	sockaddr_in source;
	socklen_t source_len = sizeof(source);
	
	while (true)
	{
		len = recvfrom(fd, buf, std::ssize(buf), 0, reinterpret_cast<sockaddr*>(&source), &source_len);
		
		if (len == -1 and EVUTIL_SOCKET_ERROR() == EAGAIN)
		{
			std::cout << "full" << "\n";
			break;
		}
		
		std::cout << "read " << len << "\n";
		std::cout.write(buf, len);
		
		if (len < std::ssize(buf))
		{
			char addr[INET_ADDRSTRLEN] {};
			std::cout << ntohs(source.sin_port) << "\n";
			std::cout << evutil_inet_ntop(AF_INET, &source, addr, std::ssize(addr)) << "\n";
			std::cout << addr << "\n";
			break;
		}
	}
	
	std::cout << len << "\n";
}

int main()
{
	event_set_log_callback(&event_log_callback);
	std::cout << evthread_use_pthreads() << "\n";
	
	auto lol = Event_base::create();
	
	for (const char** method = event_get_supported_methods(); *method != nullptr; ++method)
	{
		std::cout << *method << "\n";
	}
	
	auto fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	std::cout << fd << "\n";
	
	sockaddr_in asd
	{
		.sin_family = AF_INET,
		.sin_port = htons(25565),
		.sin_addr = ipv4_of(127, 0, 0, 1),
	};
	
	std::cout << bind(fd, reinterpret_cast<sockaddr*>(&asd), sizeof(asd)) << "\n";
	std::cout << asd.sin_port << "\n";
	
	std::cout << evutil_make_socket_nonblocking(fd) << "\n";
	auto* event = event_new(lol, fd, EV_READ | EV_PERSIST, event_callback, nullptr);
	
	std::cout << event << "\n";
	
	std::cout << asd.sin_port << "\n";
	event_add(event, nullptr);
	auto loop = std::jthread([&]() -> void {
		std::cout << "yass" << "\n";
		event_base_loop(static_cast<event_base*>(lol), 0);
		std::cout << "nass" << "\n";
	});
	std::cin.get();
	event_free(event);
	
	std::cout << evutil_closesocket(fd) << "\n";
}
