#include <cstring>

#include <unordered_map>
#include <iostream>

#include <nwd/networking/event.hpp>
#include <nwd/networking/receiver.hpp>

#include <nwd/utility/atomic_lock.hpp>

#include <nwd/containers/repository.hpp>

#include <event2/event.h>
#include <arpa/inet.h>

using namespace nwd::networking;

struct Server
{
	Event_loop& loop_;
	nwd::utility::Atomic_unique_lock lock_;
	std::unordered_map<Remote, Receiver, Remote::Hash> remotes_;
	
	Server(Event_loop& loop)
		:
		loop_(loop)
	{
	}

	void handle(UDP_data& data)
	{
		std::cout << "Handling " << data.sequence() << "\n";
		
		std::cout << data.address_ << "\n";
		std::cout << data.port_ << "\n";
		std::cout << data.sequence() << "\n";
		
		std::memcpy(data.content(), "yo answer\n", 10);
		data.size_ = 12;
		data.port_ = 25566;
		loop_.send(data);
	}
	
	void operator()(UDP_data& data)
	{
		Receiver* receiver;
		
		{
			auto lg = std::lock_guard(lock_);
			receiver = &remotes_[{data.address_, data.port_}];
		}
		
		receiver->receive(data, [this](UDP_data& data) -> void {handle(data);});
	}
};

int main()
{
	Event_loop loop(ntohl(ipv4_of(127, 0, 0, 1)), 25565);
	
	Server server(loop);
	
	loop.handle(server, 8);
	
	std::clog << "Listening..." << "\n";
	
	std::cin.get();
}
