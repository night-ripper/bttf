#pragma once

#include <memory>
#include <thread>
#include <utility>

#include <nwd/networking/common.hpp>
#include <nwd/networking/socket.hpp>
#include <nwd/utility/functor.hpp>

namespace nwd::networking
{
namespace deleters
{
void event_base(void* object) noexcept;
void event(void* object) noexcept;
} // namespace deleters

struct Event_loop;

struct Event_base : protected std::unique_ptr<void, utility::Functor<deleters::event_base>>
{
	Event_base() = default;
	
	static Event_base create();
	
	friend Event_loop;
};

struct Event : protected std::unique_ptr<void, utility::Functor<deleters::event>>
{
	friend Event_loop;
};

struct Event_loop
{
	Socket_udp socket() const
	{
		return socket_;
	}
	
	explicit Event_loop(Socket_udp socket);
	
private:
	virtual void handle(Socket_value_type socket) = 0;
	
protected:
	~Event_loop() = default;
	
private:
	// Keep the order for proper destruction
	Socket_udp socket_;
	Event_base event_base_;
	std::jthread loop_thread_;
	Event event_;
};
} // namespace nwd::networking
