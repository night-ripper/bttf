#include <random>

#include <map>
#include <chrono>
#include <iostream>

#include <nwd/networking/common.hpp>
#include <nwd/networking/socket.hpp>
#include <nwd/utility/executor.hpp>
#include <nwd/shared/globals/random.hpp>

int main()
{
	using namespace nwd::networking;
	
	auto udp = Unique_socket_udp::create();
	udp.get().bind(Endpoint(Address::localhost, Port(0)));
	
	auto dg = UDP_datagram(Endpoint(Address::localhost, Port(30'000)));
	
	for (int i = 0; i != 100; ++i)
	{
		dg.data() << std::to_string(i);
		udp.get().send(dg);
		dg.data().clear();
	}
}
