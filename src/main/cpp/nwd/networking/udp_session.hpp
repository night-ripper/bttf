#pragma once

#include <iostream>

#include <random>
#include <chrono>
#include <charconv>

#include <nwd/networking/session.hpp>
#include <nwd/utility/executor.hpp>
#include <nwd/shared/globals/random.hpp>

namespace nwd::networking
{
struct UDP_session : protected virtual Session
{
	UDP_session(utility::Executor& executor)
		:
		num_datagrams_(0.5f, 1.f),
		latency_ms_(200, 800),
		executor_(&executor)
	{
	}
	
	void send(const UDP_datagram& datagram) override
	{
		auto num_datagrams = num_datagrams_(shared::globals::random_device);
		num_datagrams = std::ceil(std::max(0.f, num_datagrams));
		
		for (int i = 0; i != int(num_datagrams); ++i)
		{
			auto time_to_execute = std::chrono::time_point_cast<utility::Executor::duration>(std::chrono::steady_clock::now())
				+ std::chrono::milliseconds(latency_ms_(shared::globals::random_device))
			;
			executor_->submit(time_to_execute, [&, datagram](auto&&...) noexcept -> void
			{
				local_socket().send(datagram);
			});
		}
	}
	
private:
	std::normal_distribution<float> num_datagrams_;
	std::uniform_int_distribution<std::int32_t> latency_ms_;
	utility::Executor* executor_;
};
} // namespace nwd::networking
