#include <random>

#include <map>
#include <chrono>
#include <iostream>

#include <nwd/networking/common.hpp>
#include <nwd/networking/socket.hpp>
#include <nwd/utility/executor.hpp>
#include <nwd/shared/globals/random.hpp>

int main()
{
	using namespace nwd::networking;
	
	auto udp = Unique_socket_udp::create();
	udp.get().bind(Endpoint(Address::localhost, Port(30'001)));
	while (true)
	{
		auto gotted = udp.get().receive();
		std::cout << std::string_view(reinterpret_cast<const char*>(gotted.data().content().data()), gotted.data().size()) << "\n";
	}
}
