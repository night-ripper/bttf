#pragma once

#include <cstddef>
#include <cstdint>

#include <memory>
#include <span>
#include <utility>

#include <nwd/networking/common.hpp>
#include <nwd/utility/functor.hpp>
#include <nwd/utility/unique_resource.hpp>

namespace nwd::networking
{
#ifdef __unix__
using Socket_value_type = std::int32_t;
#else
using Socket_value_type = std::intptr_t;
#endif

struct Socket;
struct Unique_socket;

namespace deleters
{
void socket(Socket object) noexcept;
} // namespace deleters

struct Socket
{
	Socket() = default;
	
	void set_nonblocking() const;
	
	Socket_value_type get() const noexcept
	{
		return value_;
	}
	
protected:
	template<typename R, typename D>
	friend struct utility::Unique_resource;
	
	friend Unique_socket;
	
	explicit Socket(Socket_value_type value) noexcept
		:
		value_(value)
	{
	}
	
protected:
	Socket_value_type value_;
};

struct Unique_socket : protected utility::Unique_resource<Socket, utility::Functor<deleters::socket>>
{
	Unique_socket() = default;
	Unique_socket(Unique_socket&& other) noexcept = default;
	Unique_socket& operator=(Unique_socket&& other) noexcept = default;
	
	using Unique_resource::reset;
	
protected:
	explicit Unique_socket(Socket_value_type value)
		:
		Unique_resource(Socket(value))
	{
	}
};

struct Socket_udp : Socket
{
	using Socket::Socket;
	
	void bind(Endpoint endpoint) const;
	Endpoint endpoint() const;
	
	std::ptrdiff_t send(Endpoint endpoint, std::span<const std::byte> data) const;
	
	std::tuple<Endpoint, std::ptrdiff_t> peek(std::span<std::byte> result = std::span<std::byte>()) const;
	std::tuple<Endpoint, std::ptrdiff_t> receive(std::span<std::byte> result) const;
	std::tuple<Endpoint, std::ptrdiff_t> try_receive(std::span<std::byte> result) const;
};

struct Unique_socket_udp : Unique_socket
{
	Unique_socket_udp() = default;
	Unique_socket_udp(Unique_socket_udp&& other) noexcept = default;
	Unique_socket_udp& operator=(Unique_socket_udp&& other) noexcept = default;
	
	static Unique_socket_udp create();
	
	const Socket_udp& get() const noexcept
	{
		return static_cast<const Socket_udp&>(this->Unique_socket::get());
	}
	
protected:
	explicit Unique_socket_udp(Socket_value_type value)
		:
		Unique_socket(value)
	{
	}
};
} // namespace nwd::networking
