pub mod keypair;
pub mod shared_key;

#[link(name = "sodium")]
extern "C"
{
	fn randombytes_buf(buf: *mut std::ffi::c_void, size: usize);
	
	fn crypto_generichash(out: *mut std::ffi::c_uchar, outlen: usize,
		input: *const std::ffi::c_uchar, inputlen: std::ffi::c_ulonglong,
		key: *const std::ffi::c_uchar, keylen: usize
	) -> std::ffi::c_int;
}

pub fn random_data<const Size: usize>() -> [u8; Size]
{
	let mut result = [0_u8; Size];
	unsafe {randombytes_buf(result.as_mut_ptr() as *mut std::ffi::c_void, result.len())};
	return result;
}

pub fn hash32(data: &[u8]) -> [u8; 32]
{
	let mut result = [0_u8; 32];
	
	unsafe {crypto_generichash(result.as_mut_ptr(), result.len(),
		data.as_ptr(), data.len() as std::ffi::c_ulonglong, std::ptr::null(), 0)
	};
	
	return result;
}
