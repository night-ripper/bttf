use rand::Rng;
use std::io::Write;

fn main() -> std::io::Result<()>
{
	env_logger::builder().filter_level(log::LevelFilter::Debug).target(env_logger::Target::Stderr).init();
	nwd::shared::initialize_bincode();
	
	let key_path = std::path::PathBuf::from(std::env::var_os("HOME").unwrap())
		.join(".nwd_server").join("keys").join("x25519.pub")
	;
	let pubkey = nwd::shared::keys::read_key(&mut std::fs::File::open(key_path)?)?;
	
	let mut session_keys: nwd::shared::keys::Session_keys = Default::default();
	let mut rng = rand::thread_rng();
	rng.fill(&mut session_keys.receive_key);
	rng.fill(&mut session_keys.send_key);
	rng.fill(&mut session_keys.receive_nonce);
	rng.fill(&mut session_keys.send_nonce);
	
	let server_address = std::net::SocketAddr::from((std::net::Ipv4Addr::LOCALHOST, 25565));
	let socket = std::sync::Arc::new(std::net::UdpSocket::bind(
		std::net::SocketAddr::from((std::net::Ipv4Addr::LOCALHOST, 25566))
	)?);
	
	{
		let mut encrypted = [0_u8; 256 + 128];
		let encrypted =
		{
			let mut plain_message = nwd::utility::io::Buffer_writer::<{256 + 128}>::default();
			plain_message.write(nwd::VERSION.as_bytes())?;
			plain_message.write(&['\0' as u8])?;
			plain_message.write(&session_keys.send_key)?;
			plain_message.write(&session_keys.receive_key)?;
			plain_message.write(&session_keys.send_nonce)?;
			plain_message.write(&session_keys.receive_nonce)?;
			nwd::crypto::keypair::encrypt(&plain_message, &mut encrypted, &pubkey)
		};
		
		socket.send_to(&encrypted, server_address)?;
	}
	
	let session = std::sync::Arc::new(std::sync::Mutex::new(
		nwd::shared::session::Session::new(server_address, socket.clone(), session_keys)
	));
	
	loop
	{
		let mut buffer = [0_u8; 256 + 128];
		
		match socket.recv_from(&mut buffer)
		{
			Ok((length, address)) =>
			{
				if address == server_address
				{
					let message: nwd::shared::server::Message_content = session.lock().unwrap().accept(&mut buffer[.. length]).unwrap();
					
					if message == nwd::shared::server::Message_content::initial_accept
					{
						log::info!("{}", "Received initial_accept from server");
						break;
					}
					else
					{
						log::error!("{}", "Received a wrong message");
						return Ok(());
					}
				}
				else
				{
					log::error!("{}", "Received a wrong message");
					return Ok(());
				}
			}
			Err(error) =>
			{
				return Err(error);
			}
		}
	}
	
	let mut poll = nwd::shared::session::Poller::new();
	
	drop(socket);
	
	std::sync::Arc::get_mut(session.lock().unwrap().socket_mut()).unwrap().set_nonblocking(true)?;
	let socket = session.lock().unwrap().socket_mut().clone();
	poll.add_with_mode(socket.clone(), polling::Event::readable(0), polling::PollMode::Level)?;
	
	std::thread::spawn({
		let session = session.clone();
		move ||
	{
		let mut buffer = [0_u8; 256 + 128];
		
		let mut events = polling::Events::with_capacity(128.try_into().unwrap());
		
		loop
		{
			events.clear();
			poll.wait(&mut events, None)?;
			
			for event in events.iter()
			{
				if event.key == 0 && event.readable
				{
					match socket.recv_from(&mut buffer)
					{
						Ok((length, address)) =>
						{
							if address == server_address
							{
								let message: nwd::shared::server::Message_content = session.lock().unwrap().accept(&mut buffer[.. length]).unwrap();
								log::info!("{:?}", message);
								session.lock().unwrap().send_unreliable(&nwd::shared::client::Message_content::heartbeat);
							}
							else
							{
								log::error!("{}", "Received a wrong message");
								return Ok(());
							}
						}
						// Err(error) if error.kind() == std::io::ErrorKind::WouldBlock => break,
						Err(error) =>
						{
							return Err(error);
						}
					}
				}
			}
		}
	}});
	
	let now = std::time::Instant::now();
	
	for i in 1 .. 2_000
	{
		session.lock().unwrap().send(&nwd::shared::client::Message_content::number {i});
		
		if i % 100 == 0
		{
			// std::thread::sleep(std::time::Duration::from_millis(1));
		}
	}
	
	println!("{:?}", std::time::Instant::now().duration_since(now));
	
	std::thread::sleep(std::time::Duration::from_secs(2));
	
	Ok(())
}
