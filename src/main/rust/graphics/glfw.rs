pub mod constants;

type GLFWerrorfun = Option<extern "C" fn(error_code: std::ffi::c_int, description: *const std::ffi::c_char)>;

#[repr(C)]
struct GLFWimage
{
    width: std::ffi::c_int,
    height: std::ffi::c_int,
    pixels: *mut std::ffi::c_uchar,
}

impl From<&crate::graphics::image::Image> for GLFWimage
{
	fn from(value: &crate::graphics::image::Image) -> Self
	{
		Self
		{
			width: value.width() as std::ffi::c_int,
			height: value.height() as std::ffi::c_int,
			pixels: value.data().as_ptr() as *mut std::ffi::c_uchar,
		}
	}
}

#[link(name = "glfw")]
extern "C"
{
	fn glfwSetErrorCallback(callback: GLFWerrorfun) -> GLFWerrorfun;
	
	fn glfwTerminate();
	fn glfwInit() -> std::ffi::c_int;
	
	fn glfwDestroyCursor(cursor: *mut std::ffi::c_void);
	fn glfwCreateCursor(image: *const GLFWimage, xhot: std::ffi::c_int, yhot: std::ffi::c_int) -> *mut std::ffi::c_void;
	
	fn glfwDestroyWindow(window: *mut std::ffi::c_void);
	fn glfwCreateWindow(width: std::ffi::c_int, height: std::ffi::c_int,
		title: *const std::ffi::c_char, monitor: *mut std::ffi::c_void,
		share: *mut std::ffi::c_void) -> *mut std::ffi::c_void
	;
	fn glfwSetCursor(window: *mut std::ffi::c_void, cursor: *mut std::ffi::c_void);
	fn glfwSetWindowIcon(window: *mut std::ffi::c_void, count: std::ffi::c_int, images: *const GLFWimage);
	
	fn glfwWindowHint(hint: std::ffi::c_int, value: std::ffi::c_int);
	fn glfwMakeContextCurrent(window: *mut std::ffi::c_void);
	
	fn glfwPollEvents();
	fn glfwSwapBuffers(window: *mut std::ffi::c_void);
	fn glfwWindowShouldClose(window: *mut std::ffi::c_void) -> std::ffi::c_int;
	
	fn glfwGetRequiredInstanceExtensions(count: *mut u32) -> *mut *const std::ffi::c_char;
	fn glfwCreateWindowSurface(
		instance: crate::graphics::vulkan::ffi::VkInstance, window: *mut std::ffi::c_void,
		allocator: *const crate::graphics::vulkan::ffi::VkAllocationCallbacks,
		surface: *mut crate::graphics::vulkan::ffi::VkSurfaceKHR,
	) -> crate::graphics::vulkan::ffi::VkResult;
}

pub fn set_error_callback(callback: GLFWerrorfun) -> GLFWerrorfun {unsafe {glfwSetErrorCallback(callback)}}

pub struct Library {}

impl Drop for Library
{
	fn drop(&mut self) {unsafe {glfwTerminate()};}
}

pub fn init() -> Library
{
	unsafe {glfwInit()};
	return Library {};
}

impl Library
{
	pub fn poll_events(&self) {unsafe {glfwPollEvents()}}
	
	pub fn vulkan_required_extensions<'t>(&'t self) -> &'t [*const std::ffi::c_char]
	{
		unsafe
		{
			let mut count: u32 = 0;
			std::slice::from_raw_parts(glfwGetRequiredInstanceExtensions(&mut count), count as usize)
		}
	}
}

pub struct Cursor(std::ptr::NonNull<std::ffi::c_void>);

impl Drop for Cursor
{
	fn drop(&mut self) {unsafe {glfwDestroyCursor(self.0.as_ptr())};}
}

impl Cursor
{
	pub fn create(image: &crate::graphics::image::Image, xhot: std::ffi::c_int, yhot: std::ffi::c_int) -> Self
	{
		match std::ptr::NonNull::new(unsafe {glfwCreateCursor(&image.into(), xhot, yhot)})
		{
			None => panic!("GLFW: Could not create a cursor"),
			Some(cursor) => Self(cursor),
		}
	}
}

pub struct Window
{
	object: std::ptr::NonNull<std::ffi::c_void>,
	cursor: Option<Cursor>,
}

impl Drop for Window
{
	fn drop(&mut self) {unsafe {glfwDestroyWindow(self.object.as_ptr())};}
}

impl Window
{
	pub fn hint(hint: std::ffi::c_int, value: std::ffi::c_int) {unsafe {glfwWindowHint(hint, value)};}
	
	pub fn create(width: std::ffi::c_int, height: std::ffi::c_int, title: &str) -> Self
	{
		let result = unsafe {glfwCreateWindow(width, height, title.as_ptr().cast::<std::ffi::c_char>(), std::ptr::null_mut(), std::ptr::null_mut())};
		
		return match std::ptr::NonNull::new(result)
		{
			None => panic!("GLFW: Could not create a window"),
			Some(object) => Self {object, cursor: None},
		}
	}
	
	pub fn make_current(&self) {unsafe {glfwMakeContextCurrent(self.object.as_ptr())};}
	pub fn should_close(&self) -> bool {unsafe {glfwWindowShouldClose(self.object.as_ptr()) != 0}}
	pub fn swap_buffers(&self) {unsafe {glfwSwapBuffers(self.object.as_ptr())}}
	
	pub fn set_cursor(&mut self, mut cursor: Option<Cursor>)
	{
		unsafe {glfwSetCursor(self.object.as_ptr(), match &mut cursor
		{
			None => std::ptr::null_mut(),
			Some(ref mut cursor) => cursor.0.as_mut(),
		})};
		
		self.cursor = cursor;
	}
	
	pub fn set_icons(&self, icons: &[crate::graphics::image::Image])
	{
		let glfw_icons = icons.iter().map(GLFWimage::from).collect::<Vec<_>>();
		unsafe {glfwSetWindowIcon(self.object.as_ptr(), glfw_icons.len() as std::ffi::c_int, glfw_icons.as_ptr())};
	}
}
