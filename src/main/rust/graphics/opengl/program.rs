use crate::graphics::opengl::types::*;
use crate::graphics::opengl::constants;

#[link(name = "GL")]
extern "C"
{
	fn glDeleteShader(shader: GLuint);
	fn glCreateShader(type_v: GLenum) -> GLuint;
	
	fn glShaderSource(shader: GLuint, count: GLsizei, string: *const *const GLchar, length: *const GLint);
	fn glCompileShader(shader: GLuint);
	fn glGetShaderiv(shader: GLuint, pname: GLenum, params: *mut GLint);
	fn glGetShaderInfoLog(shader: GLuint, bufSize: GLsizei, length: *mut GLsizei, infoLog: *mut GLchar);
	
	fn glDeleteProgram(program: GLuint);
	fn glCreateProgram() -> GLuint;
	fn glUseProgram(program: GLuint);
	
	fn glAttachShader(program: GLuint, shader: GLuint);
	fn glDetachShader(program: GLuint, shader: GLuint);
	fn glLinkProgram(program: GLuint);
	fn glGetProgramiv(program: GLuint, pname: GLenum, params: *mut GLint);
	fn glGetProgramInfoLog(program: GLuint, bufSize: GLsizei, length: *mut GLsizei, infoLog: *mut GLchar);
	
	fn glProgramUniform1f(program: GLuint, location: GLint, v0: GLfloat);
	fn glProgramUniform2f(program: GLuint, location: GLint, v0: GLfloat, v1: GLfloat);
	fn glProgramUniform3f(program: GLuint, location: GLint, v0: GLfloat, v1: GLfloat, v2: GLfloat);
	fn glProgramUniform4f(program: GLuint, location: GLint, v0: GLfloat, v1: GLfloat, v2: GLfloat, v3: GLfloat);
	
	fn glProgramUniform1i(program: GLuint, location: GLint, v0: GLint);
	fn glProgramUniform2i(program: GLuint, location: GLint, v0: GLint, v1: GLint);
	fn glProgramUniform3i(program: GLuint, location: GLint, v0: GLint, v1: GLint, v2: GLint);
	fn glProgramUniform4i(program: GLuint, location: GLint, v0: GLint, v1: GLint, v2: GLint, v3: GLint);
	
	fn glProgramUniform1ui(program: GLuint, location: GLint, v0: GLuint);
	fn glProgramUniform2ui(program: GLuint, location: GLint, v0: GLuint, v1: GLuint);
	fn glProgramUniform3ui(program: GLuint, location: GLint, v0: GLuint, v1: GLuint, v2: GLuint);
	fn glProgramUniform4ui(program: GLuint, location: GLint, v0: GLuint, v1: GLuint, v2: GLuint, v3: GLuint);
	
	fn glProgramUniformMatrix2fv(program: GLuint, location: GLint, count: GLsizei, transpose: GLboolean, value: *const GLfloat);
	fn glProgramUniformMatrix2x3fv(program: GLuint, location: GLint, count: GLsizei, transpose: GLboolean, value: *const GLfloat);
	fn glProgramUniformMatrix2x4fv(program: GLuint, location: GLint, count: GLsizei, transpose: GLboolean, value: *const GLfloat);
	
	fn glProgramUniformMatrix3x2fv(program: GLuint, location: GLint, count: GLsizei, transpose: GLboolean, value: *const GLfloat);
	fn glProgramUniformMatrix3fv(program: GLuint, location: GLint, count: GLsizei, transpose: GLboolean, value: *const GLfloat);
	fn glProgramUniformMatrix3x4fv(program: GLuint, location: GLint, count: GLsizei, transpose: GLboolean, value: *const GLfloat);
	
	fn glProgramUniformMatrix4x2fv(program: GLuint, location: GLint, count: GLsizei, transpose: GLboolean, value: *const GLfloat);
	fn glProgramUniformMatrix4x3fv(program: GLuint, location: GLint, count: GLsizei, transpose: GLboolean, value: *const GLfloat);
	fn glProgramUniformMatrix4fv(program: GLuint, location: GLint, count: GLsizei, transpose: GLboolean, value: *const GLfloat);
	
	fn glProgramUniformHandleui64ARB(program: GLuint, location: GLint, value: GLuint64); 
}

pub struct Shader(GLuint);

impl Drop for Shader
{
	fn drop(&mut self) {unsafe {glDeleteShader(self.0)};}
}

#[derive(Debug)]
pub struct Compile_error(String);

#[repr(u32)]
pub enum Shader_type
{
	fragment = constants::GL_FRAGMENT_SHADER,
	geometry = constants::GL_GEOMETRY_SHADER,
	vertex = constants::GL_VERTEX_SHADER,
}

impl Shader
{
	pub fn create(type_v: Shader_type) -> Self
	{
		Self {0: unsafe {glCreateShader(type_v as GLenum)}}
	}
	
	pub fn compile<'t>(&self, sources: impl std::iter::Iterator<Item = &'t[u8]>) -> Result<&Shader, Compile_error>
	{
		let mut source_strings = Vec::<*const GLchar>::with_capacity(8);
		let mut lengths = Vec::<GLint>::with_capacity(8);
		let mut count: GLsizei = 0;
		
		for source in sources
		{
			source_strings.push(source.as_ptr().cast::<GLchar>());
			lengths.push(source.len() as GLint);
			count += 1;
		}
		
		unsafe {glShaderSource(self.0, count, source_strings.as_ptr(), lengths.as_ptr())};
		
		let mut info_log_length: GLint = 0;
		let mut info_log = String::new();
		let mut success: GLint = 0;
		
		unsafe {glCompileShader(self.0)};
		unsafe {glGetShaderiv(self.0, constants::GL_COMPILE_STATUS, &mut success)};
		unsafe {glGetShaderiv(self.0, constants::GL_INFO_LOG_LENGTH, &mut info_log_length)};
		
		info_log.reserve(info_log_length as usize);
		(0 .. info_log_length).for_each(|_| info_log.push('\0'));
		
		if success == 0
		{
			unsafe {glGetShaderInfoLog(self.0, info_log_length, std::ptr::null_mut(), info_log.as_mut_ptr().cast::<i8>())};
			return Err(Compile_error(info_log));
		}
		
		return Ok(self);
	}
}

pub struct Program(GLuint);

impl Drop for Program
{
	fn drop(&mut self) {unsafe {glDeleteProgram(self.0)};}
}

#[derive(Debug)]
pub struct Link_error(String);

impl Program
{
	pub fn create() -> Self {Self {0: unsafe {glCreateProgram()}}}
	
	pub fn link<'t>(&self, shaders: impl std::iter::Iterator<Item = &'t Shader> + Clone) -> Result<&Program, Link_error>
	{
		for shader in shaders.clone()
		{
			unsafe {glAttachShader(self.0, shader.0)};
		}
		
		if std::panic::catch_unwind(||
		{
			let mut success: GLint = 0;
			unsafe {glLinkProgram(self.0)};
			unsafe {glGetProgramiv(self.0, constants::GL_LINK_STATUS, &mut success)};
			
			if success == 0
			{
				panic!();
			}
		}).is_err()
		{
			let mut info_log_length: GLsizei = 0;
			let mut info_log = String::new();
			
			unsafe {glGetProgramiv(self.0, constants::GL_INFO_LOG_LENGTH, &mut info_log_length)};
			
			info_log.reserve(info_log_length as usize);
			(0 .. info_log_length).for_each(|_| info_log.push('\0'));
			
			unsafe {glGetProgramInfoLog(self.0, info_log_length, std::ptr::null_mut(), info_log.as_mut_ptr().cast::<i8>())};
			
			return Err(Link_error(info_log));
		}
		
		for shader in shaders
		{
			unsafe {glDetachShader(self.0, shader.0)};
		}
		
		return Ok(self);
	}
	
	pub fn use_program(&self) {unsafe {glUseProgram(self.0)}}
}
