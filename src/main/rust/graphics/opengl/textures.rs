use crate::graphics::opengl::types::*;
use crate::graphics::opengl::constants;

#[link(name = "GL")]
extern "C"
{
	fn glDeleteTextures(n: GLsizei, textures: *const GLuint);
	fn glCreateTextures(target: GLenum, n: GLsizei, textures: *mut GLuint);
	fn glBindTextureUnit(unit: GLuint, texture: GLuint);
	
	fn glGenerateTextureMipmap(texture: GLuint);
	
	fn glTextureStorage2D(texture: GLuint, levels: GLsizei, internalformat: GLenum,
		width: GLsizei, height: GLsizei
	);
	fn glTextureStorage2DMultisample(texture: GLuint, samples: GLsizei, internalformat: GLenum,
		width: GLsizei, height: GLsizei, fixedsamplelocations: GLboolean
	);
	fn glTextureSubImage2D(texture: GLuint, level: GLint,
		xoffset: GLint, yoffset: GLint, width: GLsizei, height: GLsizei,
		format: GLenum, type_: GLenum, pixels: *const std::ffi::c_void
	);
	
	fn glTextureStorage3D(texture: GLuint, levels: GLsizei, internalformat: GLenum,
		width: GLsizei, height: GLsizei, depth: GLsizei
	);
	fn glTextureStorage3DMultisample(texture: GLuint, samples: GLsizei, internalformat: GLenum,
		width: GLsizei, height: GLsizei, depth: GLsizei, fixedsamplelocations: GLboolean
	);
	fn glTextureSubImage3D(texture: GLuint, level: GLint,
		xoffset: GLint, yoffset: GLint, zoffset: GLint,
		width: GLsizei, height: GLsizei, depth: GLsizei,
		format: GLenum, type_: GLenum, pixels: *const std::ffi::c_void
	);
	
	fn glTextureParameteri(texture: GLuint, pname: GLenum, param: GLint);
	fn glTextureParameterfv(texture: GLuint, pname: GLenum, param: *const GLfloat);
	
	fn glDeleteRenderbuffers(n: GLsizei, renderbuffers: *const GLuint);
	fn glCreateRenderbuffers(n: GLsizei, renderbuffers: *mut GLuint);
	fn glNamedRenderbufferStorageMultisample(renderbuffer: GLuint, samples: GLsizei,
		internalformat: GLenum, width: GLsizei, height: GLsizei
	);
	
	fn glDeleteFramebuffers(n: GLsizei, framebuffers: *const GLuint);
	fn glCreateFramebuffers(n: GLsizei, framebuffers: *mut GLuint);
	fn glNamedFramebufferTexture(framebuffer: GLuint, attachment: GLenum,
		texture: GLuint, level: GLint
	);
	fn glNamedFramebufferRenderbuffer(framebuffer: GLuint, attachment: GLenum,
		renderbuffertarget: GLenum, renderbuffer: GLuint
	);
	fn glNamedFramebufferDrawBuffer(framebuffer: GLuint, buf: GLenum);
	fn glBindFramebuffer(target: GLenum, framebuffer: GLuint);
	
	fn glMakeTextureHandleNonResidentARB(handle: GLuint64);
	fn glMakeTextureHandleResidentARB(handle: GLuint64);
	fn glGetTextureHandleARB(texture: GLuint) -> GLuint64;
}

pub struct Format
{
	r: u8,
	g: u8,
	b: u8,
	a: u8,
	
	// Number of channels, 1 : 4
	channels: u8,
	
	// Number of bytes
	size: u8,
	
	is_signed: bool,
	
	internal: GLuint,
	external: GLuint,
	type_v: GLuint,
}

impl Format
{
	const fn new(is_signed: bool, r: u8, g: u8, b: u8, a: u8,
		internal: GLuint, external: GLuint, type_v: GLuint) -> Self
	{
		Self
		{
			r, g, b, a, is_signed, internal, external, type_v,
			channels: (r != 0) as u8 + (g != 0) as u8 + (b != 0) as u8 + (a != 0) as u8,
			size: ((r as u32 + g as u32 + b as u32 + a as u32) / 8) as u8,
		}
	}
}

const rgb_u_3_3_2: Format = Format::new(false, 3, 3, 2, 0, constants::GL_R3_G3_B2, constants::GL_RGB, constants::GL_UNSIGNED_BYTE_3_3_2);
const rgb_u_5_6_5: Format = Format::new(false, 5, 6, 5, 0, constants::GL_RGB565, constants::GL_RGB, constants::GL_UNSIGNED_SHORT_5_6_5);
const rgba_u_4_4_4_4: Format = Format::new(false, 4, 4, 4, 4, constants::GL_RGBA4, constants::GL_RGBA, constants::GL_UNSIGNED_SHORT_4_4_4_4);
const rgba_u_5_5_5_1: Format = Format::new(false, 5, 5, 5, 1, constants::GL_RGB5_A1, constants::GL_RGBA, constants::GL_UNSIGNED_SHORT_5_5_5_1);
const rgba_u_8_8_8_8: Format = Format::new(false, 8, 8, 8, 8, constants::GL_RGBA8, constants::GL_RGBA, constants::GL_UNSIGNED_INT_8_8_8_8);
const rgba_u_10_10_10_2: Format = Format::new(false, 10, 10, 10, 2, constants::GL_RGB10_A2, constants::GL_RGBA, constants::GL_UNSIGNED_INT_10_10_10_2);

const r_s_8: Format = Format::new(true, 8, 0, 0, 0, constants::GL_R8, constants::GL_RED, constants::GL_BYTE);
const r_u_8: Format = Format::new(false, 8, 0, 0, 0, constants::GL_R8, constants::GL_RED, constants::GL_UNSIGNED_BYTE);
const rg_s_8: Format = Format::new(true, 8, 8, 0, 0, constants::GL_RG8, constants::GL_RG, constants::GL_BYTE);
const rg_u_8: Format = Format::new(false, 8, 8, 0, 0, constants::GL_RG8, constants::GL_RG, constants::GL_UNSIGNED_BYTE);
const rgb_s_8: Format = Format::new(true, 8, 8, 8, 0, constants::GL_RGB8, constants::GL_RGB, constants::GL_BYTE);
const rgb_u_8: Format = Format::new(false, 8, 8, 8, 0, constants::GL_RGB8, constants::GL_RGB, constants::GL_UNSIGNED_BYTE);
const rgba_s_8: Format = Format::new(true, 8, 8, 8, 8, constants::GL_RGBA8, constants::GL_RGBA, constants::GL_BYTE);
const rgba_u_8: Format = Format::new(false, 8, 8, 8, 8, constants::GL_RGBA8, constants::GL_RGBA, constants::GL_UNSIGNED_BYTE);

const r_s_16: Format = Format::new(true, 16, 0, 0, 0, constants::GL_R16, constants::GL_RED, constants::GL_SHORT);
const r_u_16: Format = Format::new(false, 16, 0, 0, 0, constants::GL_R16, constants::GL_RED, constants::GL_UNSIGNED_SHORT);
const rg_s_16: Format = Format::new(true, 16, 16, 0, 0, constants::GL_RG16, constants::GL_RG, constants::GL_SHORT);
const rg_u_16: Format = Format::new(false, 16, 16, 0, 0, constants::GL_RG16, constants::GL_RG, constants::GL_UNSIGNED_SHORT);
const rgb_s_16: Format = Format::new(true, 16, 16, 16, 0, constants::GL_RGB16, constants::GL_RGB, constants::GL_SHORT);
const rgb_u_16: Format = Format::new(false, 16, 16, 16, 0, constants::GL_RGB16, constants::GL_RGB, constants::GL_UNSIGNED_SHORT);
const rgba_s_16: Format = Format::new(true, 16, 16, 16, 16, constants::GL_RGBA16, constants::GL_RGBA, constants::GL_SHORT);
const rgba_u_16: Format = Format::new(false, 16, 16, 16, 16, constants::GL_RGBA16, constants::GL_RGBA, constants::GL_UNSIGNED_SHORT);

const r_s_32: Format = Format::new(true, 32, 0, 0, 0, u32::MAX, constants::GL_RED, constants::GL_INT);
const r_u_32: Format = Format::new(false, 32, 0, 0, 0, u32::MAX, constants::GL_RED, constants::GL_UNSIGNED_INT);
const rg_s_32: Format = Format::new(true, 32, 32, 0, 0, u32::MAX, constants::GL_RG, constants::GL_INT);
const rg_u_32: Format = Format::new(false, 32, 32, 0, 0, u32::MAX, constants::GL_RG, constants::GL_UNSIGNED_INT);
const rgb_s_32: Format = Format::new(true, 32, 32, 32, 0, u32::MAX, constants::GL_RGB, constants::GL_INT);
const rgb_u_32: Format = Format::new(false, 32, 32, 32, 0, u32::MAX, constants::GL_RGB, constants::GL_UNSIGNED_INT);
const rgba_s_32: Format = Format::new(true, 32, 32, 32, 32, u32::MAX, constants::GL_RGBA, constants::GL_INT);
const rgba_u_32: Format = Format::new(false, 32, 32, 32, 32, u32::MAX, constants::GL_RGBA, constants::GL_UNSIGNED_INT);

const r_f_16: Format = Format::new(true, 16, 0, 0, 0, constants::GL_R16F, constants::GL_RED, constants::GL_HALF_FLOAT);
const rg_f_16: Format = Format::new(true, 16, 16, 0, 0, constants::GL_RG16F, constants::GL_RG, constants::GL_HALF_FLOAT);
const rgb_f_16: Format = Format::new(true, 16, 16, 16, 0, constants::GL_RGB16F, constants::GL_RGB, constants::GL_HALF_FLOAT);
const rgba_f_16: Format = Format::new(true, 16, 16, 16, 16, constants::GL_RGBA16F, constants::GL_RGBA, constants::GL_HALF_FLOAT);

const r_f_32: Format = Format::new(true, 32, 0, 0, 0, constants::GL_R32F, constants::GL_RED, constants::GL_FLOAT);
const rg_f_32: Format = Format::new(true, 32, 32, 0, 0, constants::GL_RG32F, constants::GL_RG, constants::GL_FLOAT);
const rgb_f_32: Format = Format::new(true, 32, 32, 32, 0, constants::GL_RGB32F, constants::GL_RGB, constants::GL_FLOAT);
const rgba_f_32: Format = Format::new(true, 32, 32, 32, 32, constants::GL_RGBA32F, constants::GL_RGBA, constants::GL_FLOAT);

const depth_16: Format = Format::new(false, 16, 0, 0, 0, constants::GL_DEPTH_COMPONENT16, constants::GL_DEPTH_COMPONENT, u32::MAX);
const depth_24: Format = Format::new(false, 24, 0, 0, 0, constants::GL_DEPTH_COMPONENT24, constants::GL_DEPTH_COMPONENT, u32::MAX);
const depth_32: Format = Format::new(false, 32, 0, 0, 0, constants::GL_DEPTH_COMPONENT32, constants::GL_DEPTH_COMPONENT, u32::MAX);
const depth_24_stencil_8: Format = Format::new(false, 24, 8, 0, 0, constants::GL_DEPTH24_STENCIL8, constants::GL_DEPTH_STENCIL, u32::MAX);
const stencil_8: Format = Format::new(false, 8, 0, 0, 0, constants::GL_STENCIL_INDEX8, constants::GL_STENCIL_INDEX, u32::MAX);
const stencil_16: Format = Format::new(false, 16, 0, 0, 0, constants::GL_STENCIL_INDEX16, constants::GL_STENCIL_INDEX, u32::MAX);

pub struct Texture
{
	object: GLuint,
	levels: u32,
	width: u32,
	height: u32,
}

pub trait Texture_shared
{
	fn object(&self) -> GLuint;
	
	fn bind(&self, binding: GLuint)
	{
		unsafe {glBindTextureUnit(binding, self.object())};
	}
}

impl Drop for Texture
{
	fn drop(&mut self) {unsafe {glDeleteTextures(1, &self.object)};}
}

impl Texture
{
	pub fn create(target: GLenum, levels: u32, width: u32, height: u32) -> Self
	{
		let mut texture: GLuint = 0;
		unsafe {glCreateTextures(target, 1, &mut texture)};
		return Self {object: texture, levels, width, height};
	}
}

pub struct Texture_2D
{
	texture: Texture,
}

impl Texture_shared for Texture_2D
{
	fn object(&self) -> GLuint {self.texture.object}
}

impl Texture_2D
{
	pub fn create(levels: u32, width: u32, height: u32) -> Self
	{
		Self {texture: Texture::create(constants::GL_TEXTURE_2D, levels, width, height)}
	}
	
	pub fn storage(&self, internal_format: Format)
	{
		unsafe {glTextureStorage2D(self.texture.object, self.texture.levels as GLint,
			internal_format.internal, self.texture.width as GLint, self.texture.height as GLint);
		};
	}
	
	pub fn upload(&self, external_format: Format, data: &[u8])
	{
		unsafe
		{
			glTextureSubImage2D(self.texture.object, 0, 0, 0,
				self.texture.width as GLint, self.texture.height as GLint,
				external_format.external, external_format.type_v,
				data.as_ptr().cast::<std::ffi::c_void>()
			);
			
			if self.texture.levels > 1
			{
				glGenerateTextureMipmap(self.texture.object);
			}
		};
	}
}

pub struct Texture_2D_array
{
	texture: Texture,
	layers: u32,
}

impl Texture_shared for Texture_2D_array
{
	fn object(&self) -> GLuint {self.texture.object}
}

impl Texture_2D_array
{
	pub fn create(width: u32, height: u32, levels: u32) -> Self
	{
		Self {texture: Texture::create(constants::GL_TEXTURE_2D_ARRAY, levels, width, height), layers: 0}
	}
	
	pub fn storage(&mut self, internal_format: Format, layers: u32)
	{
		self.layers = layers;
		
		unsafe
		{
			glTextureStorage3D(self.texture.object,
				self.texture.levels as GLint, internal_format.internal,
				self.texture.width as GLint, self.texture.height as GLint, layers as GLint
			);
		};
	}
	
	pub fn upload(&self, layer_begin: usize, number: usize, external_format: Format, data: &[u8])
	{
		if layer_begin + number > self.layers as usize
		{
			panic!("Texture_2D_array::upload: Number of layers exceeds array capacity");
		}
		
		unsafe
		{
			glTextureSubImage3D(self.texture.object, 0, 0, 0, layer_begin as GLint,
				self.texture.width as GLsizei, self.texture.height as GLsizei, number as GLsizei,
				external_format.external, external_format.type_v,
				data.as_ptr().cast::<std::ffi::c_void>()
			);
		}
		
		if self.texture.levels > 1
		{
			unsafe {glGenerateTextureMipmap(self.texture.object)};
		}
	}
}

pub struct Renderbuffer
{
	object: GLuint,
	width: GLsizei,
	height: GLsizei,
	samples: GLsizei,
}

impl Drop for Renderbuffer
{
	fn drop(&mut self) {unsafe {glDeleteRenderbuffers(1, &self.object)}}
}

impl Renderbuffer
{
	pub fn create(width: GLsizei, height: GLsizei, samples: GLsizei) -> Self
	{
		let mut object: GLuint = 0;
		unsafe {glCreateRenderbuffers(1, &mut object)};
		return Self {object, width, height, samples};
	}
	
	pub fn storage(&self, internal_format: Format)
	{
		unsafe
		{
			glNamedRenderbufferStorageMultisample(self.object,
				self.samples, internal_format.internal, self.width, self.height
			);
		};
	}
}

pub struct Attachment(GLenum);

impl Attachment
{
	pub const depth: Attachment = Attachment(constants::GL_DEPTH_ATTACHMENT);
	pub const stencil: Attachment = Attachment(constants::GL_STENCIL_ATTACHMENT);
	pub const depth_stencil: Attachment = Attachment(constants::GL_DEPTH_STENCIL_ATTACHMENT);
	pub const fn color(index: crate::graphics::opengl::types::GLenum) -> Attachment
	{
		Attachment(constants::GL_COLOR_ATTACHMENT0 + index)
	}
}

pub struct Target(GLenum);

impl Target
{
	pub const read: Target = Target(constants::GL_DRAW_FRAMEBUFFER);
	pub const draw: Target = Target(constants::GL_READ_FRAMEBUFFER);
	pub const read_draw: Target = Target(constants::GL_FRAMEBUFFER);
}

pub struct Draw_buffer(GLenum);

impl Draw_buffer
{
	pub const none: Draw_buffer = Draw_buffer(constants::GL_NONE);
	pub const front_left: Draw_buffer = Draw_buffer(constants::GL_FRONT_LEFT);
	pub const front_right: Draw_buffer = Draw_buffer(constants::GL_FRONT_RIGHT);
	pub const back_left: Draw_buffer = Draw_buffer(constants::GL_BACK_LEFT);
	pub const back_right: Draw_buffer = Draw_buffer(constants::GL_BACK_RIGHT);
	pub const front: Draw_buffer = Draw_buffer(constants::GL_FRONT);
	pub const back: Draw_buffer = Draw_buffer(constants::GL_BACK);
	pub const left: Draw_buffer = Draw_buffer(constants::GL_LEFT);
	pub const right: Draw_buffer = Draw_buffer(constants::GL_RIGHT);
	pub const front_back: Draw_buffer = Draw_buffer(constants::GL_FRONT_AND_BACK);
}

pub struct Framebuffer
{
	object: GLuint,
	draw_buffer: Draw_buffer,
}

impl Drop for Framebuffer
{
	fn drop(&mut self) {unsafe {glDeleteFramebuffers(1, &self.object)};}
}

pub struct Bound_framebuffer
{
	target: Target,
}

impl Drop for Bound_framebuffer
{
	fn drop(&mut self) {unsafe {glBindFramebuffer(self.target.0, 0);};}
}

impl Framebuffer
{
	pub fn create() -> Self
	{
		let mut object: GLuint = 0;
		unsafe {glCreateFramebuffers(1, &mut object)};
		return Self {object, draw_buffer: Draw_buffer::none}
	}
	
	pub fn attach_texture(&self, texture: &Texture, attachment: Attachment)
	{
		unsafe {glNamedFramebufferTexture(self.object, attachment.0, texture.object, 0)};
	}

	pub fn attach_renderbuffer(&self, renderbuffer: &Renderbuffer, attachment: Attachment)
	{
		unsafe {glNamedFramebufferRenderbuffer(self.object, attachment.0, constants::GL_RENDERBUFFER, renderbuffer.object)};
	}

	pub fn set_draw_buffer(&mut self, draw_buffer: Draw_buffer)
	{
		self.draw_buffer = draw_buffer;
		unsafe {glNamedFramebufferDrawBuffer(self.object, self.draw_buffer.0)};
	}
	
	pub fn bind(&self, target: Target) -> Bound_framebuffer
	{
		unsafe {glBindFramebuffer(target.0, self.object)};
		return Bound_framebuffer {target};
	}
}

struct Texture_handle(GLuint64);

impl From<&Texture> for Texture_handle
{
	fn from(value: &Texture) -> Self {Self(unsafe {glGetTextureHandleARB(value.object)})}
}

struct Texture_handle_resident(GLuint64);

impl Drop for Texture_handle_resident
{
	fn drop(&mut self) {unsafe {glMakeTextureHandleNonResidentARB(self.0)};}
}

impl From<&Texture_handle> for Texture_handle_resident
{
	fn from(value: &Texture_handle) -> Self
	{
		unsafe {glMakeTextureHandleResidentARB(value.0)};
		return Self(value.0);
	}
}
