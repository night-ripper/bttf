#[link(name = "png")]
extern "C"
{
	fn png_destroy_read_struct(png_ptr_ptr: *mut *mut std::ffi::c_void,
		info_ptr_ptr: *mut *mut std::ffi::c_void, end_info_ptr_ptr: *mut *mut std::ffi::c_void,
	);
	fn png_create_read_struct(user_png_ver: *const std::ffi::c_char, error_ptr: *mut std::ffi::c_void,
		error_fn: Option<extern "C" fn(png_ptr: *mut std::ffi::c_void, message: *const std::ffi::c_char)>,
		warn_fn: Option<extern "C" fn(png_ptr: *mut std::ffi::c_void, message: *const std::ffi::c_char)>,
	) -> *mut std::ffi::c_void;
	fn png_create_info_struct(png_ptr: *mut std::ffi::c_void) -> *mut std::ffi::c_void;
	fn png_sig_cmp(sig: *const std::ffi::c_uchar, start: usize, num_to_check: usize) -> std::ffi::c_int;
	fn png_set_read_fn(png_ptr: *mut std::ffi::c_void, io_ptr: *mut std::ffi::c_void,
		read_data_fn: Option<extern "C" fn(png_ptr: *mut std::ffi::c_void, *mut std::ffi::c_uchar, usize)>
	);
	fn png_get_io_ptr(png_ptr: *const std::ffi::c_void) -> *mut std::ffi::c_void;
	fn png_set_sig_bytes(png_ptr: *mut std::ffi::c_void, num_bytes: std::ffi::c_int);
	fn png_read_info(png_ptr: *mut std::ffi::c_void, info_ptr: *mut std::ffi::c_void);
	fn png_get_image_width(png_ptr: *const std::ffi::c_void, info_ptr: *const std::ffi::c_void) -> u32;
	fn png_get_image_height(png_ptr: *const std::ffi::c_void, info_ptr: *const std::ffi::c_void) -> u32;
	fn png_get_bit_depth(png_ptr: *const std::ffi::c_void, info_ptr: *const std::ffi::c_void) -> std::ffi::c_uchar;
	fn png_get_channels(png_ptr: *const std::ffi::c_void, info_ptr: *const std::ffi::c_void) -> std::ffi::c_uchar;
	fn png_get_color_type(png_ptr: *const std::ffi::c_void, info_ptr: *const std::ffi::c_void) -> std::ffi::c_uchar;
	fn png_set_palette_to_rgb(png_ptr: *mut std::ffi::c_void);
	fn png_set_add_alpha(png_ptr: *mut std::ffi::c_void, filler: u32, flags: std::ffi::c_int);
	fn png_set_expand_gray_1_2_4_to_8(png_ptr: *mut std::ffi::c_void);
	fn png_get_valid(png_ptr: *const std::ffi::c_void, info_ptr: *const std::ffi::c_void, flag: u32) -> u32;
	fn png_set_tRNS_to_alpha(png_ptr: *mut std::ffi::c_void);
	fn png_set_strip_16(png_ptr: *mut std::ffi::c_void);
	fn png_read_update_info(png_ptr: *mut std::ffi::c_void, info_ptr: *mut std::ffi::c_void);
	fn png_read_row(png_ptr: *mut std::ffi::c_void, row: *mut std::ffi::c_uchar, display_row: *mut std::ffi::c_uchar);
}

const PNG_COLOR_MASK_PALETTE: std::ffi::c_int = 1;
const PNG_COLOR_MASK_COLOR: std::ffi::c_int = 2;
const PNG_COLOR_MASK_ALPHA: std::ffi::c_int = 4;
const PNG_COLOR_TYPE_GRAY: std::ffi::c_int = 0;
const PNG_COLOR_TYPE_PALETTE: std::ffi::c_int = PNG_COLOR_MASK_COLOR | PNG_COLOR_MASK_PALETTE;
const PNG_COLOR_TYPE_RGB: std::ffi::c_int = PNG_COLOR_MASK_COLOR;
const PNG_COLOR_TYPE_RGB_ALPHA: std::ffi::c_int = PNG_COLOR_MASK_COLOR | PNG_COLOR_MASK_ALPHA;
const PNG_COLOR_TYPE_GRAY_ALPHA: std::ffi::c_int = PNG_COLOR_MASK_ALPHA;
const PNG_COLOR_TYPE_RGBA: std::ffi::c_int = PNG_COLOR_TYPE_RGB_ALPHA;
const PNG_COLOR_TYPE_GA: std::ffi::c_int = PNG_COLOR_TYPE_GRAY_ALPHA;

const PNG_FILLER_BEFORE: std::ffi::c_int = 0;
const PNG_FILLER_AFTER: std::ffi::c_int = 1;

const PNG_INFO_gAMA: std::ffi::c_uint = 0x0001;
const PNG_INFO_sBIT: std::ffi::c_uint = 0x0002;
const PNG_INFO_cHRM: std::ffi::c_uint = 0x0004;
const PNG_INFO_PLTE: std::ffi::c_uint = 0x0008;
const PNG_INFO_tRNS: std::ffi::c_uint = 0x0010;
const PNG_INFO_bKGD: std::ffi::c_uint = 0x0020;
const PNG_INFO_hIST: std::ffi::c_uint = 0x0040;
const PNG_INFO_pHYs: std::ffi::c_uint = 0x0080;
const PNG_INFO_oFFs: std::ffi::c_uint = 0x0100;
const PNG_INFO_tIME: std::ffi::c_uint = 0x0200;
const PNG_INFO_pCAL: std::ffi::c_uint = 0x0400;
const PNG_INFO_sRGB: std::ffi::c_uint = 0x0800  /* GR-P, 0.96a */;
const PNG_INFO_iCCP: std::ffi::c_uint = 0x1000  /* ESR, 1.0.6 */;
const PNG_INFO_sPLT: std::ffi::c_uint = 0x2000  /* ESR, 1.0.6 */;
const PNG_INFO_sCAL: std::ffi::c_uint = 0x4000  /* ESR, 1.0.6 */;
const PNG_INFO_IDAT: std::ffi::c_uint = 0x8000  /* ESR, 1.0.6 */;
const PNG_INFO_eXIf: std::ffi::c_uint = 0x10000 /* GR-P, 1.6.31 */;

extern "C" fn read_data<Type: std::io::Read>(png_ptr: *mut std::ffi::c_void, data: *mut std::ffi::c_uchar, length: usize)
{
	unsafe
	{
		let stream = png_get_io_ptr(png_ptr).cast::<Type>().as_mut().unwrap();
		stream.read_exact(std::slice::from_raw_parts_mut(data, length)).unwrap();
	};
}

extern "C" fn png_error_callback(_png_ptr: *mut std::ffi::c_void, error_message: *const std::ffi::c_char)
{
	panic!("{}", unsafe {std::ffi::CStr::from_ptr(error_message).to_str().unwrap()});
}

extern "C" fn png_warning_callback(_png_ptr: *mut std::ffi::c_void, warning_message: *const std::ffi::c_char)
{
	log::warn!("Libpng warning: {}", unsafe {std::ffi::CStr::from_ptr(warning_message).to_str().unwrap()});
}

struct Png_struct
{
	struct_p: std::ptr::NonNull<std::ffi::c_void>,
	info_p: std::ptr::NonNull<std::ffi::c_void>,
	end_info_p: std::ptr::NonNull<std::ffi::c_void>,
}

impl Drop for Png_struct
{
	fn drop(&mut self)
	{
		unsafe {png_destroy_read_struct(&mut self.struct_p.as_ptr(), &mut self.info_p.as_ptr(), &mut self.end_info_p.as_ptr())};
	}
}

impl Png_struct
{
	fn new() -> Self
	{
		let version = std::ffi::CStr::from_bytes_until_nul(b"1.6.0\0").unwrap();
		
		let Some(struct_p) = std::ptr::NonNull::new(unsafe {png_create_read_struct(
			version.as_ptr(), std::ptr::null_mut(), Some(png_error_callback), Some(png_warning_callback))}) else
		{
			panic!("Could not create a PNG read struct");
		};
		
		let Some(info_p) = std::ptr::NonNull::new(unsafe {png_create_info_struct(struct_p.as_ptr())}) else
		{
			unsafe {png_destroy_read_struct(&mut struct_p.as_ptr(), std::ptr::null_mut(), std::ptr::null_mut())};
			panic!("Could not create a PNG info struct");
		};
		
		let Some(end_info_p) = std::ptr::NonNull::new(unsafe {png_create_info_struct(struct_p.as_ptr())}) else
		{
			unsafe {png_destroy_read_struct(&mut struct_p.as_ptr(), &mut info_p.as_ptr(), std::ptr::null_mut())};
			panic!("Could not create a PNG end info struct");
		};
		
		Self {struct_p, info_p, end_info_p}
	}
}

const png_signature_size: usize = 8;

fn read_signature(stream: &mut impl std::io::Read)
{
	let mut signature: [u8; png_signature_size] = [0; png_signature_size];
	
	stream.read_exact(&mut signature).unwrap();
	
	if unsafe {png_sig_cmp(signature.as_ptr(), 0, png_signature_size)} != 0
	{
		panic!("PNG signature does not match");
	}
}

fn read_info<Type: std::io::Read>(png: &mut Png_struct, stream: &mut Type) -> [u32; 2]
{
	unsafe
	{
		png_set_read_fn(png.struct_p.as_ptr(), stream as *mut Type as *mut std::ffi::c_void, Some(read_data::<Type>));
		png_set_sig_bytes(png.struct_p.as_ptr(), png_signature_size as std::ffi::c_int);
		
		png_read_info(png.struct_p.as_ptr(), png.info_p.as_ptr());
		
		return [
			png_get_image_width(png.struct_p.as_ptr(), png.info_p.as_ptr()),
			png_get_image_height(png.struct_p.as_ptr(), png.info_p.as_ptr()),
		];
	};
}

fn set_rgba_8(png: &mut Png_struct)
{
	unsafe
	{
		let bit_depth = png_get_bit_depth(png.struct_p.as_ptr(), png.info_p.as_ptr());
		let _channels = png_get_channels(png.struct_p.as_ptr(), png.info_p.as_ptr());
		let color_type = png_get_color_type(png.struct_p.as_ptr(), png.info_p.as_ptr());
		
		if color_type as std::ffi::c_int == PNG_COLOR_TYPE_PALETTE
		{
			png_set_palette_to_rgb(png.struct_p.as_ptr());
		}
		
		if color_type as std::ffi::c_int & PNG_COLOR_MASK_ALPHA == 0
		{
			png_set_add_alpha(png.struct_p.as_ptr(), u32::MAX, PNG_FILLER_AFTER);
		}
		
		if color_type as std::ffi::c_int == PNG_COLOR_TYPE_GRAY
		{
			if bit_depth < 8
			{
				png_set_expand_gray_1_2_4_to_8(png.struct_p.as_ptr());
			}
		}
		
		if png_get_valid(png.struct_p.as_ptr(), png.info_p.as_ptr(), PNG_INFO_tRNS) != 0
		{
			png_set_tRNS_to_alpha(png.struct_p.as_ptr());
		}
		
		if bit_depth == 16
		{
			png_set_strip_16(png.struct_p.as_ptr());
		}
		
		png_read_update_info(png.struct_p.as_ptr(), png.info_p.as_ptr());
		
		// Ensure that the data are in RGBA8 format
		if png_get_bit_depth(png.struct_p.as_ptr(), png.info_p.as_ptr()) != 8
			|| png_get_channels(png.struct_p.as_ptr(), png.info_p.as_ptr()) != 4
		{
			panic!("PNG image is not in RGBA8 format");
		}
	}
}

unsafe fn read_row(png: &mut Png_struct, target: *mut std::ffi::c_uchar)
{
	png_read_row(png.struct_p.as_ptr(), target, std::ptr::null_mut());
}

fn row_stride(width: u32) -> u32
{
	/// RGBA8
	const bit_depth: u32 = 8;
	const channels: u32 = 4;
	
	return width * (bit_depth * channels / 8);
}

unsafe fn read_rows<Row_index_fn: std::ops::FnMut(u32) -> u32>(png: &mut Png_struct,
	width: u32, height: u32, target: *mut std::ffi::c_uchar,
	mut row_index_function: Row_index_fn) -> *mut std::ffi::c_uchar
{
	for i in 0 .. height
	{
		let offset = row_stride(width) * row_index_function(i);
		read_row(png, target.offset(offset as isize));
	}
	
	return target.offset(row_stride(width) as isize * height as isize);
}

pub fn read_bottom_up<Type: std::io::Read>(stream: &mut Type) -> image::RgbaImage
{
	let mut png = Png_struct::new();
	read_signature(stream);
	let [width, height] = read_info(&mut png, stream);
	set_rgba_8(&mut png);
	let mut data = Vec::<u8>::new();
	data.resize(width as usize * height as usize * 4, 0);
	unsafe {read_rows(&mut png, width, height, data.as_mut_ptr(), |index: u32| height - index - 1)};
	return image::RgbaImage::from_raw(width, height, data).unwrap();
}

pub fn read_top_down<Type: std::io::Read>(stream: &mut Type) -> image::RgbaImage
{
	let mut png = Png_struct::new();
	read_signature(stream);
	let [width, height] = read_info(&mut png, stream);
	set_rgba_8(&mut png);
	let mut data = Vec::<u8>::new();
	data.resize(width as usize * height as usize * 4, 0);
	unsafe {read_rows(&mut png, width, height, data.as_mut_ptr(), |index: u32| index)};
	return image::RgbaImage::from_raw(width, height, data).unwrap();
}
