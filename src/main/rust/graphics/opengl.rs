use std::io::Write;

pub mod types;
pub mod constants;

pub mod buffers;
pub mod program;
pub mod textures;

trait Enum_of
{
	const as_enum: types::GLenum;
}

impl Enum_of for types::GLbyte {const as_enum: types::GLenum = constants::GL_BYTE;}
impl Enum_of for types::GLubyte {const as_enum: types::GLenum = constants::GL_UNSIGNED_BYTE;}
impl Enum_of for types::GLshort {const as_enum: types::GLenum = constants::GL_SHORT;}
impl Enum_of for types::GLushort {const as_enum: types::GLenum = constants::GL_UNSIGNED_SHORT;}
impl Enum_of for types::GLint {const as_enum: types::GLenum = constants::GL_INT;}
impl Enum_of for types::GLuint {const as_enum: types::GLenum = constants::GL_UNSIGNED_INT;}
impl Enum_of for types::GLint64 {const as_enum: types::GLenum = constants::GL_INT64_ARB;}
impl Enum_of for types::GLuint64 {const as_enum: types::GLenum = constants::GL_UNSIGNED_INT64_ARB;}
// impl Enum_of for types::_Float16 {const as_enum: types::GLenum = constants::GL_HALF_FLOAT;}
impl Enum_of for types::GLfloat {const as_enum: types::GLenum = constants::GL_FLOAT;}
impl Enum_of for types::GLdouble {const as_enum: types::GLenum = constants::GL_DOUBLE;}

#[link(name = "GL")]
extern "C"
{
	pub fn glEnable(cap: types::GLenum);
	pub fn glDisable(cap: types::GLenum);
	pub fn glDebugMessageCallback(callback: Option<extern "C" fn(
		source: types::GLenum,
		type_v: types::GLenum,
		id: types::GLuint,
		severity: types::GLenum,
		length: types::GLsizei,
		message: *const std::ffi::c_char,
		user_parameter: *const std::ffi::c_void)>,
		user_parameter: *const std::ffi::c_void
	);
	pub fn glDepthFunc(func: types::GLenum);
}

pub extern "C" fn opengl_debug_callback(
	source: types::GLenum,
	type_v: types::GLenum,
	_id: types::GLuint,
	severity: types::GLenum,
	length: types::GLsizei,
	message: *const std::ffi::c_char,
	_user_parameter: *const std::ffi::c_void,
)
{
	let source_string = match source
	{
		constants::GL_DEBUG_SOURCE_API => "api",
		constants::GL_DEBUG_SOURCE_WINDOW_SYSTEM => "window system",
		constants::GL_DEBUG_SOURCE_SHADER_COMPILER => "shader compiler",
		constants::GL_DEBUG_SOURCE_THIRD_PARTY => "third party",
		constants::GL_DEBUG_SOURCE_APPLICATION => "application",
		constants::GL_DEBUG_SOURCE_OTHER => "other",
		_ => "(unknown)",
	};
	
	let type_string = match type_v
	{
		constants::GL_DEBUG_TYPE_ERROR => "error",
		constants::GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR => "deprecated behaviour",
		constants::GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR => "undefined behaviour",
		constants::GL_DEBUG_TYPE_PORTABILITY => "portability",
		constants::GL_DEBUG_TYPE_PERFORMANCE => "performance",
		constants::GL_DEBUG_TYPE_MARKER => "marker",
		constants::GL_DEBUG_TYPE_PUSH_GROUP => "push group",
		constants::GL_DEBUG_TYPE_POP_GROUP => "pop group",
		constants::GL_DEBUG_TYPE_OTHER => "other",
		_ => "(unknown)",
	};
	
	let severity_string = match severity
	{
		constants::GL_DEBUG_SEVERITY_HIGH => "high",
		constants::GL_DEBUG_SEVERITY_MEDIUM => "medium",
		constants::GL_DEBUG_SEVERITY_LOW => "low",
		constants::GL_DEBUG_SEVERITY_NOTIFICATION => "notification",
		_ => "(unknown)",
	};
	
	let message = format!("OpenGL error: (source: {}, type: {}, severity: {}): {}",
		source_string, type_string, severity_string, unsafe {std::str::from_utf8(
			std::slice::from_raw_parts(message.cast::<u8>(), length as usize)
		)}.unwrap_or("<OpenGL error could not be decoded...>")
	);
	
	if severity == constants::GL_DEBUG_SEVERITY_HIGH
	{
		panic!("{}", message);
	}
	else
	{
		writeln!(std::io::stderr(), "{}", message);
	}
}
