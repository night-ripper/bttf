pub const GLFW_RELEASE: std::ffi::c_int = 0;
pub const GLFW_PRESS: std::ffi::c_int = 1;
pub const GLFW_REPEAT: std::ffi::c_int = 2;
pub const GLFW_HAT_CENTERED: std::ffi::c_int = 0;
pub const GLFW_HAT_UP: std::ffi::c_int = 1;
pub const GLFW_HAT_RIGHT: std::ffi::c_int = 2;
pub const GLFW_HAT_DOWN: std::ffi::c_int = 4;
pub const GLFW_HAT_LEFT: std::ffi::c_int = 8;
pub const GLFW_HAT_RIGHT_UP: std::ffi::c_int = GLFW_HAT_RIGHT | GLFW_HAT_UP;
pub const GLFW_HAT_RIGHT_DOWN: std::ffi::c_int = GLFW_HAT_RIGHT | GLFW_HAT_DOWN;
pub const GLFW_HAT_LEFT_UP: std::ffi::c_int = GLFW_HAT_LEFT  | GLFW_HAT_UP;
pub const GLFW_HAT_LEFT_DOWN: std::ffi::c_int = GLFW_HAT_LEFT  | GLFW_HAT_DOWN;
pub const GLFW_KEY_UNKNOWN: std::ffi::c_int = -1;
pub const GLFW_KEY_SPACE: std::ffi::c_int = 32;
pub const GLFW_KEY_APOSTROPHE: std::ffi::c_int = 39  /* ' */;
pub const GLFW_KEY_COMMA: std::ffi::c_int = 44  /* , */;
pub const GLFW_KEY_MINUS: std::ffi::c_int = 45  /* - */;
pub const GLFW_KEY_PERIOD: std::ffi::c_int = 46  /* . */;
pub const GLFW_KEY_SLASH: std::ffi::c_int = 47  /* / */;
pub const GLFW_KEY_0: std::ffi::c_int = 48;
pub const GLFW_KEY_1: std::ffi::c_int = 49;
pub const GLFW_KEY_2: std::ffi::c_int = 50;
pub const GLFW_KEY_3: std::ffi::c_int = 51;
pub const GLFW_KEY_4: std::ffi::c_int = 52;
pub const GLFW_KEY_5: std::ffi::c_int = 53;
pub const GLFW_KEY_6: std::ffi::c_int = 54;
pub const GLFW_KEY_7: std::ffi::c_int = 55;
pub const GLFW_KEY_8: std::ffi::c_int = 56;
pub const GLFW_KEY_9: std::ffi::c_int = 57;
pub const GLFW_KEY_SEMICOLON: std::ffi::c_int = 59  /* ; */;
pub const GLFW_KEY_EQUAL: std::ffi::c_int = 61  /* = */;
pub const GLFW_KEY_A: std::ffi::c_int = 65;
pub const GLFW_KEY_B: std::ffi::c_int = 66;
pub const GLFW_KEY_C: std::ffi::c_int = 67;
pub const GLFW_KEY_D: std::ffi::c_int = 68;
pub const GLFW_KEY_E: std::ffi::c_int = 69;
pub const GLFW_KEY_F: std::ffi::c_int = 70;
pub const GLFW_KEY_G: std::ffi::c_int = 71;
pub const GLFW_KEY_H: std::ffi::c_int = 72;
pub const GLFW_KEY_I: std::ffi::c_int = 73;
pub const GLFW_KEY_J: std::ffi::c_int = 74;
pub const GLFW_KEY_K: std::ffi::c_int = 75;
pub const GLFW_KEY_L: std::ffi::c_int = 76;
pub const GLFW_KEY_M: std::ffi::c_int = 77;
pub const GLFW_KEY_N: std::ffi::c_int = 78;
pub const GLFW_KEY_O: std::ffi::c_int = 79;
pub const GLFW_KEY_P: std::ffi::c_int = 80;
pub const GLFW_KEY_Q: std::ffi::c_int = 81;
pub const GLFW_KEY_R: std::ffi::c_int = 82;
pub const GLFW_KEY_S: std::ffi::c_int = 83;
pub const GLFW_KEY_T: std::ffi::c_int = 84;
pub const GLFW_KEY_U: std::ffi::c_int = 85;
pub const GLFW_KEY_V: std::ffi::c_int = 86;
pub const GLFW_KEY_W: std::ffi::c_int = 87;
pub const GLFW_KEY_X: std::ffi::c_int = 88;
pub const GLFW_KEY_Y: std::ffi::c_int = 89;
pub const GLFW_KEY_Z: std::ffi::c_int = 90;
pub const GLFW_KEY_LEFT_BRACKET: std::ffi::c_int = 91  /* [ */;
pub const GLFW_KEY_BACKSLASH: std::ffi::c_int = 92  /* \ */;
pub const GLFW_KEY_RIGHT_BRACKET: std::ffi::c_int = 93  /* ] */;
pub const GLFW_KEY_GRAVE_ACCENT: std::ffi::c_int = 96  /* ` */;
pub const GLFW_KEY_WORLD_1: std::ffi::c_int = 161 /* non-US #1 */;
pub const GLFW_KEY_WORLD_2: std::ffi::c_int = 162 /* non-US #2 */;
pub const GLFW_KEY_ESCAPE: std::ffi::c_int = 256;
pub const GLFW_KEY_ENTER: std::ffi::c_int = 257;
pub const GLFW_KEY_TAB: std::ffi::c_int = 258;
pub const GLFW_KEY_BACKSPACE: std::ffi::c_int = 259;
pub const GLFW_KEY_INSERT: std::ffi::c_int = 260;
pub const GLFW_KEY_DELETE: std::ffi::c_int = 261;
pub const GLFW_KEY_RIGHT: std::ffi::c_int = 262;
pub const GLFW_KEY_LEFT: std::ffi::c_int = 263;
pub const GLFW_KEY_DOWN: std::ffi::c_int = 264;
pub const GLFW_KEY_UP: std::ffi::c_int = 265;
pub const GLFW_KEY_PAGE_UP: std::ffi::c_int = 266;
pub const GLFW_KEY_PAGE_DOWN: std::ffi::c_int = 267;
pub const GLFW_KEY_HOME: std::ffi::c_int = 268;
pub const GLFW_KEY_END: std::ffi::c_int = 269;
pub const GLFW_KEY_CAPS_LOCK: std::ffi::c_int = 280;
pub const GLFW_KEY_SCROLL_LOCK: std::ffi::c_int = 281;
pub const GLFW_KEY_NUM_LOCK: std::ffi::c_int = 282;
pub const GLFW_KEY_PRINT_SCREEN: std::ffi::c_int = 283;
pub const GLFW_KEY_PAUSE: std::ffi::c_int = 284;
pub const GLFW_KEY_F1: std::ffi::c_int = 290;
pub const GLFW_KEY_F2: std::ffi::c_int = 291;
pub const GLFW_KEY_F3: std::ffi::c_int = 292;
pub const GLFW_KEY_F4: std::ffi::c_int = 293;
pub const GLFW_KEY_F5: std::ffi::c_int = 294;
pub const GLFW_KEY_F6: std::ffi::c_int = 295;
pub const GLFW_KEY_F7: std::ffi::c_int = 296;
pub const GLFW_KEY_F8: std::ffi::c_int = 297;
pub const GLFW_KEY_F9: std::ffi::c_int = 298;
pub const GLFW_KEY_F10: std::ffi::c_int = 299;
pub const GLFW_KEY_F11: std::ffi::c_int = 300;
pub const GLFW_KEY_F12: std::ffi::c_int = 301;
pub const GLFW_KEY_F13: std::ffi::c_int = 302;
pub const GLFW_KEY_F14: std::ffi::c_int = 303;
pub const GLFW_KEY_F15: std::ffi::c_int = 304;
pub const GLFW_KEY_F16: std::ffi::c_int = 305;
pub const GLFW_KEY_F17: std::ffi::c_int = 306;
pub const GLFW_KEY_F18: std::ffi::c_int = 307;
pub const GLFW_KEY_F19: std::ffi::c_int = 308;
pub const GLFW_KEY_F20: std::ffi::c_int = 309;
pub const GLFW_KEY_F21: std::ffi::c_int = 310;
pub const GLFW_KEY_F22: std::ffi::c_int = 311;
pub const GLFW_KEY_F23: std::ffi::c_int = 312;
pub const GLFW_KEY_F24: std::ffi::c_int = 313;
pub const GLFW_KEY_F25: std::ffi::c_int = 314;
pub const GLFW_KEY_KP_0: std::ffi::c_int = 320;
pub const GLFW_KEY_KP_1: std::ffi::c_int = 321;
pub const GLFW_KEY_KP_2: std::ffi::c_int = 322;
pub const GLFW_KEY_KP_3: std::ffi::c_int = 323;
pub const GLFW_KEY_KP_4: std::ffi::c_int = 324;
pub const GLFW_KEY_KP_5: std::ffi::c_int = 325;
pub const GLFW_KEY_KP_6: std::ffi::c_int = 326;
pub const GLFW_KEY_KP_7: std::ffi::c_int = 327;
pub const GLFW_KEY_KP_8: std::ffi::c_int = 328;
pub const GLFW_KEY_KP_9: std::ffi::c_int = 329;
pub const GLFW_KEY_KP_DECIMAL: std::ffi::c_int = 330;
pub const GLFW_KEY_KP_DIVIDE: std::ffi::c_int = 331;
pub const GLFW_KEY_KP_MULTIPLY: std::ffi::c_int = 332;
pub const GLFW_KEY_KP_SUBTRACT: std::ffi::c_int = 333;
pub const GLFW_KEY_KP_ADD: std::ffi::c_int = 334;
pub const GLFW_KEY_KP_ENTER: std::ffi::c_int = 335;
pub const GLFW_KEY_KP_EQUAL: std::ffi::c_int = 336;
pub const GLFW_KEY_LEFT_SHIFT: std::ffi::c_int = 340;
pub const GLFW_KEY_LEFT_CONTROL: std::ffi::c_int = 341;
pub const GLFW_KEY_LEFT_ALT: std::ffi::c_int = 342;
pub const GLFW_KEY_LEFT_SUPER: std::ffi::c_int = 343;
pub const GLFW_KEY_RIGHT_SHIFT: std::ffi::c_int = 344;
pub const GLFW_KEY_RIGHT_CONTROL: std::ffi::c_int = 345;
pub const GLFW_KEY_RIGHT_ALT: std::ffi::c_int = 346;
pub const GLFW_KEY_RIGHT_SUPER: std::ffi::c_int = 347;
pub const GLFW_KEY_MENU: std::ffi::c_int = 348;
pub const GLFW_KEY_LAST: std::ffi::c_int = GLFW_KEY_MENU;
pub const GLFW_MOD_SHIFT: std::ffi::c_int = 0x0001;
pub const GLFW_MOD_CONTROL: std::ffi::c_int = 0x0002;
pub const GLFW_MOD_ALT: std::ffi::c_int = 0x0004;
pub const GLFW_MOD_SUPER: std::ffi::c_int = 0x0008;
pub const GLFW_MOD_CAPS_LOCK: std::ffi::c_int = 0x0010;
pub const GLFW_MOD_NUM_LOCK: std::ffi::c_int = 0x0020;
pub const GLFW_MOUSE_BUTTON_1: std::ffi::c_int = 0;
pub const GLFW_MOUSE_BUTTON_2: std::ffi::c_int = 1;
pub const GLFW_MOUSE_BUTTON_3: std::ffi::c_int = 2;
pub const GLFW_MOUSE_BUTTON_4: std::ffi::c_int = 3;
pub const GLFW_MOUSE_BUTTON_5: std::ffi::c_int = 4;
pub const GLFW_MOUSE_BUTTON_6: std::ffi::c_int = 5;
pub const GLFW_MOUSE_BUTTON_7: std::ffi::c_int = 6;
pub const GLFW_MOUSE_BUTTON_8: std::ffi::c_int = 7;
pub const GLFW_MOUSE_BUTTON_LAST: std::ffi::c_int = GLFW_MOUSE_BUTTON_8;
pub const GLFW_MOUSE_BUTTON_LEFT: std::ffi::c_int = GLFW_MOUSE_BUTTON_1;
pub const GLFW_MOUSE_BUTTON_RIGHT: std::ffi::c_int = GLFW_MOUSE_BUTTON_2;
pub const GLFW_MOUSE_BUTTON_MIDDLE: std::ffi::c_int = GLFW_MOUSE_BUTTON_3;
pub const GLFW_JOYSTICK_1: std::ffi::c_int = 0;
pub const GLFW_JOYSTICK_2: std::ffi::c_int = 1;
pub const GLFW_JOYSTICK_3: std::ffi::c_int = 2;
pub const GLFW_JOYSTICK_4: std::ffi::c_int = 3;
pub const GLFW_JOYSTICK_5: std::ffi::c_int = 4;
pub const GLFW_JOYSTICK_6: std::ffi::c_int = 5;
pub const GLFW_JOYSTICK_7: std::ffi::c_int = 6;
pub const GLFW_JOYSTICK_8: std::ffi::c_int = 7;
pub const GLFW_JOYSTICK_9: std::ffi::c_int = 8;
pub const GLFW_JOYSTICK_10: std::ffi::c_int = 9;
pub const GLFW_JOYSTICK_11: std::ffi::c_int = 10;
pub const GLFW_JOYSTICK_12: std::ffi::c_int = 11;
pub const GLFW_JOYSTICK_13: std::ffi::c_int = 12;
pub const GLFW_JOYSTICK_14: std::ffi::c_int = 13;
pub const GLFW_JOYSTICK_15: std::ffi::c_int = 14;
pub const GLFW_JOYSTICK_16: std::ffi::c_int = 15;
pub const GLFW_JOYSTICK_LAST: std::ffi::c_int = GLFW_JOYSTICK_16;
pub const GLFW_GAMEPAD_BUTTON_A: std::ffi::c_int = 0;
pub const GLFW_GAMEPAD_BUTTON_B: std::ffi::c_int = 1;
pub const GLFW_GAMEPAD_BUTTON_X: std::ffi::c_int = 2;
pub const GLFW_GAMEPAD_BUTTON_Y: std::ffi::c_int = 3;
pub const GLFW_GAMEPAD_BUTTON_LEFT_BUMPER: std::ffi::c_int = 4;
pub const GLFW_GAMEPAD_BUTTON_RIGHT_BUMPER: std::ffi::c_int = 5;
pub const GLFW_GAMEPAD_BUTTON_BACK: std::ffi::c_int = 6;
pub const GLFW_GAMEPAD_BUTTON_START: std::ffi::c_int = 7;
pub const GLFW_GAMEPAD_BUTTON_GUIDE: std::ffi::c_int = 8;
pub const GLFW_GAMEPAD_BUTTON_LEFT_THUMB: std::ffi::c_int = 9;
pub const GLFW_GAMEPAD_BUTTON_RIGHT_THUMB: std::ffi::c_int = 10;
pub const GLFW_GAMEPAD_BUTTON_DPAD_UP: std::ffi::c_int = 11;
pub const GLFW_GAMEPAD_BUTTON_DPAD_RIGHT: std::ffi::c_int = 12;
pub const GLFW_GAMEPAD_BUTTON_DPAD_DOWN: std::ffi::c_int = 13;
pub const GLFW_GAMEPAD_BUTTON_DPAD_LEFT: std::ffi::c_int = 14;
pub const GLFW_GAMEPAD_BUTTON_LAST: std::ffi::c_int = GLFW_GAMEPAD_BUTTON_DPAD_LEFT;
pub const GLFW_GAMEPAD_BUTTON_CROSS: std::ffi::c_int = GLFW_GAMEPAD_BUTTON_A;
pub const GLFW_GAMEPAD_BUTTON_CIRCLE: std::ffi::c_int = GLFW_GAMEPAD_BUTTON_B;
pub const GLFW_GAMEPAD_BUTTON_SQUARE: std::ffi::c_int = GLFW_GAMEPAD_BUTTON_X;
pub const GLFW_GAMEPAD_BUTTON_TRIANGLE: std::ffi::c_int = GLFW_GAMEPAD_BUTTON_Y;
pub const GLFW_GAMEPAD_AXIS_LEFT_X: std::ffi::c_int = 0;
pub const GLFW_GAMEPAD_AXIS_LEFT_Y: std::ffi::c_int = 1;
pub const GLFW_GAMEPAD_AXIS_RIGHT_X: std::ffi::c_int = 2;
pub const GLFW_GAMEPAD_AXIS_RIGHT_Y: std::ffi::c_int = 3;
pub const GLFW_GAMEPAD_AXIS_LEFT_TRIGGER: std::ffi::c_int = 4;
pub const GLFW_GAMEPAD_AXIS_RIGHT_TRIGGER: std::ffi::c_int = 5;
pub const GLFW_GAMEPAD_AXIS_LAST: std::ffi::c_int = GLFW_GAMEPAD_AXIS_RIGHT_TRIGGER;
pub const GLFW_NO_ERROR: std::ffi::c_int = 0;
pub const GLFW_NOT_INITIALIZED: std::ffi::c_int = 0x00010001;
pub const GLFW_NO_CURRENT_CONTEXT: std::ffi::c_int = 0x00010002;
pub const GLFW_INVALID_ENUM: std::ffi::c_int = 0x00010003;
pub const GLFW_INVALID_VALUE: std::ffi::c_int = 0x00010004;
pub const GLFW_OUT_OF_MEMORY: std::ffi::c_int = 0x00010005;
pub const GLFW_API_UNAVAILABLE: std::ffi::c_int = 0x00010006;
pub const GLFW_VERSION_UNAVAILABLE: std::ffi::c_int = 0x00010007;
pub const GLFW_PLATFORM_ERROR: std::ffi::c_int = 0x00010008;
pub const GLFW_FORMAT_UNAVAILABLE: std::ffi::c_int = 0x00010009;
pub const GLFW_NO_WINDOW_CONTEXT: std::ffi::c_int = 0x0001000A;
pub const GLFW_FOCUSED: std::ffi::c_int = 0x00020001;
pub const GLFW_ICONIFIED: std::ffi::c_int = 0x00020002;
pub const GLFW_RESIZABLE: std::ffi::c_int = 0x00020003;
pub const GLFW_VISIBLE: std::ffi::c_int = 0x00020004;
pub const GLFW_DECORATED: std::ffi::c_int = 0x00020005;
pub const GLFW_AUTO_ICONIFY: std::ffi::c_int = 0x00020006;
pub const GLFW_FLOATING: std::ffi::c_int = 0x00020007;
pub const GLFW_MAXIMIZED: std::ffi::c_int = 0x00020008;
pub const GLFW_CENTER_CURSOR: std::ffi::c_int = 0x00020009;
pub const GLFW_TRANSPARENT_FRAMEBUFFER: std::ffi::c_int = 0x0002000A;
pub const GLFW_HOVERED: std::ffi::c_int = 0x0002000B;
pub const GLFW_FOCUS_ON_SHOW: std::ffi::c_int = 0x0002000C;
pub const GLFW_RED_BITS: std::ffi::c_int = 0x00021001;
pub const GLFW_GREEN_BITS: std::ffi::c_int = 0x00021002;
pub const GLFW_BLUE_BITS: std::ffi::c_int = 0x00021003;
pub const GLFW_ALPHA_BITS: std::ffi::c_int = 0x00021004;
pub const GLFW_DEPTH_BITS: std::ffi::c_int = 0x00021005;
pub const GLFW_STENCIL_BITS: std::ffi::c_int = 0x00021006;
pub const GLFW_ACCUM_RED_BITS: std::ffi::c_int = 0x00021007;
pub const GLFW_ACCUM_GREEN_BITS: std::ffi::c_int = 0x00021008;
pub const GLFW_ACCUM_BLUE_BITS: std::ffi::c_int = 0x00021009;
pub const GLFW_ACCUM_ALPHA_BITS: std::ffi::c_int = 0x0002100A;
pub const GLFW_AUX_BUFFERS: std::ffi::c_int = 0x0002100B;
pub const GLFW_STEREO: std::ffi::c_int = 0x0002100C;
pub const GLFW_SAMPLES: std::ffi::c_int = 0x0002100D;
pub const GLFW_SRGB_CAPABLE: std::ffi::c_int = 0x0002100E;
pub const GLFW_REFRESH_RATE: std::ffi::c_int = 0x0002100F;
pub const GLFW_DOUBLEBUFFER: std::ffi::c_int = 0x00021010;
pub const GLFW_CLIENT_API: std::ffi::c_int = 0x00022001;
pub const GLFW_CONTEXT_VERSION_MAJOR: std::ffi::c_int = 0x00022002;
pub const GLFW_CONTEXT_VERSION_MINOR: std::ffi::c_int = 0x00022003;
pub const GLFW_CONTEXT_REVISION: std::ffi::c_int = 0x00022004;
pub const GLFW_CONTEXT_ROBUSTNESS: std::ffi::c_int = 0x00022005;
pub const GLFW_OPENGL_FORWARD_COMPAT: std::ffi::c_int = 0x00022006;
pub const GLFW_OPENGL_DEBUG_CONTEXT: std::ffi::c_int = 0x00022007;
pub const GLFW_OPENGL_PROFILE: std::ffi::c_int = 0x00022008;
pub const GLFW_CONTEXT_RELEASE_BEHAVIOR: std::ffi::c_int = 0x00022009;
pub const GLFW_CONTEXT_NO_ERROR: std::ffi::c_int = 0x0002200A;
pub const GLFW_CONTEXT_CREATION_API: std::ffi::c_int = 0x0002200B;
pub const GLFW_SCALE_TO_MONITOR: std::ffi::c_int = 0x0002200C;
pub const GLFW_COCOA_RETINA_FRAMEBUFFER: std::ffi::c_int = 0x00023001;
pub const GLFW_COCOA_FRAME_NAME: std::ffi::c_int = 0x00023002;
pub const GLFW_COCOA_GRAPHICS_SWITCHING: std::ffi::c_int = 0x00023003;
pub const GLFW_X11_CLASS_NAME: std::ffi::c_int = 0x00024001;
pub const GLFW_X11_INSTANCE_NAME: std::ffi::c_int = 0x00024002;
pub const GLFW_NO_API: std::ffi::c_int = 0;
pub const GLFW_OPENGL_API: std::ffi::c_int = 0x00030001;
pub const GLFW_OPENGL_ES_API: std::ffi::c_int = 0x00030002;
pub const GLFW_NO_ROBUSTNESS: std::ffi::c_int = 0;
pub const GLFW_NO_RESET_NOTIFICATION: std::ffi::c_int = 0x00031001;
pub const GLFW_LOSE_CONTEXT_ON_RESET: std::ffi::c_int = 0x00031002;
pub const GLFW_OPENGL_ANY_PROFILE: std::ffi::c_int = 0;
pub const GLFW_OPENGL_CORE_PROFILE: std::ffi::c_int = 0x00032001;
pub const GLFW_OPENGL_COMPAT_PROFILE: std::ffi::c_int = 0x00032002;
pub const GLFW_CURSOR: std::ffi::c_int = 0x00033001;
pub const GLFW_STICKY_KEYS: std::ffi::c_int = 0x00033002;
pub const GLFW_STICKY_MOUSE_BUTTONS: std::ffi::c_int = 0x00033003;
pub const GLFW_LOCK_KEY_MODS: std::ffi::c_int = 0x00033004;
pub const GLFW_RAW_MOUSE_MOTION: std::ffi::c_int = 0x00033005;
pub const GLFW_CURSOR_NORMAL: std::ffi::c_int = 0x00034001;
pub const GLFW_CURSOR_HIDDEN: std::ffi::c_int = 0x00034002;
pub const GLFW_CURSOR_DISABLED: std::ffi::c_int = 0x00034003;
pub const GLFW_ANY_RELEASE_BEHAVIOR: std::ffi::c_int = 0;
pub const GLFW_RELEASE_BEHAVIOR_FLUSH: std::ffi::c_int = 0x00035001;
pub const GLFW_RELEASE_BEHAVIOR_NONE: std::ffi::c_int = 0x00035002;
pub const GLFW_NATIVE_CONTEXT_API: std::ffi::c_int = 0x00036001;
pub const GLFW_EGL_CONTEXT_API: std::ffi::c_int = 0x00036002;
pub const GLFW_OSMESA_CONTEXT_API: std::ffi::c_int = 0x00036003;
pub const GLFW_ARROW_CURSOR: std::ffi::c_int = 0x00036001;
pub const GLFW_IBEAM_CURSOR: std::ffi::c_int = 0x00036002;
pub const GLFW_CROSSHAIR_CURSOR: std::ffi::c_int = 0x00036003;
pub const GLFW_HAND_CURSOR: std::ffi::c_int = 0x00036004;
pub const GLFW_HRESIZE_CURSOR: std::ffi::c_int = 0x00036005;
pub const GLFW_VRESIZE_CURSOR: std::ffi::c_int = 0x00036006;
pub const GLFW_CONNECTED: std::ffi::c_int = 0x00040001;
pub const GLFW_DISCONNECTED: std::ffi::c_int = 0x00040002;
pub const GLFW_JOYSTICK_HAT_BUTTONS: std::ffi::c_int = 0x00050001;
pub const GLFW_COCOA_CHDIR_RESOURCES: std::ffi::c_int = 0x00051001;
pub const GLFW_COCOA_MENUBAR: std::ffi::c_int = 0x00051002;
pub const GLFW_DONT_CARE: std::ffi::c_int = -1;
