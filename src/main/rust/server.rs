use std::io::prelude::*;

fn get_or_create_keypair(directory: std::path::PathBuf) -> std::io::Result<Box<nwd::crypto::keypair::Keypair>>
{
	let pub_path = directory.join("x25519.pub");
	let priv_path = directory.join("x25519.priv");
	
	let recreate_keys = || -> std::io::Result<nwd::crypto::keypair::Keypair>
	{
		let result = nwd::crypto::keypair::new();
		writeln!(&mut std::fs::File::create(&pub_path)?, "\
-----BEGIN PUBLIC KEY-----
{}
-----END PUBLIC KEY-----", nwd::utility::hex::Encoder::new(result.public.iter().copied()))?;
		writeln!(&mut std::fs::File::create(&priv_path)?, "\
-----BEGIN PRIVATE KEY-----
{}
-----END PRIVATE KEY-----", nwd::utility::hex::Encoder::new(result.private.iter().copied()))?;
		Ok(result)
	};
	
	let mut keypair = Box::new(nwd::crypto::keypair::Keypair {public: Default::default(), private: Default::default()});
	
	if ! pub_path.exists() || ! priv_path.exists()
	{
		log::info!("Keypair files do not exist in {}, recreating", directory.to_str().unwrap_or("<unreadable file name>"));
		std::fs::create_dir_all(directory);
		*keypair = recreate_keys()?;
	}
	else
	{
		*keypair = nwd::crypto::keypair::Keypair
		{
			public: nwd::shared::keys::read_key(&mut std::fs::File::open(&pub_path)?)?,
			private: nwd::shared::keys::read_key(&mut std::fs::File::open(&priv_path)?)?,
		};
		
		if let None = nwd::crypto::keypair::decrypt(
			&nwd::crypto::keypair::encrypt(&[], &mut [0; nwd::crypto::keypair::seal_length], &keypair.public),
			&mut [], &keypair.public, &keypair.private)
		{
			log::warn!("Keypair files does not match, recreating");
			*keypair = recreate_keys()?;
		}
		else
		{
			log::debug!("Found valid keypair files in {}", directory.to_str().unwrap_or("<unreadable file name>"));
		}
	}
	
	Ok(keypair)
}

fn main() -> std::io::Result<()>
{
	env_logger::builder().filter_level(log::LevelFilter::Debug).target(env_logger::Target::Stderr).init();
	nwd::shared::initialize_bincode();
	
	let server_path = std::path::PathBuf::from(std::env::var_os("HOME").unwrap()).join(".nwd_server").join("keys");
	let keypair = get_or_create_keypair(server_path.as_path().to_path_buf())?;
	
	let port = 25565;
	let mut poll = nwd::shared::session::Poller::new();
	
	let address_v4 = std::net::SocketAddr::from((std::net::Ipv4Addr::LOCALHOST, port));
	let mut socket_v4 = std::sync::Arc::new(std::net::UdpSocket::bind(address_v4)?);
	std::sync::Arc::get_mut(&mut socket_v4).unwrap().set_nonblocking(true)?;
	const event_key_v4: usize = 0;
	poll.add_with_mode(socket_v4.clone(), polling::Event::readable(event_key_v4), polling::PollMode::Level)?;
	
	let address_v6 = std::net::SocketAddr::from((std::net::Ipv6Addr::LOCALHOST, port));
	let mut socket_v6 = std::sync::Arc::new(std::net::UdpSocket::bind(address_v6)?);
	std::sync::Arc::get_mut(&mut socket_v6).unwrap().set_nonblocking(true)?;
	const event_key_v6: usize = 1;
	poll.add_with_mode(socket_v6.clone(), polling::Event::readable(event_key_v6), polling::PollMode::Level)?;
	
	let sessions = std::sync::Arc::new(std::sync::Mutex::new(
		std::collections::BTreeMap::<std::net::SocketAddr, Box<nwd::shared::session::Session>>::new()
	));
	
	let receive = {
		let sessions = sessions.clone();
		move |socket: std::sync::Arc<std::net::UdpSocket>, buffer: &mut [u8]|
	{
		match socket.recv_from(buffer)
		{
			Ok((received_size, source_address)) =>
			{
				let buffer = &mut buffer[.. received_size];
				
				match sessions.lock().unwrap().entry(source_address)
				{
					std::collections::btree_map::Entry::Vacant(entry) =>
					{
						let mut decrypted = [0_u8; 256 + 128];
						
						if let Some(decrypted) = nwd::crypto::keypair::decrypt(&buffer, &mut decrypted, &keypair.public, &keypair.private)
						{
							match nwd::shared::keys::Session_keys::try_new(&decrypted)
							{
								Ok(session_keys) =>
								{
									let session = entry.insert(Box::new(nwd::shared::session::Session::new(source_address, socket.clone(), session_keys)));
									session.send(&nwd::shared::server::Message_content::initial_accept);
									log::info!("New session: {}", source_address);
								}
								Err(nwd::shared::keys::Session_keys_error::no_terminating_0) =>
								{
									log::info!("No terminating \\0 found");
								}
								Err(nwd::shared::keys::Session_keys_error::invalid_version_string) =>
								{
									log::info!("Could not read the version string");
								}
								Err(nwd::shared::keys::Session_keys_error::version_mismatch {client_version}) =>
								{
									log::info!("Client version {} does not match", client_version);
								}
								Err(nwd::shared::keys::Session_keys_error::wrong_data_length) =>
								{
									log::info!("Wrong length of data slice");
								}
							}
						}
						else
						{
							log::info!("Wrong key from: {}", source_address);
						};
					}
					std::collections::btree_map::Entry::Occupied(mut entry) =>
					{
						match entry.get_mut().accept::<nwd::shared::client::Message_content>(buffer)
						{
							Ok(message) =>
							{
								println!("{:?}", message);
							}
							Err(error) =>
							{
								log::info!("{}", error);
							}
						}
					}
				};
			}
			// Err(error) if error.kind() == std::io::ErrorKind::WouldBlock => break,
			Err(error) =>
			{
				log::error!("Socket receive error: {}", error);
				// break;
			}
		}
	}};
	
	std::thread::spawn(move ||
	{
		let mut buffer = [0_u8; 256 + 128];
		let mut events = polling::Events::with_capacity(128.try_into().unwrap());
		
		loop
		{
			events.clear();
			poll.wait(&mut events, None).unwrap();
			
			for event in events.iter()
			{
				if event.readable
				{
					match event.key
					{
						event_key_v4 => receive(socket_v4.clone(), &mut buffer),
						event_key_v6 => receive(socket_v6.clone(), &mut buffer),
						_ =>
						{
							log::warn!("Got event for unexpected token: {:?}", event);
						}
					}
				}
			}
		}
	});
	
	std::thread::spawn({
		let sessions = sessions.clone();
		move ||
	{
		loop
		{
			for session in sessions.lock().unwrap().iter_mut()
			{
				if let Err(error) = session.1.resend()
				{
					log::info!("{}", error);
				}
				
				session.1.send_unreliable(&nwd::shared::server::Message_content::heartbeat);
			}
			
			std::thread::sleep(std::time::Duration::from_millis(1));
		}
	}});
	
	std::io::stdin().read(&mut [0; 0]);
	
	Ok(())
}
