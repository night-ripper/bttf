pub mod openal;
pub mod vorbis;
pub mod wav;

use std::io::Read;
use std::io::BufRead;
use std::io::Write;

pub(crate) const buffer_size: usize = 2520;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct Format
{
	channels: i16,
	bit_depth: i16,
}

impl Format
{
	pub const fn new(channels: i16, bit_depth: i16) -> Self
	{
		Self {channels, bit_depth}
	}
	
	pub const fn channels(self) -> i16 {self.channels}
	pub const fn bit_depth(self) -> i16 {self.bit_depth}
	pub const fn byte_depth(self) -> i16 {self.bit_depth / 8}
	pub const fn frame_size(self) -> i16 {self.byte_depth() * self.channels()}
	
	pub const mono_8: Format = Format::new(1, 8);
	pub const mono_16: Format = Format::new(1, 16);
	pub const stereo_8: Format = Format::new(2, 8);
	pub const stereo_16: Format = Format::new(2, 16);
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct Sound_info
{
	format: Format,
	frequency: i32,
}

impl Sound_info
{
	pub const fn new(format: Format, frequency: i32) -> Self
	{
		Self {format, frequency}
	}
	
	pub const fn format(self) -> Format {self.format}
	pub const fn frequency(self) -> i32 {self.frequency}
}

impl std::ops::Deref for Sound_info
{
	type Target = Format;
	
	fn deref(&self) -> &Self::Target
	{
		&self.format
	}
}

pub(crate) struct Streaming_buffer
{
	pub begin: u16,
	pub end: u16,
	pub data: [u8; buffer_size],
}

impl Streaming_buffer
{
	pub fn new() -> Self
	{
		Self
		{
			begin: 0,
			end: 0,
			data: [0; buffer_size],
		}
	}
	
	pub fn consume(&mut self, bytes: usize)
	{
		self.begin += bytes as u16;
		
		if self.begin == self.end
		{
			self.begin = 0;
			self.end = 0;
		}
	}
}

pub fn buf_read(reader: &mut impl std::io::BufRead, buf: &mut [u8]) -> std::io::Result<usize>
{
	let mut result: usize = 0;
	
	loop
	{
		let diff = buf.len() - result;
		
		if diff == 0
		{
			break;
		}
		
		let data = reader.fill_buf()?;
		
		if data.len() == 0
		{
			break;
		}
		
		let data_transferred = std::cmp::min(diff, data.len());
		buf[result ..][.. data_transferred].copy_from_slice(&data[.. data_transferred]);
		reader.consume(data_transferred);
		result += data_transferred;
	}
	
	Ok(result)
}

pub trait Streaming_sound: std::io::BufRead + std::ops::Deref<Target = Sound_info>
{
	fn has_data_left(&mut self) -> std::io::Result<bool>
	{
		self.fill_buf().map(|b| ! b.is_empty())
	}
}

/// Duplicates channels
pub(crate) fn channels_function_1_2(source: &mut [u8], size: usize)
{
	for i in (1 .. source.len() / 2 / size).rev()
	{
		let (source, target) = source.split_at_mut(i * 2 * size);
		let source = &source[i * size ..][.. size];
		target[.. size].copy_from_slice(source);
		target[size .. 2 * size].copy_from_slice(source);
	}
	
	let (source, target) = source.split_at_mut(size);
	target[.. size].copy_from_slice(source);
}

#[test]
fn test_channels_function_1_2()
{
	{
		let mut arr = [1,2,3,0,0,0_u8];
		channels_function_1_2(&mut arr, 1);
		assert_eq!(&[1,1,2,2,3,3_u8], &arr);
	}
	
	{
		let mut arr = [1,2,3,0,0,0_i16];
		unsafe
		{
			let arr = std::slice::from_raw_parts_mut(arr.as_mut_ptr() as *mut u8, arr.len() * 2);
			channels_function_1_2(arr, 2);
		}
		assert_eq!(&[1,1,2,2,3,3_i16], &arr);
	}
}

/// Selects the first (left) channel
pub(crate) fn channels_function_2_1(source: &mut [u8], size: usize)
{
	for i in 1 .. source.len() / 2 / size
	{
		let (target, source) = source.split_at_mut(i * 2 * size);
		target[i * size .. (i + 1) * size].copy_from_slice(&source[.. size]);
	}
}

#[test]
fn test_channels_function_2_1()
{
	{
		let mut arr = [1,2,3,4,5,6_u8];
		channels_function_2_1(&mut arr, 1);
		assert_eq!(&[1,3,5_u8], &arr[.. 3]);
	}
	
	{
		let mut arr = [1,2,3,4,5,6_i16];
		unsafe
		{
			let arr = std::slice::from_raw_parts_mut(arr.as_mut_ptr() as *mut u8, arr.len() * 2);
			channels_function_2_1(arr, 2);
		}
		assert_eq!(&[1,3,5_i16], &arr[.. 3]);
	}
}

pub fn split_channels<LW, RW>(byte_size: i16, mut source: impl BufRead, mut lhs: LW, mut rhs: RW)
-> std::io::Result<(LW, RW)>
where LW: Write, RW: Write
{
	while std::io::copy(&mut source.by_ref().take(byte_size as u64), &mut lhs)? == byte_size as u64
		&& std::io::copy(&mut source.by_ref().take(byte_size as u64), &mut rhs)? == byte_size as u64
	{
	}
	
	Ok((lhs, rhs))
}

#[test]
fn test_split_channels()
{
	let source = [1,2,3,4,5,6,7,8_u8];
	let (lhs, rhs) = split_channels(1, &source[..], Vec::with_capacity(4), Vec::with_capacity(4)).unwrap();
	assert_eq!(&[1,3,5,7], &lhs[..]);
	assert_eq!(&[2,4,6,8], &rhs[..]);
}

pub fn from_file(format: Format, source_path: &std::path::Path)
-> std::io::Result<Box<dyn Streaming_sound>>
{
	let Some(extension) = source_path.extension() else
	{
		return Err(std::io::Error::new(std::io::ErrorKind::InvalidInput, "no extension found"));
	};
	let Some(extension) = extension.to_str() else
	{
		return Err(std::io::Error::new(std::io::ErrorKind::InvalidInput, "file extension is not printable"));
	};
	
	Ok(match extension
	{
		"ogg" => Box::new(vorbis::from_file(format, std::fs::File::open(source_path)?)?),
		"wav" => Box::new(wav::from_file(format, std::fs::File::open(source_path)?)?),
		_ => return Err(std::io::Error::new(std::io::ErrorKind::Unsupported,
			format!("extension {} is not supported", extension)
		)),
	})
}
