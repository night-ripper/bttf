#[derive(Clone, Copy, Eq)]
pub struct Cyclic_angle
{
	value: u32,
}

impl Cyclic_angle
{
	pub const MIN: Self = Self::from_underlying(u32::MIN);
	pub const MAX: Self = Self::from_underlying(u32::MAX);
	pub const EPSILON: Self = Self::from_underlying(1);
	
	pub const fn underlying_value(self) -> u32
	{
		self.value
	}
	
	// std::pow(2, std::log2(2 * std::numbers::pi_v<long double>) - std::numeric_limits<std::uint32_t>::digits)
	const minimum_value: f32 = 1.46291807926715968188249154e-09;
	const inv_minimum_value: f32 = 1.0 / Self::minimum_value;
	
	pub const fn from_underlying(value: u32) -> Self
	{
		Cyclic_angle {value}
	}
	
	pub fn round_from(mut value: f32) -> Self
	{
		value %= 2.0 * std::f32::consts::PI;
		value -= std::f32::consts::PI;
		value *= Self::inv_minimum_value;
		value = value.round();
		return Self::from_underlying((value as i32) as u32 ^ (1 << 31) as u32);
	}
}

impl From<Cyclic_angle> for f32
{
	fn from(this: Cyclic_angle) -> f32
	{
		this.value as f32 * Cyclic_angle::minimum_value
	}
}

impl std::cmp::Ord for Cyclic_angle
{
	fn cmp(&self, rhs: &Self) -> std::cmp::Ordering
	{
		self.value.cmp(&rhs.value)
	}
}

impl std::cmp::PartialOrd for Cyclic_angle
{
	fn partial_cmp(&self, rhs: &Self) -> Option<std::cmp::Ordering>
	{
		Some(self.cmp(rhs))
	}
}

impl std::cmp::PartialEq for Cyclic_angle
{
	fn eq(&self, rhs: &Self) -> bool
	{
		self.cmp(rhs) == std::cmp::Ordering::Equal
	}
}

impl std::ops::Neg for Cyclic_angle
{
	type Output = Self;
	
	fn neg(mut self) -> Self::Output
	{
		self.value = u32::MAX - self.value;
		return self;
	}
}

impl std::ops::AddAssign for Cyclic_angle
{
	fn add_assign(&mut self, rhs: Self)
	{
		self.value += rhs.value;
	}
}

impl std::ops::Add<Cyclic_angle> for Cyclic_angle
{
	type Output = Self;
	
	fn add(mut self, rhs: Self) -> Self::Output
	{
		self += rhs;
		return self;
	}
}

impl std::ops::SubAssign for Cyclic_angle
{
	fn sub_assign(&mut self, rhs: Self)
	{
		self.value -= rhs.value;
	}
}

impl std::ops::Sub<Cyclic_angle> for Cyclic_angle
{
	type Output = Self;
	
	fn sub(mut self, rhs: Self) -> Self::Output
	{
		self -= rhs;
		return self;
	}
}

impl std::ops::Mul<f32> for Cyclic_angle
{
	type Output = Self;
	
	fn mul(self, rhs: f32) -> Self::Output
	{
		Self::round_from(self.value as f32 * rhs)
	}
}

impl std::ops::MulAssign<f32> for Cyclic_angle
{
	fn mul_assign(&mut self, rhs: f32)
	{
		*self = *self * rhs
	}
}

impl std::ops::Div<f32> for Cyclic_angle
{
	type Output = Self;
	
	fn div(self, rhs: f32) -> Self::Output
	{
		Self::round_from(self.value as f32 / rhs)
	}
}

impl std::ops::DivAssign<f32> for Cyclic_angle
{
	fn div_assign(&mut self, rhs: f32)
	{
		*self = *self / rhs
	}
}
