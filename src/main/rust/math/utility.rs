type Vec<Type, const Size: usize> = [Type; Size];
type Vec3<Type> = Vec<Type, 3>;
type Vec3f = Vec3<f32>;
type Vec3d = Vec3<f64>;
type Vec4<Type> = Vec<Type, 4>;

pub fn hypot<const Size: usize>(values: [f32; Size]) -> f32
{
	let mut max = values.iter().fold(0.0 as f64, |acc, v| acc.max(v.abs() as f64));
	
	if max != 0.0
	{
		let inv_max = 1.0 / max;
		max *= (0 .. Size).fold(1.0, |acc, i| acc + (inv_max * values[i] as f64).powi(2)).sqrt();
	}
	
	return max as f32;
}

#[test]
fn test_hypot()
{
	assert!(3.7416573867739413 - hypot([1.0, 2.0, 3.0]) < 1.5 * f32::EPSILON);
	assert!(3.7416573867739413 - hypot([3.0, 2.0, 1.0]) < 1.5 * f32::EPSILON);
	assert!(3741657386773941.5 - hypot([3.0_e15, 2.0_e15, 1.0_e15]) < 1.5 * f32::EPSILON);
}

pub fn normalize<const Size: usize>(mut values: [f32; Size]) -> [f32; Size]
{
	let inverse = 1.0 / hypot(values) as f64;
	
	for v in &mut values
	{
		*v = (*v as f64 * inverse) as f32;
	}
	
	return values;
}

pub fn cross<Type>(lhs: Type, rhs: Type) -> Type where
	Type: std::ops::Index<usize> + From<[<<<Type as std::ops::Index<usize>>::Output as std::ops::Mul>::Output as std::ops::Sub>::Output; 3]>,
	<Type as std::ops::Index<usize>>::Output: Copy + std::ops::Sub + std::ops::Mul,
	<<Type as std::ops::Index<usize>>::Output as std::ops::Mul>::Output: std::ops::Sub,
{
	Type::from([
		lhs[1] * rhs[2] - lhs[2] * rhs[1],
		lhs[2] * rhs[0] - lhs[0] * rhs[2],
		lhs[0] * rhs[1] - lhs[1] * rhs[0],
	])
}

#[test]
fn test_cross()
{
	assert_eq!([-3,6,-3], cross([1,2,3], [4,5,6]));
}

pub fn dot_array<Type, const Size: usize>(lhs: &[Type; Size], rhs: &[Type; Size]) -> Type where
	Type: Copy + num_traits::NumOps + num_traits::NumCast
{
	let mut result = Type::from(0).unwrap();
	
	for i in 0 .. Size
	{
		result = result + lhs[i] * rhs[i];
	}
	
	return result;
}

pub fn dot_sqr_array<Type, const Size: usize>(value: &[Type; Size]) -> Type where
	Type: Copy + num_traits::NumOps + num_traits::NumCast
{
	dot_array(value, value)
}

/// @return The first 3 solutions and their divisor
/// The real result is obtained by division of each of the 3 results
/// by the 4-th one.
/// The divisor always has non-negative value
pub fn solve<Type>(mut columns: [Vec3<Type>; 4]) -> Vec4<Type>
where Type: Copy + num_traits::float::Float,
{
	// fn determinant_3() -> Type
	let determinant_3 = |columns: &[Vec3<Type>; 4]| -> Type
	{
		(columns[0][0]) * ((columns[1][1]) * (columns[2][2]) - (columns[2][1]) * (columns[1][2]))
			+ (columns[1][0]) * ((columns[2][1]) * (columns[0][2]) - (columns[0][1]) * (columns[2][2]))
			+ (columns[2][0]) * ((columns[0][1]) * (columns[1][2]) - (columns[1][1]) * (columns[0][2]))
	};
	
	let mut result = [Type::from(0).unwrap(); 4];
	let divisor = determinant_3(&columns);
	
	for i in 0 .. 3
	{
		columns.swap(i, 3);
		result[i] = determinant_3(&columns) * Type::from(1).unwrap().copysign(divisor);
		
		// Return to the original state
		columns.swap(i, 3);
	}
	
	result[3] = divisor.copysign(Type::from(1).unwrap());
	
	return result;
}

#[test]
fn test_solve_matrix()
{
	let matrix = [
		[4.0, -2.0, 1.0],
		[-3.0, 1.0, -1.0],
		[1.0, -3.0, 2.0],
		[-8.0, -4.0, 2.0],
	];
	
	let [mut x, mut y, mut z, div] = solve(matrix);
	
	assert_eq!(6.0, div);
	x /= div;
	y /= div;
	z /= div;
	
	assert_eq!(matrix[3][0], x * matrix[0][0] + y * matrix[1][0] + z * matrix[2][0]);
	assert_eq!(matrix[3][1], x * matrix[0][1] + y * matrix[1][1] + z * matrix[2][1]);
	assert_eq!(matrix[3][2], x * matrix[0][2] + y * matrix[1][2] + z * matrix[2][2]);
}
