use crate::audio;

#[repr(C)]
#[derive(Clone, Copy)]
struct ov_callbacks
{
	read_func: Option<extern "C" fn(ptr: *mut std::ffi::c_void, size: usize, nmemb: usize, datasource: *mut std::ffi::c_void) -> usize>,
	seek_func: Option<extern "C" fn(datasource: *mut std::ffi::c_void, offset: i64, whence: std::ffi::c_int) -> std::ffi::c_int>,
	close_func: Option<extern "C" fn(datasource: *mut std::ffi::c_void) -> std::ffi::c_int>,
	tell_func: Option<extern "C" fn(datasource: *mut std::ffi::c_void) -> std::ffi::c_long>,
}

#[repr(C)]
struct vorbis_info
{
	version: std::ffi::c_int,
	channels: std::ffi::c_int,
	rate: std::ffi::c_long,
	
	bitrate_upper: std::ffi::c_long,
	bitrate_nominal: std::ffi::c_long,
	bitrate_lower: std::ffi::c_long,
	bitrate_window: std::ffi::c_long,
	
	codec_setup: *mut std::ffi::c_void,
}

/*
#[repr(C)]
struct vorbis_dsp_state
{
	analysisp: std::ffi::c_int,
	vi: *mut vorbis_info,
	
	pcm: *mut *mut std::ffi::c_float,
	pcmret: *mut *mut std::ffi::c_float,
	
	pcm_storage: std::ffi::c_int,
	pcm_current: std::ffi::c_int,
	pcm_returned: std::ffi::c_int,

	preextrapolate: std::ffi::c_int,
	eofflag: std::ffi::c_int,
	
	lW: std::ffi::c_long,
	W: std::ffi::c_long,
	nW: std::ffi::c_long,
	centerW: std::ffi::c_long,

	granulepos: i64,
	sequence: i64,
	glue_bits: i64,
	time_bits: i64,
	floor_bits: i64,
	res_bits: i64,
	
	backend_state: *mut std::ffi::c_void,
}

#[repr(C)]
struct oggpack_buffer
{
	endbyte: std::ffi::c_long,
	endbit: std::ffi::c_int,
	
	buffer: *mut std::ffi::c_uchar,
	ptr: *mut std::ffi::c_uchar,
	storage: std::ffi::c_long,
}

#[repr(C)]
struct vorbis_block
{
	pcm: *mut *mut std::ffi::c_float,
	opb: oggpack_buffer,
	lW: std::ffi::c_long,
	W: std::ffi::c_long,
	nW: std::ffi::c_long,
	
	pcmend: std::ffi::c_int,
	mode: std::ffi::c_int,
	
	eofflag: std::ffi::c_int,
	granulepos: i64,
	sequence: i64,
	vd: *mut vorbis_dsp_state,
	
	localstore: *mut std::ffi::c_void,
	localtop: std::ffi::c_long,
	localalloc: std::ffi::c_long,
	totaluse: std::ffi::c_long,
	reap: *mut std::ffi::c_void,
	
	glue_bits: std::ffi::c_long,
	time_bits: std::ffi::c_long,
	floor_bits: std::ffi::c_long,
	res_bits: std::ffi::c_long,
	
	internal: *mut std::ffi::c_void,
}

#[repr(C)]
struct alloc_chain
{
	ptr: *mut std::ffi::c_void,
	next: *mut alloc_chain,
}

#[repr(C)]
struct vorbis_comment
{
	user_comments: *mut *mut std::ffi::c_char,
	comment_lengths: *mut std::ffi::c_int,
	comments: std::ffi::c_int,
	vendor: *mut *mut std::ffi::c_char,
}

#[repr(C)]
struct ogg_sync_state
{
	data: *mut std::ffi::c_uchar,
	storage: std::ffi::c_int,
	fill: std::ffi::c_int,
	returned: std::ffi::c_int,
	
	unsynced: std::ffi::c_int,
	headerbytes: std::ffi::c_int,
	bodybytes: std::ffi::c_int,
}

#[repr(C)]
struct ogg_stream_state
{
	body_data: *mut std::ffi::c_uchar,
	body_storage: std::ffi::c_long,
	body_fill: std::ffi::c_long,
	body_returned: std::ffi::c_long,
	
	lacing_vals: *mut std::ffi::c_int,
	granule_vals: *mut i64,
	
	lacing_storage: std::ffi::c_long,
	lacing_fill: std::ffi::c_long,
	lacing_packet: std::ffi::c_long,
	lacing_returned: std::ffi::c_long,
	
	

  unsigned char    header[282];      /* working space for header encode */
  int              header_fill;

  int     e_o_s;          /* set when we have buffered the last packet in the
                             logical bitstream */
  int     b_o_s;          /* set after we've written the initial page
                             of a logical bitstream */
  long    serialno;
  long    pageno;
  ogg_int64_t  packetno;  /* sequence number for decode; the framing
                             knows where there's a hole in the data,
                             but we need coupling so that the codec
                             (which is in a separate abstraction
                             layer) also knows about the gap */
  ogg_int64_t   granulepos;

}

#[repr(C)]
struct OggVorbis_File
{
	datasource: *mut std::ffi::c_void,
	seekable: std::ffi::c_int,
	offset: i64,
	end: i64,
	oy: ogg_sync_state,
	
	links: std::ffi::c_int,
	offsets: *mut i64,
	dataoffsets: *mut i64,
	serialnos: *mut std::ffi::c_long,
	pcmlengths: *mut i64,
	
	vi: *mut vorbis_info,
	vc: *mut vorbis_comment,
	
	pcm_offset: i64,
	ready_state: std::ffi::c_int,
	current_serialno: std::ffi::c_long,
	current_link: std::ffi::c_int,
	
	bittrack: std::ffi::c_double,
	samptrack: std::ffi::c_double,
	
	os
	
	vd
	vb
	
	callbacks: ov_callbacks,
}
*/

#[repr(C, align(16))]
struct OggVorbis_File
{
	data: [u8; 944],
}

#[link(name = "vorbisfile")]
extern "C"
{
	fn ov_clear(vf: *mut OggVorbis_File) -> std::ffi::c_int;
	fn ov_open_callbacks(datasource: *mut std::ffi::c_void, vf: *mut OggVorbis_File,
		initial: *const std::ffi::c_char, ibytes: std::ffi::c_long, callbacks: ov_callbacks) -> std::ffi::c_int;
	fn ov_info(vf: *mut OggVorbis_File, link: std::ffi::c_int) -> *mut vorbis_info;
	fn ov_read(vf: *mut OggVorbis_File, buffer: *mut std::ffi::c_char,
		length: std::ffi::c_int, bigendianp: std::ffi::c_int,
		word: std::ffi::c_int, sgned: std::ffi::c_int, bitstream: *mut std::ffi::c_int
	) -> std::ffi::c_long;
	fn ov_pcm_total(vf: *mut OggVorbis_File, i: std::ffi::c_int) -> i64;
	fn ov_pcm_seek(vf: *mut OggVorbis_File, pos: i64) -> std::ffi::c_int;
	fn ov_pcm_tell(vf: *mut OggVorbis_File) -> i64;
}

impl Drop for OggVorbis_File
{
	fn drop(&mut self)
	{
		unsafe {ov_clear(self)};
	}
}

const OV_FALSE: std::ffi::c_int = -1;
const OV_EOF: std::ffi::c_int = -2;
const OV_HOLE: std::ffi::c_int = -3;

const OV_EREAD: std::ffi::c_int = -128;
const OV_EFAULT: std::ffi::c_int = -129;
const OV_EIMPL: std::ffi::c_int = -130;
const OV_EINVAL: std::ffi::c_int = -131;
const OV_ENOTVORBIS: std::ffi::c_int = -132;
const OV_EBADHEADER: std::ffi::c_int = -133;
const OV_EVERSION: std::ffi::c_int = -134;
const OV_ENOTAUDIO: std::ffi::c_int = -135;
const OV_EBADPACKET: std::ffi::c_int = -136;
const OV_EBADLINK: std::ffi::c_int = -137;
const OV_ENOSEEK: std::ffi::c_int = -138;

fn check_error(return_code: std::ffi::c_int) -> std::io::Result<()>
{
	match return_code
	{
		0 => Ok(()),
		code => Err(std::io::Error::new(std::io::ErrorKind::Other, format!(
			"Vorbis error: an unknown error occured, error code: {}", code
		))),
	}
}

impl OggVorbis_File
{
	fn new() -> OggVorbis_File
	{
		OggVorbis_File {data: [0; 944]}
	}
	
	fn read_info(&mut self, bit_depth: i16) -> std::io::Result<audio::Sound_info>
	{
		if bit_depth != 8 && bit_depth != 16
		{
			return Err(std::io::Error::new(std::io::ErrorKind::Unsupported, format!("Vorbis error: unsupported bit depth {}", bit_depth)));
		}
		
		let Some(info) = (unsafe {ov_info(self, -1).as_ref()}) else
		{
			return Err(std::io::Error::new(std::io::ErrorKind::InvalidData, "Vorbis error: input does not appear to be an Ogg bit stream"));
		};
		
		Ok(audio::Sound_info::new(audio::Format::new(info.channels as i16, bit_depth), info.rate as i32))
	}
	
	fn read_once(&mut self, bit_depth: i16, target: &mut [u8]) -> std::io::Result<usize>
	{
		let word_size = bit_depth / 8;
		let is_signed = word_size - 1;
		let endianness = crate::utility::endian::Endianness::native == crate::utility::endian::Endianness::big;
		
		let result = unsafe {ov_read(self, target.as_mut_ptr() as *mut i8, target.len() as i32, endianness as i32, word_size as i32, is_signed as i32, std::ptr::null_mut())};
		
		match result as std::ffi::c_int
		{
			OV_HOLE => Err(std::io::Error::new(std::io::ErrorKind::InvalidData,
				"Vorbis error: one of: garbage between pages, loss of sync followed by recapture, or a corrupt page"
			)),
			OV_EINVAL => Err(std::io::Error::new(std::io::ErrorKind::InvalidData,
				"Vorbis error: the initial file headers could not be read or are corrupt, or the initial open call for vorbis file failed"
			)),
			OV_EBADLINK => Err(std::io::Error::new(std::io::ErrorKind::InvalidData,
				"Vorbis error: an invalid stream section was supplied to libvorbisfile, or the requested link is corrupt"
			)),
			std::ffi::c_int::MIN ..= -1 => Err(std::io::Error::new(std::io::ErrorKind::Other, format!(
				"Vorbis error: an unknown error occured, error code: {}", result
			))),
			result => Ok(result as usize),
		}
	}
}

extern "C" fn read_func<Type: std::io::Read + std::io::Seek + 'static>(
	ptr: *mut std::ffi::c_void, size: usize, nmemb: usize, datasource: *mut std::ffi::c_void) -> usize
{
	unsafe
	{
		let mut output = std::slice::from_raw_parts_mut(ptr.cast::<u8>(), size * nmemb);
		return datasource.cast::<Type>().as_mut().unwrap().read(&mut output).unwrap();
	};
}

extern "C" fn seek_func<Type: std::io::Read + std::io::Seek + 'static>(
	datasource: *mut std::ffi::c_void, offset: i64, whence: std::ffi::c_int) -> std::ffi::c_int
{
	let whence = match whence
	{
		0 => std::io::SeekFrom::Start(offset as u64),
		1 => std::io::SeekFrom::Current(offset),
		2 => std::io::SeekFrom::End(offset),
		_ => unreachable!("Vorbis error: invalid Seek enum"),
	};
	
	return match unsafe {datasource.cast::<Type>().as_mut().unwrap().seek(whence)}
	{
		Ok(_) => 0,
		Err(_) => -1,
	};
}

extern "C" fn tell_func<Type: std::io::Read + std::io::Seek + 'static>(
	datasource: *mut std::ffi::c_void) -> std::ffi::c_long
{
	match unsafe {datasource.cast::<Type>().as_mut()}.unwrap().seek(std::io::SeekFrom::Current(0))
	{
		Ok(v) => v as std::ffi::c_long,
		Err(_) => -1,
	}
}

pub struct Streaming_vorbis
{
	sound_info: audio::Sound_info,
	source_channels: i16,
	source_data_size: Option<u64>,
	source: Box<dyn std::any::Any>,
	vf: OggVorbis_File,
	buffer: audio::Streaming_buffer,
}

impl Streaming_vorbis
{
	fn new<Type: std::io::Read + std::io::Seek + 'static>(format: audio::Format, source: Type, callbacks: ov_callbacks)
	-> std::io::Result<Self>
	{
		let source = Box::new(source);
		let source = Box::into_raw(source);
		let mut vf = OggVorbis_File::new();
		
		match unsafe {ov_open_callbacks(source.cast::<std::ffi::c_void>(), &mut vf, std::ptr::null_mut(), 0, callbacks)}
		{
			OV_EREAD => Err(std::io::Error::new(std::io::ErrorKind::Other, "Vorbis error: a read from media returned an error")),
			OV_ENOTVORBIS => Err(std::io::Error::new(std::io::ErrorKind::InvalidData, "Vorbis error: bitstream does not contain any Vorbis data")),
			OV_EVERSION => Err(std::io::Error::new(std::io::ErrorKind::InvalidData, "Vorbis error: vorbis version mismatch")),
			OV_EBADHEADER => Err(std::io::Error::new(std::io::ErrorKind::InvalidData, "Vorbis error: invalid Vorbis bitstream header")),
			OV_EFAULT => Err(std::io::Error::new(std::io::ErrorKind::Other, "Vorbis error: internal logic fault; indicates a bug or heap/stack corruption")),
			_ => Ok(()),
		}?;
		
		let mut sound_info = vf.read_info(format.bit_depth())?;
		let source_channels = sound_info.channels();
		sound_info = audio::Sound_info::new(format, sound_info.frequency());
		
		let mut source_data_size = None;
		
		if callbacks.tell_func.is_some()
		{
			let data_size = unsafe {ov_pcm_total(&mut vf, -1)};
			if data_size == OV_EINVAL as i64
			{
				return Err(std::io::Error::new(std::io::ErrorKind::Other, "Vorbis error: the requested bitstream does not exist or the bitstream is unseekable"));
			}
			
			if data_size >= 0
			{
				source_data_size = Some(data_size as u64 * source_channels as u64 * format.byte_depth() as u64);
			}
		}
		
		Ok(Self
		{
			sound_info,
			source_channels,
			source_data_size,
			source: unsafe {Box::from_raw(source)},
			vf,
			buffer: audio::Streaming_buffer::new(),
		})
	}
}

impl std::ops::Deref for Streaming_vorbis
{
	type Target = audio::Sound_info;
	
	fn deref(&self) -> &Self::Target
	{
		&self.sound_info
	}
}

impl audio::Streaming_sound for Streaming_vorbis
{
}

impl std::io::Read for Streaming_vorbis
{
	fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize>
	{
		audio::buf_read(self, buf)
	}
}

impl std::io::BufRead for Streaming_vorbis
{
	fn consume(&mut self, bytes: usize)
	{
		self.buffer.consume(bytes);
	}
	
	fn fill_buf(&mut self) -> std::io::Result<&[u8]>
	{
		if self.buffer.begin >= self.buffer.end
		{
			let mut max_data_length = self.buffer.data.len();
			
			if self.channels() > self.source_channels as i16
			{
				max_data_length /= self.channels() as usize;
				max_data_length *= self.source_channels as usize;
			}
			
			let want_to_read = max_data_length;
			let mut data_length = self.vf.read_once(self.bit_depth(),
				&mut self.buffer.data[.. want_to_read]
			)?;
			
			if data_length == 0
			{
				return Ok(&[]);
			}
			
			////////////////////////////////////////////////////////////////////////
			
			let byte_depth = self.byte_depth() as usize;
			
			if self.source_channels == 1 && self.channels() == 2
			{
				data_length *= 2;
				audio::channels_function_1_2(&mut self.buffer.data[.. data_length], byte_depth);
			}
			else if self.source_channels == 2 && self.channels() == 1
			{
				audio::channels_function_2_1(&mut self.buffer.data[.. data_length], byte_depth);
				data_length /= 2;
			}
			
			self.buffer.end = data_length as u16;
		}
		
		Ok(&self.buffer.data[self.buffer.begin as usize .. self.buffer.end as usize])
	}
}

impl std::io::Seek for Streaming_vorbis
{
	fn seek(&mut self, pos: std::io::SeekFrom) -> std::io::Result<u64>
	{
		let in_coeff = self.source_channels as u64;
		let out_coeff = self.channels() as u64;
		
		let current_pos = unsafe {ov_pcm_tell(&mut self.vf)};
		if current_pos == OV_EINVAL as i64
		{
			return Err(std::io::Error::new(std::io::ErrorKind::Other, "Vorbis error: the requested bitstream does not exist or the bitstream is unseekable"));
		}
		
		let current_pos = current_pos as u64
			* self.source_channels as u64 * self.byte_depth() as u64
			* out_coeff / in_coeff + self.buffer.begin as u64 - self.buffer.end as u64
		;
		
		let abs_pos = match pos
		{
			std::io::SeekFrom::Start(s) => s,
			std::io::SeekFrom::Current(c) => (current_pos as i64 + c) as u64,
			std::io::SeekFrom::End(e) => match self.source_data_size
			{
				None => return Err(std::io::Error::new(std::io::ErrorKind::Unsupported, "Vorbis error: stream is not seekable")),
				Some(v) => ((v as u64 * out_coeff / in_coeff) as i64 + e) as u64,
			},
		};
		
		let diff = abs_pos as i64 - current_pos as i64;
		
		if 0 <= self.buffer.begin as i64 + diff
			&& self.buffer.begin as i64 + diff <= self.buffer.end as i64
		{
			self.buffer.begin = (self.buffer.begin as i64 + diff) as u16;
		}
		else
		{
			let in_offset = abs_pos * in_coeff / out_coeff / self.source_channels as u64 / self.byte_depth() as u64;
			
			match unsafe {ov_pcm_seek(&mut self.vf, in_offset as i64)}
			{
				0 => Ok(()),
				OV_ENOSEEK => Err(std::io::Error::new(std::io::ErrorKind::Other,
					"Vorbis error: bitstream is not seekable"
				)),
				OV_EINVAL => Err(std::io::Error::new(std::io::ErrorKind::InvalidInput,
					"Vorbis error: invalid argument value; possibly called with an OggVorbis_File structure that isn't open"
				)),
				OV_EREAD => Err(std::io::Error::new(std::io::ErrorKind::Other,
					"Vorbis error: a read from media returned an error"
				)),
				OV_EFAULT => Err(std::io::Error::new(std::io::ErrorKind::Other,
					"Vorbis error: internal logic fault; indicates a bug or heap/stack corruption"
				)),
				OV_EBADLINK => Err(std::io::Error::new(std::io::ErrorKind::InvalidData,
					"Vorbis error: invalid stream section supplied to libvorbisfile, or the requested link is corrupt"
				)),
				error_code => Err(std::io::Error::new(std::io::ErrorKind::Other, format!(
					"Vorbis error: an unknown error occured, error code: {}", error_code
				))),
			}?;
			
			self.buffer.begin = (abs_pos - in_offset * out_coeff
				* self.source_channels as u64 * self.byte_depth() as u64 / in_coeff
			) as u16;
			
			self.buffer.end = 0;
		}
		
		Ok(abs_pos)
	}
}

pub fn from_file<Type: std::io::Read + std::io::Seek + 'static>(format: audio::Format, source: Type)
-> std::io::Result<Streaming_vorbis>
{
	Streaming_vorbis::new(format, source, ov_callbacks
	{
		read_func: Some(read_func::<Type>),
		seek_func: Some(seek_func::<Type>),
		close_func: None,
		tell_func: Some(tell_func::<Type>),
	})
}
