use crate::audio;

mod efx
{
pub const ALC_EFX_MAJOR_VERSION: i32 = 0x20001;
pub const ALC_EFX_MINOR_VERSION: i32 = 0x20002;
pub const ALC_MAX_AUXILIARY_SENDS: i32 = 0x20003;

// Listener properties
pub const AL_METERS_PER_UNIT: i32 = 0x20004;

// Source properties
pub const AL_DIRECT_FILTER: i32 = 0x20005;
pub const AL_AUXILIARY_SEND_FILTER: i32 = 0x20006;
pub const AL_AIR_ABSORPTION_FACTOR: i32 = 0x20007;
pub const AL_ROOM_ROLLOFF_FACTOR: i32 = 0x20008;
pub const AL_CONE_OUTER_GAINHF: i32 = 0x20009;
pub const AL_DIRECT_FILTER_GAINHF_AUTO: i32 = 0x2000A;
pub const AL_AUXILIARY_SEND_FILTER_GAIN_AUTO: i32 = 0x2000B;
pub const AL_AUXILIARY_SEND_FILTER_GAINHF_AUTO: i32 = 0x2000C;

// Effect properties
// Reverb effect parameters
pub const AL_REVERB_DENSITY: i32 = 0x0001;
pub const AL_REVERB_DIFFUSION: i32 = 0x0002;
pub const AL_REVERB_GAIN: i32 = 0x0003;
pub const AL_REVERB_GAINHF: i32 = 0x0004;
pub const AL_REVERB_DECAY_TIME: i32 = 0x0005;
pub const AL_REVERB_DECAY_HFRATIO: i32 = 0x0006;
pub const AL_REVERB_REFLECTIONS_GAIN: i32 = 0x0007;
pub const AL_REVERB_REFLECTIONS_DELAY: i32 = 0x0008;
pub const AL_REVERB_LATE_REVERB_GAIN: i32 = 0x0009;
pub const AL_REVERB_LATE_REVERB_DELAY: i32 = 0x000A;
pub const AL_REVERB_AIR_ABSORPTION_GAINHF: i32 = 0x000B;
pub const AL_REVERB_ROOM_ROLLOFF_FACTOR: i32 = 0x000C;
pub const AL_REVERB_DECAY_HFLIMIT: i32 = 0x000D;

// EAX Reverb effect parameters
pub const AL_EAXREVERB_DENSITY: i32 = 0x0001;
pub const AL_EAXREVERB_DIFFUSION: i32 = 0x0002;
pub const AL_EAXREVERB_GAIN: i32 = 0x0003;
pub const AL_EAXREVERB_GAINHF: i32 = 0x0004;
pub const AL_EAXREVERB_GAINLF: i32 = 0x0005;
pub const AL_EAXREVERB_DECAY_TIME: i32 = 0x0006;
pub const AL_EAXREVERB_DECAY_HFRATIO: i32 = 0x0007;
pub const AL_EAXREVERB_DECAY_LFRATIO: i32 = 0x0008;
pub const AL_EAXREVERB_REFLECTIONS_GAIN: i32 = 0x0009;
pub const AL_EAXREVERB_REFLECTIONS_DELAY: i32 = 0x000A;
pub const AL_EAXREVERB_REFLECTIONS_PAN: i32 = 0x000B;
pub const AL_EAXREVERB_LATE_REVERB_GAIN: i32 = 0x000C;
pub const AL_EAXREVERB_LATE_REVERB_DELAY: i32 = 0x000D;
pub const AL_EAXREVERB_LATE_REVERB_PAN: i32 = 0x000E;
pub const AL_EAXREVERB_ECHO_TIME: i32 = 0x000F;
pub const AL_EAXREVERB_ECHO_DEPTH: i32 = 0x0010;
pub const AL_EAXREVERB_MODULATION_TIME: i32 = 0x0011;
pub const AL_EAXREVERB_MODULATION_DEPTH: i32 = 0x0012;
pub const AL_EAXREVERB_AIR_ABSORPTION_GAINHF: i32 = 0x0013;
pub const AL_EAXREVERB_HFREFERENCE: i32 = 0x0014;
pub const AL_EAXREVERB_LFREFERENCE: i32 = 0x0015;
pub const AL_EAXREVERB_ROOM_ROLLOFF_FACTOR: i32 = 0x0016;
pub const AL_EAXREVERB_DECAY_HFLIMIT: i32 = 0x0017;

// Chorus effect parameters
pub const AL_CHORUS_WAVEFORM: i32 = 0x0001;
pub const AL_CHORUS_PHASE: i32 = 0x0002;
pub const AL_CHORUS_RATE: i32 = 0x0003;
pub const AL_CHORUS_DEPTH: i32 = 0x0004;
pub const AL_CHORUS_FEEDBACK: i32 = 0x0005;
pub const AL_CHORUS_DELAY: i32 = 0x0006;

// Distortion effect parameters
pub const AL_DISTORTION_EDGE: i32 = 0x0001;
pub const AL_DISTORTION_GAIN: i32 = 0x0002;
pub const AL_DISTORTION_LOWPASS_CUTOFF: i32 = 0x0003;
pub const AL_DISTORTION_EQCENTER: i32 = 0x0004;
pub const AL_DISTORTION_EQBANDWIDTH: i32 = 0x0005;

// Echo effect parameters
pub const AL_ECHO_DELAY: i32 = 0x0001;
pub const AL_ECHO_LRDELAY: i32 = 0x0002;
pub const AL_ECHO_DAMPING: i32 = 0x0003;
pub const AL_ECHO_FEEDBACK: i32 = 0x0004;
pub const AL_ECHO_SPREAD: i32 = 0x0005;

// Flanger effect parameters
pub const AL_FLANGER_WAVEFORM: i32 = 0x0001;
pub const AL_FLANGER_PHASE: i32 = 0x0002;
pub const AL_FLANGER_RATE: i32 = 0x0003;
pub const AL_FLANGER_DEPTH: i32 = 0x0004;
pub const AL_FLANGER_FEEDBACK: i32 = 0x0005;
pub const AL_FLANGER_DELAY: i32 = 0x0006;

// Frequency shifter effect parameters
pub const AL_FREQUENCY_SHIFTER_FREQUENCY: i32 = 0x0001;
pub const AL_FREQUENCY_SHIFTER_LEFT_DIRECTION: i32 = 0x0002;
pub const AL_FREQUENCY_SHIFTER_RIGHT_DIRECTION: i32 = 0x0003;

// Vocal morpher effect parameters
pub const AL_VOCAL_MORPHER_PHONEMEA: i32 = 0x0001;
pub const AL_VOCAL_MORPHER_PHONEMEA_COARSE_TUNING: i32 = 0x0002;
pub const AL_VOCAL_MORPHER_PHONEMEB: i32 = 0x0003;
pub const AL_VOCAL_MORPHER_PHONEMEB_COARSE_TUNING: i32 = 0x0004;
pub const AL_VOCAL_MORPHER_WAVEFORM: i32 = 0x0005;
pub const AL_VOCAL_MORPHER_RATE: i32 = 0x0006;

// Pitchshifter effect parameters
pub const AL_PITCH_SHIFTER_COARSE_TUNE: i32 = 0x0001;
pub const AL_PITCH_SHIFTER_FINE_TUNE: i32 = 0x0002;

// Ringmodulator effect parameters
pub const AL_RING_MODULATOR_FREQUENCY: i32 = 0x0001;
pub const AL_RING_MODULATOR_HIGHPASS_CUTOFF: i32 = 0x0002;
pub const AL_RING_MODULATOR_WAVEFORM: i32 = 0x0003;

// Autowah effect parameters
pub const AL_AUTOWAH_ATTACK_TIME: i32 = 0x0001;
pub const AL_AUTOWAH_RELEASE_TIME: i32 = 0x0002;
pub const AL_AUTOWAH_RESONANCE: i32 = 0x0003;
pub const AL_AUTOWAH_PEAK_GAIN: i32 = 0x0004;

// Compressor effect parameters
pub const AL_COMPRESSOR_ONOFF: i32 = 0x0001;

// Equalizer effect parameters
pub const AL_EQUALIZER_LOW_GAIN: i32 = 0x0001;
pub const AL_EQUALIZER_LOW_CUTOFF: i32 = 0x0002;
pub const AL_EQUALIZER_MID1_GAIN: i32 = 0x0003;
pub const AL_EQUALIZER_MID1_CENTER: i32 = 0x0004;
pub const AL_EQUALIZER_MID1_WIDTH: i32 = 0x0005;
pub const AL_EQUALIZER_MID2_GAIN: i32 = 0x0006;
pub const AL_EQUALIZER_MID2_CENTER: i32 = 0x0007;
pub const AL_EQUALIZER_MID2_WIDTH: i32 = 0x0008;
pub const AL_EQUALIZER_HIGH_GAIN: i32 = 0x0009;
pub const AL_EQUALIZER_HIGH_CUTOFF: i32 = 0x000A;

// Effect type
pub const AL_EFFECT_FIRST_PARAMETER: i32 = 0x0000;
pub const AL_EFFECT_LAST_PARAMETER: i32 = 0x8000;
pub const AL_EFFECT_TYPE: i32 = 0x8001;

// Effect types, used with the AL_EFFECT_TYPE property
pub const AL_EFFECT_NULL: i32 = 0x0000;
pub const AL_EFFECT_REVERB: i32 = 0x0001;
pub const AL_EFFECT_CHORUS: i32 = 0x0002;
pub const AL_EFFECT_DISTORTION: i32 = 0x0003;
pub const AL_EFFECT_ECHO: i32 = 0x0004;
pub const AL_EFFECT_FLANGER: i32 = 0x0005;
pub const AL_EFFECT_FREQUENCY_SHIFTER: i32 = 0x0006;
pub const AL_EFFECT_VOCAL_MORPHER: i32 = 0x0007;
pub const AL_EFFECT_PITCH_SHIFTER: i32 = 0x0008;
pub const AL_EFFECT_RING_MODULATOR: i32 = 0x0009;
pub const AL_EFFECT_AUTOWAH: i32 = 0x000A;
pub const AL_EFFECT_COMPRESSOR: i32 = 0x000B;
pub const AL_EFFECT_EQUALIZER: i32 = 0x000C;
pub const AL_EFFECT_EAXREVERB: i32 = 0x8000;

// Auxiliary Effect Slot properties
pub const AL_EFFECTSLOT_EFFECT: i32 = 0x0001;
pub const AL_EFFECTSLOT_GAIN: i32 = 0x0002;
pub const AL_EFFECTSLOT_AUXILIARY_SEND_AUTO: i32 = 0x0003;

// NULL Auxiliary Slot ID to disable a source send
pub const AL_EFFECTSLOT_NULL: i32 = 0x0000;

// Filter properties
/// Lowpass filter parameters
pub const AL_LOWPASS_GAIN: i32 = 0x0001;
pub const AL_LOWPASS_GAINHF: i32 = 0x0002;

/// Highpass filter parameters
pub const AL_HIGHPASS_GAIN: i32 = 0x0001;
pub const AL_HIGHPASS_GAINLF: i32 = 0x0002;

/// Bandpass filter parameters
pub const AL_BANDPASS_GAIN: i32 = 0x0001;
pub const AL_BANDPASS_GAINLF: i32 = 0x0002;
pub const AL_BANDPASS_GAINHF: i32 = 0x0003;

/// Filter type
pub const AL_FILTER_FIRST_PARAMETER: i32 = 0x0000;
pub const AL_FILTER_LAST_PARAMETER: i32 = 0x8000;
pub const AL_FILTER_TYPE: i32 = 0x8001;

/// Filter types, used with the AL_FILTER_TYPE property
pub const AL_FILTER_NULL: i32 = 0x0000;
pub const AL_FILTER_LOWPASS: i32 = 0x0001;
pub const AL_FILTER_HIGHPASS: i32 = 0x0002;
pub const AL_FILTER_BANDPASS: i32 = 0x0003;
}

#[cfg_attr(target_os = "windows", link(name = "OpenAL32"))]
#[cfg_attr(not(target_os = "windows"), link(name = "openal"))]
extern "C"
{
	fn alGetError() -> i32;
	fn alcGetError(device: *mut std::ffi::c_void) -> i32;
	
	pub fn alGetString(param: i32) -> *const std::ffi::c_char;
	pub fn alcGetString(device: *mut std::ffi::c_void, param: i32) -> *const std::ffi::c_char;
	
	fn alIsExtensionPresent(extension_name: *const std::ffi::c_char) -> std::ffi::c_char;
	fn alcIsExtensionPresent(device: *mut std::ffi::c_void, extension_name: *const std::ffi::c_char) -> std::ffi::c_char;
	
	fn alGetProcAddress(function_name: *const std::ffi::c_char) -> *mut std::ffi::c_void;
	fn alcGetProcAddress(device: *mut std::ffi::c_void, function_name: *const std::ffi::c_char) -> *mut std::ffi::c_void;
	
	pub fn alGetEnumValue(device: *mut std::ffi::c_void, enum_name: *const std::ffi::c_char) -> i32;
	pub fn alcGetEnumValue(device: *mut std::ffi::c_void, enum_name: *const std::ffi::c_char) -> i32;
	
	fn alcCloseDevice(device: *mut std::ffi::c_void);
	fn alcOpenDevice(result: *const std::ffi::c_char) -> *mut std::ffi::c_void;
	
	fn alcDestroyContext(context: *mut std::ffi::c_void);
	fn alcCreateContext(device: *mut std::ffi::c_void, attrlist: *const i32) -> *mut std::ffi::c_void;
	fn alcGetCurrentContext() -> *mut std::ffi::c_void;
	fn alcMakeContextCurrent(context: *mut std::ffi::c_void) -> std::ffi::c_char;
	
	fn alDeleteBuffers(n: i32, buffers: *const u32);
	fn alGenBuffers(n: i32, buffers: *mut u32);
	fn alGetBufferi(buffer: u32, param: i32, value: *mut i32);
	fn alBufferData(buffer: u32, format: i32, data: *const std::ffi::c_void, size: i32, freq: i32);
	
	fn alDeleteSources(n: i32, buffers: *const u32);
	fn alGenSources(n: i32, buffers: *mut u32);
	
	fn alGetSourcei(source: u32, param: i32, value: *mut i32);
	fn alSourcei(source: u32, param: i32, value: i32);
	fn alGetSourcef(source: u32, param: i32, value: *mut f32);
	fn alSourcef(source: u32, param: i32, value: f32);
	
	fn alSourcePlay(source: u32);
	fn alSourceStop(source: u32);
	fn alSourceRewind(source: u32);
	fn alSourcePause(source: u32);
	
	fn alSourceQueueBuffers(source: u32, nb: i32, buffers: *const u32);
	fn alSourceUnqueueBuffers(source: u32, nb: i32, buffers: *mut u32);
	
	fn alListenerf(param: i32, value: f32);
	fn alListenerfv(param: i32, values: *const f32);
	fn alListeneri(param: i32, value: i32);
	fn alListeneriv(param: i32, values: *const i32);
	
	fn alEnable(capability: i32);
	fn alDisable(capability: i32);
	fn alIsEnabled(capability: i32) -> std::ffi::c_char;

	fn alDopplerFactor(value: f32);
	fn alDopplerVelocity(value: f32);
	fn alSpeedOfSound(value: f32);
	fn alDistanceModel(distance_model: i32);
	
	// Soft
	/*
	fn alEventControlSOFT(count: i32, types: *const i32, enable: std::ffi::c_char);
	fn alEventCallbackSOFT(callback: Option<extern "C" fn(
		event_type: std::ffi::c_int, object: std::ffi::c_uint, param: std::ffi::c_uint,
		length: std::ffi::c_int, message: *const std::ffi::c_char, user_param: *mut std::ffi::c_void,
	)>, user_param: *mut std::ffi::c_void);
	fn alGetPointerSOFT(pname: std::ffi::c_int) -> *mut std::ffi::c_void;
	
	fn alcReopenDeviceSOFT(device: *mut std::ffi::c_void, device_name: *const std::ffi::c_char,
		attribs: *const std::ffi::c_int) -> std::ffi::c_char
	;
	*/
}

const AL_NO_ERROR: i32 = 0;
const AL_INVALID_NAME: i32 = 0xA001;
const AL_INVALID_ENUM: i32 = 0xA002;
const AL_INVALID_VALUE: i32 = 0xA003;
const AL_INVALID_OPERATION: i32 = 0xA004;
const AL_OUT_OF_MEMORY: i32 = 0xA005;

const ALC_NO_ERROR: i32 = 0;
const ALC_INVALID_DEVICE: i32 = 0xA001;
const ALC_INVALID_CONTEXT: i32 = 0xA002;
const ALC_INVALID_ENUM: i32 = 0xA003;
const ALC_INVALID_VALUE: i32 = 0xA004;
const ALC_OUT_OF_MEMORY: i32 = 0xA005;

const ALC_DEFAULT_DEVICE_SPECIFIER: i32 = 0x1004;
const ALC_DEVICE_SPECIFIER: i32 = 0x1005;
const ALC_ALL_DEVICES_SPECIFIER: i32 = 0x1013;
const ALC_EXTENSIONS: i32 = 0x1006;
const ALC_CAPTURE_DEVICE_SPECIFIER: i32 = 0x310;
const ALC_CAPTURE_DEFAULT_DEVICE_SPECIFIER: i32 = 0x311;
const ALC_CAPTURE_SAMPLES: i32 = 0x312;

const AL_DIRECTION: i32 = 0x1005;
const AL_BUFFER: i32 = 0x1009;
const AL_SOURCE_STATE: i32 = 0x1010;

const AL_INITIAL: i32 = 0x1011;
const AL_PLAYING: i32 = 0x1012;
const AL_PAUSED: i32 = 0x1013;
const AL_STOPPED: i32 = 0x1014;

const AL_BUFFERS_QUEUED: i32 = 0x1015;
const AL_BUFFERS_PROCESSED: i32 = 0x1016;

const AL_POSITION: i32 = 0x1004;
const AL_VELOCITY: i32 = 0x1006;
const AL_ORIENTATION: i32 = 0x100F;

const AL_VENDOR: i32 = 0xB001;
const AL_VERSION: i32 = 0xB002;
const AL_RENDERER: i32 = 0xB003;
const AL_EXTENSIONS: i32 = 0xB004;

const AL_DISTANCE_MODEL: i32 = 0xD000;

const AL_INVERSE_DISTANCE: i32 = 0xD001;
const AL_INVERSE_DISTANCE_CLAMPED: i32 = 0xD002;
const AL_LINEAR_DISTANCE: i32 = 0xD003;
const AL_LINEAR_DISTANCE_CLAMPED: i32 = 0xD004;
const AL_EXPONENT_DISTANCE: i32 = 0xD005;
const AL_EXPONENT_DISTANCE_CLAMPED: i32 = 0xD006;

const AL_EVENT_CALLBACK_FUNCTION_SOFT: i32 = 0x19A2;
const AL_EVENT_CALLBACK_USER_PARAM_SOFT: i32 = 0x19A3;
const AL_EVENT_TYPE_BUFFER_COMPLETED_SOFT: i32 = 0x19A4;
const AL_EVENT_TYPE_SOURCE_STATE_CHANGED_SOFT: i32 = 0x19A5;
const AL_EVENT_TYPE_DISCONNECTED_SOFT: i32 = 0x19A6;

#[derive(Debug)]
pub enum OpenAL_error
{
	unknown {code: i32},
	error {code: i32, message: &'static str},
}

impl std::fmt::Display for OpenAL_error
{
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
	{
		write!(f, "OpenAL error: ")?;
		
		match self
		{
			Self::unknown {code} => write!(f, "code {}: <unknown error>", code),
			Self::error {code, message} => write!(f, "code {}: {}", code, message),
		}
	}
}

impl std::error::Error for OpenAL_error
{
}

fn check_error(code: i32) -> Result<(), OpenAL_error>
{
	match code
	{
		AL_NO_ERROR => Ok(()),
		AL_INVALID_NAME => Err(OpenAL_error::error {code, message:"invalid name (a bad name was passed to an OpenAL function)"}),
		AL_INVALID_ENUM => Err(OpenAL_error::error {code, message: "invalid enum (an invalid enum value was passed to an OpenAL function)"}),
		AL_INVALID_VALUE => Err(OpenAL_error::error {code, message: "invalid value (an invalid value was passed to an OpenAL function)"}),
		AL_INVALID_OPERATION => Err(OpenAL_error::error {code, message: "invalid operation (the requested operation is not valid)"}),
		AL_OUT_OF_MEMORY => Err(OpenAL_error::error {code, message: "out of memory"}),
		code => Err(OpenAL_error::unknown {code}),
	}
}

fn check_context_error(code: i32) -> Result<(), OpenAL_error>
{
	match code
	{
		ALC_NO_ERROR => Ok(()),
		ALC_INVALID_DEVICE => Err(OpenAL_error::error {code, message: "invalid device (a bad device was passed to an OpenAL function)"}),
		ALC_INVALID_CONTEXT => Err(OpenAL_error::error {code, message: "invalid context (a bad context was passed to an OpenAL function)"}),
		ALC_INVALID_ENUM => Err(OpenAL_error::error {code, message: "invalid enum (an invalid enum value was passed to an OpenAL function)"}),
		ALC_INVALID_VALUE => Err(OpenAL_error::error {code, message: "invalid value (an invalid value was passed to an OpenAL function)"}),
		ALC_OUT_OF_MEMORY => Err(OpenAL_error::error {code, message: "out of memory"}),
		code => Err(OpenAL_error::unknown {code}),
	}
}

macro_rules! call_checked
{
	($function: expr) =>
	{
		unsafe
		{
			alGetError();
			let result = $function;
			match check_error(alGetError())
			{
				Ok(_) => Ok(result),
				Err(err) => Err(err),
			}
		}
	};
	
	($device: expr, $function: expr) =>
	{
		unsafe
		{
			alcGetError($device);
			let result = $function;
			match check_context_error(alcGetError($device))
			{
				Ok(_) => Ok(result),
				Err(err) => Err(err),
			}
		}
	};
}

fn openal_format(format: audio::Format) -> i32
{
	let mut result: i32 = 0x1100;
	result += (format.byte_depth() - 1) as i32;
	result += 2 * (format.channels() - 1) as i32;
	return result;
}

fn al_get_string(param: i32) -> &'static str
{
	let result = call_checked!(alGetString(param)).unwrap();
	return unsafe {std::ffi::CStr::from_ptr(result)}.to_str().unwrap();
}

pub fn get_vendor() -> &'static str {al_get_string(AL_VENDOR)}
pub fn get_version() -> &'static str {al_get_string(AL_VERSION)}
pub fn get_renderer() -> &'static str {al_get_string(AL_RENDERER)}
pub fn get_extensions() -> &'static str {al_get_string(AL_EXTENSIONS)}

pub struct Device
{
	pub device: std::ptr::NonNull<std::ffi::c_void>,
}

impl Drop for Device
{
	fn drop(&mut self)
	{
		unsafe {alcCloseDevice(self.device.as_ptr())};
	}
}

impl Default for Device
{
	fn default() -> Self
	{
		Self::new_impl(std::ptr::null())
	}
}

impl Device
{
	fn new_impl(name: *const std::ffi::c_char) -> Self
	{
		let Some(device) = std::ptr::NonNull::new(unsafe {alcOpenDevice(name)}) else
		{
			let name = unsafe {std::ffi::CStr::from_ptr(name).to_str().unwrap_or("<unknown>")};
			panic!("OpenAL error: could not open device, name: {}", name);
		};
		
		check_context_error(unsafe {alcGetError(device.as_ptr())}).unwrap();
		
		Self {device}
	}
	
	pub fn new(name: &std::ffi::CStr) -> Self
	{
		Self::new_impl(name.as_ptr())
	}
	
	pub fn name(&self) -> &std::ffi::CStr
	{
		call_checked!(self.device.as_ptr(), std::ffi::CStr::from_ptr(alcGetString(self.device.as_ptr(), ALC_DEVICE_SPECIFIER))).unwrap()
	}
	
	pub fn names() -> impl std::iter::Iterator<Item = &'static std::ffi::CStr>
	{
		struct Device_name_iterator
		{
			names: *const std::ffi::c_char,
		}
		
		impl std::iter::Iterator for Device_name_iterator
		{
			type Item = &'static std::ffi::CStr;
			
			fn next(&mut self) -> Option<Self::Item>
			{
				if unsafe {*self.names == '\0' as std::ffi::c_char}
				{
					return None;
				}
				
				let result;
				result = unsafe {std::ffi::CStr::from_ptr(self.names)};
				self.names = unsafe {self.names.offset(result.to_bytes_with_nul().len() as isize)};
				
				return Some(result);
			}
		}
		
		let names = unsafe {alcGetString(std::ptr::null_mut(), ALC_ALL_DEVICES_SPECIFIER)};
		
		Device_name_iterator {names}
	}
	
	pub fn get_extensions(&self) -> &'static str
	{
		unsafe {std::ffi::CStr::from_ptr(alcGetString(self.device.as_ptr(), ALC_EXTENSIONS))}.to_str().unwrap()
	}
}

pub struct Streaming_source_data
{
	pub source: Source,
	buffer_objects: Buffers<3>,
	data_source: Box<dyn audio::Streaming_sound>,
}

pub struct Streaming_source
{
	data: std::sync::Arc<std::sync::Mutex<Streaming_source_data>>,
}

impl Streaming_source
{
	pub fn is_finished(&self) -> bool
	{
		let mut data = self.data.as_ref().lock().unwrap();
		// data.source.buffers_queued() == 0
		data.source.state() == Source_state::stopped && ! audio::Streaming_sound::has_data_left(data.data_source.as_mut()).unwrap()
	}
	
	pub fn play(&mut self)
	{
		self.data.as_ref().lock().unwrap().source.play();
	}
	
	pub fn pause(&mut self)
	{
		self.data.as_ref().lock().unwrap().source.pause();
	}
}

impl std::ops::Deref for Streaming_source
{
	type Target = std::sync::Mutex<Streaming_source_data>;
	
	fn deref(&self) -> &Self::Target
	{
		self.data.as_ref()
	}
}

unsafe impl Send for Streaming_source_data {}

struct Streaming_data
{
	shutdown: bool,
	sources: Vec<std::sync::Weak<std::sync::Mutex<Streaming_source_data>>>,
	buffer: [u8; audio::buffer_size],
}

fn streaming_handler(streaming_data: std::sync::Arc<std::sync::Mutex<Streaming_data>>)
{
	loop
	{
		let mut i = 0;
		
		loop
		{
			let mut lock = streaming_data.as_ref().lock().unwrap();
			
			if lock.shutdown
			{
				return;
			}
			
			if i == lock.sources.len()
			{
				break;
			}
			
			let Some(streaming_source) = lock.sources[i].upgrade() else
			{
				lock.sources.swap_remove(i);
				continue
			};
			
			let mut streaming_source = streaming_source.as_ref().lock().unwrap();
			let buffers_processed = streaming_source.source.buffers_processed();
			
			if buffers_processed != 0
			{
				let mut buffers = [0; 3];
				
				call_checked!(alSourceUnqueueBuffers(streaming_source.source.source, buffers_processed, buffers.as_mut_ptr())).unwrap();
				
				let mut buffers_queued = 0;
				let format = streaming_source.data_source.as_ref().format();
				let frequency = streaming_source.data_source.as_ref().frequency();
				
				for &buffer in &buffers[.. buffers_processed as usize]
				{
					let data_length = lock.buffer.len();
					let data_length = streaming_source.data_source.as_mut().read(&mut lock.buffer[
						.. data_length
						/ format.channels() as usize * format.channels() as usize
						/ format.byte_depth() as usize * format.byte_depth() as usize
					]).unwrap();
					
					if data_length > 0
					{
						call_checked!(alBufferData(buffer, openal_format(format),
							lock.buffer.as_ptr().cast::<std::ffi::c_void>(), data_length as i32, frequency
						)).unwrap();
						buffers_queued += 1;
					}
					else
					{
						break;
					}
				}
				
				call_checked!(alSourceQueueBuffers(streaming_source.source.source, buffers_queued, buffers.as_ptr())).unwrap();
				
				if buffers_queued != 0
				{
					if streaming_source.source.state() == Source_state::stopped
					{
						streaming_source.source.rewind();
						streaming_source.source.play();
					}
					
				}
			}
			
			i += 1;
		}
		
		std::thread::sleep(std::time::Duration::from_micros(16_667));
	}
}

pub struct Context<'d>
{
	context: std::ptr::NonNull<std::ffi::c_void>,
	streaming_data: std::sync::Arc<std::sync::Mutex<Streaming_data>>,
	streaming_thread: Option<std::thread::JoinHandle<()>>,
	_device: std::marker::PhantomData<&'d Device>,
}

impl<'d> Drop for Context<'d>
{
	fn drop(&mut self)
	{
		{
			let mut lock = self.streaming_data.as_ref().lock().unwrap();
			lock.shutdown = true;
			lock.sources.clear();
		}
		
		self.streaming_thread.take().unwrap().join();
		unsafe {alcMakeContextCurrent(std::ptr::null_mut())};
		unsafe {alcDestroyContext(self.context.as_ptr())};
	}
}

impl<'d> Context<'d>
{
	pub fn new(device: &'d Device) -> Self
	{
		let context = call_checked!(device.device.as_ptr(), alcCreateContext(device.device.as_ptr(), std::ptr::null())).unwrap();
		
		let Some(context) = std::ptr::NonNull::new(context) else
		{
			panic!("OpenAL error: could not create context");
		};
		
		// Do not check for errors while the context is not current
		if unsafe {alcMakeContextCurrent(context.as_ptr())} != 1
		{
			panic!("OpenAL error: failed to make context current");
		}
		
		unsafe {check_error(alGetError())};
		
		let streaming_data = std::sync::Arc::new(std::sync::Mutex::new(Streaming_data
		{
			shutdown: false,
			sources: Vec::with_capacity(32),
			buffer: [0; audio::buffer_size],
		}));
		
		let streaming_thread = Some(std::thread::spawn({
			let streaming_data = streaming_data.clone();
			move || streaming_handler(streaming_data)
		}));
		
		Self {context, streaming_data, streaming_thread, _device: Default::default()}
	}
	
	pub fn new_stream(&mut self, mut sound: Box<dyn audio::Streaming_sound>) -> Streaming_source
	{
		let source = Source::new();
		let buffers = Buffers::<3>::new();
		let mut buffers_filled = 0;
		
		let mut data_buffer = [0; audio::buffer_size];
		
		for buffer in buffers.buffers
		{
			if sound.read(&mut data_buffer).unwrap() == 0
			{
				break;
			}
			
			call_checked!(alBufferData(buffer, openal_format(sound.format()),
				data_buffer.as_ptr().cast::<std::ffi::c_void>(), data_buffer.len() as i32, sound.frequency()
			)).unwrap();
			
			buffers_filled += 1;
		}
		
		call_checked!(alSourceQueueBuffers(source.source, buffers_filled, buffers.buffers.as_ptr())).unwrap();
		
		let mut streaming_source = std::sync::Arc::new(std::sync::Mutex::new(
			Streaming_source_data {source, buffer_objects: buffers, data_source: sound}
		));
		self.streaming_data.as_ref().lock().unwrap().sources.push(std::sync::Arc::downgrade(&mut streaming_source));
		
		return Streaming_source {data: streaming_source};
	}
}

pub struct Buffers<const Size: usize>
{
	buffers: [u32; Size],
}

impl<const Size: usize> Drop for Buffers<Size>
{
	fn drop(&mut self)
	{
		call_checked!(alDeleteBuffers(Size as i32, self.buffers.as_ptr())).unwrap();
	}
}

impl<const Size: usize> Buffers<Size>
{
	pub fn new() -> Self
	{
		let mut buffers = [0; Size];
		call_checked!(alGenBuffers(Size as i32, buffers.as_mut_ptr())).unwrap();
		Self {buffers}
	}
	
	/*
	fn bit_depth() -> i32;
	fn channels() -> i32;
	fn frequency() -> i32;
	*/
}

pub type Buffer = Buffers<1>;

impl Buffer
{
	pub fn data(&self, sound_info: audio::Sound_info, data: &[u8])
	{
		call_checked!(alBufferData(self.buffers[0], openal_format(sound_info.format()),
			data.as_ptr() as *const std::ffi::c_void, data.len() as i32, sound_info.frequency())
		).unwrap();
	}
}

pub struct Source
{
	pub source: u32,
}

impl Drop for Source
{
	fn drop(&mut self)
	{
		call_checked!(alDeleteSources(1, &self.source)).unwrap();
	}
}

#[derive(PartialEq, Eq, Debug)]
pub enum Source_state
{
	initial = AL_INITIAL as isize,
	playing = AL_PLAYING as isize,
	paused = AL_PAUSED as isize,
	stopped = AL_STOPPED as isize,
}

impl Source
{
	pub fn new() -> Self
	{
		let mut source: u32 = 0;
		call_checked!(alGenSources(1, &mut source)).unwrap();
		Self {source}
	}
	
	pub fn play(&mut self) {call_checked!(alSourcePlay(self.source)).unwrap();}
	pub fn stop(&mut self) {call_checked!(alSourceStop(self.source)).unwrap();}
	pub fn rewind(&mut self) {call_checked!(alSourceRewind(self.source)).unwrap();}
	pub fn pause(&mut self) {call_checked!(alSourcePause(self.source)).unwrap();}
	
	pub fn get_source_i(&self, parameter: i32) -> i32
	{
		let mut result = 0;
		call_checked!(alGetSourcei(self.source, parameter, &mut result)).unwrap();
		return result;
	}
	
	pub fn source_i(&mut self, parameter: i32, value: i32)
	{
		call_checked!(alSourcei(self.source, parameter, value)).unwrap();
	}
	
	pub fn get_source_f(&self, parameter: i32) -> f32
	{
		let mut result = 0.0;
		call_checked!(alGetSourcef(self.source, parameter, &mut result)).unwrap();
		return result;
	}
	
	pub fn source_f(&mut self, parameter: i32, value: f32)
	{
		call_checked!(alSourcef(self.source, parameter, value)).unwrap();
	}
	
	pub fn buffers_queued(&self) -> i32 {self.get_source_i(AL_BUFFERS_QUEUED)}
	pub fn buffers_processed(&self) -> i32 {self.get_source_i(AL_BUFFERS_PROCESSED)}
	
	pub fn state(&self) -> Source_state
	{
		match self.get_source_i(AL_SOURCE_STATE)
		{
			AL_INITIAL => Source_state::initial,
			AL_PLAYING => Source_state::playing,
			AL_PAUSED => Source_state::paused,
			AL_STOPPED => Source_state::stopped,
			result => unreachable!("unknown value {} for AL_SOURCE_STATE", result),
		}
	}
	
	pub fn buffer(&mut self, buffer: Option<Buffer>)
	{
		self.source_i(AL_BUFFER, if let Some(b) = buffer {b.buffers[0]} else {0} as i32);
	}
	
	/// Valid range is [0, 10]
	pub fn air_absorption_factor(&mut self, factor: f32)
	{
		call_checked!(alSourcef(self.source, efx::AL_AIR_ABSORPTION_FACTOR, factor)).unwrap();
	}
	
	/// Valid range is [0, 10]
	pub fn room_rolloff_factor(&mut self, factor: f32)
	{
		call_checked!(alSourcef(self.source, efx::AL_ROOM_ROLLOFF_FACTOR, factor)).unwrap();
	}
}

pub type Vec3f = [f32; 3];

pub fn listener_position(pos: Vec3f)
{
	call_checked!(alListenerfv(AL_POSITION, pos.as_ptr())).unwrap();
}

pub fn listener_orientation(at: Vec3f, up: Vec3f)
{
	let value = [at[0], at[1], at[2], up[0], up[1], up[2]];
	call_checked!(alListenerfv(AL_ORIENTATION, value.as_ptr())).unwrap();
}
