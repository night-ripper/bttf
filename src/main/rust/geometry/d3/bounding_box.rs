use crate::geometry::d3::Vector;
use crate::geometry::coord_t;
use crate::geometry::d3::Move_by;
use crate::geometry::d3::Contains;
use crate::geometry::d3::Overlaps;
use crate::math::Midpoint;

#[derive(Clone, Copy, Eq)]
pub struct Bounding_box
{
	min: Vector,
	max: Vector,
}

impl std::fmt::Display for Bounding_box
{
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
	{
		write!(f, "Bounding_box3D {{min: {}, max: {}}}", self.min(), self.max())
	}
}

impl From<Vector> for Bounding_box
{
	fn from(value: Vector) -> Self
	{
		Bounding_box {min: value, max: value}
	}
}

pub trait Box_of
{
	fn box_of(self) -> Bounding_box;
}

impl Box_of for Bounding_box
{
	fn box_of(self) -> Bounding_box {self}
}

impl Bounding_box
{
	pub fn new(min: Vector, max: Vector) -> Self
	{
		Self {min, max}
	}
	
	pub fn empty() -> Self
	{
		Self::new(Vector::of_value(coord_t::MAX), Vector::of_value(coord_t::MIN))
	}
	
	pub fn full() -> Self
	{
		Self::new(Vector::of_value(coord_t::MIN), Vector::of_value(coord_t::MAX))
	}
	
	pub fn min(&self) -> Vector {self.min}
	pub fn max(&self) -> Vector {self.max}
	pub fn center(&self) -> Vector {self.min.midpoint(self.max)}
	
	pub fn vertex(&self, index: usize) -> Vector
	{
		Vector::from([
			if index & 0b001 == 0 {self.min} else {self.max}[0],
			if index & 0b010 == 0 {self.min} else {self.max}[1],
			if index & 0b100 == 0 {self.min} else {self.max}[2],
		])
	}
	
	pub fn vertices(&self) -> impl std::iter::Iterator<Item = Vector>
	{
		struct Result
		{
			index: u32,
			bbox: Bounding_box,
		}
		
		impl std::iter::Iterator for Result
		{
			type Item = Vector;
			
			fn next(&mut self) -> Option<Self::Item>
			{
				if self.index == 8
				{
					return None;
				}
				
				let index = self.index as usize;
				self.index += 1;
				
				Some(self.bbox.vertex(index))
			}
		}
		
		return Result {index: 0, bbox: *self};
	}
}

impl std::cmp::PartialEq for Bounding_box
{
	fn eq(&self, rhs: &Self) -> bool
	{
		self.min == rhs.min && self.max == rhs.max
	}
}

impl std::ops::AddAssign<Vector> for Bounding_box
{
	fn add_assign(&mut self, rhs: Vector)
	{
		for i in 0 .. 3
		{
			self.min[i] = self.min[i].min(rhs[i]);
			self.max[i] = self.max[i].max(rhs[i]);
		}
	}
}

impl std::ops::Add<Vector> for Bounding_box
{
	type Output = Bounding_box;
	
	fn add(mut self, rhs: Vector) -> Self::Output
	{
		self += rhs; self
	}
}

impl std::ops::AddAssign<Bounding_box> for Bounding_box
{
	fn add_assign(&mut self, rhs: Bounding_box)
	{
		for i in 0 .. 3
		{
			self.min[i] = self.min[i].min(rhs.min[i]);
			self.max[i] = self.max[i].max(rhs.max[i]);
		}
	}
}

impl std::ops::Add<Bounding_box> for Bounding_box
{
	type Output = Bounding_box;
	
	fn add(mut self, rhs: Bounding_box) -> Self::Output
	{
		self += rhs; self
	}
}

impl Move_by for Bounding_box
{
	fn move_by(&mut self, vector: Vector)
	{
		self.min += vector;
		self.max += vector;
	}
}

impl Contains<Vector> for Bounding_box
{
	fn contains(self, inner: Vector) -> bool
	{
		inner[0] >= self.min[0] && inner[0] <= self.max[0]
		&& inner[1] >= self.min[1] && inner[1] <= self.max[1]
		&& inner[2] >= self.min[2] && inner[2] <= self.max[2]
	}
}

impl Overlaps<Vector> for Bounding_box
{
	fn overlaps(self, rhs: Vector) -> bool
	{
		self.contains(rhs)
	}
}

impl Contains<Bounding_box> for Bounding_box
{
	fn contains(self, inner: Bounding_box) -> bool
	{
		self.min[0] <= inner.min[0] && self.min[1] <= inner.min[1]
		&& self.min[2] <= inner.min[2] && self.max[0] >= inner.max[0]
		&& self.max[1] >= inner.max[1] && self.max[2] >= inner.max[2]
	}
}

impl Overlaps<Bounding_box> for Bounding_box
{
	fn overlaps(self, rhs: Bounding_box) -> bool
	{
		self.min[0] <= rhs.max[0] && self.min[1] <= rhs.max[1]
		&& self.min[2] <= rhs.max[2] && rhs.min[0] <= self.max[0]
		&& rhs.min[1] <= self.max[1] && rhs.min[2] <= self.max[2]
	}
}
