use crate::geometry::coord_t;
use crate::geometry::d3::Bounding_box;
use crate::geometry::d3::Contains;
use crate::containers::linked_node::Linked_node;

#[cfg(test)] use crate::geometry::d3::Vector;
#[cfg(test)] use crate::geometry::d3::Overlaps;

type Path_bitset = u64;

#[derive(Clone, Copy)]
struct Path
{
	bitset: Path_bitset,
}

impl Path
{
	fn array_index(self) -> usize
	{
		(self.bitset & 0b_111) as usize
	}
}

impl std::fmt::Display for Path
{
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
	{
		let mut copy = *self;
		let mut count = 0;
		
		while copy.bitset > 1
		{
			if count == 4
			{
				f.write_str(",");
				count = 0;
			}
			
			write!(f, "{}", copy.array_index());
			
			copy.bitset >>= 3;
			count += 1;
		}
		
		Ok(())
	}
}

const bitset_size: u32 = Path_bitset::BITS;
const box_divisor: u32 = 16;
const box_larger_multiplier: u32 = 9;
const box_smaller_multiplier: u32 = box_divisor - box_larger_multiplier;

fn cast_s_u(value: coord_t) -> u32
{
	value.underlying_value() as u32 ^ (1 << 31)
}

fn box_fraction(bbox: Bounding_box) -> u32
{
	(cast_s_u(bbox.max()[0]) - cast_s_u(bbox.min()[0])) / box_divisor
}

fn reduce_box(bbox: Bounding_box, array_index: usize) -> Bounding_box
{
	let mut minv = bbox.min();
	let mut maxv = bbox.max();
	
	let fraction = box_fraction(bbox);
	
	for ax in 0 .. 3
	{
		if array_index & (1 << ax) != 0
		{
			minv[ax] = coord_t::from_underlying(
				(bbox.min()[ax].underlying_value() as i64 + box_smaller_multiplier as i64 * fraction as i64) as i32
			);
		}
		else
		{
			maxv[ax] = coord_t::from_underlying(
				(bbox.min()[ax].underlying_value() as i64 + box_larger_multiplier as i64 * fraction as i64) as i32
			);
		}
	}
	
	return Bounding_box::new(minv, maxv);
}

fn reduce_box_candidate_set(outer: Bounding_box, bbox: Bounding_box) -> usize
{
	const masks: [usize; 3] = [
		0b_0101_0101,
		0b_0011_0011,
		0b_0000_1111,
	];
	
	let outer_fraction = box_fraction(outer);
	
	let mut result = usize::MAX;
	
	for ax in 0 .. 3
	{
		if box_larger_multiplier * outer_fraction + cast_s_u(outer.min()[ax])
			< cast_s_u(bbox.max()[ax])
		{
			result &= ! masks[ax];
		}
		
		if cast_s_u(bbox.min()[ax])
			< box_smaller_multiplier * outer_fraction + cast_s_u(outer.min()[ax])
		{
			result &= masks[ax];
		}
	}
	
	return result;
}

fn box_of_path(mut path: Path) -> Bounding_box
{
	let mut result = Bounding_box::full();
	
	while path.bitset != 1
	{
		result = reduce_box(result, path.array_index());
		path.bitset >>= 3;
	}
	
	return result;
}

struct Path_computation
{
	outer: Bounding_box,
	bbox: Bounding_box,
	result: Path,
	depth: u32,
}

impl Path_computation
{
	fn from_outer(outer: Bounding_box, bbox: Bounding_box) -> Path_computation
	{
		Path_computation
		{
			outer,
			bbox,
			result: Path {bitset: 0},
			depth: 0,
		}
	}
	
	fn from_node(node: &Node, bbox: Bounding_box) -> Path_computation
	{
		let mut result = Path_computation::from_outer(node.bbox, bbox);
		result.result = node.path;
		result.depth = Path_bitset::BITS - result.result.bitset.leading_zeros() - 1;
		result.result.bitset &= ! (1 << result.depth);
		return result;
	}
	
	fn get_result(&self) -> Path
	{
		let mut result = self.result;
		result.bitset |= 1 << self.depth;
		return result;
	}
	
	fn is_leaf(&self) -> bool
	{
		return self.depth >= Path_bitset::BITS - 3;
	}
	
	fn move_down(&mut self, array_index: usize)
	{
		self.outer = reduce_box(self.outer, array_index);
		self.result.bitset |= (array_index as Path_bitset) << self.depth;
		self.depth += 3;
	}
	
	fn compute(&mut self) -> usize
	{
		if self.is_leaf()
		{
			return usize::MAX;
		}
		
		let candidate_set = reduce_box_candidate_set(self.outer, self.bbox);
		
		if candidate_set == 0
		{
			return usize::MAX;
		}
		
		let array_index = candidate_set.trailing_zeros() as usize;
		self.move_down(array_index);
		
		return array_index;
	}
}

struct Node
{
	path: Path,
	first: usize,
	last: usize,
	parent: usize,
	indices: [usize; 8],
	depth: u32,
	bbox: Bounding_box,
}

impl Node
{
	fn new(path: Path, depth: u32, parent: usize, bbox: Bounding_box) -> Node
	{
		Node
		{
			path,
			first: usize::MAX,
			last: usize::MAX,
			parent,
			indices: [usize::MAX; 8],
			depth,
			bbox,
		}
	}
	
	fn is_empty(&self) -> bool
	{
		self.first == usize::MAX
	}
}

#[derive(Clone, Copy)]
pub struct Handle
{
	node_index: usize,
	object_index: usize,
}

pub struct Object_iterator<Objects>
{
	index: usize,
	objects: Objects,
}

impl<Objects> Object_iterator<Objects>
{
	fn set_index(&mut self, index: usize) {self.index = index}
}

impl<'t, Type> std::iter::Iterator for Object_iterator<&'t [Linked_node<(Type, Bounding_box)>]>
{
	type Item = (&'t Type, Bounding_box);
	
	fn next(&mut self) -> Option<Self::Item>
	{
		if self.index == usize::MAX
		{
			return None;
		}
		
		let entry = &self.objects[self.index];
		let next = entry.next();
		let result = entry.as_ref().1;
		let result = (&entry.as_ref().0, result);
		self.index = next;
		
		return Some(result);
	}
}

impl<'t, Type> std::iter::Iterator for Object_iterator<&'t mut [Linked_node<(Type, Bounding_box)>]>
{
	type Item = (&'t mut Type, Bounding_box);
	
	fn next(&mut self) -> Option<Self::Item>
	{
		if self.index == usize::MAX
		{
			return None;
		}
		
		let entry = unsafe {self.objects.as_mut_ptr().cast::<Linked_node<(Type, Bounding_box)>>()
			.offset(self.index as isize).as_mut().unwrap()
		};
		let next = entry.next();
		let result = entry.as_ref().1;
		let result = (&mut entry.as_mut().0, result);
		self.index = next;
		
		return Some(result);
	}
}

pub struct Query<'t, Objects, Filter: std::ops::FnMut(Bounding_box) -> bool>
{
	it: Objects,
	node_index: usize,
	nodes: &'t [Node],
	filter: Filter,
}

impl<'t, Type, Objects, Filter> std::iter::Iterator for Query<'t, Object_iterator<Objects>, Filter>
where
	Object_iterator<Objects>: std::iter::Iterator<Item = (Type, Bounding_box)>,
	Filter: std::ops::FnMut(Bounding_box) -> bool,
{
	type Item = <Object_iterator<Objects> as std::iter::Iterator>::Item;
	
	fn next(&mut self) -> Option<Self::Item>
	{
		while let Some(v) = self.it.next()
		{
			if (self.filter)(v.1)
			{
				return Some(v);
			}
		}
		
		if self.node_index == usize::MAX
		{
			return None;
		}
		
		for i in 0 .. 8
		{
			let descendant_index = self.nodes[self.node_index].indices[i];
			
			if descendant_index != usize::MAX && (self.filter)(self.nodes[descendant_index].bbox)
			{
				self.node_index = descendant_index;
				self.it.set_index(self.nodes[descendant_index].first);
				return self.next();
			}
		}
		
		let mut node_index;
		
		while {node_index = self.node_index; self.node_index = self.nodes[self.node_index].parent;
			self.node_index != usize::MAX}
		{
			let mut i = 0;
			
			while node_index != self.nodes[self.node_index].indices[i]
			{
				i += 1;
			}
			
			while {i += 1; i != 8}
			{
				let descendant_index = self.nodes[self.node_index].indices[i];
				
				if descendant_index != usize::MAX && (self.filter)(self.nodes[descendant_index].bbox)
				{
					self.node_index = descendant_index;
					self.it.set_index(self.nodes[descendant_index].first);
					return self.next();
				}
			}
		}
		
		return None;
	}
}

pub struct Gridtree<Type>
{
	root: usize,
	nodes: crate::containers::repository::Repository<Node>,
	objects: crate::containers::repository::Repository<Linked_node<(Type, Bounding_box)>>,
}

impl<Type> Gridtree<Type>
{
	pub fn new() -> Self
	{
		Self
		{
			root: usize::MAX,
			objects: crate::containers::repository::Repository::new(),
			nodes: crate::containers::repository::Repository::new(),
		}
	}
	
	fn insert_new_path_from(&mut self, mut node_index: usize, bbox: Bounding_box) -> usize
	{
		let mut cmp = Path_computation::from_node(&self.nodes[node_index], bbox);
		
		let mut candidate_set = reduce_box_candidate_set(cmp.outer, bbox);
		let array_index = candidate_set.trailing_zeros() as usize;
		
		while ! cmp.is_leaf() && candidate_set != 0
		{
			cmp.move_down(candidate_set.trailing_zeros() as usize);
			candidate_set = reduce_box_candidate_set(cmp.outer, bbox);
		}
		
		if array_index < 8
		{
			let mut descendant = self.nodes[node_index].indices[array_index];
			
			if descendant == usize::MAX
			{
				descendant = self.nodes.insert(Node::new(cmp.get_result(), cmp.depth, node_index, cmp.outer));
				self.nodes[node_index].indices[array_index] = descendant;
			}
			
			node_index = descendant;
		}
		
		return node_index;
	}
	
	fn find_to_insert_from(&mut self, node_index: usize, bbox: Bounding_box) -> usize
	{
		let mut cmp = Path_computation::from_node(&self.nodes[node_index], bbox);
		
		let candidate_set = reduce_box_candidate_set(cmp.outer, bbox);
		
		if ! cmp.is_leaf() && candidate_set != 0
		{
			let mut array_index = candidate_set.trailing_zeros() as usize;
			
			if ! candidate_set.is_power_of_two()
			{
				for i in 0 .. 8
				{
					if (candidate_set & (1 << i)) != 0 && self.nodes[node_index].indices[i] != usize::MAX
					{
						array_index = i;
						break;
					}
				}
			}
			
			let descendant = self.nodes[node_index].indices[array_index];
			
			if descendant == usize::MAX
			{
				return self.insert_new_path_from(node_index, bbox);
			}
			else if self.nodes[descendant].bbox.contains(bbox)
			{
				return self.find_to_insert_from(descendant, bbox);
			}
			else
			{
				let mut descendant_path = self.nodes[descendant].path;
				descendant_path.bitset >>= self.nodes[node_index].depth;
				descendant_path.bitset >>= 3;
				cmp.move_down(array_index);
				
				while reduce_box_candidate_set(cmp.outer, bbox) & (1 << descendant_path.array_index()) != 0
				{
					cmp.move_down(descendant_path.array_index());
					descendant_path.bitset >>= 3;
				}
				
				let new_node_index = self.nodes.insert(Node::new(cmp.get_result(), cmp.depth, node_index, cmp.outer));
				self.nodes[descendant].parent = new_node_index;
				self.nodes[node_index].indices[array_index] = new_node_index;
				self.nodes[new_node_index].indices[descendant_path.array_index()] = descendant;
				
				return self.insert_new_path_from(new_node_index, bbox);
			}
		}
		
		return node_index;
	}
	
	fn find_to_insert(&mut self, bbox: Bounding_box) -> usize
	{
		let mut cmp = Path_computation::from_outer(Bounding_box::full(), bbox);
		let mut root_path = self.nodes[self.root].path;
		
		while reduce_box_candidate_set(cmp.outer, bbox) & (1 << root_path.array_index()) != 0
		{
			cmp.move_down(root_path.array_index());
			root_path.bitset >>= 3;
		}
		
		if root_path.bitset != 1
		{
			let old_root = self.root;
			let path = cmp.get_result();
			let outer = cmp.outer;
			self.root = self.nodes.insert(Node::new(path, cmp.depth, usize::MAX, outer));
			let node_index = self.root;
			
			self.nodes[node_index].indices[root_path.array_index()] = old_root;
			self.nodes[old_root].parent = node_index;
		}
		
		return self.insert_new_path_from(self.root, bbox);
	}
	
	/// If there is a single value in the @param array different from usize::MAX, returns
	/// its position, otherwise returns usize::MAX.
	fn find_single_index(array: &[usize; 8]) -> usize
	{
		let mut descendant_index = usize::MAX;
		
		for i in 0 .. array.len()
		{
			if array[i] != usize::MAX
			{
				if descendant_index != usize::MAX
				{
					descendant_index = usize::MAX;
					break;
				}
				
				descendant_index = i;
			}
		}
		
		return descendant_index;
	}
	
	fn erase_upwards(&mut self, mut node_index: usize)
	{
		while self.nodes[node_index].is_empty() && self.nodes[node_index].indices.iter().all(|&v| v != usize::MAX)
		{
			let parent_index = self.nodes[node_index].parent;
			
			if parent_index != usize::MAX
			{
				for idx in self.nodes[parent_index].indices.iter_mut()
				{
					if *idx == node_index
					{
						*idx = usize::MAX;
						break;
					}
				}
			}
			
			self.nodes.erase(node_index);
			
			if self.nodes.is_empty()
			{
				self.root = usize::MAX;
				return;
			}
			
			if parent_index == usize::MAX
			{
				break;
			}
			
			node_index = parent_index;
		}
		
		if self.nodes[node_index].is_empty()
		{
			let descendant_array_index = Self::find_single_index(&self.nodes[node_index].indices);
			let parent_index = self.nodes[node_index].parent;
			
			if parent_index != usize::MAX && descendant_array_index != usize::MAX
			{
				let descendant_index = self.nodes[node_index].indices[descendant_array_index];
				let parent_index = self.nodes[node_index].parent;
				
				for idx in self.nodes[parent_index].indices.iter_mut()
				{
					if *idx == node_index
					{
						*idx = descendant_index;
						break;
					}
				}
				
				self.nodes[descendant_index].parent = parent_index;
				self.nodes.erase(node_index);
				node_index = parent_index;
			}
		}
		
		if node_index == self.root
		{
			while self.nodes[self.root].is_empty()
			{
				let descendant_index = Self::find_single_index(&self.nodes[self.root].indices);
				
				if descendant_index == usize::MAX
				{
					break;
				}
				
				let new_root = self.nodes[self.root].indices[descendant_index];
				self.nodes.erase(self.root);
				self.root = new_root;
			}
			
			self.nodes[self.root].parent = usize::MAX;
		}
	}
	
	fn append_object(&mut self, node_index: usize, object_index: usize)
	{
		let node = &mut self.nodes[node_index];
		crate::containers::linked_node::link_back(self.objects.values_mut(), object_index, node.last);
		node.last = object_index;
		
		if node.first == usize::MAX
		{
			node.first = object_index;
		}
	}
	
	fn unlink_object(&mut self, handle: Handle)
	{
		let [prev, next] = crate::containers::linked_node::unlink(self.objects.values_mut(), handle.object_index);
		
		let first = &mut self.nodes[handle.node_index].first;
		
		if *first == handle.object_index
		{
			*first = next;
		}
		
		let last = &mut self.nodes[handle.node_index].last;
		
		if *last == handle.object_index
		{
			*last = prev;
		}
	}
	
	fn walk_up(&self, mut node_index: usize, bbox: Bounding_box) -> usize
	{
		while node_index != usize::MAX && ! self.nodes[node_index].bbox.contains(bbox)
		{
			node_index = self.nodes[node_index].parent;
		}
		
		return node_index;
	}
	
	pub fn insert(&mut self, object: Type, bbox: Bounding_box) -> Handle
	{
		let node_index;
		
		if self.nodes.is_empty()
		{
			let mut cmp = Path_computation::from_outer(Bounding_box::full(), bbox);
			
			while cmp.compute() != usize::MAX
			{
			}
			
			node_index = self.nodes.insert(Node::new(cmp.get_result(), cmp.depth, usize::MAX, cmp.outer));
			self.root = 0;
		}
		else if self.nodes[self.root].bbox.contains(bbox)
		{
			node_index = self.find_to_insert_from(self.root, bbox);
		}
		else
		{
			node_index = self.find_to_insert(bbox);
		}
		
		let object_index = self.objects.insert((object, bbox).into());
		self.append_object(node_index, object_index);
		
		return Handle {node_index, object_index};
	}
	
	pub fn insert_near(&mut self, near: Handle, object: Type, bbox: Bounding_box) -> Handle
	{
		let mut node_index = self.walk_up(near.node_index, bbox);
		
		if node_index != usize::MAX
		{
			node_index = self.find_to_insert_from(node_index, bbox);
		}
		else
		{
			node_index = self.find_to_insert(bbox);
		}
		
		let object_index = self.objects.insert((object, bbox).into());
		self.append_object(node_index, object_index);
		
		return Handle {node_index, object_index};
	}
	
	pub fn transform(&mut self, mut handle: Handle, new_box: Bounding_box) -> Handle
	{
		let mut node_index = self.walk_up(handle.node_index, new_box);
		
		if node_index != handle.node_index
		{
			if node_index != usize::MAX
			{
				node_index = self.find_to_insert_from(node_index, new_box);
			}
			else
			{
				node_index = self.find_to_insert(new_box);
			}
			
			self.unlink_object(handle);
			self.append_object(node_index, handle.object_index);
			self.objects[handle.object_index].as_mut().1 = new_box;
			self.erase_upwards(handle.node_index);
		}
		
		handle.node_index = node_index;
		
		return handle;
	}
	
	pub fn erase(&mut self, handle: Handle)
	{
		self.unlink_object(handle);
		self.objects.erase(handle.object_index);
		self.erase_upwards(handle.node_index);
	}
	
	pub fn query<'t, Filter: std::ops::FnMut(Bounding_box) -> bool>(&'t self, mut filter: Filter)
	-> Query<'t, Object_iterator<&'t [Linked_node<(Type, Bounding_box)>]>, Filter>
	{
		let mut node_index = self.root;
		
		if node_index != usize::MAX && ! filter(self.nodes[node_index].bbox)
		{
			node_index = usize::MAX;
		}
		
		let index = if node_index == usize::MAX {usize::MAX} else {self.nodes[node_index].first};
		
		return Query::<'t, Object_iterator<&'t [Linked_node<(Type, Bounding_box)>]>, Filter>
		{
			it: Object_iterator {objects: self.objects.values(), index},
			node_index,
			nodes: self.nodes.values(),
			filter,
		};
	}
	
	pub fn query_mut<'t, Filter: std::ops::FnMut(Bounding_box) -> bool>(&'t mut self, mut filter: Filter)
	-> Query<'t, Object_iterator<&'t mut [Linked_node<(Type, Bounding_box)>]>, Filter>
	{
		let mut node_index = self.root;
		
		if node_index != usize::MAX && ! filter(self.nodes[node_index].bbox)
		{
			node_index = usize::MAX;
		}
		
		let index = if node_index == usize::MAX {usize::MAX} else {self.nodes[node_index].first};
		
		return Query::<'t, Object_iterator<&'t mut [Linked_node<(Type, Bounding_box)>]>, Filter>
		{
			it: Object_iterator {objects: self.objects.values_mut(), index},
			node_index,
			nodes: self.nodes.values(),
			filter,
		};
	}
	
	pub fn to_dot(&self, writer: &mut (impl std::io::Write + ?Sized)) -> std::io::Result<()>
	{
		writeln!(writer, "digraph nodes {{")?;
		
		let mut print_node = |i: usize| -> std::io::Result<()>
		{
			let node = &self.nodes[i];
			
			writeln!(writer, r#"node{} [shape=plaintext, label=<<TABLE BORDER="0" CELLBORDER="1" CELLSPACING="0">"#, i)?;
			writeln!(writer, r#"<TR><TD>position</TD><TD>{}</TD></TR>"#, i)?;
			write!(writer, r#"<TR><TD>parent</TD><TD>{}</TD></TR>"#, if node.parent == usize::MAX {&"-" as &dyn std::fmt::Display} else {&node.parent})?;
			writeln!(writer, r#"<TR><TD>depth</TD><TD>{}</TD></TR>"#, node.depth)?;
			writeln!(writer, r#"<TR><TD>path</TD><TD>{}</TD></TR>"#, node.path)?;
			
			write!(writer, r#"<TR><TD>box min</TD><TD><TABLE BORDER="0" CELLBORDER="1" CELLSPACING="0"><TR>"#)?;
			for j in 0 .. 3
			{
				write!(writer, r#"<TD>{:.3}</TD>"#, f64::from(node.bbox.min()[j]))?;
			}
			writeln!(writer, r#"</TR></TABLE></TD></TR>"#)?;
			
			write!(writer, r#"<TR><TD>box max</TD><TD><TABLE BORDER="0" CELLBORDER="1" CELLSPACING="0"><TR>"#)?;
			for j in 0 .. 3
			{
				write!(writer, r#"<TD>{:.3}</TD>"#, f64::from(node.bbox.max()[j]))?;
			}
			writeln!(writer, r#"</TR></TABLE></TD></TR>"#)?;
			
			write!(writer, r#"<TR><TD>indices</TD><TD><TABLE BORDER="0" CELLBORDER="1" CELLSPACING="0"><TR>"#)?;
			for j in 0 .. 8
			{
				write!(writer, r#"<TD PORT="d{}">{}</TD>"#, j, if node.indices[j] == usize::MAX {&"-" as &dyn std::fmt::Display} else {&node.indices[j]})?;
			}
			writeln!(writer, r#"</TR></TABLE></TD></TR>"#)?;
			writeln!(writer, r#"</TABLE>>];"#)?;
			
			Ok(())
		};
		
		if self.root != usize::MAX
		{
			print_node(self.root)?;
		}
		
		for i in self.nodes.index_iter()
		{
			if i != self.root
			{
				print_node(i)?;
			}
		}
		
		for i in self.nodes.index_iter()
		{
			let node = &self.nodes[i];
			
			if node.parent != usize::MAX
			{
				writeln!(writer, "node{} -> node{} [style=dotted];", i, node.parent)?;
			}
			
			for j in 0 .. node.indices.len()
			{
				if node.indices[j] != usize::MAX
				{
					writeln!(writer, "node{}:d{} -> node{};", i, j, node.indices[j])?;
				}
			}
		}
		
		writeln!(writer, "}}")
	}
}

#[test]
fn test_gridtree()
{
	let mut gd = Gridtree::<i32>::new();
	
	assert_eq!(0, gd.query(|_| true).count());
	gd.insert(0, Bounding_box::new(
		Vector::from([-1.0, -1.0, -1.0].map(coord_t::round_from)),
		Vector::from([1.0, 1.0, 1.0].map(coord_t::round_from)),
	));
	assert_eq!(1, gd.query(|_| true).count());
	gd.insert(1, Bounding_box::full());
	assert_eq!(2, gd.query(|_| true).count());
	gd.insert(2, Bounding_box::new(
		Vector::from([-100.0, -100.0, -100.0].map(coord_t::round_from)),
		Vector::from([101.0, 101.0, 101.0].map(coord_t::round_from)),
	));
	assert_eq!(3, gd.query(|_| true).count());
	gd.insert(3, Bounding_box::new(
		Vector::from([1000.0, 0.0, -1000.0].map(coord_t::round_from)),
		Vector::from([1001.0, 1.0, -1001.0].map(coord_t::round_from)),
	));
	assert_eq!(4, gd.query(|_| true).count());
	gd.insert(4, Bounding_box::new(
		Vector::from([1000.0, 1000.0, 1000.0].map(coord_t::round_from)),
		Vector::from([1001.0, 1001.0, 1001.0].map(coord_t::round_from)),
	));
	assert_eq!(5, gd.query(|_| true).count());
	
	let mut values: Vec<i32> = gd.query(|bb| bb.overlaps(Vector::from([0.0, 0.0, 0.0]
		.map(coord_t::round_from)))).map(|(&i, _)| i).collect()
	;
	values.sort();
	
	assert_eq!(3, values.len());
	for i in 0 .. values.len()
	{
		assert_eq!(i as i32, values[i]);
	}
	
	for (i, _) in gd.query_mut(|_| true)
	{
		*i += 1;
	}
	
	let mut values: Vec<_> = gd.query(|bb| bb.overlaps(Vector::from([0.0, 0.0, 0.0]
		.map(coord_t::round_from)))).map(|(&i, _)| i).collect()
	;
	values.sort();
	
	for i in 0 .. values.len()
	{
		assert_eq!(i as i32 + 1, values[i]);
	}
}
