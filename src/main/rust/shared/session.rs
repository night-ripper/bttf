use std::io::Write;
use crate::utility::byte_array::Byte_array;

pub struct Poller
{
	poller: polling::Poller,
	sources: Vec<std::sync::Arc<dyn polling::AsSource>>,
}

impl Drop for Poller
{
	fn drop(&mut self)
	{
		for source in &self.sources
		{
			self.poller.delete(source.source()).unwrap();
		}
	}
}

impl std::ops::Deref for Poller
{
	type Target = polling::Poller;
	
	fn deref(&self) -> &Self::Target
	{
		&self.poller
	}
}

impl Poller
{
	pub fn new() -> Self
	{
		Self {poller: polling::Poller::new().unwrap(), sources: Vec::with_capacity(8)}
	}
	
	pub fn add_with_mode(&mut self, source: std::sync::Arc<dyn polling::AsSource>, interest: polling::Event, mode: polling::PollMode) -> std::io::Result<()>
	{
		unsafe {self.poller.add_with_mode(&source.source(), interest, mode)}?;
		self.sources.push(source);
		Ok(())
	}
}

unsafe impl Send for Poller {}

#[derive(Clone, Copy, Debug, std::cmp::PartialEq, std::cmp::Eq, serde::Serialize, serde::Deserialize)]
pub struct Sequence
{
	pub value: u32,
}

impl From<&crate::crypto::shared_key::Nonce> for Sequence
{
	fn from(value: &crate::crypto::shared_key::Nonce) -> Self
	{
		Sequence {value: u32::from_be_bytes(
			value[std::mem::size_of::<crate::crypto::shared_key::Nonce>() - std::mem::size_of::<Self>() ..].try_into().unwrap())
		}
	}
}

impl std::cmp::Ord for Sequence
{
	fn cmp(&self, other: &Self) -> std::cmp::Ordering
	{
		std::cmp::Ord::cmp(&(u32::MAX / 2), &(self.value.overflowing_sub(other.value).0))
	}
}

impl std::cmp::PartialOrd for Sequence
{
	fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering>
	{
		Some(self.cmp(other))
	}
}

pub struct Session
{
	pub remote_endpoint: std::net::SocketAddr,
	pub keys: crate::shared::keys::Session_keys,
	unreliable_send_nonce: crate::crypto::shared_key::Nonce,
	other_last_received_sequence: Sequence,
	send_socket: std::sync::Arc<std::net::UdpSocket>,
	pub sent_messages: std::collections::VecDeque<u8>,
	write_buffer: Vec<u8>,
	received_bitset: Vec<u8>,
}

#[derive(Debug)]
pub enum Send_error
{
	serialization {error: bincode::Error},
	io {error: std::io::Error},
}

impl std::fmt::Display for Send_error
{
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
	{
		write!(f, "{:?}", &self)
	}
}

impl std::error::Error for Send_error
{
}

#[derive(Debug)]
pub enum Accept_error
{
	already_received,
	message_too_short,
	decryption,
	deserialization {error: bincode::Error},
	wrong_last_received,
}

impl std::fmt::Display for Accept_error
{
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
	{
		write!(f, "{:?}", &self)
	}
}

impl std::error::Error for Accept_error
{
}

impl Session
{
	const sequence_length: usize = std::mem::size_of::<Sequence>();
	const nonce_length: usize = std::mem::size_of::<crate::crypto::shared_key::Nonce>();
	
	pub fn socket_mut(&mut self) -> &mut std::sync::Arc<std::net::UdpSocket>
	{
		&mut self.send_socket
	}
	
	pub fn new(remote_endpoint: std::net::SocketAddr,
		send_socket: std::sync::Arc<std::net::UdpSocket>, keys: crate::shared::keys::Session_keys) -> Self
	{
		let unreliable_send_nonce = keys.send_nonce;
		let other_last_received_sequence = Sequence::from(&keys.send_nonce);
		let mut received_bitset = Vec::new();
		received_bitset.resize(128, 0);
		
		Self
		{
			remote_endpoint,
			keys,
			unreliable_send_nonce,
			other_last_received_sequence,
			send_socket,
			sent_messages: std::collections::VecDeque::<u8>::with_capacity(1024),
			write_buffer: Vec::with_capacity(1024),
			received_bitset,
		}
	}
	
	fn prepare_message<'t, Content>(&self, content: &'t Content) -> crate::shared::Message<&'t Content>
	where Content: serde::Serialize
	{
		crate::shared::Message
		{
			last_received: Sequence::from(&self.keys.receive_nonce),
			received_bitset: self.received_bitset[.. 4].try_into().unwrap(),
			content,
		}
	}
	
	pub fn send_unreliable(&mut self, content: &impl serde::Serialize) -> Result<(), Send_error>
	{
		self.unreliable_send_nonce.decrement();
		self.write_buffer.push(0);
		self.write_buffer.extend_from_slice(&self.unreliable_send_nonce);
		let encrypt_begin = self.write_buffer.len();
		self.write_buffer.extend_from_slice(&[0; crate::crypto::shared_key::mac_length]);
		
		let message = self.prepare_message(content);
		
		if let Err(error) = bincode::serialize_into(&mut self.write_buffer, &message)
		{
			self.unreliable_send_nonce.increment();
			self.write_buffer.clear();
			return Err(Send_error::serialization {error});
		}
		
		crate::crypto::shared_key::encrypt_in_place(
			&mut self.write_buffer[encrypt_begin ..], &self.unreliable_send_nonce, &self.keys.send_key
		);
		
		let result = self.send_socket.send_to(&self.write_buffer, self.remote_endpoint);
		self.write_buffer.clear();
		
		if let Err(error) = result
		{
			self.unreliable_send_nonce.increment();
			return Err(Send_error::io {error});
		}
		
		Ok(())
	}
	
	pub fn send(&mut self, content: &impl serde::Serialize) -> Result<(), Send_error>
	{
		self.keys.send_nonce.increment();
		self.write_buffer.push(1);
		self.write_buffer.extend_from_slice(
			&self.keys.send_nonce[Self::nonce_length - Self::sequence_length ..]
		);
		let encrypt_begin = self.write_buffer.len();
		self.write_buffer.extend_from_slice(&[0; crate::crypto::shared_key::mac_length]);
		
		let message = self.prepare_message(content);
		
		if let Err(error) = bincode::serialize_into(&mut self.write_buffer, &message)
		{
			self.keys.send_nonce.decrement();
			self.write_buffer.clear();
			return Err(Send_error::serialization {error});
		}
		
		crate::crypto::shared_key::encrypt_in_place(
			&mut self.write_buffer[encrypt_begin ..], &self.keys.send_nonce, &self.keys.send_key
		);
		
		let result = self.send_socket.send_to(&self.write_buffer, self.remote_endpoint);
		
		if let Ok(_) = result
		{
			self.sent_messages.write(&(self.write_buffer.len() as u16).to_ne_bytes());
			self.sent_messages.write(self.write_buffer.as_slice());
		}
		
		self.write_buffer.clear();
		
		if let Err(error) = result
		{
			self.keys.send_nonce.decrement();
			return Err(Send_error::io {error});
		}
		
		Ok(())
	}
	
	pub fn resend(&mut self) -> Result<(), Send_error>
	{
		let mut it = self.sent_messages.iter();
		let mut length = self.sent_messages.len();
		
		while length >= 2
		{
			let size = u16::from_ne_bytes([it.next(), it.next()].map(|v| *v.unwrap()));
			length = length.saturating_sub(2 + size as usize);
			
			for _ in 0 .. size
			{
				self.write_buffer.push(*it.next().unwrap());
			}
			
			let result = self.send_socket.send_to(&self.write_buffer, self.remote_endpoint);
			self.write_buffer.clear();
			
			if let Err(error) = result
			{
				return Err(Send_error::io {error});
			}
		}
		
		Ok(())
	}
	
	fn read_sent_message(vec: &std::collections::VecDeque<u8>, index: usize) -> Option<(u16, u32)>
	{
		if vec.len() <= index
		{
			return None;
		}
		
		Some((u16::from_ne_bytes([vec[index], vec[index + 1]]),
			u32::from_be_bytes([vec[index + 3], vec[index + 4], vec[index + 5], vec[index + 6]])
		))
	}
	
	pub fn accept<Type: serde::de::DeserializeOwned>(&mut self, data: &mut [u8]) -> Result<Type, Accept_error>
	{
		if data.len() < Self::sequence_length + 1
		{
			return Err(Accept_error::message_too_short);
		}
		
		let reliable: bool = data[0] == 1;
		let mut data = &mut data[1 ..];
		
		let mut nonce;
		
		if reliable
		{
			let sequence = u32::from_be_bytes(data[.. Self::sequence_length].try_into().unwrap());
			data = &mut data[Self::sequence_length ..];
			let expected_sequence = Sequence::from(&self.keys.receive_nonce);
			let distance = sequence.overflowing_sub(expected_sequence.value).0;
			
			eprintln!("sequence: {} - {}", expected_sequence.value, sequence);
			
			if distance >= 8 * self.received_bitset.len() as u32
			{
				return Err(Accept_error::already_received);
			}
			
			nonce = self.keys.receive_nonce.clone();
			
			if sequence < expected_sequence.value
			{
				nonce[Self::nonce_length - Self::sequence_length ..].copy_from_slice(&u32::MAX.to_be_bytes());
				nonce.increment();
			}
			
			nonce[Self::nonce_length - Self::sequence_length ..].copy_from_slice(&sequence.to_be_bytes());
			
			if sequence == expected_sequence.value.overflowing_add(1).0
			{
				self.keys.receive_nonce.increment();
				
				let mut shift = 0;
				
				for i in 0 .. self.received_bitset.len() * 8
				{
					if crate::containers::bitset::get(&self.received_bitset, i)
					{
						self.keys.receive_nonce.increment();
						shift += 1;
					}
					else
					{
						break;
					}
				}
				
				crate::containers::bitset::shift_left(&mut self.received_bitset, shift);
			}
			else
			{
				crate::containers::bitset::set(&mut self.received_bitset, distance as usize);
			}
		}
		else
		{
			if data.len() < Self::nonce_length
			{
				return Err(Accept_error::message_too_short);
			}
			
			nonce = data[.. Self::nonce_length].try_into().unwrap();
			data = &mut data[Self::nonce_length ..];
		}
		
		let Some(data) = crate::crypto::shared_key::decrypt_in_place(data, &nonce, &self.keys.receive_key) else
		{
			return Err(Accept_error::decryption);
		};
		
		let message = bincode::deserialize_from::<_, crate::shared::Message<Type>>(&*data)
			.map_err(|error| Accept_error::deserialization {error})
		?;
		
		loop
		{
			if self.other_last_received_sequence <= message.last_received
			{
				if let Some((length, _)) = Self::read_sent_message(&self.sent_messages, 0)
				{
					self.sent_messages.drain(.. 2 + length as usize);
				}
				else
				{
					return Err(Accept_error::wrong_last_received);
				}
			}
			else
			{
				break;
			}
			
			if self.other_last_received_sequence != message.last_received
			{
				self.other_last_received_sequence.value = self.other_last_received_sequence.value.overflowing_add(1).0;
			}
		}
		
		let mut begin = 0;
		let mut message_seq = message.last_received;
		message_seq.value = message_seq.value.overflowing_add(1).0;
		
		loop
		{
			let mut bitset_index = 0;
			
			if let Some((length, sequence)) = Self::read_sent_message(&self.sent_messages, begin)
			{
				let sequence = Sequence {value: sequence};
				let length = 2 + length as usize;
				
				while bitset_index != 8 * message.received_bitset.len() && message_seq < sequence
				{
					bitset_index += 1;
					message_seq.value = message_seq.value.overflowing_add(1).0;
				}
				
				if message_seq == sequence && crate::containers::bitset::get(&message.received_bitset, bitset_index)
				{
					self.sent_messages.drain(begin .. begin + length);
				}
				else
				{
					begin += length;
				}
			}
			else
			{
				break;
			}
		}
		
		return Ok(message.content);
	}
}
