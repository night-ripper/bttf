pub type Username = [char; 32];
pub type Password = [char; 32];

#[derive(Debug, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct Login
{
	pub username: Username,
	pub password: Password,
}

#[derive(Debug, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub enum Message_content
{
	_0,
	heartbeat,
	register {login: Login},
	login {login: Login},
	number {i: i32},
}
