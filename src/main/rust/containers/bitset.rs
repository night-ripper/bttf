pub fn get(data: &[u8], index: usize) -> bool
{
	data[index / 8] & (1 << (index % 8)) != 0
}

#[test]
fn test_get()
{
	let data = [0b_0011_0011, 0b_1100_1100];
	assert_eq!(true, get(&data, 0));
	assert_eq!(true, get(&data, 1));
	assert_eq!(false, get(&data, 2));
	assert_eq!(false, get(&data, 3));
	
	assert_eq!(false, get(&data, 8));
	assert_eq!(false, get(&data, 9));
	assert_eq!(true, get(&data, 10));
	assert_eq!(true, get(&data, 11));
}

pub fn set(data: &mut [u8], index: usize)
{
	data[index / 8] |= 1 << (index % 8);
}

#[test]
fn test_set()
{
	let mut data = [0, 0];
	set(&mut data, 0);
	assert_eq!([0b_0000_0001, 0], data);
	set(&mut data, 7);
	assert_eq!([0b_1000_0001, 0], data);
	set(&mut data, 10);
	assert_eq!([0b_1000_0001, 0b_0000_0100], data);
}

pub fn shift_left(data: &mut [u8], distance: usize)
{
	if distance == 0
	{
		return;
	}
	
	let divisor = distance / 8;
	let modulus = distance % 8;
	
	for i in 0 .. data.len() - divisor - 1
	{
		data[i] = data[i + divisor] << modulus;
		data[i] |= data[i + divisor + 1] >> (8 - modulus);
	}
	
	data[data.len() - divisor - 1] = data[data.len() - 1] << modulus;
	
	for i in data.len() - divisor .. data.len()
	{
		data[i] = 0;
	}
}

#[test]
fn test_shift_left()
{
	let mut data = [0b_0101_1100];
	shift_left(&mut data, 0);
	assert_eq!(data, data);
	shift_left(&mut data, 4);
	assert_eq!([0b_1100_0000], data);
	
	let mut data = [0b_1111_1111, 0b_1111_1111];
	shift_left(&mut data, 0);
	assert_eq!(data, data);
	shift_left(&mut data, 1);
	assert_eq!([0b_1111_1111, 0b_1111_1110], data);
	shift_left(&mut data, 9);
	assert_eq!([0b_1111_1100, 0], data);
	shift_left(&mut data, 0);
	assert_eq!(data, data);
}

/*
pub struct Iterator<'t>
{
	index: usize,
	data: &'t [u8],
}

impl<'t> std::iter::Iterator for Iterator<'t>
{
	type Item = usize;
	
	fn next(&mut self) -> Option<Self::Item>
	{
		let byte = self.data[self.index / 8];
		let modulus = self.index % 8;
		
	}
}

pub fn iterate(data: &mut [u8]) -> impl std::iter::Iterator<Item = usize>
{
	
}
*/

#[derive(Debug)]
pub struct Bitset<const Size: usize>
{
	pub data: [u8; Size],
}

impl<const Size: usize> Bitset<Size>
{
	pub fn get(&self, index: usize) -> bool
	{
		get(&self.data, index)
	}
	
	pub fn set(&mut self, index: usize)
	{
		set(&mut self.data, index)
	}
}

impl<const Size: usize> std::ops::ShlAssign<usize> for Bitset<Size>
{
	fn shl_assign(&mut self, rhs: usize)
	{
		shift_left(&mut self.data, rhs);
	}
}
