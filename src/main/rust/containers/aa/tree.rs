use crate::containers::aa::node;

pub struct Tree<Type>
{
	pub(super) root: usize,
	pub(super) first: usize,
	pub(super) last: usize,
	pub(super) repository: crate::containers::repository::Repository<node::Node<Type>>,
}

impl<Type> Tree<Type>
{
	pub fn len(&self) -> usize {self.repository.len()}
	pub fn is_empty(&self) -> bool {self.len() == 0}
	
	pub fn new() -> Self
	{
		Self
		{
			root: usize::MAX,
			first: usize::MAX,
			last: usize::MAX,
			repository: crate::containers::repository::Repository::new(),
		}
	}
	
	pub fn clear(&mut self)
	{
		self.repository.clear();
		self.root = usize::MAX;
		self.first = usize::MAX;
		self.last = usize::MAX;
	}
	
	pub(super) fn try_insert<Consumer, Result_type>(&mut self, value: Type, consumer: Consumer) -> Result_type
	where
		Type: node::Entry,
		<Type as node::Entry>::Key: std::cmp::Ord,
		Consumer: std::ops::FnOnce(Option<Type>) -> Result_type
	{
		if self.is_empty()
		{
			self.root = self.repository.insert(node::Node::new(value));
			self.first = self.root;
			self.last = self.root;
			return consumer(None);
		}
		
		let mut values = self.repository.values_mut();
		let (mut position, parent, parent_index) = node::find(values, self.root, value.key());
		
		if position != usize::MAX
		{
			return consumer(Some(std::mem::replace(&mut values[position].as_mut(), value)));
		}
		
		position = self.repository.insert(node::Node::new(value));
		values = self.repository.values_mut();
		
		if node::insert_rebalance(values, parent, parent_index, position)
		{
			self.root = node::skew(values, self.root);
			self.root = node::split(values, self.root);
			values[self.root].parent = usize::MAX;
		}
		
		if values[self.first].descendants[0] == position || values[position].descendants[1] == self.first
		{
			self.first = position;
		}
		
		if values[position].parent == self.last
		{
			self.last = position;
		}
		
		return consumer(None);
	}
	
	pub fn remove_at(&mut self, position: usize)
	{
		let values = self.repository.values_mut();
		let parent = values[position].parent;
		let rdes = values[position].descendants[1];
		self.repository.erase(position);
		
		let new_root = node::erase_rebalance(self.repository.values_mut(), position);
		
		if new_root != usize::MAX
		{
			self.root = new_root;
		}
		else if self.is_empty()
		{
			self.root = usize::MAX;
		}
		
		if position == self.first
		{
			if rdes != usize::MAX
			{
				self.first = rdes;
			}
			else
			{
				self.first = parent;
			}
		}
		
		if position == self.last
		{
			self.last = parent;
		}
	}
}
