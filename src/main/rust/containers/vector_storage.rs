pub struct Vector_storage
{
	pub(super) data: std::ptr::NonNull<u8>,
	pub(super) capacity: usize,
}

unsafe impl Send for Vector_storage {}
unsafe impl Sync for Vector_storage {}

impl Vector_storage
{
	pub fn new() -> Self
	{
		Self
		{
			data: std::ptr::NonNull::dangling(),
			capacity: 0,
		}
	}
	
	pub fn default_capacity_growth(capacity: usize) -> usize
	{
		8 + capacity * 3 / 2
	}
}
