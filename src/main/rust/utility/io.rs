use std::io::Read;
use std::io::Seek;

pub struct Buffer_writer<const Size: usize>
{
	size: usize,
	data: [u8; Size],
}

impl<const Size: usize> Buffer_writer<Size>
{
	pub fn len(&self) -> usize
	{
		return self.size;
	}
	
	pub fn as_slice(&self) -> &[u8]
	{
		return &self.data[.. self.size];
	}
	
	pub fn as_mut_slice(&mut self) -> &mut [u8]
	{
		return &mut self.data[.. self.size];
	}
	
	pub fn clear(&mut self)
	{
		self.size = 0;
	}
}

impl<const Size: usize> Default for Buffer_writer<Size>
{
	fn default() -> Self
	{
		Self
		{
			size: 0,
			data: [0; Size],
		}
	}
}

impl<const Size: usize> std::ops::Deref for Buffer_writer<Size>
{
	type Target = [u8];
	
	fn deref(&self) -> &Self::Target
	{
		return self.as_slice();
	}
}

impl<const Size: usize> std::ops::DerefMut for Buffer_writer<Size>
{
	fn deref_mut(&mut self) -> &mut Self::Target
	{
		return self.as_mut_slice();
	}
}

impl<const Size: usize> std::io::Write for Buffer_writer<Size>
{
	fn flush(&mut self) -> std::io::Result<()>
	{
		Ok(())
	}
	
	fn write(&mut self, buf: &[u8]) -> std::io::Result<usize>
	{
		let result = std::cmp::min(self.data.len() - self.size, buf.len());
		self.data[self.size ..][.. result].copy_from_slice(&buf[.. result]);
		self.size += result;
		Ok(result)
	}
}

pub fn read_file_binary(path: impl AsRef<std::path::Path>) -> std::io::Result<Vec<u8>>
{
	let mut file = std::fs::File::open(path)?;
	let len = file.seek(std::io::SeekFrom::End(0))?;
	let mut result = Vec::<u8>::with_capacity(len as usize);
	file.seek(std::io::SeekFrom::Start(0))?;
	file.read_to_end(&mut result)?;
	return Ok(result);
}

struct Looping_reader<R>
{
	reader: R,
}

impl<R> Seek for Looping_reader<R>
where R: Read + Seek
{
	fn seek(&mut self, pos: std::io::SeekFrom) -> std::io::Result<u64>
	{
		self.reader.seek(pos)
	}
}

impl<R> Read for Looping_reader<R>
where R: Read + Seek
{
	fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize>
	{
		let mut result = self.reader.read(buf)?;
		
		if result < buf.len()
		{
			self.reader.rewind()?;
		}
		
		result += self.reader.read(&mut buf[result ..])?;
		
		return Ok(result);
	}
}
