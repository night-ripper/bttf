#[derive(PartialEq, Eq)]
pub enum Endianness
{
	little,
	big,
}

impl Endianness
{
	#[cfg(target_endian = "little")]
	pub const native: Endianness = Endianness::little;
	
	#[cfg(target_endian = "big")]
	pub const native: Endianness = Endianness::big;
}
