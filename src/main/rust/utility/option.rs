pub fn as_ptr<Type>(option: &Option<Type>) -> *const Type
{
	option.as_ref().map_or(std::ptr::null(), |v| v)
}

pub fn as_mut<Type>(option: &mut Option<Type>) -> *mut Type
{
	option.as_mut().map_or(std::ptr::null_mut(), |v| v)
}
