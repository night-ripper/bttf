#[derive(Clone)]
pub struct Encoder<Iterator_type>
where Encoder<Iterator_type>: Iterator<Item = char>
{
	half: bool,
	value: u8,
	it: Iterator_type,
}

impl<Iterator_type> Encoder<Iterator_type>
where Encoder<Iterator_type>: Iterator<Item = char>
{
	pub fn new(it: Iterator_type) -> Self
	{
		return Encoder
		{
			half: false,
			value: 0,
			it,
		};
	}
}

impl<Iterator_type> Iterator for Encoder<Iterator_type>
where Iterator_type: std::iter::Iterator, <Iterator_type as Iterator>::Item: Into<u8>
{
	type Item = char;
	
	fn next(&mut self) -> Option<Self::Item>
	{
		if ! self.half
		{
			if let Some(value) = self.it.next()
			{
				self.value = value.into();
			}
			else
			{
				return None;
			}
		}
		
		let mut char_value = self.value;
		
		if ! self.half
		{
			char_value >>= 4;
		}
		else
		{
			char_value &= 0b1111;
		}
		
		if char_value < 10
		{
			char_value += b'0';
		}
		else
		{
			char_value += b'A' - 10;
		}
		
		self.half = ! self.half;
		
		return Some(char_value as char);
	}
}

impl<Iterator_type> std::fmt::Display for Encoder<Iterator_type>
where Encoder<Iterator_type>: Iterator<Item = char>, Encoder<Iterator_type>: Clone
{
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
	{
		let mut buffer = [0_u8; 64];
		let mut it = self.clone();
		
		loop
		{
			for i in 0 .. buffer.len()
			{
				if let Some(c) = it.next()
				{
					buffer[i] = c as u8;
				}
				else
				{
					return unsafe {f.write_str(std::str::from_utf8_unchecked(&buffer[.. i]))};
				}
			}
			
			unsafe {f.write_str(std::str::from_utf8_unchecked(&buffer))?};
		}
	}
}

#[test]
fn test_encoder()
{
	let arr: [u8; 4] = [0,0,0,0];
	assert_eq!("00000000", Encoder::new(arr.iter().copied()).collect::<String>());
	
	let arr: [u8; 4] = [0,0x11,0x2a,0xff];
	assert_eq!("00112AFF", Encoder::new(arr.iter().copied()).collect::<String>());
}

pub struct Decoder<Iterator_type>
where Iterator_type: std::iter::Iterator, <Iterator_type as Iterator>::Item: Into<char>
{
	it: Iterator_type,
}

impl<Iterator_type> Decoder<Iterator_type>
where Iterator_type: std::iter::Iterator, <Iterator_type as Iterator>::Item: Into<char>
{
	pub fn new(it: Iterator_type) -> Self
	{
		return Decoder {it};
	}
}

impl<Iterator_type> Iterator for Decoder<Iterator_type>
where Iterator_type: std::iter::Iterator, <Iterator_type as Iterator>::Item: Into<char>
{
	type Item = u8;
	
	fn next(&mut self) -> Option<Self::Item>
	{
		if let Some(next) = self.it.next()
		{
			let mut chars = [char::from(next.into()) as u8, b'0'];
			
			if let Some(next) = self.it.next()
			{
				chars[1] = char::from(next.into()) as u8;
			}
			
			for c in &mut chars
			{
				if b'0' <= *c && *c <= b'9'
				{
					*c -= b'0';
				}
				else if b'A' <= *c && *c <= b'F'
				{
					*c += 10;
					*c -= b'A';
				}
				else
				{
					// Ignore invalid values
				}
			}
			
			let mut result = chars[0];
			result <<= 4;
			result += chars[1];
			
			return Some(result);
		}
		
		return None;
	}
}

#[test]
fn test_decoder()
{
	let string: &str = "00000000";
	assert_eq!([0,0,0,0], Decoder::new(string.chars()).collect::<Vec<u8>>().as_slice());
	
	let string: &str = "00112AFF";
	assert_eq!([0,0x11,0x2a,0xff], Decoder::new(string.chars()).collect::<Vec<u8>>().as_slice());
	
	let string: &str = "00112AF";
	assert_eq!([0,0x11,0x2a,0xf0], Decoder::new(string.chars()).collect::<Vec<u8>>().as_slice());
}
