fn main() -> std::io::Result<()>
{
	println!("cargo:rerun-if-changed={}", "build.rs");
	
	// Use faster linkers if possible
	{
		let linker_prefix = if std::env::var_os("CARGO_CFG_TARGET_OS").unwrap() == "windows"
		{
			"x86_64-w64-mingw32-"
		}
		else
		{
			""
		};
		
		for linker in ["mold", "lld"]
		{
			if std::process::Command::new(format!("{}ld.{}", linker_prefix, linker))
				.stdout(std::process::Stdio::null()).stderr(std::process::Stdio::null()).status().is_ok()
			{
				println!("cargo:rustc-link-arg=-fuse-ld={}", linker);
				break;
			}
		}
	}
	
	Ok(())
}
