MAKEFLAGS += -r
.PHONY: all clean targets
.DEFAULT_GOAL = all

>glslc := $(shell which glslc)

define glslc # stage, target, source
target/spv/$(2): src/main/glsl/$(3) $(>glslc)
	@mkdir -p $$(@D)
	$(>glslc) -fshader-stage=$(1) -o $$@ $$<
targets += target/spv/$(2)
endef

$(eval $(call glslc,compute,compute.spv,compute.comp.glsl))
$(eval $(call glslc,vertex,simple.vert.spv,simple.vert.glsl))
$(eval $(call glslc,fragment,simple.frag.spv,simple.frag.glsl))

all: $(targets)
clean:
	@rm -rfv target/spv

cargo\ %: $(targets)
	@$@
