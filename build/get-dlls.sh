#!/bin/bash

# This script traverses all the windows executable files in the directory
# provided as the first paramter plus all the additional files provided as
# next parameter and transitively pulls dependent .dlls from sysroot into the
# same directory as the first parameter

set -e

sysroot='/usr/x86_64-w64-mingw32/sys-root/mingw/bin/'

while [ ${#} -gt 1 ]; do
	case "${1}" in
		--sysroot*)
			sysroot="${1#*=}/"
			shift
		;;
		*)
			break
		;;
	esac
done

if [ ! -d "${sysroot}" ]; then
	echo "sysroot ${sysroot} does not exist"
	exit 1
fi

if [ -z "${1}" ]; then
	echo "You must pass the directory path as the first parameter"
	exit 2
fi

readonly workdir="${1}"
shift

declare -a files

while [ ${#} -ne 0 ]; do
	files+=("${1}")
	shift
done

# We assume these are provided by windows and therefore we do not want to copy them
declare -A mingw_dlls
for line in $(rpm -q -l "wine-core" | sed -n 's|.*/\(.*\.dll\)$|\1|p' | sort -u); do
	mingw_dlls["${line,,}"]='system'
done

readonly mingw_dlls

while IFS= read -r -d '' file; do
	files+=("${file}")
done < <(find "${workdir}" -maxdepth 1 \( -name '*.dll' -or -name '*.exe' \) -print0)

function main
{
	while [ ${#files[@]} -ne 0 ]; do
		local previous_files=("${files[@]}")
		files=()
		
		local dependencies
		dependencies=$(x86_64-w64-mingw32-objdump -p "${previous_files[@]}" |\
			sed '/^[[:blank:]]*DLL Name:\ [^[:blank:]]*$/!d;s/.*DLL Name:\ \([^[:blank:]]*\)/\1/'
		)
		
		for dependency in ${dependencies}; do
			if [ -n "${mingw_dlls[${dependency,,}]}" ]; then
				continue
			elif [ -f "${workdir}/${dependency}" ]; then
				continue
			elif ln -s -v -t "${workdir}" "${dependency/#/${sysroot}}"; then
				files+=("${workdir}/${dependency}")
			else
				exit 3
			fi
		done
	done
}

main
